''' 
 @package   pNbody
 @file      old.setup.py
 @brief     old setup
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

'''
import sys,os,string,glob

from distutils.sysconfig import *
from distutils.core import setup,Extension
from distutils.command.build_ext import build_ext
from distutils.command.install import install
from distutils.command.install_data import install_data

import numpy


long_description = """\
This module provides lots of tools used
to deal with Nbody particles models.
"""

INCDIRS=['.']
SCRIPTS = glob.glob('scripts/*')

INCDIRS.append(numpy.get_include())
INCDIRS.append(numpy.get_numarray_include())
               

##############################################################
# variables for ptree
##############################################################
#MPICH_DIR = "/usr/local/lib/"
MPICH_DIR = "/cvos/shared/apps/ofed/1.2.5.3/mpi/gcc/mvapich2-0.9.8-15/lib"
MPICH_LIB = "mpich"
MPICH_INC = "/cvos/shared/apps/ofed/1.2.5.3/mpi/gcc/mvapich2-0.9.8-15/include"
##############################################################

class pNbody_install_data (install_data):
  def finalize_options (self):
    if self.install_dir is None:
      install_lib = self.get_finalized_command('install_lib')
      self.warn_dir = 0
      self.install_dir = os.path.normpath(os.path.join(install_lib.install_dir,"pNbody"))
      

     

setup 	(       name	       	= "pNbody",
       		version         = "4.0.0",
       		author	      	= "Revaz Yves",
       		author_email    = "yves.revaz@epfl.ch yves.revaz@obspm.fr ",
		url 		= "http://obswww.unige.ch/~revaz/pNbody/index.html",
       		description     = "pNbody module",
		
		packages 	= ['pNbody'],
		
		cmdclass = {'install_data': pNbody_install_data},
		
                ext_modules 	= [Extension("pNbody.nbodymodule", 
				             ["src/nbodymodule/nbodymodule.c"],
					     include_dirs=INCDIRS),
			 	   Extension("pNbody.myNumeric", 
				             ["src/myNumeric/myNumeric.c"],
					     include_dirs=INCDIRS),
			 	   Extension("pNbody.mapping", 
				             ["src/mapping/mapping.c"],
					     include_dirs=INCDIRS),           
			 	   Extension("pNbody.coolinglib", 
				             ["src/coolinglib/coolinglib.c"],
					     include_dirs=INCDIRS),      					     
			 	   Extension("pNbody.cosmolib", 
				             ["src/cosmolib/cosmolib.c"],
					     include_dirs=INCDIRS),   	
			 	   Extension("pNbody.iclib", 
				             ["src/iclib/iclib.c"],
					     include_dirs=INCDIRS),  
			 	   Extension("pNbody.asciilib", 
				             ["src/asciilib/asciilib.c"],
					     include_dirs=INCDIRS),  					      						     		
			 	   Extension("pNbody.nbdrklib", 
				             ["src/nbdrklib/nbdrklib.c"],
					     include_dirs=INCDIRS),  
					     	
			 	   Extension("pNbody.treelib", 
				             ["src/treelib/treelib.c"],
					     include_dirs=INCDIRS),
#			 	   Extension("pNbody.ptreelib", 
#					     ["src/ptreelib/domain.c",
#					      "src/ptreelib/endrun.c",
#					      "src/ptreelib/forcetree.c",
#					      "src/ptreelib/ngb.c",
#					      "src/ptreelib/peano.c",
#					      "src/ptreelib/ptreelib.c",
#					      "src/ptreelib/potential.c",
#					      "src/ptreelib/gravtree.c",
#					      "src/ptreelib/density.c",
#					      "src/ptreelib/sph.c",
#					      "src/ptreelib/io.c",
#    				 	      "src/ptreelib/system.c"],
#					      include_dirs=["src/ptreelib/",MPICH_INC],
#					      library_dirs=[MPICH_DIR],
#					      libraries=[MPICH_LIB]),  					        	
			 	   Extension("pNbody.peanolib", 
				             ["src/peanolib/peanolib.c"],
					     include_dirs=INCDIRS)   						     
				   ],
				   

		data_files=[('config',glob.glob('./config/*parameters')),
		            ('config/rgb_tables',glob.glob('./config/rgb_tables/*')),
			    ('config/formats',glob.glob('./config/formats/*')),
		            ('plugins',glob.glob('./config/plugins/*')),
			    ('fonts',glob.glob('./fonts/*')),
			    ('tests',glob.glob('./tests/*')),
			    ('examples',glob.glob('./examples/*.dat')),
			    ('examples',glob.glob('./examples/*.py')),			    
			    ('examples/scripts',glob.glob('./examples/scripts/*')),
			    ('examples/films',glob.glob('./examples/films/*')),
			    ('doc',glob.glob('./doc/man.ps'))
			    ],

			    
                scripts = SCRIPTS			    
 	)
		
