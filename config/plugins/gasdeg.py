'''
 @package   pNbody
 @file      gasdeg.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from Nbody import *
from Numeric import *

self.X = self.X.subpart('gas')
c = (self.X.u!=0.) 
self.X = self.X.selectc(c)

self.display()
self.display_info(self.X)
