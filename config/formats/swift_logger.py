'''
 @package   pNbody
 @file      gh5.py
 @brief     gh5 format (Gear in HDF5)
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''  
  
####################################################################################################################################
#
# SWIFT LOGGER CLASS
#
####################################################################################################################################

import sys
import types
import string
import numpy as np
import h5py
from copy import deepcopy

import pNbody
from pNbody import mpi, error, units, logger_loader


class Nbody_logger:

  def _init_spec(self):
      return

  def get_excluded_extension(self):
    """
    Return a list of file to avoid when extending the default class.
    """
    return []

  def check_spec_ftype(self,f):
    try:
      import h5py
      if mpi.NTask > 1:
        fd = h5py.File(self.p_name_global[0], 'r', driver="mpio", comm=MPI.COMM_WORLD)
      else:
        fd = h5py.File(self.p_name_global[0])
      header = fd["Header"]
    except IOError as e:
      print "logger not recognized: %s" % e
      raise error.FormatError("logger")

  def get_read_fcts(self):
    return [self.read_particles]

  def get_write_fcts(self):
    return [self.write_particles]

  def get_mxntpe(self):
    return 6

  def get_header_translation(self):
    """
    Gives a dictionnary containing all the header translation.
    If a new variable is possible in the HDF5 format, only the translation
    is required for the reader/writer.
    As h5py is not supporting dictionnary, they need special care when reading/writing.
    """

  def get_default_spec_vars(self):
    '''
    return specific variables default values for the class
    '''


    return {'massarr'              :np.array([0,0,self.nbody,0,0,0]),
            'atime'                :0.,
            'redshift'             :0.,
            'flag_sfr'             :0,
            'flag_feedback'        :0,
            'nall'                 :np.array([0,0,self.nbody,0,0,0]),
            'npart'                :np.array([0,0,self.nbody,0,0,0]),
            'flag_cooling'         :0,
            'num_files'            :1,
            'boxsize'              :0.,
            'omega0'               :0.,
            'omegalambda'          :0.,
            'hubbleparam'          :0.,
            'flag_age'             :0.,
            'hubbleparam'          :0.,
            'flag_metals'          :0.,
            'nallhw'               :np.array([0,0,0,0,0,0]),
            'flag_entr_ic'         :0,
            'flag_chimie_extraheader':0,
            'critical_energy_spec' :0.,
            'empty'                :48*'',
            'comovingintegration'  :None,
            'ChimieNelements'      :0,
            }

  def get_massarr_and_nzero(self):
    """
    return massarr and nzero

    !!! when used in //, if only a proc has a star particle,
    !!! nzero is set to 1 for all cpu, while massarr has a length of zero !!!
    """

  def getHeaderTranslation(self):
      header_var = {}

      header_var["Header/NumPart_ThisFile"]          = "npart"
      header_var["Header/NumPart_Total"]             = "nall"
      header_var["Header/NumPart_Total_HighWord"]    = "nallhw"
      header_var["Header/NumFilesPerSnapshot"]       = "num_files"
      header_var["Header/BoxSize"]                   = "boxsize"

      return header_var

  def getArrayTranslation(self):
      array_var = {}

      array_var["ParticleIDs"]   = "num"
      array_var["Offset"]        = "offset"

      return array_var

  def getArrayDefaultValue(self):
    """
    Gives a dictionary of default value for pNbody's arrays
    """
    # default value
    dval = {}
    dval["ParticleIDs"] = -1
    dval["Offset"] = -1
    return dval

  def getArrayDimension(self):
    """
    Gives a dictionary of dimension for pNbody's arrays
    """
    # dimension
    vdim = {}
    vdim["ParticleIDs"]  = 1
    vdim["Offset"]       = 1
    return vdim

  def get_list_header(self):
    """
    Gives a list of header directory from self.get_header_translation
    """
    list_header = []
    trans = self.getHeaderTranslation()
    for key, tmp in trans.items():
      directory = key.split("/")[0]
      if directory not in list_header:
        list_header.append(directory)
    return list_header

  def read_particles(self,f):
    '''
    read logger file
    '''
    if f is not None:
      f.seek(0,2)

    fd = h5py.File(self.p_name_global[0], 'r')

    trans = self.getHeaderTranslation()
    list_header = self.get_list_header()
    
    for name in list_header:
      # e.g. create self.npart with the value fd["Header"].attrs["NumPart_ThisFile"]
      for key in fd[name].attrs:
        full_name = name + "/" + key
        if full_name not in trans.keys():
          print "'%s' key not recognized, please add it to config/format/swift_logger.py file in get_header_translation" % full_name
        else:
          setattr(self, trans[full_name], fd[name].attrs[key])

    n = np.sum(self.npart)

    list_of_vectors = []

    ntab = self.getArrayTranslation()
    vdim = self.getArrayDimension()
    dval = self.getArrayDefaultValue()

    for i,n in enumerate(self.npart):
      
      if n==0:
        continue

      if fd.keys().count("PartType%d"%i)==0:
        print "type=%d n=%d but group %s is not found !"%(i,n,"PartType%d"%i)
        sys.exit()

      # loop over dataset
      block = fd["PartType%d"%i]
      for key in block.keys():
        if not ntab.has_key(key):
          print "get a dataset with name %s but no such key in ntab"%key
          continue
        
        varname = ntab[key]

        if not self.has_var(varname):
          setattr(self, varname, None)

        # record the variable in a list
        if list_of_vectors.count(varname)==0:
          list_of_vectors.append(varname)

    # read the tables
    for i,n in enumerate(self.npart):

      if n!=0:
        if self.verbose and mpi.mpi_IsMaster():
          print "Reading particles (type %i)..." % i

        remaining_vectors = deepcopy(list_of_vectors)

        # loop over dataset
        block = fd["PartType%d"%i]
        if mpi.mpi_NTask() > 1:
          init, end = self.get_particles_limits(i)

        for key in block.keys():
          varname = ntab[key]
          if self.skip_io_block(varname):
            print "Skipping %s block" % varname
            continue

          if mpi.mpi_NTask() > 1:
            data = block[key][init:end]
          else:
            data = block[key].value
          if getattr(self,varname) is None:
            setattr(self,varname,data)
          else:
            setattr(self,varname, np.concatenate((getattr(self,varname), data )) )

          remaining_vectors.remove(varname)


        # loop over vectors absent from the block
        for varname in remaining_vectors:
          var = getattr(self,varname)
          data = (np.ones(getsize(n,vdim[varname]))*dval[varname]).astype(np.float32)
          if var!=None:
            setattr(self,varname, np.concatenate((getattr(self,varname), data )) )
          else:
            setattr(self,varname,data)

    # set tpe
    self.tpe = np.array([],np.int32)
    for i in range(len(self.npart)):
      self.tpe = np.concatenate( (self.tpe,np.ones(self.npart[i])*i) )



    data = logger_loader.loadFromIndex(self.offset, "/home/loikki/data/snap/snapshot.dump")

    self.pos = data["position"]
