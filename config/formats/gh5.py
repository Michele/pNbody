'''
 @package   pNbody
 @file      gh5.py
 @brief     gh5 format (Gear in HDF5)
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''  
  
####################################################################################################################################
#
# GEAR HDF5 CLASS
#
####################################################################################################################################

import sys
import types
import string
import numpy as np

import pNbody
from pNbody import mpi, error, units

try:				# all this is usefull to read files
  from mpi4py import MPI
except:
  MPI = None

  
class Nbody_gh5:

  def _init_spec(self):
    # create an empty header (in case the user is creating a dataset)
    self._header = []

  def get_excluded_extension(self):
    """
    Return a list of file to avoid when extending the default class.
    """
    return []

  def check_spec_ftype(self,f):
    try:
      import h5py
      if mpi.NTask > 1:
        fd = h5py.File(self.p_name_global[0], 'r', driver="mpio", comm=MPI.COMM_WORLD)
      else:
        fd = h5py.File(self.p_name_global[0])
      header = fd["Header"]
      if "Unit temperature in cgs (U_T)" in fd["Units"].attrs:
        raise error.FormatError("swift")
      fd.close()
    except IOError as e:
      print "gh5 not recognized: %s" % e
      raise error.FormatError("gh5")

  def set_pio(self, pio):
    """ Overwrite function
    """
    pNbody.Nbody.set_pio(self, "no")


  def get_read_fcts(self):
    return [self.read_particles]

  def get_write_fcts(self):
    return [self.write_particles]

  def get_mxntpe(self):
    return 6

  def get_header_translation(self):
    """
    Gives a dictionnary containing all the header translation.
    If a new variable is possible in the HDF5 format, only the translation
    is required for the reader/writer.
    As h5py is not supporting dictionnary, they need special care when reading/writing.
    """
    # dict containing all the main header variables (=> easy acces)
    # e.g. self.npart will contain NumPart_ThisFile
    header_var = {}
    header_var["Header/Code"]                      = "Gear"
    # Size variables
    header_var["Header/NumPart_ThisFile"]          = "npart"
    header_var["Header/NumPart_Total"]             = "nall"
    header_var["Header/NumPart_Total_HighWord"]    = "nallhw"
    header_var["Header/MassTable"]                 = "massarr"
    header_var["Header/NumFilesPerSnapshot"]       = "num_files"
    header_var["Header/BoxSize"]                   = "boxsize"
    header_var["Header/ChimieNumberElement"]       = "ChimieNelements"

    # Physics
    header_var["Header/Time"]                      = "atime"
    header_var["Header/Redshift"]                  = "redshift"
    header_var["Header/OmegaBaryon"]               = "omegabaryon"
    header_var["Header/Omega0"]                    = "omega0"
    header_var["Header/OmegaLambda"]               = "omegalambda"
    header_var["Header/HubbleParam"]               = "hubbleparam"
    header_var["Header/HubbleParamNIU"]               = "hubbleparam_niu"
    header_var["Header/ChimieSolarMassAbundances"] = "ChimieSolarMassAbundances"
    header_var["Header/ChimieLabels"]              = "ChimieElements"

    # Stars
    header_var["Header/JeansMassFactor"]           = "jeans_mass_factor"
    header_var["Header/StarFormationType"]         = "star_formation_type"
    header_var["Header/StarFormationCstar"]        = "star_formation_cstar"
    header_var["Header/StarFormationTime"]         = "star_formation_time"
    header_var["Header/StarFormationDensity"]      = "star_formation_density"
    header_var["Header/StarFormationTemperature"]  = "star_formation_temperature"
    header_var["Header/StarFormationNStarsFromGas"]= "star_formation_n_stars_from_gas"
    header_var["Header/StarFormationMgMsFraction"] = "star_formation_mg_ms_fraction"
    header_var["Header/StarFormationStarMass"]     = "star_formation_star_mass"
    header_var["Header/StarFormationJeansMassFactor"] = "star_formation_jeans_mass_factor"
    header_var["Header/StarFormationSoftening"]    = "star_formation_softening"
    header_var["Header/StarFormationSofteningMaxPhys"] = "star_formation_softening_max_phys"

    # Chemistry
    header_var["Header/ChimieParameterFile"]       = "chimie_parameter_file"
    header_var["Header/ChimieMaxSizeTimestep"]     = "chimie_max_size_timestep"
    header_var["Header/ChimieKineticFeedbackFraction"]= "chimie_kinetic_feedback_fraction"
    header_var["Header/ChimieWindSpeed"]           = "chimie_wind_speed"
    header_var["Header/ChimieWindTime"]            = "chimie_wind_time"
    header_var["Header/ChimieSNIIThermalTime"]     = "chimie_snii_thermal_time"
    header_var["Header/ChimieSNIaThermalTime"]     = "chimie_snia_thermal_time"
    header_var["Header/ChimieSupernovaEnergy"]     = "chimie_supernova_energy"

    # Flags
    header_var["Header/Flag_Sfr"]                  = "flag_sfr"
    header_var["Header/Flag_Feedback"]             = "flag_feedback"
    header_var["Header/Flag_Cooling"]              = "flag_cooling"
    header_var["Header/Flag_Supernova_Thermaltime"]= "flag_thermaltime"
    header_var["Header/Flag_StellarAge"]           = "flag_age"
    header_var["Header/Flag_Metals"]               = "flag_metals"
    header_var["Header/Flag_Entropy_ICs"]          = "flag_entr_ic"
    header_var["Header/Flag_Chimie_Extraheader"]   = "flag_chimie_extraheader"

    # Cooling
    header_var["Header/CoolingType"]               = "cooling_type"
    header_var["Header/CutofCoolingTemperature"]   = "cutoff_cooling_temperature"
    header_var["Header/GrackleUVbackground"]       = "grackle_uv_background"
    header_var["Header/GrackleRedshift"]           = "grackle_redshift"
    header_var["Header/GrackleHSShieldingDensityThreshold"] = "grackle_hs_shielding_density_threshold"
    header_var["Header/InitGasMetallicity"]        = "init_gas_metallicity"
    header_var["Header/InitGasTemp"]               = "init_gas_temp"
    header_var["Header/MinGasTemp"]                = "min_gas_temp"

    # Softening
    header_var["Header/SofteningGas"]              = "softening_gas"
    header_var["Header/SofteningHalo"]             = "softening_halo"
    header_var["Header/SofteningDisk"]             = "softening_disk"
    header_var["Header/SofteningBulge"]            = "softening_bulge"
    header_var["Header/SofteningStars"]            = "softening_stars"
    header_var["Header/SofteningBndry"]            = "softening_bndry"
    header_var["Header/SofteningGasMaxPhys"]       = "softening_gas_max_phys"
    header_var["Header/SofteningHaloMaxPhys"]      = "softening_halo_max_phys"
    header_var["Header/SofteningDiskMaxPhys"]      = "softening_disk_max_phys"
    header_var["Header/SofteningBulgeMaxPhys"]     = "softening_bulge_max_phys"
    header_var["Header/SofteningStarsMaxPhys"]     = "softening_stars_max_phys"
    header_var["Header/SofteningBndryMaxPhys"]     = "softening_bndry_max_phys"
    header_var["Header/MinGasHsmlFractional"]      = "min_gas_hsml_fractional"

    # Gear details
    header_var["Header/RandomSeed"]                = "randomseed"
    header_var["Header/TypeOfTimestepCriterion"]   = "type_timestep_criterion"
    header_var["Header/ComovingIntegrationOn"]     = "comovingintegration"
    header_var["Header/ErrTolIntAccuracy"]         = "err_tol_int_accuracy"
    header_var["Header/CourantFac"]                = "courant_fac"
    header_var["Header/MaxSizeTimestep"]           = "max_size_timestep"
    header_var["Header/MinSizeTimestep"]           = "min_size_timestep"
    header_var["Header/NgbFactorTimestep"]         = "nbg_factor_timestep"
    header_var["Header/ErrTolTheta"]               = "err_tol_theta"
    header_var["Header/TypeOfOpeningCriterion"]    = "type_opening_criterion"
    header_var["Header/ErrTolForceAcc"]            = "err_tol_force_acc"
    header_var["Header/TreeDomainUpdateFrequency"] = "tree_domain_update_frequency"
    header_var["Header/MaxRMSDisplacementFac"]     = "max_rms_displacement_fac"
    header_var["Header/DesNumNgb"]                 = "des_num_ngb"
    header_var["Header/MaxNumNgbDeviation"]        = "max_num_ngb_deviation"
    header_var["Header/ArtBulkViscConst"]          = "art_bulk_visc_const"
    header_var["Header/ArtBulkViscConstMin"]       = "art_bulk_visc_const_min"
    header_var["Header/ArtBulkViscConstMax"]       = "art_bulk_visc_const_max"
    header_var["Header/ArtBulkViscConstL"]         = "art_bulk_visc_const_l"

    # Outer Potential
    header_var["Header/PotDirectory"]              = "potential_directory"
    header_var["Header/TracePosition"]             = "trace_position"
    header_var["Header/TraceVelocity"]             = "trace_velocity"
    header_var["Header/TraceAcceleration"]         = "trace_acceleration"
    header_var["Header/TraceAngular"]              = "trace_angular"
    header_var["Header/TracePotential"]            = "trace_potential"
    header_var["Header/TraceTimeStep"]             = "trace_timestep"
    header_var["Header/TraceEpsilon"]              = "trace_epsilon"
    header_var["Header/FricCoef"]                  = "trace_fric_coef"
    header_var["Header/OscCoef"]                   = "trace_osc_coef"
    header_var["Header/TraceIDs"]                  = "trace_ids"
    header_var["Header/HaloScale"]                 = "halo_scale"
    header_var["Header/HaloT"]                     = "halo_t"
    header_var["Header/HaloPMass"]                 = "halo_p_mass"
    header_var["Header/GalMaxID"]                  = "gal_max_id"

    # Units
    #header_var["Units/Unit velocity in cgs (U_V)"] = "UnitVelocity_in_cm_per_s"
    header_var["Units/Unit length in cgs (U_L)"]   = "UnitLength_in_cm"
    header_var["Units/Unit mass in cgs (U_M)"]     = "UnitMass_in_g"
    header_var["Units/Unit time in cgs (U_t)"]     = "Unit_time_in_cgs"
    header_var["Units/GravityConstant"]            = "G"
    header_var["Units/GravityConstantInternal"]    = "G"

    # Swift directory
    header_var["RuntimePars/PeriodicBoundariesOn"] = "periodicboundaries"

    return header_var

  def get_list_header(self):
    """
    Gives a list of header directory from self.get_header_translation
    """
    list_header = []
    trans = self.get_header_translation()
    for key, tmp in trans.items():
      directory = key.split("/")[0]
      if directory not in list_header:
        list_header.append(directory)
    return list_header


  def get_array_translation(self):
    """
    Gives a dictionnary containing all the header translation with the particles type
    requiring the array.
    If a new variable is possible in the HDF5 format, the translation,
    default value (function get_array_default_values) and dimension (function get_array_dimension)
    are required for the reader/writer.
    """
    # hdf5 names -> pNbody names
    # [name, partType, type] where partType is a list of particles with the datas
    # True means all, False means none
    # and type is the data type
    ntab={}
    # common data
    ntab["Coordinates"]      = ["pos", True, np.float32]
    ntab["Velocities"]       = ["vel", True, np.float32]
    ntab["ParticleIDs"]      = ["num", True, np.uint32]
    ntab["Masses"]           = ["mass", True, np.float32]
    # gas data
    ntab["InternalEnergy"]   = ["u", [0], np.float32]
    ntab["Density"]          = ["rho", [0], np.float32]
    ntab["SmoothingLength"]  = ["rsp", [0], np.float32]
    ntab["Metals"]           = ["metals", [0], np.float32]
    ntab["OptVar1"]          = ["opt1", [0], np.float32]
    ntab["OptVar2"]          = ["opt2", [0], np.float32]
    ntab["SNII_ThermalTime"] = ["snii_thermal_time", [0], np.float32]
    ntab["SNIa_ThermalTime"] = ["snia_thermal_time", [0], np.float32]
    # stars data
    ntab["InitialMass"]      = ["minit", [1], np.float32]
    ntab["StarFormationTime"]= ["tstar", [1], np.float32]
    ntab["StarIDProj"]       = ["idp", [1], np.uint32]
    ntab["StarHsml"]         = ["rsp", [1], np.float32]
    ntab["StarMetals"]       = ["metals", [1], np.float32]
    ntab["StarRho"]          = ["rho", [1], np.float32]
    return ntab

  def get_array_default_value(self):
    """
    Gives a dictionary of default value for pNbody's arrays
    """
    # default value
    dval = {}
    dval["pos"]          = 0.0
    dval["vel"]          = 0.0
    dval["num"]          = 0.0
    dval["mass"]         = 0.0
    dval["u"]            = 0.0
    dval["rho"]          = 0.0
    dval["metals"]       = 0.0
    dval["opt1"]         = 0.0
    dval["opt2"]         = 0.0
    dval["rsp"]          = 0.0
    dval["minit"]        = 0.0
    dval["tstar"]        = 0.0
    dval["idp"]          = 0.0
    dval["rsp_stars"]    = 0.0
    dval["metals_stars"] = 0.0
    dval["rho_stars"]    = 0.0
    dval["snii_thermal_time"] = 0.0
    dval["snia_thermal_time"] = 0.0
    return dval

  def get_array_dimension(self):
    """
    Gives a dictionary of dimension for pNbody's arrays
    """
    # dimension
    vdim = {}
    vdim["pos"]          = 3
    vdim["vel"]          = 3

    vdim["num"]          = 1
    vdim["mass"]         = 1
    vdim["u"]            = 1
    vdim["rho"]          = 1
    vdim["opt1"]         = 1
    vdim["opt2"]         = 1
    vdim["rsp"]          = 1
    vdim["minit"]        = 1
    vdim["tstar"]        = 1
    vdim["idp"]          = 1
    vdim["rsp_stars"]    = 1
    vdim["rho_stars"]    = 1
    vdim["snii_thermal_time"] = 1
    vdim["snia_thermal_time"] = 1

    vdim["metals"]       = self.ChimieNelements
    vdim["metals_stars"] = self.ChimieNelements
    return vdim

  def get_default_spec_vars(self):
    '''
    return specific variables default values for the class
    '''


    return {'massarr'              :np.array([0,0,self.nbody,0,0,0]),
            'atime'                :0.,
            'redshift'             :0.,
            'flag_sfr'             :0,
            'flag_feedback'        :0,
            'nall'                 :np.array([0,0,self.nbody,0,0,0]),
            'npart'                :np.array([0,0,self.nbody,0,0,0]),
            'flag_cooling'         :0,
            'num_files'            :1,
            'boxsize'              :0.,
            'omega0'               :0.,
            'omegalambda'          :0.,
            'hubbleparam'          :0.,
            'flag_age'             :0.,
            'hubbleparam'          :0.,
            'flag_metals'          :0.,
            'nallhw'               :np.array([0,0,0,0,0,0]),
            'flag_entr_ic'         :0,
            'flag_chimie_extraheader':0,
            'critical_energy_spec' :0.,
            'empty'                :48*'',
            'comovingintegration'  :None,
            'ChimieNelements'      :0,
            }

  def get_particles_limits(self, i):
    """ Gives the limits for a thread. 
    In order to get the particles, slice them like this pos[start:end].
    :param int i: Particle type
    :returns: (start, end)
    """
    nber = float(self.nall[i])/mpi.mpi_NTask()
    start = int(mpi.mpi_ThisTask()*nber)
    end = int((mpi.mpi_ThisTask()+1)*nber)
    return start, end


  def set_local_value(self):
    N = mpi.mpi_NTask()
    if N == 1:
      return
    part = len(self.nall)
    for i in range(part):
      s, e = self.get_particles_limits(i)
      self.npart[i] = e-s

  def get_massarr_and_nzero(self):
    """
    return massarr and nzero

    !!! when used in //, if only a proc has a star particle,
    !!! nzero is set to 1 for all cpu, while massarr has a length of zero !!!
    """

    if self.has_var('massarr') and self.has_var('nzero'):
      if self.massarr !=None and self.nzero!=None:
        if mpi.mpi_IsMaster():
          print "warning : get_massarr_and_nzero : here we use massarr and nzero",self.massarr,self.nzero
        return self.massarr,self.nzero

    massarr = zeros(len(self.npart),float)
    nzero = 0

    # for each particle type, see if masses are equal
    for i in range(len(self.npart)):
      first_elt = sum((arange(len(self.npart)) < i) * self.npart)
      last_elt  = first_elt + self.npart[i]

      if first_elt!=last_elt:
        c = (self.mass[first_elt]==self.mass[first_elt:last_elt]).astype(int)
        if sum(c)==len(c):
          massarr[i] = self.mass[first_elt]
        else:
          nzero = nzero+len(c)

    return massarr.tolist(),nzero


  def read_particles(self,f):
    '''
    read gadget file
    '''
    from copy import deepcopy

    # go to the end of the file
    if f is not None:
      f.seek(0,2)

    import h5py
    if mpi.mpi_NTask() > 1:
      fd = h5py.File(self.p_name_global[0], 'r', driver="mpio", comm=MPI.COMM_WORLD)
    else:
      fd = h5py.File(self.p_name_global[0], 'r')


    ################
    # read header
    ################
    if self.verbose and mpi.mpi_IsMaster():
      print "reading header..."

    # set default values
    default = self.get_default_spec_vars()
    for key, i in default.items():
      setattr(self, key, i)

    # get values from snapshot
    trans = self.get_header_translation()

    list_header = self.get_list_header()

    for name in list_header:
      # e.g. create self.npart with the value fd["Header"].attrs["NumPart_ThisFile"]
      for key in fd[name].attrs:
        full_name = name + "/" + key
        if full_name not in trans.keys():
          print "'%s' key not recognized, please add it to config/format/gh5.py file in get_header_translation" % full_name
          sys.exit(2)
        tmp = fd[name].attrs[key]
        if tmp == "None":
          tmp = None
        setattr(self, trans[full_name], tmp)

    if self.flag_chimie_extraheader==1:
      if self.verbose and mpi.mpi_IsMaster():
        print "reading chimie extra-header..."

      self.ChimieNelements    = self.ChimieNelements

      self.ChimieElements = string.split(self.ChimieElements,',')[:self.ChimieNelements]

      self.ChimieSolarMassAbundances = np.fromstring(self.ChimieSolarMassAbundances,np.float32)
      tmp = {}
      for i,elt in enumerate(self.ChimieElements):
        tmp[elt] = self.ChimieSolarMassAbundances[i]
      self.ChimieSolarMassAbundances = tmp
    else:
      self.ChimieNelements    = self.ChimieNelements
      self.ChimieElements     = ['Fe','Mg','O','Metals']

      self.ChimieSolarMassAbundances = {}
      self.ChimieSolarMassAbundances['Fe']      = 0.001771
      self.ChimieSolarMassAbundances['Mg']      = 0.00091245
      self.ChimieSolarMassAbundances['O']       = 0.0108169
      self.ChimieSolarMassAbundances['Metals']  = 0.02

    # get local value from global ones
    self.set_local_value()

    ###############
    # read units
    ###############
    if self.verbose and mpi.mpi_IsMaster():
      print "reading units..."

    # define system of units
    params = {}

    # consider that if we have length unit, we should have them all
    if hasattr(self, "UnitLength_in_cm"):
      params['UnitLength_in_cm']         = float(self.UnitLength_in_cm)
      if hasattr(self, "UnitVelocity_in_cm_per_s"):
        params['UnitVelocity_in_cm_per_s'] = float(self.UnitVelocity_in_cm_per_s)
      else:
        self.UnitVelocity_in_cm_per_s = float(self.UnitLength_in_cm)/float(self.Unit_time_in_cgs)
        params['UnitVelocity_in_cm_per_s'] = self.UnitVelocity_in_cm_per_s
      params['UnitMass_in_g']            = float(self.UnitMass_in_g)
      self.localsystem_of_units = units.Set_SystemUnits_From_Params(params)
    else:
      print "WARNING: snapshot seems broken! There is no units!"


    ################
    # read particles
    ################

    if self.verbose and mpi.mpi_IsMaster():
      print "reading particles..."
    def getsize(n,dim):
      if dim==1:
        return (n,)
      if dim>1:
        return (n,dim)


    list_of_vectors=[]

    # check what vector we needs to be created

    ntab = self.get_array_translation()
    vdim = self.get_array_dimension()
    dval = self.get_array_default_value()

    for i,n in enumerate(self.npart):

      if n!=0:

        if fd.keys().count ("PartType%d"%i)==0:
          print "type=%d n=%d but group %s is not found !"%(i,n,"PartType%d"%i)
          sys.exit()

        # loop over dataset
        block = fd["PartType%d"%i]
        for key in block.keys():
          if not ntab.has_key(key):
            print "get a dataset with name %s but no such key in ntab"%key

          varname = ntab[key][0]

          if self.has_var(varname):
             pass # do nothing
          else:
             setattr(self, varname, None)

          # record the variable in a list
          if list_of_vectors.count(varname)==0:
            list_of_vectors.append(varname)

    # read the tables
    for i,n in enumerate(self.npart):

      if n!=0:
        if self.verbose and mpi.mpi_IsMaster():
          print "Reading particles (type %i)..." % i

        remaining_vectors = deepcopy(list_of_vectors)

        # loop over dataset
        block = fd["PartType%d"%i]
        if mpi.mpi_NTask() > 1:
          init, end = self.get_particles_limits(i)

        for key in block.keys():
          varname = ntab[key][0]
          if self.skip_io_block(varname):
           print "Skipping %s block" % varname
           continue
          var = getattr(self,varname)

          if mpi.mpi_NTask() > 1:
            data = block[key][init:end]
          else:
            data = block[key].value
          if var is not None:
            setattr(self,varname, np.concatenate((getattr(self,varname), data )) )
          else:
            setattr(self,varname,data)

          remaining_vectors.remove(varname)


        # loop over vectors absent from the block
        for varname in remaining_vectors:
          var = getattr(self,varname)
          data = (np.ones(getsize(n,vdim[varname]))*dval[varname]).astype(np.float32)
          if var is not None:
            setattr(self,varname, np.concatenate((getattr(self,varname), data )) )
          else:
            setattr(self,varname,data)

    # set tpe
    self.tpe = np.array([],np.int32)
    for i in range(len(self.npart)):
      self.tpe = np.concatenate( (self.tpe,np.ones(self.npart[i])*i) )

    # compute nzero
    nzero=0
    mass = np.array([])

    for i in range(len(self.npart)):
      if self.massarr is None or self.massarr[i] == 0:
        nzero = nzero+self.npart[i]
      else:
        print "Massarr is not supported! Please specify the mass of all the particles!"


    self.nzero = nzero

    mpi.mpi_barrier()
    fd.close()

  def write_particles(self,f):
    '''
    specific format for particle file
    '''
    # go to the end of the file
    if f is not None:
      f.seek(0,2)

    import h5py
    # not clean, but work around pNbody
    filename = self.p_name_global[0]
    # open file
    if mpi.mpi_NTask() > 1:
      from mpi4py import MPI
      h5f = h5py.File(filename, "w", driver="mpio", comm=MPI.COMM_WORLD)
    else:
      h5f = h5py.File(filename, "w")

    # add units to the usual gh5 struct
    if hasattr(self, "unitsparameters"):
      units = self.unitsparameters.get_dic()
      for key, i in units.items():
        if not hasattr(self, key):
          setattr(self, key, i)

    if hasattr(self, "UnitVelocity_in_cm_per_s") and hasattr(self, "UnitLength_in_cm"):
      self.Unit_time_in_cgs = self.UnitLength_in_cm / self.UnitVelocity_in_cm_per_s

    ############
    # HEADER
    ############
    if self.verbose and mpi.mpi_IsMaster():
      print "Writing header..."

    if self.massarr is None:
      self.massarr = np.zeros(len(self.npart))

    list_header = self.get_list_header()
    trans = self.get_header_translation()
    # cheat a little bit in order to get the real number of particles in this file
    trans["Header/NumPart_ThisFile"] = "npart_tot"

    for name in list_header:
      h5f.create_group(name)
    for key in self.get_list_of_vars():
      if key in trans.values():
        ind = trans.values().index(key)
        name, hdf5 = trans.keys()[ind].split("/")
        value = getattr(self, key)
        if type(value) != types.DictType:
         if value is None:
          h5f[name].attrs[hdf5] = "None"
         else:
          h5f[name].attrs[hdf5] = value
    # transform data into Gear's format
    name = "ChimieElements"
    if hasattr(self, name):
      string = ""
      for i in getattr(self, name):
        string = string + i + ","
      ind = trans.values().index(name)
      hdf5 = trans.keys()[ind].split("/")[-1]
      # use a fixed size string from numpy
      h5f["Header"].attrs[hdf5] = np.string_(string)

    name = "ChimieSolarMassAbundances"
    if hasattr(self, name):
      list_abund = []
      for key in self.ChimieElements:
        list_abund.append(getattr(self, name)[key])
      ind = trans.values().index(name)
      hdf5 = trans.keys()[ind].split("/")[-1]
      h5f["Header"].attrs[hdf5] = list_abund

    ##############
    # PARTICULES
    ##############
    if self.verbose and mpi.mpi_IsMaster():
      print "Writing particles..."

    for i in range(len(self.npart)):
      if self.massarr is not None and self.massarr[i] != 0:
        print "Massarr is not supported! Please specify the mass of all the particles!"

    ntab = self.get_array_translation()
    # get particles type present
    type_part = []
    for i in range(len(self.npart)):
      if self.npart[i] > 0:
        type_part.append(i)

    # write particles
    for i in type_part:
      if mpi.mpi_NTask() > 1:
        init, end = self.get_particles_limits(i)
      if self.verbose and mpi.mpi_IsMaster():
        print "Writing particles (type %i)..." % i
      group = "PartType%i"%i
      grp = h5f.create_group(group)
      nb_sel = self.select(i)
      for key,j in ntab.items():
        varname = j[0]
        var_type = j[1]
        if (var_type is True) or i in var_type:
          if hasattr(nb_sel, varname):
            # get and transform type
            tmp = getattr(nb_sel, varname)
            tmp = tmp.astype(j[2])
            # create dataset
            size = list(tmp.shape)
            size[0] = self.npart_tot[i]
            if mpi.mpi_NTask() > 1:
              dset = grp.create_dataset(key, size, dtype=j[2])
              dset[init:end] = tmp
            else:
              h5f[group][key] = tmp
              
    h5f.close()
