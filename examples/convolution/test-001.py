#!/usr/bin/env python

'''
 @package   pNbody
 @file      test-001.py
 @brief     Test 1D c convolution
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
from numpy import fft

import sys
import libcon


# do the fourrier transform
N=18
random.seed(0)
G2 = random.random(N)
M2 = random.random(N)#ones(N,float)

print '-->'
print G2
print M2
out = libcon.convolution_1d(G2,M2)
print '<--'
print out



##############
#print  fft.fft(G2)
