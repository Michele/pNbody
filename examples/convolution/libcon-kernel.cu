#include <stdint.h>
#include <cuda.h>
#include <cuda_runtime_api.h>

#include <cutil.h>
#include <cutil_inline_runtime.h>



__global__ void konvol(cufftComplex *dev_Ai, cufftComplex *dev_Bi,cufftComplex *dev_Ci)
  {
    int i = blockIdx.x;
    
    float rea,img;
    rea = dev_Ai[i].x*dev_Bi[i].x - dev_Ai[i].y*dev_Bi[i].y;
    img = dev_Ai[i].x*dev_Bi[i].y + dev_Ai[i].y*dev_Bi[i].x;  
    dev_Ci[i].x = rea;
    dev_Ci[i].y = img;    
    
    
  }



void dev_fft_1dcu(int N, double *Ad,double *Crd, double *Cid)
  {
  
          int i;
	  
          /* tranform in float */
          float *A;
	  cufftComplex *C;
          A=(float *)malloc(sizeof(float)*N);
          C=(cufftComplex *)malloc(sizeof(cufftComplex)*N);  
         
	  for (i=0;i<N;i++)
	      A[i]=(float)Ad[i];
	 
	 
	  
	  /* allocate gpu */
	  float *dev_A;	  
	  cudaMalloc((void**) &dev_A, sizeof(float)*N);
	  cudaMemcpy(dev_A, A, sizeof(float)*N, cudaMemcpyHostToDevice);

          cufftComplex *dev_C;
	  cudaMalloc((void**) &dev_C, sizeof(cufftComplex)*N);
	  
	  cufftHandle plan;
	  cufftResult cres;
	  cufftPlan1d(&plan, N, CUFFT_R2C,1);
          
	  cres = cufftExecR2C(plan, dev_A, dev_C);
	  cudaThreadSynchronize();

	  if(cres != CUFFT_SUCCESS)
	    {
	  	  printf("ERROR\n");
	  	  exit(10);
	    }

          cufftDestroy(plan);
 
          /* done */
	  cudaMemcpy(C, dev_C,sizeof(cufftComplex)*N, cudaMemcpyDeviceToHost);
	  
	  for (i=0;i<N;i++)
	    {
	      Crd[i]=(double)C[i].x;
	      Cid[i]=(double)C[i].y;
	    } 	  

	  cudaFree(dev_A);
	  cudaFree(dev_C);

	  free(A);
	  free(C);
  }













void dev_convolution_1dcu(int N, double *Ad,double *Bd, double *Cd)
  {
  
          int i;
	  
          /* tranform in float */
          float *A,*B,*C;
          A=(float *)malloc(sizeof(float)*N);
          B=(float *)malloc(sizeof(float)*N);  
          C=(float *)malloc(sizeof(float)*N);  
         
	  for (i=0;i<N;i++)
	    {
	      A[i]=(float)Ad[i];
	      B[i]=(float)Bd[i];
	    }	 
	 
	 
	  
	  /* allocate gpu */
	  float *dev_A;
	  float *dev_B;
	  float *dev_C;
	  
	  
	  cudaMalloc((void**) &dev_A, sizeof(float)*N);
	  cudaMemcpy(dev_A, A, sizeof(float)*N, cudaMemcpyHostToDevice);
	  
	  cudaMalloc((void**) &dev_B, sizeof(float)*N);
	  cudaMemcpy(dev_B, B, sizeof(float)*N, cudaMemcpyHostToDevice);

	  cudaMalloc((void**) &dev_C, sizeof(float)*N);


          cufftComplex *dev_Ai,*dev_Bi,*dev_Ci;
	  cudaMalloc((void**) &dev_Ai, sizeof(cufftComplex)*N);
	  cudaMalloc((void**) &dev_Bi, sizeof(cufftComplex)*N);
	  cudaMalloc((void**) &dev_Ci, sizeof(cufftComplex)*N);
	  
	  cufftHandle plan;
	  cufftResult cres;
	  cufftPlan1d(&plan, N, CUFFT_R2C,1);
	  
	  cres = cufftExecR2C(plan, dev_A, dev_Ai);
	  cudaThreadSynchronize();
	  if(cres != CUFFT_SUCCESS)
	    {
	  	  printf("ERROR\n");
	  	  exit(10);
	    }
	  cres = cufftExecR2C(plan, dev_B, dev_Bi);
	  cudaThreadSynchronize();
	  if(cres != CUFFT_SUCCESS)
	    {
	  	  printf("ERROR\n");
	  	  exit(10);
	    }	  
          cufftDestroy(plan);


          /* a kernel */
          konvol<<<(N-1)/2,1>>>(dev_Ai,dev_Bi,dev_Ci);




          cufftPlan1d(&plan, N, CUFFT_C2R,1);
	  cres = cufftExecC2R(plan, dev_Ci, dev_C);
	  cudaThreadSynchronize();
	  if(cres != CUFFT_SUCCESS)
	    {
	  	  printf("ERROR\n");
	  	  exit(10);
	    }	  
          cufftDestroy(plan);

 
          /* done */
	  cudaMemcpy(C, dev_C,sizeof(float)*N, cudaMemcpyDeviceToHost);
	  
	  
	  for (i=0;i<N;i++)
	      Cd[i]=(double)C[i]/N;		/* we put here the factor */
	  	  
	  cudaFree(dev_A);
	  cudaFree(dev_B);
	  cudaFree(dev_C);

	  cudaFree(dev_Ai);
	  cudaFree(dev_Bi);
	  cudaFree(dev_Ci);	
	  
	  free(A);
	  free(B);
	  free(C);
	      
  
  }
