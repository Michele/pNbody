#!/usr/bin/env python

'''
 @package   pNbody
 @file      test-002.py
 @brief     ...
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
from numpy import fft

import sys
import libcon
import time


# do the fourrier transform
N=32	# val max = 65535
random.seed(0)
A = random.random(N)


#####################
# python fft
#####################

C = fft.fft(A)
print C.real.astype(float)
print C.imag.astype(float)
print

#####################
# fftw
#####################

Cr,Ci = libcon.fftw_dft_r2c_1d(A)
print Ci.astype(float)
print Cr.astype(float)
print 

#####################
# cufft
#####################

Crcu,Cicu = libcon.fftw_dft_r2c_1dcu(A)
print Cicu.astype(float)
print Crcu.astype(float)


'''
import Ptools as pt

x = arange(N)
pt.plot(x,C.imag.astype(float))
pt.plot(x,Ci.astype(float))
pt.plot(x,Cicu.astype(float))
pt.axis([0,N,-1,1])

pt.show()
'''




'''
print '-->'
#print G2
#print M2


#####################
# python fft
#####################
t5 = time.time()
G2ft = fft.fft(G2)
M2ft = fft.fft(M2)
Phi2ft = G2ft*M2ft
out = fft.ifft(Phi2ft)
t6 = time.time()
print '<--'
print out[-1]


#####################
# fftw
#####################
t1 = time.time()
out = libcon.convolution_1d(G2,M2)
t2 = time.time()
print '<--'
print out[-1]

#####################
# cufft
#####################
t3 = time.time()
out = libcon.convolution_1dcu(G2,M2)
t4 = time.time()
print '<--'
print out[-1]





# times
print t2-t1
print t4-t3
print t6-t5

'''

