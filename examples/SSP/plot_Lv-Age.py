#!/usr/bin/env python

'''
 @package   pNbody
 @file      plot_Lv-Age.py
 @brief     Plot luminostiy versus age of stars
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt
from numpy import *
import scipy
import scipy.interpolate
from optparse import OptionParser


from pNbody.SSP import libmaraston
from pNbody.SSP import libvazdekis
from pNbody.SSP import libbruzual

from pNbody import OPTDIR
import os,sys


def parse_options():


  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = pt.add_postscript_options(parser)
  parser = pt.add_limits_options(parser)
  parser = pt.add_log_options(parser)

  parser.add_option("--legend",
  		    action="store_true", 
  		    dest="legend",
		    default = False,		    
  		    help="add a legend")


  (options, args) = parser.parse_args()

          
  files = args
  
  return files,options






####################################################
# main
####################################################

files,opt = parse_options()  
pt.InitPlot(files,opt)

LumObject1 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_SALP_BHB.txt'))
LumObject2 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_SALP_IRHB.txt'))
LumObject3 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_KRUP_BHB.txt'))
LumObject4 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_KRUP_IRHB.txt'))

LumObjectVaz1 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_un_mu1.3.txt'))
LumObjectVaz2 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_bi_mu1.3.txt'))
LumObjectVaz3 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_ku_mu1.3.txt'))
LumObjectVaz4 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_kb_mu1.3.txt'))


LumObjectBruzual1 = libbruzual.BruzualLuminosities(os.path.join(OPTDIR,'SSP','P94_salpeter'))




LObj = LumObjectVaz1
 

Zs_idx = arange(len(LObj.Zs))

zmin  = -5
zmax  =  1
nz    = 50 
order = 1


datas = []
colors     = pt.Colors(n=len(Zs_idx)) 

for i in Zs_idx:
  
  c = colors.get()  
  ls = '-'

  Ls = LObj.MatLv[i,:]
  datas.append( pt.DataPoints(LObj.Ages,Ls,color=c,linestyle=ls,label="vaz",tpe='points') )
  datas.append( pt.DataPoints(LObj.Ages,Ls,color=c,linestyle=':',label="vaz",tpe='line') )


# use 2d interpolation
Zs0 = concatenate((LObj.Zs,[-3],[1]))
LObj.ExtrapolateMatrix()
LObj.CreateInterpolator()

Zs = Zs0
for Z in Zs:
  ages = 10**scipy.linspace(log10(min(LObj.Ages)),log10(max(LObj.Ages)),200)
  zs   = ones(len(ages))*Z
  ls = '-'
  c  = 'k'
  Ls = LObj.Luminosity(zs,ages)
  datas.append( pt.DataPoints(ages,Ls[0,:],color=c,linestyle=ls,label="[Z/H]=%g"%Z,tpe='line') )




# plot
for d in datas:     
    
  if   d.tpe=='points' or d.tpe=='both': 
    pt.scatter(d.x,d.y,c=d.color,s=5,linewidths=0,marker='o',vmin=opt.zmin,vmax=opt.zmax)
  
  if d.tpe=='line'   or d.tpe=='both':	
    pt.plot(d.x,d.y,color=d.color,ls=d.linestyle)



# set limits and draw axis
xmin,xmax,ymin,ymax = pt.SetLimitsFromDataPoints(opt.xmin,opt.xmax,opt.ymin,opt.ymax,datas,opt.log)



xlabel = r"$\rm{Age}\,[\rm{Gyr}]$"
ylabel = r"$\rm{Luminosity\,[\rm{L_{\odot}/M_{\odot}}]}$"
# plot axis
pt.SetAxis(xmin,xmax,ymin,ymax,log=opt.log)
pt.xlabel(xlabel,fontsize=pt.labelfont)
pt.ylabel(ylabel,fontsize=pt.labelfont)
pt.grid(False)


if opt.legend:
  pt.LegendFromDataPoints(datas)
  
    




pt.show()
