#!/usr/bin/env python

'''
 @package   pNbody
 @file      plot_MatLv.py
 @brief     plot luminosity
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt
from numpy import *
import scipy

from pNbody.SSP import libmaraston
from pNbody.SSP import libvazdekis
from pNbody.SSP import libbruzual
from pNbody.SSP import libtill

from optparse import OptionParser

from pNbody import OPTDIR
import os,sys



def parse_options():


  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = pt.add_postscript_options(parser)
  parser = pt.add_limits_options(parser)
  parser = pt.add_log_options(parser)

  parser.add_option("--legend",
  		    action="store_true", 
  		    dest="legend",
		    default = False,		    
  		    help="add a legend")


  (options, args) = parser.parse_args()

          
  files = args
  
  return files,options






####################################################
# main
####################################################

files,opt = parse_options()  
pt.InitPlot(files,opt)

opt.xmin = 0
opt.xmax = 20
opt.ymin = -10
opt.ymax = 2


LumObject1 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_SALP_BHB.txt'))
LumObject2 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_SALP_IRHB.txt'))
LumObject3 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_KRUP_BHB.txt'))
LumObject4 = libmaraston.MarastonLuminosities(os.path.join(OPTDIR,'SSP','Maraston_JC_KRUP_IRHB.txt'))

LumObjectVaz1 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_un_mu1.3.txt'))
LumObjectVaz2 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_bi_mu1.3.txt'))
LumObjectVaz3 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_ku_mu1.3.txt'))
LumObjectVaz4 = libvazdekis.VazdekisLuminosities(os.path.join(OPTDIR,'SSP','vazdekis_kb_mu1.3.txt'))


LumObjectBruzual1 = libbruzual.BruzualLuminosities(os.path.join(OPTDIR,'SSP','P94_salpeter'))

LumObjectTill = libtill.TillLuminosities()


LObj = LumObjectVaz1

#############################
# plot genuine matrix
#############################

data = log10(LObj.MatLv)

xmin = min(LObj.Ages)
xmax = max(LObj.Ages)
ymin = min(LObj.Zs)
ymax = max(LObj.Zs)
vmin = -1
vmax =  1

cmap = pt.GetColormap('rainbow4',revesed=True)
im = pt.imshow(data, interpolation='nearest', origin='lower',cmap=cmap, extent=(xmin,xmax,ymin,ymax),aspect='auto',vmin=vmin,vmax=vmax)

xlabel = r"$\rm{Age}\,[\rm{Gyr}]$"
ylabel = r"$\rm{[Z/H]}$"
zlabel = r"$\rm{log10 Luminosity\,[\rm{L_{\odot}/M_{\odot}}]}$"
# plot axis
pt.SetAxis(opt.xmin,opt.xmax,opt.ymin,opt.ymax)
pt.xlabel(xlabel,fontsize=pt.labelfont)
pt.ylabel(ylabel,fontsize=pt.labelfont)
cb = pt.colorbar()
cb.set_label(zlabel) 


#############################
# plot extrapolated matrix
#############################

LObj.ExtrapolateMatrix(order=1,s=0)

data = log10(LObj.MatLv)

xmin = min(LObj.Ages)
xmax = max(LObj.Ages)
ymin = min(LObj.Zs)
ymax = max(LObj.Zs)
vmin = -1
vmax =  1

pt.figure()
cmap = pt.GetColormap('rainbow4',revesed=True)
im = pt.imshow(data, interpolation='nearest', origin='lower',cmap=cmap, extent=(xmin,xmax,ymin,ymax),aspect='auto',vmin=vmin,vmax=vmax)


xlabel = r"$\rm{Age}\,[\rm{Gyr}]$"
ylabel = r"$\rm{[Z/H]}$"
zlabel = r"$\rm{log10 Luminosity\,[\rm{L_{\odot}/M_{\odot}}]}$"
# plot axis
pt.SetAxis(opt.xmin,opt.xmax,opt.ymin,opt.ymax)
pt.xlabel(xlabel,fontsize=pt.labelfont)
pt.ylabel(ylabel,fontsize=pt.labelfont)
cb = pt.colorbar()
cb.set_label(zlabel) 

#############################
# plot intra/extrapolated matrix
#############################

LObj.CreateInterpolator()
LObj.Extrapolate2DMatrix()

data = log10(LObj.MatLv)

xmin = min(LObj.Ages)
xmax = max(LObj.Ages)
ymin = min(LObj.Zs)
ymax = max(LObj.Zs)
vmin = -1
vmax =  1

pt.figure()
cmap = pt.GetColormap('rainbow4',revesed=True)
im = pt.imshow(data, interpolation='nearest', origin='lower',cmap=cmap, extent=(xmin,xmax,ymin,ymax),aspect='auto',vmin=vmin,vmax=vmax)


xlabel = r"$\rm{Age}\,[\rm{Gyr}]$"
ylabel = r"$\rm{[Z/H]}$"
zlabel = r"$\rm{log10 Luminosity\,[\rm{L_{\odot}/M_{\odot}}]}$"
# plot axis
pt.SetAxis(opt.xmin,opt.xmax,opt.ymin,opt.ymax)
pt.xlabel(xlabel,fontsize=pt.labelfont)
pt.ylabel(ylabel,fontsize=pt.labelfont)
cb = pt.colorbar()
cb.set_label(zlabel) 


#############################
# plot points on grid
#############################

Zs    = scipy.linspace(-10,2,100)
Ages  = scipy.linspace(0,20,100)


Zs,Ages = meshgrid(Zs,Ages)
Zs   = ravel(Zs)
Ages = ravel(Ages)

vals = log10(LObj.Luminosities(Zs,Ages))

pt.figure()
cmap = pt.GetColormap('rainbow4',revesed=True)
pt.scatter(Ages,Zs,c=vals,vmin=vmin,vmax=vmax,marker='o',s=10,linewidths=0,cmap=cmap)


xlabel = r"$\rm{Age}\,[\rm{Gyr}]$"
ylabel = r"$\rm{[Z/H]}$"
zlabel = r"$\rm{log10 Luminosity\,[\rm{L_{\odot}/M_{\odot}}]}$"
# plot axis
pt.SetAxis(opt.xmin,opt.xmax,opt.ymin,opt.ymax)
pt.xlabel(xlabel,fontsize=pt.labelfont)
pt.ylabel(ylabel,fontsize=pt.labelfont)
cb = pt.colorbar()
cb.set_label(zlabel) 



#############################
# plot points on grid (till luminosites)
#############################

Zs    = scipy.linspace(-10,2,100)
Ages  = scipy.linspace(0,20,100)


Zs,Ages = meshgrid(Zs,Ages)
Zs   = ravel(Zs)
Ages = ravel(Ages)

vals = log10(LumObjectTill.Luminosities(Zs,Ages))

pt.figure()
cmap = pt.GetColormap('rainbow4',revesed=True)
pt.scatter(Ages,Zs,c=vals,vmin=vmin,vmax=vmax,marker='o',s=10,linewidths=0,cmap=cmap)


xlabel = r"$\rm{Age}\,[\rm{Gyr}]$"
ylabel = r"$\rm{[Z/H]}$"
zlabel = r"$\rm{log10 Luminosity\,[\rm{L_{\odot}/M_{\odot}}]}$"
# plot axis
pt.SetAxis(opt.xmin,opt.xmax,opt.ymin,opt.ymax)
pt.xlabel(xlabel,fontsize=pt.labelfont)
pt.ylabel(ylabel,fontsize=pt.labelfont)
cb = pt.colorbar()
cb.set_label(zlabel) 



pt.show()
