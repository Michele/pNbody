#!/usr/bin/env python

'''
 @package   pNbody
 @file      write_pos.py
 @brief     Write example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import ic
import sys
from numpy import *
from pNbody import mpi
from pNbody import io


nbody = 1000/mpi.NTask
#pos = random.random((nbody,3)).astype(float32)
pos = mpi.ThisTask*ones((nbody,3)).astype(float32)



######################
# write pos
######################

def OpenFile(filename,pio='no'):
  """
  Open a file:
  
  Parameters:
  ===========
  
  pio : "multi" or "single" or "no"
      if "multi",  a file is written by each task
      if "single", each task write into the same file
      if "no",     each task send its data to the master which write the file
  
  """
  if   pio=="single":
    f = mpi.MPI.File.Open(mpi.comm,filename,amode=mpi.MPI.MODE_CREATE+mpi.MPI.MODE_RDWR)
  elif pio=="multi":
    filename = "%s.%d"%(filename,ThisTask)
    f = mpi.MPI.File.Open(mpi.comm_self,filename,amode=mpiMPI.MODE_CREATE+mpi.MPI.MODE_RDWR)
  else:
    print "not implemented yet..."
    sys.exit()
  
  return f  
  
  

def WriteFortranBlockHeader(f,nbytes,x64=False,byteorder=sys.byteorder):
  """
  Write a number of bytes in a 4 (x32) or 8 (x64) bytes format.
  
  Parameters
  ----------
  f : file
      an open file
  nbytes : an integer
      the number of bytes of the following block
  x64 : True or False
      True if the header will be coded in 64 bytes.
  byteorder : "little" or "big"
      The endianness of the block    
  
  """
  if sys.byteorder != byteorder:
    nbytes.byteswap(True)

  if x64:
    f.Write(int64(nbytes).tostring())
  else:
    f.Write(int32(nbytes).tostring())




def WriteArray(f,data,add_header=False,x64=False,byteorder=sys.byteorder):
  """
  write an array 
  f : file
      an open file
  data : data
      a numpy array
  """
  
  header_size=0
  
  if add_header:
    if x64:
      header_size=8
    else:
      header_size=4  
    
    
  if add_header:
    # compute the total size of the array
    nbytes_tot = sum(mpi.mpi_allgather(data.nbytes))
    
    if mpi.mpi_IsMaster():
      WriteFortranBlockHeader(f,nbytes_tot)      
  
  
  if sys.byteorder != byteorder:
    data.byteswap(True)
  
  f.Set_view(header_size+data.nbytes*mpi.mpi_Rank())  
  f.Write(data.tostring())

  if add_header:
    if mpi.mpi_IsMaster():
      WriteFortranBlockHeader(f,nbytes_tot) 





################################################################
# main
################################################################

print "ThisTaks=%d"%mpi.ThisTask


#####################
# write into file

filename = "pos.dat"

f = OpenFile(filename,pio='single')
WriteArray(f,pos,add_header=True)
f.Close()



#####################
# write into file

# - here, we need to split different types of particles
# - we need to check whats happend in case of a single proc
# - we need to check wats happend in case mpi4py is not present





#####################
# check


if mpi.mpi_IsMaster():

  f = open(filename)
  pos = io.readblock(f,float32)
  f.close()

  print pos











