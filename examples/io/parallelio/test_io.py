#!/usr/bin/env python

'''
 @package   pNbody
 @file      test_io.py
 @brief     test io
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import sys,string,types

from numpy import *

from pNbody import mpi
from pNbody import ic

import mpiio


pio = "no"
piomode = "mpiio"










#################################################
# main
#################################################

filename = 'test.dat'


if pio == "yes":
  filename = "%s.%d"%(filename,mpi.mpi_ThisTask())

# create data
nbody = 1000/mpi.NTask
data = mpi.ThisTask*ones((nbody,3)).astype(float32)


# data with no header
'''
#ok
f = OpenFile(filename,'w')
WriteArray(f,data)
CloseFile(f)
'''

# data with header

#ok
f = OpenFile(filename,'w')
WriteArray(f,data,add_header=True)
CloseFile(f)







