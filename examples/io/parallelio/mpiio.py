'''
 @package   pNbody
 @file      mpiio.py
 @brief     MPI IO example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import mpi
import sys,string,types
from numpy import *


pio = "no"
piomode = "mpiio"



def OpenFile(filename,mode='r'):
  '''
  here, the filename is already determined
  '''  
  
  
  if string.find(mode,'w') != -1:
    mpiiomode = mpi.MPI.MODE_CREATE+mpi.MPI.MODE_RDWR
  
  if string.find(mode,'r') != -1:
    mpiiomode = mpi.MPI.MODE_RDONLY
    
  
  ##################
  # multiple files
  ##################
  if   pio == "yes" and piomode == 'mpiio':
    f = mpi.MPI.File.Open(mpi.comm_self,filename,amode=mpiiomode)
    
    
  elif pio == "yes" and piomode == 'normal':
    f = open(filename,mode)
  
  
  ##################
  # single file
  ##################  
  elif pio == "no"  and piomode == "mpiio":
    f = mpi.MPI.File.Open(mpi.comm,filename,amode=mpiiomode)
  
  
  elif pio == "no"  and piomode == "normal":
    if mpi.mpi_IsMaster():
      f = open(filename,mode)
    else:
      f = None
    
  return f
  

def CloseFile(f):
  '''
  Close a file
  '''
  
  if type(f)==types.FileType:
    f.close()
  elif type(f)==mpi.MPI.File :
    f.Close()
  else:
    pass  
    
    
     
def WriteBlockHeader(f,nbytes,x64=False,byteorder=sys.byteorder):
  """
  Write a number of bytes in a 4 (x32) or 8 (x64) bytes format.
  
  Parameters
  ----------
  f : file
      an open file
  nbytes : an integer
      the number of bytes of the following block
  x64 : True or False
      True if the header will be coded in 64 bytes.
  byteorder : "little" or "big"
      The endianness of the block    
  
  """
  if sys.byteorder != byteorder:
    nbytes.byteswap(True)

  if x64:
    data = int64(nbytes).tostring()
  else:
    data = int32(nbytes).tostring()




  ##################
  # multiple files
  ##################
  if   pio == "yes" and piomode == 'mpiio':
    f.Set_view(0)
    f.Write(data)      
    
  elif pio == "yes" and piomode == 'normal':
    f.write(data)
  
  
  ##################
  # single file
  ##################  
  elif pio == "no"  and piomode == "mpiio":
    if mpi.mpi_IsMaster():
      #f.Seek_shared()
      print f.Get_position_shared()
      f.Write(data)  
  
  elif pio == "no"  and piomode == "normal":
    if mpi.mpi_IsMaster():
      f.write(data)











def WriteArray(f,data,add_header=False,x64=False,byteorder=sys.byteorder):
  """
  write an array 
  f : file
      an open file
  data : data
      a numpy array
  """
  
  header_size=0
    


  if sys.byteorder != byteorder:
    data.byteswap(True)

    
  if add_header:
  
    if x64:
      header_size=8
    else:
      header_size=4    
  
    # compute the total size of the array
    nbytes_tot = sum(mpi.mpi_allgather(data.nbytes))
    # and write the block
    WriteBlockHeader(f,nbytes_tot)      
  
    
    
  ##################
  # multiple files
  ##################
  if   pio == "yes" and piomode == 'mpiio':
    f.Set_view(0)  
    f.Write(data.tostring())      
    
  elif pio == "yes" and piomode == 'normal':
    f.write(data.tostring())
  
  
  ##################
  # single file
  ##################  
  elif pio == "no"  and piomode == "mpiio":
    f.Set_view(header_size+data.nbytes*mpi.mpi_Rank())  
    f.Write(data.tostring())  
  
  elif pio == "no"  and piomode == "normal":

    if mpi.mpi_IsMaster():
      f.write(data.tostring())
      
      for Task in range(1,mpi.mpi_NTask()):
    	sdata = mpi.comm.recv(source = Task)
    	f.write(sdata.tostring())    
    else:
      mpi.comm.send(data, dest = 0)      



  if add_header:
    WriteBlockHeader(f,nbytes_tot) 



def set_pio(pio_loc=pio,piomode_loc=piomode):
  global pio,piomode
  pio=pio_loc
  piomode=piomode_loc
  
  
def info():
  print "pio     = %s"%pio
  print "piomode = %s"%piomode

