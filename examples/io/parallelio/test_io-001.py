#!/usr/bin/env python

'''
 @package   pNbody
 @file      test_io-001.py
 @brief     Test IO
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import os,sys,string,types

from numpy import *

from pNbody import mpi
from pNbody import ic

import mpiio




#################################################
# main
#################################################


mpiio.set_pio("no",'mpiio')
#mpiio.info()
filename = 'test3.dat'


if mpiio.pio == "yes":
  filename = "%s.%d"%(filename,mpi.mpi_ThisTask())

# create data
nbody = 1000/mpi.NTask
data = mpi.ThisTask*ones((nbody,3)).astype(float32)


# data with no header
f = mpiio.OpenFile(filename,'w')
mpiio.WriteArray(f,data,add_header=True)
mpiio.CloseFile(f)





sys.exit()





#########################
# multiple files, mpiio
#########################

mpiio.set_pio("yes",'mpiio')
#mpiio.info()
filename = 'test1.dat'


if mpiio.pio == "yes":
  filename = "%s.%d"%(filename,mpi.mpi_ThisTask())

# create data
nbody = 1000/mpi.NTask
data = mpi.ThisTask*ones((nbody,3)).astype(float32)


# data with no header
f = mpiio.OpenFile(filename,'w')
mpiio.WriteArray(f,data,add_header=True)
mpiio.CloseFile(f)


###################
# multiple files
###################

mpiio.set_pio("yes",'normal')
#mpiio.info()
filename = 'test2.dat'


if mpiio.pio == "yes":
  filename = "%s.%d"%(filename,mpi.mpi_ThisTask())

# create data
nbody = 1000/mpi.NTask
data = mpi.ThisTask*ones((nbody,3)).astype(float32)


# data with no header
f = mpiio.OpenFile(filename,'w')
mpiio.WriteArray(f,data,add_header=True)
mpiio.CloseFile(f)


#########################
# single files, mpiio
#########################

mpiio.set_pio("no",'mpiio')
#mpiio.info()
filename = 'test3.dat'


if mpiio.pio == "yes":
  filename = "%s.%d"%(filename,mpi.mpi_ThisTask())

# create data
nbody = 1000/mpi.NTask
data = mpi.ThisTask*ones((nbody,3)).astype(float32)


# data with no header
f = mpiio.OpenFile(filename,'w')
mpiio.WriteArray(f,data,add_header=True)
mpiio.CloseFile(f)


###################
# single files
###################

mpiio.set_pio("no",'normal')
#mpiio.info()
filename = 'test4.dat'


if mpiio.pio == "yes":
  filename = "%s.%d"%(filename,mpi.mpi_ThisTask())

# create data
nbody = 1000/mpi.NTask
data = mpi.ThisTask*ones((nbody,3)).astype(float32)


# data with no header
f = mpiio.OpenFile(filename,'w')
mpiio.WriteArray(f,data,add_header=True)
mpiio.CloseFile(f)



######################################
# check 
######################################

if mpi.mpi_IsMaster():
  line = ""
  for i in range(mpi.mpi_NTask()):
    line = line + "test1.dat.%d "%i


  os.system("cat %s > test1.dat"%line)


  line = ""
  for i in range(mpi.mpi_NTask()):
    line = line + "test2.dat.%d "%i
      
  os.system("cat %s > test2.dat"%line)


  os.system("diff test1.dat test2.dat")
  os.system("diff test1.dat test3.dat")
  os.system("diff test1.dat test4.dat")

  os.system("rm test*.dat* ")










