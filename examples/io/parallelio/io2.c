/* example of parallel Unix write into separate files */ 
#include "mpi.h" 
#include <stdio.h> 
#define BUFSIZE 100 
 
int main(int argc, char *argv[]) 
{ 
    int i, myrank, buf[BUFSIZE]; 
    char filename[128]; 
    FILE *myfile; 
 
    MPI_Init(&argc, &argv); 
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank); 
    for (i=0; i<BUFSIZE; i++) 
	buf[i] = myrank * BUFSIZE + i; 
    sprintf(filename, "testfile.%d", myrank); 
    myfile = fopen(filename, "w"); 
    fwrite(buf, sizeof(int), BUFSIZE, myfile); 
    fclose(myfile); 
    MPI_Finalize(); 
    return 0; 
} 
