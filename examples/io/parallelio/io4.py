#!/usr/bin/env python

'''
 @package   pNbody
 @file      io4.py
 @brief     IO example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from mpi4py import MPI
from numpy import *
import time 

from optparse import OptionParser

def parse_options():
  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser.add_option("-o","--outputfile",
  		    action="store", 
		    type="string",
  		    dest="outputfile",
		    default = "pytest-io4.dat",		    
  		    help="output file name")  	
		    
  parser.add_option("--DataSize",
  		    action="store", 
		    type="int",
  		    dest="DataSize",
		    default = 10,		    
  		    help="Data size in Mb")  			    
		    	    		    		    

  (options, args) = parser.parse_args()

  files = args
  return files,options

files,opt = parse_options()

comm = MPI.COMM_WORLD
ThisTask = comm.Get_rank()
NTask	 = comm.Get_size()
Procnm   = MPI.Get_processor_name()
Rank	 = ThisTask


print "ThisTaks=%d"%ThisTask

# set data
DataSize = 1024*1024 * opt.DataSize # 10 Mega
Nnumbers = int(DataSize/8)
random.seed(ThisTask)
data = random.random(Nnumbers)
nbytes = data.nbytes

if ThisTask==0:
  t1 = time.time()

filename = opt.outputfile
myfile = MPI.File.Open(comm,filename,amode=MPI.MODE_CREATE+MPI.MODE_RDWR)
myfile.Set_view(data.nbytes*Rank)
myfile.Write(data.tostring())
myfile.Close()

if ThisTask==0:
  t2 = time.time()
  print "elapsed time : ",t2-t1



# check
'''
if ThisTask==0:

  f = open(filename)
  data = fromstring(f.read(nbytes*NTask),int32)
  
  print data
'''


