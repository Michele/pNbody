#!/usr/bin/env python
'''
 @package   pNbody
 @file      Icmap.py
 @brief     Color palette
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import pylab as pl
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Slider, Button, RadioButtons
import matplotlib.cm as cm

N_colors = 256

fig = plt.figure(figsize=(6,18))
ax = fig.add_subplot(111)
fig.subplots_adjust(left=0.25, bottom=0.75)

sr0_init_gas = 0.* N_colors
sg0_init_gas = 0.* N_colors
sb0_init_gas = 0.* N_colors

sr1_init_gas = 0.* N_colors
sg1_init_gas = 0.* N_colors
sb1_init_gas = 0.5* N_colors

sr2_init_gas = 0.85* N_colors
sg2_init_gas = 0.85* N_colors
sb2_init_gas = 0.95* N_colors


sr0_init_stars = 0.* N_colors
sg0_init_stars = 0.* N_colors
sb0_init_stars = 0.* N_colors

sr1_init_stars = 0.99* N_colors
sg1_init_stars = 0.84* N_colors
sb1_init_stars = 0.* N_colors

sr2_init_stars = 0.98* N_colors
sg2_init_stars = 0.98* N_colors
sb2_init_stars = 0.82* N_colors

sX0_init=0.5

sr0_init =  sr0_init_gas
sg0_init =  sg0_init_gas
sb0_init =  sb0_init_gas

sr1_init =  sr1_init_gas
sg1_init =  sg1_init_gas
sb1_init =  sb1_init_gas

sr2_init =  sr2_init_gas
sg2_init =  sg2_init_gas
sb2_init =  sb2_init_gas


cdict = {'red': ((0.0, sr0_init/N_colors, sr0_init/N_colors),
                 (sX0_init, sr1_init/N_colors, sr1_init/N_colors),
                 (1.0, sr2_init/N_colors, sr2_init/N_colors)),
         'green': ((0.0, sg0_init/N_colors, sg0_init/N_colors),
                   (sX0_init, sg1_init/N_colors, sg1_init/N_colors),
                   (1.0, sg2_init/N_colors, sg2_init/N_colors)),
         'blue': ((0.0, sb0_init/N_colors, sb0_init/N_colors),
                  (sX0_init, sb1_init/N_colors, sb1_init/N_colors),
                  (1.0, sb2_init/N_colors, sb2_init/N_colors))}




my_cmap = pl.matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,N_colors)

delta = 0.01
x = y = z = np.arange(0, 1.0, delta)
X, Y = np.meshgrid(x, y)
Z = Y

im1 = ax.imshow(Z, cmap=my_cmap, origin='lower', extent=[0,1,0,1])
pl.xticks(label='')
#fig.colorbar(im1)

axcolor = 'lightgoldenrodyellow'

axr0 = fig.add_axes([0.25, 0.2, 0.65, 0.03], axisbg=axcolor)
axg0 = fig.add_axes([0.25, 0.15, 0.65, 0.03], axisbg=axcolor)
axb0 = fig.add_axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)

axr1 = fig.add_axes([0.25, 0.4, 0.65, 0.03], axisbg=axcolor)
axg1 = fig.add_axes([0.25, 0.35, 0.65, 0.03], axisbg=axcolor)
axb1 = fig.add_axes([0.25, 0.3, 0.65, 0.03], axisbg=axcolor)

axr2 = fig.add_axes([0.25, 0.6, 0.65, 0.03], axisbg=axcolor)
axg2 = fig.add_axes([0.25, 0.55, 0.65, 0.03], axisbg=axcolor)
axb2 = fig.add_axes([0.25, 0.5, 0.65, 0.03], axisbg=axcolor)

axX0 = fig.add_axes([0.25, 0.67, 0.65, 0.03], axisbg=axcolor)

sr0 = Slider(axr0, 'r0', 0, N_colors, valinit=sr0_init)
sg0 = Slider(axg0, 'g0', 0, N_colors, valinit=sg0_init)
sb0 = Slider(axb0, 'b0', 0, N_colors, valinit=sb0_init)

sr1 = Slider(axr1, 'r1', 0, N_colors, valinit=sr1_init)
sg1 = Slider(axg1, 'g1', 0, N_colors, valinit=sg1_init)
sb1 = Slider(axb1, 'b1', 0, N_colors, valinit=sb1_init)

sr2 = Slider(axr2, 'r2', 0, N_colors, valinit=sr2_init)
sg2 = Slider(axg2, 'g2', 0, N_colors, valinit=sg2_init)
sb2 = Slider(axb2, 'b2', 0, N_colors, valinit=sb2_init)

sX0 = Slider(axX0, 'X0', 0, 1, valinit=sX0_init)

def update(val):
    
    cdict = {'red': ((0.0, sr0.val/N_colors, sr0.val/N_colors),
                 (sX0.val, sr1.val/N_colors, sr1.val/N_colors),
                 (1.0, sr2.val/N_colors, sr2.val/N_colors)),
         'green': ((0.0, sg0.val/N_colors, sg0.val/N_colors),
                   (sX0.val, sg1.val/N_colors, sg1.val/N_colors),
                   (1.0, sg2.val/N_colors, sg2.val/N_colors)),
         'blue': ((0.0, sb0.val/N_colors, sb0.val/N_colors),
                  (sX0.val, sb1.val/N_colors, sb1.val/N_colors),
                  (1.0, sb2.val/N_colors, sb2.val/N_colors))}
    my_cmap = pl.matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,N_colors)
    im1.set_cmap(my_cmap)
    fig.canvas.draw()
    

sr0.on_changed(update)
sg0.on_changed(update)
sb0.on_changed(update)

sr1.on_changed(update)
sg1.on_changed(update)
sb1.on_changed(update)

sr2.on_changed(update)
sg2.on_changed(update)
sb2.on_changed(update)

sX0.on_changed(update)

saveax = fig.add_axes([0.55, 0.025, 0.35, 0.03])
button_save = Button(saveax, 'Save map', color='grey', hovercolor='0.975')
def savemap(event):
    cdict = {'red': ((0.0, sr0.val/N_colors, sr0.val/N_colors),
                 (sX0.val, sr1.val/N_colors, sr1.val/N_colors),
                 (1.0, sr2.val/N_colors, sr2.val/N_colors)),
         'green': ((0.0, sg0.val/N_colors, sg0.val/N_colors),
                   (sX0.val, sg1.val/N_colors, sg1.val/N_colors),
                   (1.0, sg2.val/N_colors, sg2.val/N_colors)),
         'blue': ((0.0, sb0.val/N_colors, sb0.val/N_colors),
                  (sX0.val, sb1.val/N_colors, sb1.val/N_colors),
                  (1.0, sb2.val/N_colors, sb2.val/N_colors))}

    my_cmap = pl.matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,N_colors)
    cmap = cm.get_cmap(my_cmap, N_colors) 
    map_array = cmap(np.arange(N_colors)) * N_colors
#    np.savetxt('map.dat', map_array[:,:-1], fmt='%i')
    
    f = open('map.dat','w')
    f.write("# icmap\n")
    np.savetxt(f, map_array[:,:-1], fmt='%i')
    f.close()
    
    print 'map saved in map.dat'

   

button_save.on_clicked(savemap)


importax = fig.add_axes([0.05, 0.025, 0.35, 0.03])
button_import = Button(importax, 'Import map', color='grey', hovercolor='0.975')
def importmap(event):
    print 'import map?'
    mapfile = raw_input(">")
    maparray = np.loadtxt(mapfile,skiprows = 0)
    
    #plt.figure()
    
    diffmap1 = np.diff(maparray[:,:], axis=0)
    diffmap2 = np.diff(diffmap1[:,:], axis=0)
    diffmap2 = np.abs(diffmap2)
    
    #plt.plot(maparray[:,0],'r-')
    #plt.plot(maparray[:,1],'g-')
    #plt.plot(maparray[:,2],'b-')
    
    #plt.plot(diffmap2[:,0],'r-')
    #plt.plot(diffmap2[:,1],'g-')
    #plt.plot(diffmap2[:,2],'b-')
    
    fX0_r = np.amax(diffmap2[:,0])
    fX0_g = np.amax(diffmap2[:,1])
    fX0_b = np.amax(diffmap2[:,2])
    
    fX0 = max(fX0_r, fX0_g, fX0_b)
    print 'fX0',fX0
    X0_imp = np.where(diffmap2 == fX0)[0][0]
    print X0_imp 
    
    #X0_r = np.argmax(diffmap2[:,0])
    #X0_g = np.argmax(diffmap2[:,1])
    #X0_b = np.argmax(diffmap2[:,2])
    
    #X0_imp = (X0_r + X0_g + X0_b)/3. #/ float(N_colors)
  
    #print maparray
  
    sr0_imp =  maparray[0, 0]
    sg0_imp =  maparray[0, 1]
    sb0_imp =  maparray[0, 2]

    sr1_imp =  maparray[X0_imp, 0]
    sg1_imp =  maparray[X0_imp, 1]
    sb1_imp =  maparray[X0_imp, 2]

    sr2_imp =  maparray[-1, 0]
    sg2_imp =  maparray[-1, 1]
    sb2_imp =  maparray[-1, 2]
  
    X0_imp = X0_imp / float(N_colors)

    print X0_imp 
    print sr0_imp, sg0_imp, sb0_imp
    print sr1_imp, sg1_imp, sb1_imp
    print sr2_imp, sg2_imp, sb2_imp
    
    cdict = {'red': ((0.0, sr0_imp/N_colors, sr0_imp/N_colors),
                 (X0_imp, sr1_imp/N_colors, sr1_imp/N_colors),
                 (1.0, sr2_imp/N_colors, sr2_imp/N_colors)),
         'green': ((0.0, sg0_imp/N_colors, sg0_imp/N_colors),
                   (X0_imp, sg1_imp/N_colors, sg1_imp/N_colors),
                   (1.0, sg2_imp/N_colors, sg2_imp/N_colors)),
         'blue': ((0.0, sb0_imp/N_colors, sb0_imp/N_colors),
                  (X0_imp, sb1_imp/N_colors, sb1_imp/N_colors),
                  (1.0, sb2_imp/N_colors, sb2_imp/N_colors))}


    
    #plt.plot(fx0,'k-')
    #plt.show()
    sr0.set_val(sr0_imp)
    sg0.set_val(sg0_imp)
    sb0.set_val(sb0_imp)
        
    sr1.set_val(sr1_imp)
    sg1.set_val(sg1_imp)
    sb1.set_val(sb1_imp)
    
    sr2.set_val(sr2_imp)
    sg2.set_val(sg2_imp)
    sb2.set_val(sb2_imp)

    sX0.set_val(X0_imp)
    my_cmap = pl.matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,N_colors)
    im1.set_cmap(my_cmap)
    fig.canvas.draw()
    
    
button_import.on_clicked(importmap)


rax = fig.add_axes([0.025, 0.8, 0.15, 0.05], axisbg=axcolor)
radio = RadioButtons(rax, ('gas', 'stars'), active=0)
def colorfunc(label):
    print label
    
    if (label=='gas'):
        
        sr0_init =  sr0_init_gas
        sg0_init =  sg0_init_gas
        sb0_init =  sb0_init_gas

        sr1_init =  sr1_init_gas
        sg1_init =  sg1_init_gas
        sb1_init =  sb1_init_gas

        sr2_init =  sr2_init_gas
        sg2_init =  sg2_init_gas
        sb2_init =  sb2_init_gas

    if (label=='stars'):
        
        sr0_init =  sr0_init_stars
        sg0_init =  sg0_init_stars
        sb0_init =  sb0_init_stars

        sr1_init =  sr1_init_stars
        sg1_init =  sg1_init_stars
        sb1_init =  sb1_init_stars

        sr2_init =  sr2_init_stars
        sg2_init =  sg2_init_stars
        sb2_init =  sb2_init_stars

    cdict = {'red': ((0.0, sr0_init/N_colors, sr0_init/N_colors),
                 (sX0_init, sr1_init/N_colors, sr1_init/N_colors),
                 (1.0, sr2_init/N_colors, sr2_init/N_colors)),
         'green': ((0.0, sg0_init/N_colors, sg0_init/N_colors),
                   (sX0_init, sg1_init/N_colors, sg1_init/N_colors),
                   (1.0, sg2_init/N_colors, sg2_init/N_colors)),
         'blue': ((0.0, sb0_init/N_colors, sb0_init/N_colors),
                  (sX0_init, sb1_init/N_colors, sb1_init/N_colors),
                  (1.0, sb2_init/N_colors, sb2_init/N_colors))}
    
 
    sr0.set_val(sr0_init)
    sg0.set_val(sg0_init)
    sb0.set_val(sb0_init)
        
    sr1.set_val(sr1_init)
    sg1.set_val(sg1_init)
    sb1.set_val(sb1_init)
    
    sr2.set_val(sr2_init)
    sg2.set_val(sg2_init)
    sb2.set_val(sb2_init)

    sX0.set_val(sX0_init)
    my_cmap = pl.matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,N_colors)
    im1.set_cmap(my_cmap)
    fig.canvas.draw()


radio.on_clicked(colorfunc)


plt.show()
