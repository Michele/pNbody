#!/usr/bin/env python
'''
 @package   pNbody
 @file      png2palette-yr.py
 @brief     Transform a png image to a palette
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


import sys
import Mtools as mt
import Ptools as pt
from numpy import *

n = -150

file = sys.argv[1]

img = mt.img_open(file)


r,g,b = img.split()

sizex,sizey= img.size

# get
ra = array(r.getdata())
ga = array(g.getdata())
ba = array(b.getdata())


#ra.shape=img.size
#ra = ra.transpose()
#ra = ra.mean(axis=1)
ra = ra[:n]

#ga.shape=img.size
#ga = ga.transpose()
#ga = ga.mean(axis=1)
ga = ga[:n]

#ba.shape=img.size
#ba = ba.transpose()
#ba = ba.mean(axis=1)
ba = ba[:n]

# plot


dx = float(255.8)/len(ra)
x = arange(0,255+dx,dx) 

print len(x)
print len(ra)

pt.plot(x,ra,c='r')
pt.plot(x,ga,c='g')
pt.plot(x,ba,c='b')


# interpolate

from scipy import interpolate
sfact = 1.
k = 2
n=len(x)
s1 = n-sqrt(2*n)
s2 = n+sqrt(2*n)
s = s1*sfact
  
tckr   = interpolate.fitpack.splrep(x,ra,s=s,k=k)
tckg   = interpolate.fitpack.splrep(x,ga,s=s,k=k)
tckb   = interpolate.fitpack.splrep(x,ba,s=s,k=k)

i = arange(0,256)
r = interpolate.fitpack.splev(i,tckr)
g = interpolate.fitpack.splev(i,tckg)
b = interpolate.fitpack.splev(i,tckb)



pt.plot(i,r,c='r')
pt.plot(i,g,c='g')
pt.plot(i,b,c='b')
pt.show()


# write palette
name = 'gnedin1'
f = open(name,'w')
f.write("# table rgb (%s)\n"%name) 

for i in range(256):
 f.write("%3d %3d %3d\n"%(r[255-i],g[255-i],b[255-i]))
 
f.close()













