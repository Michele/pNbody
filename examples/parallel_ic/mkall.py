#!/usr/bin/env python
'''
 @package   pNbody
 @file      mkall.py
 @brief     Generate initial condition in parallel
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import ic
from numpy import *


write=True
N=4000
Npcpu=1000

##########################################
# box
##########################################
if mpi.mpi_IsMaster():
  print "box"

# here, we want n particles in total
n = N
nb = ic.box(n,1,1,1,name='box1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.box(n,1,1,1,name='box2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()




##########################################
# homodisk
##########################################
if mpi.mpi_IsMaster():
  print "homodisk"

# here, we want n particles in total
n = N
nb = ic.homodisk(n,1,1,0.1,name='homodisk1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.homodisk(n,1,1,0.1,name='homodisk2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()



##########################################
# homosphere
##########################################
if mpi.mpi_IsMaster():
  print "homosphere"

# here, we want n particles in total
n = N
nb = ic.homosphere(n,1,1,1,name='homosphere1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.homosphere(n,1,1,1,name='homosphere2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


##########################################
# shell
##########################################
if mpi.mpi_IsMaster():
  print "shell"

# here, we want n particles in total
n = N
nb = ic.shell(n,1,name='shell1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,2])
nb = ic.shell(n,1,name='shell2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

##########################################
# plummer
##########################################
if mpi.mpi_IsMaster():
  print "plummer"

# here, we want n particles in total
n = N
nb = ic.plummer(n,1,1,1,0.1,1,name='plummer1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.plummer(n,1,1,1,0.1,1,name='plummer2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()



##########################################
# kuzmin
##########################################
if mpi.mpi_IsMaster():
  print "kuzmin"

# here, we want n particles in total
n = N
nb = ic.kuzmin(n,1,0.1,name='kuzmin1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.kuzmin(n,1,0.1,name='kuzmin2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()



##########################################
# expd
##########################################
if mpi.mpi_IsMaster():
  print "expd"

# here, we want n particles in total
n = N
nb = ic.expd(n,3,0.3,10,1,name='expd1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


##########################################
# miyamoto_nagai
##########################################
if mpi.mpi_IsMaster():
  print "miyamoto_nagai"

# here, we want n particles in total
n = N
nb = ic.miyamoto_nagai(n,3,0.3,10,1,name='miyamoto1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


##########################################
# generic_alpha
##########################################
if mpi.mpi_IsMaster():
  print "generic_alpha"

# here, we want n particles in total
n = N
nb = ic.generic_alpha(n,1.5,0.1,10,name='generic_alpha1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


##########################################
# dl2
##########################################
if mpi.mpi_IsMaster():
  print "dl2"

# here, we want n particles in total
n = N
nb = ic.dl2(n,1,1,1,0.1,1,name='dl21.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.dl2(n,1,1,1,0.1,1,name='dl22.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()



##########################################
# isothm
##########################################
if mpi.mpi_IsMaster():
  print "isothm"

# here, we want n particles in total
n = N
nb = ic.isothm(n,0.1,1,name='isothm1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.isothm(n,0.1,1,name='isothm2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()



##########################################
# pisothm
##########################################
if mpi.mpi_IsMaster():
  print "pisothm"

# here, we want n particles in total
n = N
nb = ic.pisothm(n,0.1,1,name='pisothm1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.pisothm(n,0.1,1,name='pisothm2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()



##########################################
# nfw
##########################################
if mpi.mpi_IsMaster():
  print "nfw"

# here, we want n particles in total
n = N
nb = ic.nfw(n,0.1,1,0.01,name='nfw1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.nfw(n,0.1,1,0.01,name='nfw2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()




##########################################
# hernquist
##########################################
if mpi.mpi_IsMaster():
  print "hernquist"

# here, we want n particles in total
n = N
nb = ic.hernquist(n,0.1,1,0.01,name='hernquist1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.hernquist(n,0.1,1,0.01,name='hernquist2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()



##########################################
# burkert
##########################################
if mpi.mpi_IsMaster():
  print "burkert"

# here, we want n particles in total
n = N
nb = ic.burkert(n,0.1,1,0.01,name='burkert1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.burkert(n,0.1,1,0.01,name='burkert2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()




##########################################
# nfwg
##########################################
if mpi.mpi_IsMaster():
  print "nfwg"

# here, we want n particles in total
n = N
nb = ic.nfwg(n,0.1,1,1,0.01,name='nfwg1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.nfwg(n,0.1,1,1,0.01,name='nfwg2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()




##########################################
# generic2c
##########################################
if mpi.mpi_IsMaster():
  print "generic2c"

# here, we want n particles in total
n = N
nb = ic.generic2c(n,0.1,1,3,1,0.01,name='generic2c1.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()

# here, each task compute n particles
n = Npcpu
irand=mpi.mpi_ThisTask()
n = random.random([n,3])
nb = ic.generic2c(n,0.1,1,3,1,0.01,name='generic2c2.dat',ftype='gadget')
if mpi.mpi_IsMaster():
  print nb.nbody_tot,nb.mass_tot
if write:
  nb.write()


