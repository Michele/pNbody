'''
 @package   pNbody
 @file      filmparam.py
 @brief     Movie Parameters
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

# number of horizontal and vertical subfilms
nh = 1  # horizontal								     
nw = 2	# vertical
							     
# size of subfilms						     
width = 256							     
height = 256
				     
# size of the film						     
numByte = width * nw						     
numLine = height * nh						     						     
# init parameters						     
param = initparams(nh,nw)	 


# params 1
param[1]['view'] = 'xy'
param[1]['mode'] = 'pos'
param[1]['size'] = (35.,35.)


# params 2
param[2]['x0'] = [0,-1e-5,50]
param[2]['xp'] = [0,0,0]
param[2]['size'] = (35.,35.)
param[2]['exec'] = '''nb = nb.selectp(file='sub.txt')'''
param[2]['mode'] = 'vr'
param[2]['filter_name'] = 'convol'
param[2]['mn'] = -0.14
param[2]['mx'] = 0.14
param[2]['cd'] = 0.0002346
