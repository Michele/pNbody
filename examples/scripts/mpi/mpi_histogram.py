'''
 @package   pNbody
 @file      mpi_histogram.py
 @brief     Makes Histogram 
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from numarray import random_array as RandomArray

x = RandomArray.random([10])


binx = arange(0,1,0.25)
h = mpi.mpi_histogram(x,binx)

mpi.mpi_iprint(x)
mpi.mpi_iprint(binx)
mpi.mpi_iprint(h)
