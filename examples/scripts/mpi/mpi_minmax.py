'''
 @package   pNbody
 @file      mpi_minmax.py
 @brief     MPI compute min and max of arrays
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

if mpi.mpi_IsMaster():
  x = arange(0,3)
else:
  x = arange(3,6)
  
mpi.mpi_iprint(x)
mn = mpi.mpi_min(x)   
mx = mpi.mpi_max(x)   

mpi.mpi_iprint("min= %g "%mn)
mpi.mpi_iprint("max= %g "%mx)




