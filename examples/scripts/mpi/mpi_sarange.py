'''
 @package   pNbody
 @file      mpi_sarange.py
 @brief     arange
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

nall = array([[2,3],[5,1]])

x = mpi.mpi_sarange(nall)
mpi.mpi_iprint(x)




