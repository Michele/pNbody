'''
 @package   pNbody
 @file      mpi_Allgather.py
 @brief     Gather data
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

if mpi.mpi_IsMaster():
  x = 1
else:
  x = 2
   
x = mpi.mpi_Allgather(x)
mpi.mpi_iprint(x)




