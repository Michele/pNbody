'''
 @package   pNbody
 @file      mpi_Bcast.py
 @brief     Broadcast messages
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

if mpi.mpi_IsMaster():
  msg = "hello from the master"
else:
  msg = "hello from a slave"  

msg = mpi.mpi_Bcast(0,msg)
mpi.mpi_iprint(msg)


