#!/usr/bin/env python
'''
 @package   pNbody
 @file      split.py
 @brief     Write file in parallel => split
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import sys
from pNbody import *

files = sys.argv[1:]

for file in files:
  nb = Nbody(file,ftype='gadget')
  nb.set_pio('yes')	# enable parallel input/output
  nb.write()
