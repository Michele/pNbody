'''
 @package   pNbody
 @file      mpi_sum.py
 @brief     Sum arrays
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

if mpi.mpi_IsMaster():
  x = arange(0,3)
else:
  x = arange(3,6)
  
mpi.mpi_iprint(x)
s = mpi.mpi_sum(x)   
mpi.mpi_iprint(s)




