'''
 @package   pNbody
 @file      mpi_SendRecv.py
 @brief     Send and Receive messages
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

if mpi.mpi_IsMaster():
  mpi.mpi_Send(123,1)
else:
  x = mpi.mpi_Recv(0)
  msg = '''[1] : get %d from node 0'''%x
  print msg 
   





