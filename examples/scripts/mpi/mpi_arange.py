'''
 @package   pNbody
 @file      mpi_arange.py
 @brief     Arange data
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

x = mpi.mpi_arange(10)
mpi.mpi_iprint(x)




