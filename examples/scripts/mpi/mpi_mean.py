'''
 @package   pNbody
 @file      mpi_mean.py
 @brief     Compute the mean
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

if mpi.mpi_IsMaster():
  x = arange(0,3).astype(Float)
else:
  x = arange(3,6).astype(Float)
  
mpi.mpi_iprint(x)
mean = mpi.mpi_mean(x)   

mpi.mpi_iprint("mean= %g "%mean)




