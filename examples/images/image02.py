#!/usr/bin/env python

'''
 @package   pNbody
 @file      image02.py
 @brief     Image example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *


nb = Nbody('../snap.dat',ftype='gadget')

obs = None
x0 = array([0,0,0]) 
xp = array([10,0,3]) 
alpha = 10*pi/180.
cut = 'no'
persp = 'no'

shape = (512,512)
size  = (50,50)

nb.show(obs=obs,x0=x0,xp=xp,alpha=alpha,cut=cut,persp=persp,shape=shape,size=size)













