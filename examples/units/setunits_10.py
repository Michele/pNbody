#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits_10.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import units,io,ctes
from numpy import *



#set the units
Unit_atom = ctes.PROTONMASS.into(units.cgs)*units.Unit_g
Unit_atom.set_symbol('atom') 
SN_units = units.UnitSystem('local',[units.Unit_cm,Unit_atom,units.Unit_Myr,units.Unit_K])

SN_units = units.UnitSystem('local',[units.Unit_cm,Unit_atom,units.Unit_Myr,units.Unit_K])
cgs_units = units.cgs

E_51=1.


print E_51*cgs_units.convertionFactorTo(SN_units.UnitEnergy) 			* 1e51


