#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits_7.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *

##############################
# define some system of units
##############################

# output system of units (cgs)
cgs_units = units.UnitSystem('cgs',[units.Unit_cm,units.Unit_g,units.Unit_s,units.Unit_K])

# gear system of units
params = {}
params['UnitVelocity_in_cm_per_s']      = 20725573.785998672
params['UnitMass_in_g']                 = 1.989e+43
params['UnitLength_in_cm']              = 3.085678e+21
gear_units = units.Set_SystemUnits_From_Params(params)


##############################
# compute constants
##############################

# compute mu
Xi=0.76
mu = 4./(8.-5.*(1.-Xi))


# get k (boltzmann) in cgs_units
k = ctes.BOLTZMANN.into(cgs_units)

# get mh (hydrogen mass) in cgs_units
mh = ctes.PROTONMASS.into(cgs_units)


##############################
# specific thermal energy
##############################
T=1e4
ui = 3/2.*k/(mu*mh) *T


##############################
# mass
##############################

mi = 5e-08 # gear_units (500Msol)
mi = mi*gear_units.convertionFactorTo(cgs_units.UnitMass)


##############################
# energy
##############################

Nsn = 5
Nngb= 10.

Esn  = Nsn * 1e51 * 0.03 	# already in cgs
Esni = Esn/Nngb			# already in cgs



##############################
# now compute, all in cgs
##############################

ei = mi*ui
eip= ei + Esni

print "ratio",eip/ei



##############################
# now compute, all in gear
##############################

ui  = ui*cgs_units.convertionFactorTo(gear_units.UnitSpecEnergy)
mi  = mi*cgs_units.convertionFactorTo(gear_units.UnitMass)
Esni= Esni*cgs_units.convertionFactorTo(gear_units.UnitEnergy)

print "------------"
print ui
print mi
print Esni

