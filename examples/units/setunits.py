#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *

nb = Nbody("../snap.dat",ftype='gadget')

print ""
nb.set_local_system_of_units(unitparameterfile='./unitsparameters')
print ""
nb.set_local_system_of_units(gadgetparameterfile='./params')
print ""
nb.set_local_system_of_units(UnitLength_in_cm=1,UnitVelocity_in_cm_per_s=1,UnitMass_in_g=1)
print ""
params = {}
params['UnitLength_in_cm']	   = 1.
params['UnitVelocity_in_cm_per_s'] = 1.
params['UnitMass_in_g'] 	   = 1.
nb.set_local_system_of_units(params)
