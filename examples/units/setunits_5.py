#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits_5.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *


# define a new system of unit
params = {}
params['UnitVelocity_in_cm_per_s']	= 100000.0
params['UnitMass_in_g']			= 1.989e+43
params['UnitLength_in_cm']		= 3.085678e+21
system_of_units = units.Set_SystemUnits_From_Params(params)


# get info 
system_of_units.info()



##########################################
# new system
##########################################
print

Unit_acte = units.Units('acte')
Unit_hcte = units.Units('hcte')

Utime= params['UnitLength_in_cm']/ params['UnitVelocity_in_cm_per_s']/ (units.Unit_s)
system_of_units = units.UnitSystem("cosmo",[units.Unit_kpc,  10**10*units.Unit_Msol, Utime])


system_of_units.info()









