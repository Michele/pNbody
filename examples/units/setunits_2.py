#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits_2.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *

nb = Nbody("../snap.dat",ftype='gadget',unitsfile='params')
nb = Nbody("../snap.dat",ftype='gadget',unitsfile='unitsparameters')




