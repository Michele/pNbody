#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits_9.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import units,io,ctes
from numpy import *

gadgetparameterfile="params.dSphCZ"
params = io.read_params(gadgetparameterfile) 

#system_of_units = units.Set_SystemUnits_From_Params(params)
system_of_units = units.Set_SystemUnits_From_File(gadgetparameterfile)



G=ctes.GRAVITY.into(system_of_units)
H = ctes.HUBBLE.into(system_of_units)
HubbleParam = params['HubbleParam']

rhoc = pow(HubbleParam*H,2)*3/(8*pi*G)
print rhoc,rhoc*80,"free of h"

rhoc = pow(H,2)*3/(8*pi*G)
print rhoc,rhoc*80,"in code units, h not used"


H=0.1
G=43000.1

print pow(H,2)*3/(8*pi*G)
