#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits_3.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *


# define a new system of units : kpc,Msol,Myr
system_of_units = units.UnitSystem('local',[units.Unit_kpc,  10**10*units.Unit_Ms, units.Unit_Myr])


# get info 
system_of_units.info()


# trivial conversion factor into kpc,10**10Msol,Myr 
print system_of_units.convertionFactorTo(units.Unit_kpc)
print system_of_units.convertionFactorTo(10**10*units.Unit_Ms)
print system_of_units.convertionFactorTo(units.Unit_Myr)

# length
# 
print system_of_units.convertionFactorTo(units.Unit_cm)
print system_of_units.convertionFactorTo(units.Unit_m)
print system_of_units.convertionFactorTo(units.Unit_km)

# velocity
print system_of_units.convertionFactorTo(units.Unit_km/units.Unit_s)
print system_of_units.convertionFactorTo(units.Unit_kms)


# energy
print system_of_units.convertionFactorTo(units.Unit_kg*units.Unit_ms**2)
print system_of_units.convertionFactorTo(units.Unit_J)

# density
print system_of_units.convertionFactorTo(units.Unit_g/units.Unit_cm**3)


