#!/usr/bin/env python
'''
 @package   pNbody
 @file      setunits_8.py
 @brief     Units conversion
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *

'''
convert density :

gear -> g/cm^3 -> atom/cm^3

'''


##############################
# define some system of units
##############################

# cgs units (exits in pNbody)
units.cgs

# output system of units (the mass units is the hydrogen mass)
Unit_atom = ctes.PROTONMASS.into(units.cgs)*units.Unit_g
Unit_atom.set_symbol('atom') 
out_units = units.UnitSystem('local',[units.Unit_cm,Unit_atom,units.Unit_s,units.Unit_K])


# gear system of units
params = {}
params['UnitVelocity_in_cm_per_s']      = 20725573.785998672
params['UnitMass_in_g']                 = 1.989e+43
params['UnitLength_in_cm']              = 3.085678e+21
gear_units = units.Set_SystemUnits_From_Params(params)


##############################
# now, use to convert
##############################

print gear_units.convertionFactorTo(out_units.UnitDensity)		# gear to atom/cm^3
print gear_units.convertionFactorTo(units.cgs.UnitDensity)		# gear to g/cm^3
print 0.1*out_units.convertionFactorTo(units.cgs.UnitDensity)		# atom/cm^3 to g/cm^3
print 0.1*out_units.convertionFactorTo(gear_units.UnitDensity)	        # atom/cm^3 to gear
print 1.67e-25*units.cgs.convertionFactorTo(out_units.UnitDensity)
