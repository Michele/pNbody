#!/usr/bin/env python

'''
 @package   pNbody
 @file      plot_SFE.py
 @brief     Plot value computed in a spherical grid, as a function of the radius
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt


from pNbody import *
from pNbody import myNumeric
from pNbody import libdisk
from pNbody import libgrid
from pNbody import profiles
from pNbody import cosmo

import string
import sys
import os

from numpy import *

from optparse import OptionParser



from scipy.optimize import leastsq  
#from mpfit import *


def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)


  parser = pt.add_postscript_options(parser)
  parser = pt.add_ftype_options(parser)
  parser = pt.add_reduc_options(parser)
  parser = pt.add_center_options(parser)
  parser = pt.add_select_options(parser)  
  parser = pt.add_cmd_options(parser)  	
  parser = pt.add_display_options(parser)  		    		     		    
  parser = pt.add_info_options(parser)  
  parser = pt.add_limits_options(parser)
  parser = pt.add_log_options(parser)
  parse  = pt.add_units_options(parser)
  parser = pt.add_legend_options(parser)
		    		     		    

  parser.add_option("--Rmax",
		   action="store", 
		   dest="Rmax",
		   type="float",
		   default = 50.,		   
		   help="max radius of bins",	 
		   metavar=" FLOAT")    


  parser.add_option("--nR",
		   action="store", 
		   dest="nR",
		   type="int",
		   default = 32,		   
		   help="number of bins in r",	 
		   metavar=" INT")  

  parser.add_option("--nt",
		   action="store", 
		   dest="nt",
		   type="int",
		   default = 32,		   
		   help="number of bins in r",	 
		   metavar=" INT")  		   
		
		   
  parser.add_option("--nz",
		   action="store", 
		   dest="nz",
		   type="int",
		   default = 65,		   
		   help="number of bins in z",	 
		   metavar=" INT") 		   
		   
		   
  parser.add_option("--ErrTolTheta",
		   action="store", 
		   dest="ErrTolTheta",
		   type="float",
		   default = 0.5,		   
		   help="Error tolerance theta",	 
		   metavar=" FLOAT") 			   
		   
  parser.add_option("--fct",
		   action="store", 
		   dest="fct",
		   type="string",
		   default = None,		   
		   help="transformation function ex : 'r**3' should be of the form M(r)",	 
		   metavar=" STR")  		
		      	
  parser.add_option("--fctm",
		   action="store", 
		   dest="fctm",
		   type="string",
		   default = None,		   
		   help="inverse transformation function ex : 'r**(1/3.)'",	 
		   metavar=" STR")  	
		   		   
  parser.add_option("--AdaptativeSoftenning",
		   action="store_true", 
		   dest="AdaptativeSoftenning",
		   default = False,		   
		   help="AdaptativeSoftenning") 
		   
  parser.add_option("--eps",
		   action="store", 
		   dest="eps",
		   type="float",
		   default = 0.28,		   
		   help="smoothing length",	 
		   metavar=" FLOAT")  			   


  parser.add_option("--dt",
		   action="store", 
		   dest="dt",
		   type="float",
		   default = 10,		   
		   help="max star age in Myr = inerval time",	 
		   metavar=" FLOAT")  	

  parser.add_option("--forceComovingIntegrationOn",
  		    action="store_true", 
  		    dest="forceComovingIntegrationOn",
		    default = False,		    
  		    help="force the model to be in in comoving integration")
		   
  (options, args) = parser.parse_args()


        
  if len(args) == 0:
    print "you must specify a filename"
    sys.exit(0)
    
  files = args
  
  return files,options




#######################################
# MakePlot
#######################################


def MakePlot(files,opt):


  # some inits  
  colors     = pt.Colors(n=len(files))  
  datas      = []


  #######################
  # set the grid scaling
  #######################
  
  if opt.fct!=None:
    print "g   =lambda r:(%s)"%opt.fct
    exec("g   =lambda r:(%s)"%opt.fct)
  else:
    g = None

  if opt.fctm!=None:
    print "gm  =lambda r:(%s)"%opt.fctm
    exec("gm  =lambda r:(%s)"%opt.fctm)
  else:
    gm = None


  # define local units
  unit_params = pt.do_units_options(opt)


  # read files
  for i,file in enumerate(files):


    nb = Nbody(file,ftype=opt.ftype)  
  
    # apply options
    nb = pt.do_reduc_options(nb,opt)	
    nb = pt.do_select_options(nb,opt)	 
    nb = pt.do_center_options(nb,opt)	
    nb = pt.do_cmd_options(nb,opt)   
    nb = pt.do_info_options(nb,opt)
    nb = pt.do_display_options(nb,opt)   


    ################
    # units
    ################    
    
    # define local units
    nb.set_local_system_of_units(params=unit_params)
    
    # define output units
    # nb.ToPhysicalUnits()
    
    if opt.forceComovingIntegrationOn:
      nb.setComovingIntegrationOn()


    out_units = units.UnitSystem('local',[units.Unit_pc,units.Unit_Msol,units.Unit_yr,units.Unit_K])

    tokms      = nb.localsystem_of_units.convertionFactorTo(out_units.UnitVelocity)
    toMsol     = nb.localsystem_of_units.convertionFactorTo(out_units.UnitMass)
    toMsolpc2  = nb.localsystem_of_units.convertionFactorTo(out_units.UnitSurfaceDensity)
    toMsolkpc2 = toMsolpc2*1000**2
    topc2      = nb.localsystem_of_units.convertionFactorTo(out_units.UnitSurface)
    tokpc2     = topc2/1000**2
    toyr       = nb.localsystem_of_units.convertionFactorTo(out_units.UnitTime)
    toMyr      = toyr/1e6

    
    # selection
    nb_gas   = nb.select('gas')
    nb_stars = nb.select('stars1')
    



    ############################## 
    # using a cylindrical rt grid  
    ##############################
  
   
    G = libgrid.Cylindrical_2drt_Grid(rmin=0,rmax=opt.Rmax,nr=opt.nR,nt=2,z=0,g=g,gm=gm)  
   
    # radius	
    R,t   = G.get_rt()

    
    # gas srface density   Msol/kpc2
    Sden_gas = G.get_ReducedSurfaceDensityMap(nb_gas) * toMsolkpc2
    
    # stars srface density Msol/pc2
    Sden_stars = G.get_ReducedSurfaceDensityMap(nb_stars) * toMsolpc2    



    # star formation rate map
    val =  nb_stars.sfr(opt.dt/toMyr) * toMsol/toyr	# star formation rate,  Msol/yr  per particles
    Sfr = G.get_ValMap(nb_stars,val/nb_stars.mass)  
    Sfr = sum(Sfr,axis=1)
        
    # surface
    S = G.get_SurfaceMap(None)
    S = sum(S,axis=1)*tokpc2
        
    Sfr=Sfr/S		# Msol/pc^2
     
     
    SFE =  Sfr/Sden_gas
     
     
     
    Sden_stars,SFE=pt.CleanVectorsForLogX(Sden_stars,SFE)
    Sden_stars,SFE=pt.CleanVectorsForLogY(Sden_stars,SFE)
    
    Sden_stars = log10(Sden_stars)
    SFE        = log10(SFE)
                

    # set labels
    xlabel = r'$\rm{Stars\,Surf.\,Density}\,[\rm{M_{\odot}/pc^2}]$'
    ylabel = r'$\rm{SFE}\,[\rm{yr^{-1}}]$'

    
    datas.append(pt.DataPoints(Sden_stars, SFE, color=colors.next(),label=i))
   
       
  # plot
  for d in datas:     
    if len(d.x)!=0:
      pt.scatter(d.x,d.y,color=d.color)



  # fit
  # kennicutt 98
  x=arange(-3,10)
  y= (-10.28) + 0.48 * x
  pt.plot(x,y,'r-')
  
  x=arange(-3,10)
  y= (-10.28+0.5) + 0.48 * x
  pt.plot(x,y,'r--')  

  x=arange(-3,10)
  y= (-10.28-0.5) + 0.48 * x
  pt.plot(x,y,'r--')  
  
  # set limits and draw axis
  xmin,xmax,ymin,ymax = pt.SetLimitsFromDataPoints(opt.xmin,opt.xmax,opt.ymin,opt.ymax,datas,opt.log)
  
  
  

  
  # plot axis
  pt.SetAxis(xmin,xmax,ymin,ymax,log=opt.log)
  pt.xlabel(xlabel,fontsize=pt.labelfont)
  pt.ylabel(ylabel,fontsize=pt.labelfont)
  pt.grid(False)

  if opt.legend:
    pt.LegendFromDataPoints(datas)




########################################################################
# MAIN
########################################################################



if __name__ == '__main__':
  files,opt = parse_options()  
  pt.InitPlot(files,opt)
  pt.pcolors
  MakePlot(files,opt)
  pt.EndPlot(files,opt)
  

  









