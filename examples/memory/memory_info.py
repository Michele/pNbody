#!/usr/bin/env python
'''
 @package   pNbody
 @file      memory_info.py
 @brief     Print the memory usage of pNbody
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
from pNbody import *
from pNbody import ic

n = 1e6
nb = ic.box(n,1,1,1)
nb.memory_info()


x = random.random(n)
y = random.random(n)
z = random.random(n)
pos = transpose(array([x,y,z]))

nb = Nbody(status='new',p_name='snap.dat',pos=pos)
nb.memory_info()
