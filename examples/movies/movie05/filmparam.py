# -*- coding: iso-8859-1 -*-
'''
 @package   pNbody
 @file      filmparam.py
 @brief     Parameter for a movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

# nombre de sous film horiz et vertical
nh = 2  # horizontal
nw = 1  # vertical
# size of subfilms
width = 512
height = 512
# size of the film
numByte = width * nw
numLine = height * nh
# init parameters
params = InitParameters(nh,nw)


# image 1 
params[0]['ftype'] = 'gadget'
params[0]['time'] = 'nb.atime'
params[0]['tdir'] = 'right'
params[0]['filter_name'] = None
params[0]['filter_opts'] = [10, 10, 2, 2]
params[0]['scale'] = 'log'
params[0]['mn'] = 0
params[0]['mx'] = 0
params[0]['cd'] = 0
params[0]['frsp'] = 0.
params[0]['components'] = ['gas','halo','stars','&polygon#20&sphere.dat','&polygon#4&grid.dat']

# image 2 
params[1]['ftype'] = 'gadget'
params[1]['time'] = 'nb.atime'
params[1]['tdir'] = 'left'
params[1]['filter_name'] = None
params[1]['filter_opts'] = [10, 10, 2, 2]
params[1]['scale'] = 'log'
params[1]['mn'] = 0
params[1]['mx'] = 0
params[1]['cd'] = 0
params[1]['frsp'] = 0.
params[1]['components'] = ['gas','halo','stars','&polygon#20&sphere.dat','&polygon#4&grid.dat']
