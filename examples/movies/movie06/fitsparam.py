# -*- coding: iso-8859-1 -*-
'''
 @package   pNbody
 @file      fitsparam.py
 @brief     Parameter for a movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
# comp1
f1	=  10		
ar1	=  58.   		
ag1	=  119. 
ab1	=  255.  

# comp2
f2 	=  0.	 	
ar2	=  243.  
ag2	=  211.  
ab2	=  32.  

# comp3	   
f3 	=  0.
ar3	=  240.
ag3	=  153.
ab3	=  54. 
	
# comp4   
f4 	=  0.
ar4	=  100.
ag4	=  0.
ab4	=  0.

# comp5   
f5 	=  0.0005
ar5	=  0.
ag5	=  255.
ab5	=  0.

# comp6   
f6 	=  0
ar6	=  255.
ag6	=  0.
ab6	=  0.



# 
mx  	= 0.5		# max for 255
fmx 	= 0.001		# relative turnoff


