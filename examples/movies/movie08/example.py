#!/usr/bin/env python
'''
 @package   pNbody
 @file      example.py
 @brief     Example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Mkgmov
from pNbody import ic

movie = Mkgmov.Movie('filmparam.py')


for i in xrange(100):
  nb = ic.box(1000,1,1,1,ftype='gadget')
  # now create an image
  movie.dumpimage(nb)
