'''
 @package   pNbody
 @file      filmparam11.py
 @brief     Parameter for a movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
################################
# film parameters
################################

#global film

film = {}
film['ftype']  = "gadget"
film['time']   = "nb.atime"
film['exec']   = None
film['macro']  = None
film['frames'] = []

######################
# other parameters
######################

#film['timesteps'] = "output.txt"
#film['timesteps'] = [3,2,1,0]
#film['timesteps'] = (0,10,0.1)

#film['imdir'] = 'png1.0'
#film['format'] = "png"


######################
# a frame
######################

frame = {}
frame['width']       = 512	
frame['height']      = 512
frame['tdir']        = None
frame['pfile']       = 'glparameters_right.nbd'
frame['exec']        = None
frame['macro']       = None
frame['components']  = []
frame['palette']  = "rainbow4"

# component 'total'
component = {}
component['id']          = 'all'
component['rendering']   = 'map'
component['exec']        = None
component['macro']       = None
component['frsp']        = 0.
component['filter_name'] = None
component['filter_opts'] = [2,2]
frame['components'].append(component)


film['frames'].append(frame)




