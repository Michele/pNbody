#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

'''
 @package   pNbody
 @file      gfits2png.py
 @brief     Transforms fits to png
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody.palette import *


from optparse import OptionParser

import sys,os


########################################  
#					  
# parser				  
#					  
######################################## 


def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)
  
  
  parser.add_option("-d","--dir",
                    action="store", 
		    dest="dir", 
		    type="string",
		    default="tmp",
                    help="output directory for png files")	  

  parser.add_option("-m","--mode",
                    action="store", 
		    dest="mode", 
		    type="string",
		    default=None,
                    help="image mode : L, P or RGB")


  parser.add_option("--comp1",
		   action="store", 
		   dest="comp1",
		   type="string",
		   default = None,		   
		   help="list of comp1 files",	 
		   metavar=" STRING")   
		   
  parser.add_option("--comp2",
		   action="store", 
		   dest="comp2",
		   type="string",
		   default = None,		   
		   help="list of comp2 files",	 
		   metavar=" STRING")   		   

  parser.add_option("--comp3",
		   action="store", 
		   dest="comp3",
		   type="string",
		   default = None,		   
		   help="list of comp3 files",	 
		   metavar=" STRING")   
		   
  parser.add_option("--comp4",
		   action="store", 
		   dest="comp4",
		   type="string",
		   default = None,		   
		   help="list of comp4 files",	 
		   metavar=" STRING")   

  parser.add_option("--comp5",
		   action="store", 
		   dest="comp5",
		   type="string",
		   default = None,		   
		   help="list of comp5 files",	 
		   metavar=" STRING")   

  parser.add_option("--comp6",
		   action="store", 
		   dest="comp6",
		   type="string",
		   default = None,		   
		   help="list of comp6 files",	 
		   metavar=" STRING")   

  parser.add_option("--params",
		   action="store", 
		   dest="params",
		   type="string",
		   default = None,		   
		   help="color parameters",	 
		   metavar=" STRING")   	


  parser.add_option("-p","--palette",
                    action="store", 
		    dest="palette", 
		    type="string",
		    default=None,
                    help="color palette")
		   	   		   		    		    		    		    		    

  parser.add_option("--scale",
		   action="store", 
		   dest="scale",
		   type="string",
		   default = 'log',		   
		   help="scale",	 
		   metavar=" STRING") 

  parser.add_option("--mn",
		   action="store", 
		   dest="mn",
		   type="float",
		   default = 0.0,		   
		   help="min value",	 
		   metavar=" FLOAT")    	

  parser.add_option("--mx",
		   action="store", 
		   dest="mx",
		   type="float",
		   default = 0.0,		   
		   help="max value",	 
		   metavar=" FLOAT") 

  parser.add_option("--cd",
		   action="store", 
		   dest="cd",
		   type="float",
		   default = 0.0,		   
		   help="cd value",	 
		   metavar=" FLOAT") 

  parser.add_option("-b","--background",
                    action="store", 
		    dest="background", 
		    type="string",
		    default=None,
                    help="background image")

  parser.add_option("--flipud",
		   action="store_true", 
		   dest="flipud",
		   default = False,		   
		   help="flip up/down") 		    

  parser.add_option("--text",
                    action="store", 
		    dest="text", 
		    type="string",
		    default=None,
                    help="text")

  parser.add_option("--textsize",
                    action="store", 
		    dest="textsize", 
		    type="int",
		    default=16,
                    help="textsize")

  (options, args) = parser.parse_args()
       
  files = args
  
  return files,options

#####################################################
def ReadFitsHeader(filename) :
#####################################################
  
  from pNbody import pyfits
  
  # read image
  fitsimg = pyfits.open(filename)
  #data = fitsimg[0].data
  header = fitsimg[0].header
  return header

################################################################################
#
#                                    MAIN
#
################################################################################



files,opt = parse_options()


if not os.path.exists(opt.dir):
  os.mkdir(opt.dir)
  
  


if opt.palette==None:

  files1 = glob.glob(opt.comp1)
  files2 = glob.glob(opt.comp2)
  files3 = glob.glob(opt.comp3)
  files4 = glob.glob(opt.comp4)
  files5 = glob.glob(opt.comp5)
  files6 = glob.glob(opt.comp6)

  files1.sort()
  files2.sort()
  files3.sort()
  files4.sort()
  files5.sort()
  files6.sort()
  
  #if (len(files1)!=len(files2)) or (len(files1)!=len(files3)) or (len(files1)!=len(files4)):
  #  print len(files1),len(files2),len(files3),len(files4)
  #  raise "All components must have the same number of files."

  n = len(files1)
  
else:
  n = len(files)


if opt.background!=None:
  background = Image.open(opt.background)


###################################
# loop over all files
###################################

for i in xrange(n):

 
  nums =  "%8d/%8d"%(i,n)


  if opt.palette==None:
    
    file = files1[i]
    

    try:
      file_1 = files1[i]
    except IndexError:
      pass

    try:
      file_2 = files2[i]
    except IndexError:
      pass

    try:
      file_3 = files3[i]
    except IndexError:
      pass
      
    try:
      file_4 = files4[i]
    except IndexError:
      pass
      
    try:
      file_5 = files5[i]
    except IndexError:
      pass
    
    try:
      file_6 = files6[i]
    except IndexError:
      pass

    
    # read

    data_1 = io.ReadFits(file_1)
    data_2 = io.ReadFits(file_2)
    data_3 = io.ReadFits(file_3)
    data_4 = io.ReadFits(file_4)
    data_5 = io.ReadFits(file_5)
    data_6 = io.ReadFits(file_6)

    if opt.flipud:
      data_1 = flipud(data_1)
      data_2 = flipud(data_2)
      data_3 = flipud(data_3)
      data_4 = flipud(data_4)
      data_5 = flipud(data_5)
      data_6 = flipud(data_6)


    ##################################################
    # compose
    ##################################################

    size = (data_1.shape[1],data_1.shape[0])


    if opt.params == None:

      f1      =  20.
      ar1     =  58.   
      ag1     =  119. 
      ab1     =  255.  

      f2      =  1.    
      ar2     =  243.  
      ag2     =  243.  
      ab2     =  32.  
    		 
      f3      =  1.
      ar3     =  240.
      ag3     =  178.
      ab3     =  54. 
    		 
      f4      =  1.
      ar4     =  195.
      ag4     =  251.
      ab4     =  255.  

      f5      =  1.
      ar5     =  255.
      ag5     =  0.
      ab5     =  0. 

      f6      =  1.
      ar6     =  0.
      ag6     =  255.
      ab6     =  0. 

      mx = 0.5
      mx1= mx*0.002
      cd = mx1/(1.78)
      cte = 255/log(1.+mx/cd)
  
    else:
      execfile(opt.params)
    	   
      cd = fmx * mx
      cte = 255/log(1.+mx/cd)
  


    ar1 = ar1*f1
    ag1 = ag1*f1
    ab1 = ab1*f1

    ar2 = ar2*f2
    ag2 = ag2*f2
    ab2 = ab2*f2
    	     
    ar3 = ar3*f3
    ag3 = ag3*f3
    ab3 = ab3*f3
    	     
    ar4 = ar4*f4
    ag4 = ag4*f4
    ab4 = ab4*f4

    ar5 = ar5*f5
    ag5 = ag5*f5
    ab5 = ab5*f5

    ar6 = ar6*f6
    ag6 = ag6*f6
    ab6 = ab6*f6
        
    r = ar1*data_1  +ar2*data_2  +ar3*data_3  +ar4*data_4  +ar5*data_5  +ar6*data_6
    g = ag1*data_1  +ag2*data_2  +ag3*data_3  +ag4*data_4  +ag5*data_5  +ag6*data_6
    b = ab1*data_1  +ab2*data_2  +ab3*data_3  +ab4*data_4  +ab5*data_5  +ab6*data_6


    r = cte * log(1.+r/cd)
    g = cte * log(1.+g/cd)
    b = cte * log(1.+b/cd)

    r =  uint8(clip(r,0,255))
    g =  uint8(clip(g,0,255))
    b =  uint8(clip(b,0,255))

    r = transpose(r)
    g = transpose(g)
    b = transpose(b)


    image_r = Image.fromstring("L",size,r)
    image_g = Image.fromstring("L",size,g)
    image_b = Image.fromstring("L",size,b)

    img = Image.merge("RGB",(image_r,image_g,image_b))
  
  
  else:
    
    file = files[i]    
    data = io.ReadFits(file)
    data = transpose(data)
    #data = fliplr(data)
  
    matint,mn_opt,mx_opt,cd_opt = set_ranges(data,scale=opt.scale,cd=opt.cd,mn=opt.mn,mx=opt.mx)
    opt.mn = mn_opt 
    opt.mx = mx_opt
    opt.cd = cd_opt
  
    print opt.mn,opt.mx,opt.cd
  
    img = get_image(matint,palette_name=opt.palette)
  
  
  
  # mode 
  if opt.mode != None: 
    img = img.convert(opt.mode)
  

  #######################
  # add text
  #######################
  
  if opt.text!=None:

    #font = ImageFont.truetype("./kidprbol.ttf", 28)
    font = ImageFont.truetype("./Courier_New_Bold.ttf", opt.textsize)
    
    # text pos
    postext = (25,25) 		# top left
    coltext = (253,0,0)	# (116,172,220)

    # read time  
    header = ReadFitsHeader(file)
    #txt = 'Time = %04d Myr'%(header['TIME'])
    exec("txt = %s"%opt.text)

    siztext = font.getsize(txt)
    
    # add  
    draw = ImageDraw.Draw(img)  	  
    draw.text(postext,txt,fill=coltext,font=font)	      
  

    '''
    units = "Myrs"
    if i==0:
      siztext0 = siztext[0]
    postext = (postext[0]+siztext0+siztext0*0.05,postext[1])
    
    draw.text(postext,units,fill=coltext,font=font)	      
    '''

  #######################
  # background image
  #######################
  
  if opt.background!=None:
    img = ImageChops.add(img, background)


  #######################
  # write
  #######################

  
  fout = os.path.join(opt.dir,'%08d.png'%(i))
  img.save(fout) 
  
  print "%s    --> %s"%(nums,fout)





