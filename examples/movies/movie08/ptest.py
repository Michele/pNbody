#!/usr/bin/env python
'''
 @package   pNbody
 @file      ptest.py
 @brief     Test pNbody
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *


random.seed(mpi.ThisTask)

#print mpi.ThisTask,random.random(1)
n = 100000

random = RandomArray.random([n,3])  

pos = random-[0.5,0.5,0.5]
pos = pos*array([2,2,2])

vel =  ones([n,3])*0.0
mass = ones([n])*1./n
  
nb = Nbody(status='new',p_name='box.dat',pos=pos,vel=vel,mass=mass,ftype='gadget')

nb.write()
