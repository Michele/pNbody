#!/usr/bin/env python

'''
 @package   pNbody
 @file      example-3.py
 @brief     Example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import ic
from numpy import *
from optparse import OptionParser

import Mkgmov 
import sys

def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)



  parser.add_option("-n",
  		    action="store", 
  		    dest="n",
  		    type="int",
		    default = 1000,   
  		    help="number of particles",       
  		    metavar=" INT")    

  parser.add_option("--dt",
  		    action="store", 
  		    dest="dt",
  		    type="float",
		    default = 0.01,   
  		    help="delta time",       
  		    metavar=" FLOAT")    

  parser.add_option("--t0",
  		    action="store", 
  		    dest="t0",
  		    type="float",
		    default = 0.0,   
  		    help="t0",       
  		    metavar=" FLOAT")    


  parser.add_option("--t1",
  		    action="store", 
  		    dest="t1",
  		    type="float",
		    default = 1.0,   
  		    help="t1",       
  		    metavar=" FLOAT")  


  parser.add_option("-p",
  		    action="store", 
  		    dest="param",
  		    type="string",
		    default = None,   
  		    help="parameter file",       
  		    metavar=" FILE")    

  parser.add_option("--format",
  		    action="store", 
  		    dest="format",
  		    type="string",
		    default = None,   
  		    help="format",       
  		    metavar=" STRING")    

  parser.add_option("--imdir",
  		    action="store", 
  		    dest="imdir",
  		    type="string",
		    default = None,   
  		    help="image directory",       
  		    metavar=" STRING")  
		    
  (options, args) = parser.parse_args()
  files = args
  
  return files,options



#######################################################################
#
#	M A I N
#
#######################################################################



files,opt = parse_options()





# parameters
time = opt.t0

# initial conditions
nb = ic.box(opt.n,1,1,1,ftype='gadget')
nb.vel = 2*(random.random([opt.n,3]) - 0.5)


# open filmObject
movie = Mkgmov.Movie(parameterfile=opt.param,format=opt.format,imdir=opt.imdir)


while(time<opt.t1):

  time = time + opt.dt

  nb.pos = nb.pos + nb.vel * opt.dt
  nb.rebox(2,mode='centred')
  nb.atime = time

  print "TIME=%5.2f"%time

  nexttime = movie.get_next_time()
  if nexttime!=None:
    if nexttime<=time:
      movie.set_next_time()
      #nb.rename('box%06d.dat'%i)
      #nb.write()

      # create the movie
      movie.dumpimage(nb)


