'''
 @package   pNbody
 @file      filmparam-3.py
 @brief     Parameter for a movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
################################
# film parameters
################################

#global film

film = {}
film['ftype']  = "gadget"
film['time']   = "nb.atime"
film['exec']   = None
film['macro']  = None
film['frames'] = []



######################
# a frame
######################

frame = {}
frame['width']       = 512	
frame['height']      = 512
frame['tdir']        = None
frame['pfile']       = 'glparameters-3_right.nbd'
frame['exec']        = None
frame['macro']       = None
frame['components']  = []


# component 'total'
component = {}
component['id']          = 'gas'
component['rendering']   = 'map'
component['exec']        = None
component['macro']       = None
frame['components'].append(component)

component = {}
component['id']          = 'halo'
component['rendering']   = 'map'
component['exec']        = None
component['macro']       = None
frame['components'].append(component)

component = {}
component['id']          = 'stars'
component['rendering']   = 'map'
component['exec']        = None
component['macro']       = None
frame['components'].append(component)
film['frames'].append(frame)



