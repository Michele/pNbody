'''
 @package   pNbody
 @file      filmparam.py
 @brief     Parameter for a movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
################################
# film parameters
################################

#global film

film = {}
film['ftype']  = "gadget"
film['time']   = "nb.atime"
film['exec']   = None
film['macro']  = None
film['frames'] = []

######################
# other parameters
######################

#film['timesteps'] = "every"
film['timesteps'] = (0,3000,0.5)
film['imdir'] = 'fits'
film['format'] = "fits"


######################
# a frame
######################

frame = {}
frame['width']       = 512	
frame['height']      = 512
frame['tdir']        = None
frame['pfile']       = None
frame['exec']        = None
frame['macro']       = None
frame['components']  = []
frame['palette']  = "rainbow4"

# component 'total'
component = {}
component['id']          = 'gas'
component['rendering']   = 'map'
component['exec']        = "nbfc = nbfc.selectc(fabs(nbfc.y())<0.1)"
component['macro']       = None
component['frsp']        = 10.
component['mode']        = 'log10(nb.T())'
component['filter_name'] = None
component['filter_opts'] = [2,2]
component['size'] 	 = (3,3)
frame['components'].append(component)


film['frames'].append(frame)



######################
# a frame
######################

frame = {}
frame['width']       = 512	
frame['height']      = 512
frame['tdir']        = None
frame['pfile']       = None
frame['exec']        = None
frame['macro']       = None
frame['components']  = []
frame['palette']  = "rainbow4"

# component 'total'
component = {}
component['id']          = 'gas'
component['rendering']   = 'map'
component['exec']        = "nbfc = nbfc.selectc(fabs(nbfc.y())<0.1)"
component['macro']       = None
component['frsp']        = 10.
component['mode']        = 'nb.Fe()'
component['filter_name'] = None
component['filter_opts'] = [2,2]
component['size'] 	 = (3,3)
frame['components'].append(component)


film['frames'].append(frame)

