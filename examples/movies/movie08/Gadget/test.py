'''
 @package   pNbody
 @file      test.py
 @brief     Test ImageMaker
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
import ImageModule

dict = {}

Imk = ImageModule.ImageMaker()
Imk.dump(dict)
