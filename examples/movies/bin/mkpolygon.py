#!/usr/bin/env python
'''
 @package   pNbody
 @file      mkpolygon.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
# -*- coding: iso-8859-1 -*-


from pNbody import ic
from numpy import *

from optparse import OptionParser


def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)


  parser.add_option("-o",
  		    action="store", 
  		    dest="outputname",
  		    type="string",
		    default = None,		    
  		    help="output name",       
  		    metavar=" NAME")		    		    

  parser.add_option("--relative",
  		    action="store_true", 
  		    dest="rel",
		    default = 0,		    
  		    help="plot relative value")	

  parser.add_option("-t",
  		    action="store",
		    type = "string",
  		    dest="ftype",
		    default = 'gadget',		    
  		    help="nbody format")	

  parser.add_option("--type",
  		    action="store",
		    type = "string",
  		    dest="type",
		    default = 'line',		    
  		    help="type of object")	

  parser.add_option("--cmd",
  		    action="store",
		    type = "string",
  		    dest="cmd",
		    default = None,		    
  		    help="cmd to apply to object")	

  parser.add_option("--n1",
  		    action="store",
		    type = "int",
  		    dest="n1",
		    default = 10,		    
  		    help="n1")	
	
  parser.add_option("--n2",
  		    action="store",
		    type = "int",
  		    dest="n2",
		    default = 10,		    
  		    help="n2")		
		    		    		    
  (options, args) = parser.parse_args()
  
    
  files = args
  
  return files,options

files,opt = parse_options()


'''
line   : polygon
square : polygon
circle : polygon
grid   : polygon4
cube   : segments

'''


if   opt.type=='line':
  nb = ic.line(ftype=opt.ftype)
elif opt.type=='square':
  nb = ic.square(ftype=opt.ftype)
elif opt.type=='circle':
  nb = ic.circle(n,ftype=opt.ftype)
elif opt.type=='grid':
  nb = ic.grid(opt.n1,opt.n2,ftype=opt.ftype)  
elif opt.type=='cube':
  nb = ic.cube(ftype=opt.ftype)
elif opt.type=='sphere':
  nb = ic.sphere(opt.n1,opt.n2,ftype=opt.ftype)   
else:
  print "unknown type"
  sys.exit()

# apply comd  
if (opt.cmd!= None) and  (opt.cmd!= 'None'):  
  print "exec : %s"%opt.cmd
  exec(opt.cmd) 

if opt.outputname!=None:
  nb.rename(opt.outputname)

nb.write()
