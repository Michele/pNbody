'''
 @package   pNbody
 @file      arrows.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

nb = nb.reduc(128)


pos = zeros((2*nb.nbody,3))

k = 0
for i in xrange(nb.nbody):
  
  vnorm = sqrt( nb.vel[i,0]**2 + nb.vel[i,1]**2 + nb.vel[i,2]**2 )


  pos[k] = nb.pos[i]
  k=k+1
  pos[k] = nb.pos[i] + nb.vel[i]/vnorm*10
  k=k+1


tnow = nb.tnow
nb = Nbody(status='new',pos=pos,ftype='gadget')
nb.tnow = tnow
