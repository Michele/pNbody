#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
'''
 @package   pNbody
 @file      gmkgmov.py
 @brief     Transform images to film
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from optparse import OptionParser
import os,sys
import glob


from pNbody import *
from pNbody.param import Params

import copy

def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)



  parser.add_option("-p",
  		    action="store", 
  		    dest="parameterfile",
  		    type="string",
		    default = None,   
  		    help="parameterfile file",       
  		    metavar=" FILE")    

  parser.add_option("-o",
  		    action="store", 
  		    dest="outputfile",
  		    type="string",
		    default = 'film.gmv',   
  		    help="output file",       
  		    metavar=" FILE")    

  parser.add_option("-d",
  		    action="store", 
  		    dest="directory",
  		    type="string",
		    default = None,   
  		    help="directroy containing files",       
  		    metavar=" DIRECTORY")    


  parser.add_option("--pio",
  		    action="store_true", 
  		    dest="pio",
		    default = False,   
  		    help="parallele io",       
  		    metavar=" BOOL")  
		    
  parser.add_option("--mkfits",
  		    action="store_true", 
  		    dest="mkfits",
		    default = True,   
  		    help="create fits outputs",       
  		    metavar=" BOOL") 		    
		    
  parser.add_option("--fitsdir",
  		    action="store", 
  		    dest="fitsdir",
  		    type="string",
		    default = 'fits',   
  		    help="outputdirectory for fits files",       
  		    metavar=" DIRECTORY")  		    
		    

  (options, args) = parser.parse_args()
  files = args
  
  return files,options




#######################################################################
# some usefull functions
#######################################################################


def ReadNbodyParameters(paramname):
  '''
  read param from a parameter Nbody file
  '''
  
  gparams = Params(paramname,None)
  
  param = {}
  # create new params
  for p in gparams.params:
    param[p[0]]=p[3]    

  return param
  
  
  
#######################################################################
#
#	M A I N
#
#######################################################################

files = None
film  = None
opt   = None

if mpi.mpi_IsMaster():

  ##############################
  # parse options and check dirs
  ##############################  

  files,opt = parse_options()
  
  
  if opt.pio:
    opt.pio = "yes"
  else:
    opt.pio = "no"
  

  if opt.parameterfile==None:
    print "you must specify a parameter file"
    sys.exit()
    
  if not os.path.exists(opt.parameterfile):
    print "file %s does not exists"%opt.parameterfile
    sys.exit()

  if opt.mkfits:
    if not os.path.exists(opt.fitsdir):
      os.mkdir(opt.fitsdir)

  
  ##############################
  # read parameter file
  ##############################

  execfile(opt.parameterfile)  

  # post process
  for i,frame in enumerate(film['frames']):
    frame['id'] = i

  # check
  for frame in film['frames']:
    print frame['id']
    for component in frame['components']:
      print "  ",component['id']





# broadcast files and parameters  
files = mpi.mpi_bcast(files,root=0)
film  = mpi.mpi_bcast(film,root=0)
opt   = mpi.mpi_bcast(opt,root=0)


###################################################################################
###################################################################################
##
## main loop over all files
##
###################################################################################
###################################################################################  

for ifile,file in enumerate(files):  

  print "(task=%04d) reading "%(mpi.ThisTask),file,film['ftype'],opt.pio
  nb = Nbody(file,ftype=film['ftype'],pio=opt.pio)
  
  # set time reference for this file
  exec("nb.tnow = %s"%film['time'])
  
  # exec
  if film['exec'] != None:
    exec(film['exec']) 

  # macro
  if film['macro'] != None:
    execfile(film['macro']) 
 

  for frame in film['frames']:
  
    nbf = nb
    
    # exec
    if frame['exec'] != None:
      exec(frame['exec']) 

    # macro
    if frame['macro'] != None:
      execfile(frame['macro']) 


    for component in frame['components']:
      
      computemap = True
            
      if component['id'][0] == '@':
	# here, all tasks must have an object containing all particles
	# ok, but not in the right order !!!
	componentid = component['id'][1:]
	nbfc = Nbody(componentid,ftype=film['ftype'])
	nbfc = nbfc.SendAllToAll()
        nbfc.sort()
      else:
        nbfc = nbf.select(component['id'])
	componentid = component['id']
  


      
      # exec
      if component['exec'] != None:
        exec(component['exec']) 

      # macro
      if component['macro'] != None:
        execfile(component['macro']) 

  
      # find the observer position
      # 1) from params
      # 2) from pfile
      # 3) from tdir
      # and transform into parameter

      if frame['tdir']!=None:
        
        tfiles = glob.glob(os.path.join(frame['tdir'],"*")) 
        tfiles.sort()
        
        bname = os.path.basename(file)
        
        tfiles_for_this_file = []
        for j in xrange(len(tfiles)):
          tfile = "%s.%05d"%(os.path.basename(file),j)
          tmp_tfile = os.path.join(frame['tdir'],tfile)
          if os.path.exists(tmp_tfile):
            tfiles_for_this_file.append(tmp_tfile)


      elif frame['pfile']!=None:
        	
        if not os.path.isfile(frame['pfile']):
          print "parameter file %s does not exists(1)..."%(frame['pfile'])
        
        # read from pfile defined in frame
        param = ReadNbodyParameters(frame['pfile']) 
        tfiles_for_this_file = [None]	
        
      else:
        
        # take frame as parameter
        param = copy.copy(frame)
        tfiles_for_this_file = [None]
        
        
        
      # loop over different oberver positions for this file  
      for j,tfile in enumerate(tfiles_for_this_file):
      
        if tfile!=None:
          param = ReadNbodyParameters(tfile)  
  
        
        # add parameters defined by user in the parameter file
        for key in component.keys():
          param[key] = component[key]

 
        # set image shape using frame
        param['shape'] = (frame['width'],frame['height'])
            
  
        # compute map
        mat = nbfc.CombiMap(param)	   
      
        if mpi.mpi_IsMaster():
      
          # save output
          output = '%04d_%04d-%s-%06d.fits'%(ifile,frame['id'],componentid,j)
          output = os.path.join(opt.fitsdir,output)
          print nb.atime,output
        
    
          if os.path.exists(output):
            os.remove(output)

          header = [('TIME',nb.tnow,'snapshot time')]		     
          io.WriteFits(transpose(mat), output, extraHeader = header)	  
        
  

  
  
  
  
  
