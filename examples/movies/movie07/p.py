# -*- coding: iso-8859-1 -*-
'''
 @package   pNbody
 @file      p.py
 @brief     Resize images
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import cosmo

'''
a = info_dict[i]
z = cosmo.Z_a(a)
t = cosmo.Age_a(a)
txt = "temps=%5.2f Gyr"%(-t)  
txt = "temps=%5.2f Gyr"%(-t)  


img1 = imgs[0]
img2 = imgs[1]


# add time
#img1 = img_add_text(img1,txt,color=(255,0,0),font="./Courier_New_Bold.ttf",size=16,center='both',cbox=(0,0,img1.size[0],img1.size[0]*0.05))
#img2 = img_add_text(img2,txt,color=(255,0,0),font="./Courier_New_Bold.ttf",size=16,center='both',cbox=(0,0,img2.size[0],img2.size[0]*0.05))
img1 = img_add_text(img1,txt,color=(255,0,0),font="./Courier_New_Bold.ttf",size=16,center='both',cbox=(0,0,img1.size[0]*0.2,img1.size[0]*0.05))
img2 = img_add_text(img2,txt,color=(255,0,0),font="./Courier_New_Bold.ttf",size=16,center='both',cbox=(0,0,img2.size[0]*0.2,img2.size[0]*0.05))
'''

img = img_append(imgs)
img = img_resize(img,(2000,625))

