#!/usr/bin/env python

'''
 @package   pNbody
 @file      create_arrows.py
 @brief     Create arrows
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from optparse import OptionParser
import Ptools as pt
from pNbody import *
from pNbody import units
import string 

from scipy import optimize

def parse_options():


  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)

  parser = pt.add_ftype_options(parser)

  parser.add_option("--x",
  		    action="store", 
  		    dest="x",
  		    type="string",
		    default = 'r',		    
  		    help="x value to plot",       
  		    metavar=" STRING") 

  parser.add_option("-o",
  		    action="store", 
  		    dest="outputfile",
  		    type="string",
		    default = None,		    
  		    help="output file name",       
  		    metavar=" STRING") 

  (options, args) = parser.parse_args()

            
  files = args
  
  return files,options



#################################################
# main
#################################################

files,opt = parse_options()  


file = files[0]

nb = Nbody(file,ftype=opt.ftype)

pos = zeros((2*nb.nbody,3))

k = 0
for i in xrange(nb.nbody):
  
  vnorm = sqrt( nb.vel[i,0]**2 + nb.vel[i,1]**2 + nb.vel[i,2]**2 )


  pos[k] = nb.pos[i]
  k=k+1
  pos[k] = nb.pos[i] + nb.vel[i]/vnorm
  k=k+1


# create nbody object
if opt.outputfile!=None:
  nb = Nbody(status='new',p_name=opt.outputfile,pos=pos,ftype=opt.ftype)
  nb.write()






















