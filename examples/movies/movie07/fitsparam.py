# -*- coding: utf-8 -*-
'''
 @package   pNbody
 @file      fitsparam.py
 @brief     Parameter for a movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

#opt.text= """'Time = %4.1f Myr'%(header['TIME']*4.7/1000)"""
#opt.textsize = 18

# comp1
f1	=  1e7		
ar1	=  58.   		
ag1	=  119. 
ab1	=  255.  

# comp2
f2 	=  1e-3	 	
ar2	=  255  
ag2	=  0.  
ab2	=  0.  

# comp3	   
f3 	=  0#1e2
ar3	=  0.
ag3	=  153.
ab3	=  54. 
	
# comp4   
f4 	=  0.
ar4	=  100.
ag4	=  0.
ab4	=  0.

# comp5   
f5 	=  0
ar5	=  255.
ag5	=  0.
ab5	=  0.

# comp6   
f6 	=  0.
ar6	=  255.
ag6	=  0.
ab6	=  0.



# 
mx  	= 0.5		# max for 255
fmx 	= 0.001		# relative turnoff
