'''
 @package   pNbody
 @file      filmparam.py
 @brief     Parameter for a movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

# nombre de sous film horiz et vertical
nh = 2  # horizontal
nw = 2  # vertical
# size of subfilms
width = 512
height = 512
# size of the film
numByte = width * nw
numLine = height * nh
# init parameters
param = initparams(nh,nw)


# image 1 
param[1]['ftype'] = 'gadget'
param[1]['time'] = 'nb.atime'
param[1]['obs']=None
param[1]['x0']=None
param[1]['p0']=None
param[1]['view']='xy'
param[1]['size']=(150,150)
param[1]['mode']='m'
param[1]['filter_name'] = None
param[1]['filter_opts'] = [10, 10, 2, 2]
param[1]['scale'] = 'log'
param[1]['mn'] = 0
param[1]['mx'] = 0
param[1]['cd'] = 0
param[1]['frsp'] = 0.

# image 2 
param[2]['ftype'] = 'gadget'
param[2]['time'] = 'nb.atime'
param[2]['obs']=None
param[2]['x0']=None
param[2]['p0']=None
param[2]['view']='xy'
param[2]['size']=(150,150)
param[2]['mode']='m'
param[2]['filter_name'] = None
param[2]['filter_opts'] = [5, 5, 2, 2]
param[2]['scale'] = 'log'
param[2]['mn'] = 0
param[2]['mx'] = 0
param[2]['cd'] = 0
param[2]['frsp'] = 0.
param[2]['select'] = 'gas'

# image 3 
param[3]['ftype'] = 'gadget'
param[3]['time'] = 'nb.atime'
param[3]['obs']=None
param[3]['x0']=None
param[3]['p0']=None
param[3]['view']='xz'
param[3]['size']=(150,150)
param[3]['mode']='vr'
param[3]['filter_name'] = 'gaussian'
param[3]['filter_opts'] = [5, 5]
param[3]['scale'] = 'lin'
param[3]['mn'] = -0.2
param[3]['mx'] =  0.2
param[3]['cd'] = 0
param[3]['frsp'] = 0.

# image 4 
param[4]['ftype'] = 'gadget'
param[4]['time'] = 'nb.atime'
param[4]['obs']=None
param[4]['x0']=None
param[4]['p0']=None
param[4]['view']='xz'
param[4]['size']=(150,150)
param[4]['mode']='m'
param[4]['filter_name'] = None
param[4]['filter_opts'] = [10, 10]
param[4]['scale'] = 'log'
param[4]['mn'] = 0
param[4]['mx'] = 0
param[4]['cd'] = 0
param[4]['frsp'] = 0.
param[4]['exec'] = "nb = nb.selectc(nb.z()>0)"
