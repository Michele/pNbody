#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
'''
 @package   pNbody
 @file      exemple-pNbody.mpi.py
 @brief     MPI/pNbody example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import mpi



######################
# send recv
######################

tag = "send/recv"

data = None

if mpi.ThisTask==0:
  data = 7
else:
  data = None

if   mpi.ThisTask == 0:
  mpi.mpi_send(data, dest=1)
elif mpi.ThisTask == 1:
  data = mpi.mpi_recv(source=0)


print tag,mpi.ThisTask,data
mpi.mpi_barrier()



######################
# sendrecv
######################

tag = "sendrecv"

data = mpi.ThisTask

prev = (mpi.ThisTask-1)%mpi.NTask
next = (mpi.ThisTask+1)%mpi.NTask

data = mpi.mpi_sendrecv(data,dest=next,source=prev)

print tag,mpi.ThisTask,data
mpi.mpi_barrier()



######################
# reduce
######################

tag = "reduce"

data = mpi.ThisTask

data = mpi.mpi_reduce(data,root=0)	# only root reduce

print tag,mpi.ThisTask,data
mpi.mpi_barrier()



######################
# allreduce
######################

tag = "allreduce"

data = mpi.ThisTask

data = mpi.mpi_allreduce(data)

print tag,mpi.ThisTask,data
mpi.mpi_barrier()




######################
# bcast
######################

tag = "bcast"

data = mpi.ThisTask


data = mpi.mpi_bcast(data,root=0) 

print tag,mpi.ThisTask,data
mpi.mpi_barrier()



######################
# gather
######################

tag = "gather"

data = mpi.ThisTask

data = mpi.mpi_gather(data,root=0)

print tag,mpi.ThisTask,data
mpi.mpi_barrier()


######################
# allgather
######################

tag = "allgather"

data = mpi.ThisTask

data = mpi.mpi_allgather(data)

print tag,mpi.ThisTask,data
mpi.mpi_barrier()

