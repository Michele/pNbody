#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
'''
 @package   pNbody
 @file      multiply.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *




nb = Nbody("../snap.dat",ftype='gadget')


#print mpi.ThisTask,nb.nbody,len(nb.pos)
nb = nb.SendAllToAll()
#print mpi.ThisTask,nb.nbody,len(nb.pos)

print len(nb.pos)

nb.rename('snap.dat.%d'%mpi.ThisTask)
nb.pio = "yes"
nb.write()
 


