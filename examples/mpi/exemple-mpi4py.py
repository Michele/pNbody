#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
'''
 @package   pNbody
 @file      exemple-mpi4py.py
 @brief     MPI example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from mpi4py import MPI

comm = MPI.COMM_WORLD
ThisTask = comm.Get_rank()
NTask    = comm.Get_size()
Procnm   = MPI.Get_processor_name()


def mpi_pprint(msg):
  '''
  Synchronized print.
  '''
  if NTask>1:
    MPI.pprint(msg)
  else:
    print msg  


def mpi_iprint(msg,mode=None):
  '''
  Synchronized print, including info on node.
  '''
 
  msg = "[%d] : %s"%(ThisTask,msg) 
  mpi_pprint(msg)



######################
# send recv
######################

tag = "send/recv"

data = None

if ThisTask==0:
  data = 7
else:
  data = None

if   ThisTask == 0:
  comm.send(data, dest=1, tag=11)
elif ThisTask == 1:
  data = comm.recv(source=0, tag=11)


print tag,ThisTask,data
comm.barrier()



######################
# sendrecv
######################

tag = "sendrecv"

data = ThisTask

prev = (ThisTask-1)%NTask
next = (ThisTask+1)%NTask

data = comm.sendrecv(data,dest=next,source=prev)

print tag,ThisTask,data
comm.barrier()






######################
# reduce
######################

tag = "reduce"

data = ThisTask

data = comm.reduce(data,op=MPI.SUM,root=0)	# only root reduce

print tag,ThisTask,data
comm.barrier()

######################
# allreduce
######################

tag = "allreduce"

data = ThisTask

data = comm.allreduce(data,op=MPI.SUM)

print tag,ThisTask,data
comm.barrier()



######################
# bcast
######################

tag = "bcast"

data = None

if ThisTask==0:
  data = 7
else:
  data = None


data = comm.bcast(data,root=0) 

print tag,ThisTask,data
comm.barrier()

######################
# gather
######################

tag = "gather"

data = ThisTask

data = comm.gather(data,root=0)

print tag,ThisTask,data
comm.barrier()


######################
# allgather
######################

tag = "allgather"

data = ThisTask

data = comm.allgather(data)

print tag,ThisTask,data
comm.barrier()


