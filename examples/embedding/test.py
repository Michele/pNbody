#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import mkimage
from numpy import random

Imgk = mkimage.ImageMaker()

pos = random.random((1000,3))*10


Imgk.dump(pos)
Imgk.dump(pos)
Imgk.dump(pos)
