'''
 @package   pNbody
 @file      mkimage.py
 @brief     Make an image
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
from pNbody import ic
from pNbody import Nbody

import os


def initnbody():
  nb = ic.box(1000,1,1,1)
  return nb

def createnbodyfromvec(pos):
  nb = Nbody(pos=pos)  
  return nb


def dumpimage(nb,directory,bname,i):
  name = "%s%06d.png"%(bname,i)
  file = os.path.join(directory,name)
  nb.display(save=file)
  print "write %s"%file
  print "write %s"%file




class ImageMaker():
  
  def __init__(self,directory='png',bname='img'):
    self.directory=directory
    self.bname=bname
    self.counter = 0
    
  def dump(self,pos):
    nb = createnbodyfromvec(pos)
    dumpimage(nb,self.directory,self.bname,self.counter)
    self.counter=self.counter+1

  def info(self):
    print "info "
    print "---- "
    print "counter =",self.counter


# to test
#pos = random.random((1000,3))*10
#print pos
#nb=createnbodyfromvec(pos)
#dumpimage(nb,'png','img',0)


#pos = random.random((1000,3))*10
#imgMk = ImageMaker()
#imgMk.dump(pos)
