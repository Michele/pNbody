#!/usr/bin/env python

'''
 @package   pNbody
 @file      plot_spherical_profiles.py
 @brief     Plot theoretical profiles
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt
from numpy import *

from pNbody import profiles

rs		= 10.
gamma		= 1.
Rmax		= 10.
rho0		= 1.
a = 0.
b = 3.

r = arange(0.1,Rmax,0.1)


rho_nfw       = profiles.nfw_profile(r,rs)
rho_jaffe     = profiles.jaffe_profile(r,rs)
rho_hernquist = profiles.hernquist_profile(r,rs)
rho_burkert   = profiles.burkert_profile(r,rs)
rho_plummer   = profiles.plummer_profile(r,rs)
rho_nfwg      = profiles.nfwg_profile(r,rs,gamma)  
rho_generic2c = profiles.generic2c_profile(r,rs,a,b)

pt.subplot(2,1,1)
pt.plot(r,rho_nfw,'k')
#pt.plot(r,rho_jaffe,'r')
#pt.plot(r,rho_burkert,'b')
#pt.plot(r,rho_nfwg,'y')
#pt.plot(r,rho_hernquist,'c')
#pt.plot(r,rho_plummer,'g')
pt.plot(r,rho_generic2c,'ro')

#pt.semilogx()
#pt.semilogy()
pt.loglog()
pt.xlabel('Radius')
pt.ylabel('Density')



mr_nfw       = profiles.nfw_mr(r,rs)
mr_jaffe     = profiles.jaffe_mr(r,rs)
mr_hernquist = profiles.hernquist_mr(r,rs)
mr_burkert   = profiles.burkert_mr(r,rs)
mr_plummer   = profiles.plummer_mr(r,rs)
mr_nfwg      = profiles.nfwg_mr(r,rs,gamma)  
mr_generic2c = profiles.generic2c_mr(r,rs,a,b)

pt.subplot(2,1,2)
pt.plot(r,mr_nfw,'k')
#pt.plot(r,mr_jaffe,'r')
#pt.plot(r,mr_burkert,'b')
#pt.plot(r,mr_nfwg,'y')
#pt.plot(r,mr_hernquist,'c')
#pt.plot(r,mr_plummer,'g')
pt.plot(r,mr_generic2c,'ro')

#pt.semilogx()
#pt.semilogy()
pt.xlabel('Radius')
pt.ylabel('Mass in Radius')

pt.show()








