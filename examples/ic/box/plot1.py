#!/usr/bin/env python


'''
 @package   pNbody
 @file      plot1.py
 @brief     Do a plot
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt
from numpy import *

y = arange(0,1,0.001)

rho1 = 1.
rho2 = 2.
sigma = 0.025
y1 = 0.50

rho = rho1 + (rho2-rho1)/( (1+exp(-2.*(y-y1)/sigma)) )


pt.plot(y,rho)
pt.show()
