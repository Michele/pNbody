#!/usr/bin/env python
'''
 @package   pNbody
 @file      plot.py
 @brief     Do a plot
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt
from numpy import *

y = arange(0,1,0.01)

rho1 = 1.
rho2 = 2.
sigma = 0.0025
y1 = 0.25
y2 = 0.75

#rho = rho1 + (rho2-rho1)/( (1+exp(-2.*(y-y1)/sigma)) * (1+exp(2.*(y-y2)/sigma)) )
rho = rho1 + (rho2-rho1)/( (1+exp(-2.*(y-y1)/sigma)) )


pt.plot(y,rho)
pt.show()
