#!/usr/bin/env python

'''
 @package   pNbody
 @file      example01.py
 @brief     generate particles in a box
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


import Ptools as pt
from pNbody import *
from pNbody import ic
from pNbody import pygsl

###################
# set a function

y = arange(0,1,0.001)

rho1 = 1.
rho2 = 2.
y1 = 0.5
sigma = 0.00025

rho = rho1 + (rho2-rho1)/( (1+exp(-2.*(y-y1)/sigma)) )


###################
# cumulative mass

Mys = add.accumulate(rho)

#pt.plot(y,Mys)
#pt.show()


###################
# create a grid

Xmax = 1.
Rs   = y
Mrs  = Mys


###################
# generate particles

n = 1000000
n = pygsl.sobol_sequence(n,3)

nb = ic.generic_Mx(n,xmax=Xmax,x=Rs,Mx=Mrs,name='box.dat',ftype='gadget')

nb.write()
