'''
 @package   pNbody
 @file      plot.py
 @brief     Do a plot
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


import Ptools as pt
from numpy import *

t = arange(-14,0,0.1)

#t0 = -5
#ts = 1.0
#x = exp( -(t-t0)**2 /ts**2 )


x = random.normal(-3,1,10000)
x = clip(x,-5,0)



pt.hist(x)
pt.show()
