#!/usr/bin/env python

'''
 @package   pNbody
 @file      addmetals.py
 @brief     Add metals to a snapshot
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from optparse import OptionParser
from PyChem import chemistry


########################################  
#					  
# parser				  
#					  
######################################## 



def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)
  
  parser.add_option("-t",
		   action="store", 
		   dest="ftype",
		   type="string",
		   default = 'gadget',		   
		   help="type of the file",	 
		   metavar=" TYPE")    

  parser.add_option("-o",
		   action="store", 
		   dest="outputfile",
		   type="string",
		   default = None,		   
		   help="outputfile name",	 
		   metavar=" STING")   

  parser.add_option("--exec",
		   action="store", 
		   dest="execline",
		   type="string",
		   default = None,		   
		   help="give command to execute before",	 
		   metavar=" STRING")   
	
	
  parser.add_option("-p",
		   action="store", 
		   dest="chimieparameterfile",
		   type="string",
		   default = "chimie.dat",		   
		   help="chimie parameter file",	 
		   metavar=" STING")   	
	
	
  parser.add_option("--NumberOfTables",
                    action="store", 
                    dest="NumberOfTables",
                    type="int",
                    default = 1,                    
                    help="NumberOfTables",       
                    metavar=" INT") 

  parser.add_option("--DefaultTable",
                    action="store", 
                    dest="DefaultTable",
                    type="int",
                    default = 0,                    
                    help="DefaultTable",       
                    metavar=" INT") 
	
		    		    		    
  (options, args) = parser.parse_args()
   
  if len(args) == 0:
    print "you must specify a filename"
    sys.exit(0)
    file = None
  else:  
    file = args  
  
  return file,options


     
################################################################################
#
#                                    MAIN
#
################################################################################

file,opt = parse_options()

#########################
# open nbody file

nb = Nbody(file,ftype=opt.ftype) 

if opt.execline != None:
  exec(opt.execline)
  
# set internal energy
system_of_units = units.Set_SystemUnits_From_File('params')

out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Msol,units.Unit_Gyr,units.Unit_K])
ToGyr = system_of_units.convertionFactorTo(out_units.UnitTime)
FromGyr = 1/ToGyr

####################################
# convert disk and bulge to stars1 
nb0 = nb.select(0)  
nb1 = nb.select(1)  
nb2 = nb.select(2)   
nb3 = nb.select(3)   
nb4 = nb.select(4)  
nb5 = nb.select(5)  

nb3.set_tpe(1)
nb4.set_tpe(1)
nb5.set_tpe(1)

nb = nb0 + nb3 + nb4 + nb5 + nb2
del nb0,nb1,nb2,nb3,nb4,nb5

nb.init()


#########################
# open chimie file


chemistry.init_chimie(opt.chimieparameterfile,opt.NumberOfTables,opt.DefaultTable)

#########################
# set the chimie extra-header
nb.flag_chimie_extraheader = 1
nb.ChimieNelements	   = int(chemistry.get_nelts())
nb.ChimieElements	   = chemistry.get_elts_labels()
nb.ChimieSolarMassAbundances   = chemistry.get_elts_SolarMassAbundances()


NELEMENTS = nb.ChimieNelements
nb.flag_metals=NELEMENTS

#########################
# global init
nb.rho    = ones(nb.nbody).astype(float32)
nb.rsp    = ones(nb.nbody).astype(float32)	# must be!=0 with CHIMIE_INPUT_ALL
nb.metals = zeros((nb.nbody,NELEMENTS)).astype(float32)

#########################
# variables for the gas
# Fe
Fe = -1 + nb.rxy()*(-1/10.)  
Fe = Fe.astype(float32)
metals = nb.ChimieSolarMassAbundances['Fe'] * (10**Fe - 1.0e-20)
nb.metals[:,0] = where(nb.tpe==0, metals,nb.metals[:,0] )


#########################
# variables for the stars

nb.flag_age=1

# age (formation time)
nb.tstar = zeros(nb.nbody).astype(float32)
tstar = clip(  random.normal(-3,1,nb.nbody) ,-5,0) * FromGyr
tstar = tstar.astype(float32)
nb.tstar = where(nb.tpe==1, tstar,nb.tstar )

# minit (initial mass)
nb.minit = zeros(nb.nbody).astype(float32)
minit = nb.mass		# !!!
minit = minit.astype(float32)
nb.minit = where(nb.tpe==1, minit,nb.minit )

# idp (stellar index)
nb.idp = zeros(nb.nbody).astype(float32)
idp = -1 * ones(nb.nbody)
idp = idp.astype(int32)
nb.idp = where(nb.tpe==1, idp,nb.idp )





# Fe
Fe = -1.5 + nb.rxy()*(-1/10.)  
Fe = Fe.astype(float32)
metals = nb.ChimieSolarMassAbundances['Fe'] * (10**Fe - 1.0e-20)
nb.metals[:,0] = where(nb.tpe==1, metals,nb.metals[:,0] )






#########################
# write outputs
if opt.outputfile!=None:
  nb.rename(opt.outputfile)
  nb.write()




