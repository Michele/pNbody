#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


from pNbody import *
import sys
import iclib

n = 100000

#pos = iclib.exponential_disk(n,4,0.9,200,3,1)
#pos = iclib.miyamoto_nagai(n,3,0.3,300,30,-1)
pos = iclib.generic_alpha(n,-2,0.5,10,-1)

pos = pos.astype(float32)




nb = Nbody('miyam.dat',ftype='gadget',pos=pos,status='new')
nb.npart = array([0,0,n,0,0,0])
nb.npart_tot = nb.npart
nb.write()

