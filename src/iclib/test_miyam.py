#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_miyam.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import ic
from pNbody import libgrid
from pNbody import libmiyamoto
import SM

G = 1.0

N_gas = 100000

M_gas    = 8.0
Hr_gas   = 3.
Hz_gas   = 0.3
fr_gas   = 10.
fz_gas   = 10.


# cylindrical grid
nR = 64
nz = 128+1		
Rmax =  45.0
zmin = -3.0
zmax =  3.0

nb_gas =  ic.miyamoto_nagai(N_gas,Hr_gas,Hz_gas,fr_gas*Hr_gas,fz_gas*Hz_gas,irand=-2,ftype='gadget')
nb_gas.npart = array([nb_gas.nbody,0,0,0,0,0])
nb_gas.npart_tot = nb_gas.npart
nb_gas.mass = (M_gas/N_gas) * ones(nb_gas.nbody).astype(Float32)
nb_gas.write()



# compute Sdens
Sden = libgrid.get_SurfaceDensityMap_From_Cylindrical_2dv_Grid(nb_gas,nR,nz,Rmax,zmin,zmax)
R,z  = libgrid.get_rz_Of_Cylindrical_2dv_Grid(nR,nz,Rmax,zmin,zmax)
print R

# plot

g = SM.plot()
g.limits(0,Rmax,log10(Sden))
g.box()
g.connect(R,log10(Sden))


# semi-analytical value
sd1 = array([],Float)
for r in R:
  sd1 = concatenate((sd1,libmiyamoto.SurfaceDensity(G,M_gas,Hr_gas,Hz_gas,r)))    

g.ctype('green')
g.connect(R,log10(sd1))

g.show()



