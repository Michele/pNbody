#!/usr/bin/env python
'''
 @package   pNbody
 @file      test01.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import ic
import time
import treelib

import Ptools as pt

ErrTolTheta = 0.7
eps = 0.1

# open a model
#nb = Nbody("../../examples/snap.dat",ftype='gadget')
#nb = nb.selectc(nb.rxyz()<2)
random.seed(1)
nb = ic.plummer(1000000,1,1,1,5,20)

MaxPartPerNode=1

# create a tree object
print "start tree"
MyTree = treelib.Tree(npart=array(nb.npart),pos=nb.pos,vel=nb.vel,mass=nb.mass,ErrTolTheta=ErrTolTheta,MaxPartPerNode=MaxPartPerNode)
#MyTree = treelib.Tree(npart=array(nb.npart),pos=nb.pos,vel=nb.vel,mass=nb.mass,ErrTolTheta=ErrTolTheta)
print "tree done."


R = arange(-20,20,0.1)
print len(R)
pos = concatenate((R,zeros(len(R)),zeros(len(R))))
pos.shape = (3,len(R))
pos = transpose(pos)
pos = pos.astype(float32)


#pos = array([[0,0,0]])
#pos = pos.astype(float32)


pot = MyTree.Potential(pos,eps)
#pot = MyTree.Potential(nb.pos,eps)

#print pot

print len(pot),len(pos),len(R)

#pt.plot(R,pot)
#pt.scatter(nb.x(),pot)
#pt.show()
