#!/usr/bin/env python
'''
 @package   pNbody
 @file      txt2gadget.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from optparse import OptionParser
from pNbody import *
import Ptools as pt

########################################  
#					  
# parser				  
#					  
######################################## 



def parse_options():

  usage = "usage: %prog [options] file"
  parser = OptionParser(usage=usage)
  
  parser.add_option("-t",
		   action="store", 
		   dest="ftype",
		   type="string",
		   default = 'gadget',		   
		   help="type of the file",	 
		   metavar=" TYPE")    

  parser.add_option("-o",
		   action="store", 
		   dest="outputfile",
		   type="string",
		   default = 'snap.dat',		   
		   help="name of the output file",	 
		   metavar=" FILE")    
		    		    		    
  (options, args) = parser.parse_args()
   
  if len(args) == 0:
    #print "you must specify a filename"
    #sys.exit(0)
    file = None
  else:  
    file = args  
  
  return file,options



  
      
################################################################################
#
#                                    MAIN
#
################################################################################

files,opt = parse_options()



# read input file
x,y,z=pt.io.read_ascii(files[0],[0,1,2])


n = len(x)
pos = transpose(array([x,y,z]))
vel =  zeros([n,3])*0.0
mass = ones([n])*1./n

nb = Nbody(status='new',p_name=opt.outputfile,pos=pos,vel=vel,mass=mass,ftype=opt.ftype)
nb.write()




