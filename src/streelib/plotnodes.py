#!/usr/bin/env python
'''
 @package   pNbody
 @file      plotnodes.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt
from numpy import *
from pNbody import *

l,cx,cy,cz = pt.io.read_ascii("qq",[0,1,2,3])

c = arange(len(l)) < len(l)
l  = compress(c,l)
cx = compress(c,cx)
cy = compress(c,cy)
cz = compress(c,cz)


# create an Nbody object
pos = transpose(array([cx,cy,cz]))
pos = pos.astype(float32)

nb = Nbody(status='new',p_name='tree.dat',pos=pos,ftype='gadget')
nb.write()


pt.scatter(cx,cy)

for i in xrange(len(cx)):
  
  bx = zeros(5)
  by = zeros(5)
  
  bx[0] = cx[i] - l[i]*0.5 
  bx[1] = cx[i] + l[i]*0.5 
  bx[2] = cx[i] + l[i]*0.5 
  bx[3] = cx[i] - l[i]*0.5 
  bx[4] = cx[i] - l[i]*0.5 

  by[0] = cy[i] - l[i]*0.5 
  by[1] = cy[i] - l[i]*0.5 
  by[2] = cy[i] + l[i]*0.5 
  by[3] = cy[i] + l[i]*0.5 
  by[4] = cy[i] - l[i]*0.5 
  

  pt.plot(bx,by)



pt.axis([-50,50,-50,50])
pt.show()



