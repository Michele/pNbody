# -*- coding: utf-8 -*-
'''
 @package   pNbody
 @file      examples-omp.py
 @brief     Examples of mapping with OMP
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import io
import mapping
import time

nb = Nbody('../../examples/snap.dat',ftype='gadget')



mx = max(nb.rxyz())

kx = 512
ky = 512
kz = 512

pos = ( nb.pos + [mx,mx,mx] )/(2*mx)		# between 0,1
pos = pos.astype(float32)

rsp = ones(nb.nbody)*30
rsp = rsp.astype(float32)


#print "mkmap1dn"
#mat = mapping.mkmap1dn(pos[0,:],nb.mass,nb.mass,(kx,))
#print "mkmap2dn"
#mat = mapping.mkmap2dn(pos,nb.mass,nb.mass,(kx,ky))
#print "mkmap3dn"
#mat = mapping.mkmap3dn(pos,nb.mass,nb.mass,(kx,ky,kz))



print "mkmap2dnsph"
t0 = time.time()
mat0 = mapping.mkmap2dnsph(pos,nb.mass,nb.mass,rsp,(kx,ky),1)
t1 = time.time()
print t1-t0

print "mkmap2dnsph"
t0 = time.time()
mat1 = mapping.mkmap2dnsph(pos,nb.mass,nb.mass,rsp,(kx,ky),8)
t1 = time.time()
print t1-t0

#print "mkmap2dnsph"
#c = argsort(pos[:,0])
#pos = take(pos,c,axis=0)
#nb.mass = take(nb.mass,c)
#mat1 = mapping.mkmap2dnsph(pos,nb.mass,nb.mass,rsp,(kx,ky),1)

mat = mat1 - mat0
print "0     ",min(ravel(mat0)),max(ravel(mat0)),sum(ravel(mat0))
print "1     ",min(ravel(mat1)),max(ravel(mat1)),sum(ravel(mat1))
print "1-0   ",min(ravel(mat)), max(ravel(mat)), sum(ravel(mat))
print "diff %",100*fabs(sum(ravel(mat1))-sum(ravel(mat0))) / sum(ravel(mat0))


file = "out0.fits"
if os.path.exists(file):
  os.remove(file)
io.WriteFits(mat0,file)

file = "out1.fits"
if os.path.exists(file):
  os.remove(file)
io.WriteFits(mat1,file)

file = "out.fits"
if os.path.exists(file):
  os.remove(file)
io.WriteFits(mat,file)

#print "mkmap1dw"
#mat = mapping.mkmap1dw(pos[0,:],nb.mass,nb.mass,(kx,))
#print "mkmap2dw"
#mat = mapping.mkmap2dw(pos,nb.mass,nb.mass,(kx,ky))
#print "mkmap3dw"
#mat = mapping.mkmap3dw(pos,nb.mass,nb.mass,(kx,ky,kz))




#mat = mapzero
#mat = mapzerosph
#mat = mapone
#mat = mapn


#mat = mapping.mkmap3dslicesph()
#mat = mapping.mkmap3dsortedsph()
