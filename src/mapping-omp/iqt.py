'''
 @package   pNbody
 @file      iqt.py
 @brief    
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from PyQt4 import QtCore, QtGui
from numpy import *
import Image, ImageQt
import sys

from pNbody import libutil


mat = zeros((256,256), dtype = "uint8" ) 

x,y =indices((256,256))

mat = x*y


mat = where(x>200,0,mat)
mat = where(y>50,0,mat)

#mat = flipud(mat)
mat = fliplr(mat)


matint,mn_opt,mx_opt,cd_opt = libutil.set_ranges(mat,scale='lin',cd=0,mn=0,mx=0)

matint = matint.astype(uint8)
imagePIL = Image.fromarray(matint)


# palette...
from pNbody.palette import *
palette = Palette('light') 															 
imagePIL.putpalette(palette.palette)															 


# important to make things work
imagePIL = imagePIL.convert('RGB')
imagePIL.save('qq.jpg')



mat = asarray(imagePIL)

print mat.dtype
print mat.shape



#########################
# from an image
#########################
#imagePIL = Image.open('qq.jpg')
#mat = asarray(imagePIL)
#imagePIL = Image.fromarray(mat)
#########################


imageQt  = ImageQt.ImageQt(imagePIL)

app = QtGui.QApplication(sys.argv)
imageLabel = QtGui.QLabel()
imageLabel.setScaledContents(True)
imageLabel.setPixmap(QtGui.QPixmap.fromImage(imageQt))  
imageLabel.show()
#sys.exit(app.exec_())
app.exec_()











