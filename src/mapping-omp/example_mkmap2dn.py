'''
 @package   pNbody
 @file      example_mkmap2dn.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import io
import mapping

nb = Nbody('../../examples/snap.dat',ftype='gadget')



mx = max(nb.rxyz())

kx = 256
ky = 256
kz = 256

pos = ( nb.pos + [mx,mx,mx] )/(2*mx)		# between 0,1
#pos = pos*[kx,ky,kz]
pos = pos.astype(float32)

mat = mapping.mkmap2dn(pos,nb.mass,nb.mass,(kx,ky))



#io.WriteFits(mat,'qq.fits')
mplot(mat)



