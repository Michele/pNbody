'''
 @package   pNbody
 @file      example.py
 @brief     Example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
import mapping

nb = Nbody('/home/revaz/work/pNbody/disk.dat',ftype='gadget')



mx = max(nb.rxyz())

kx = 32
ky = 32
kz = 32

pos = ( nb.pos + [mx,mx,mx] )/(2*mx)		# between 0,1
pos = pos*[kx,ky,kz]
pos = pos.astype(Float32)


mat = mapping.mkmap3d(pos,nb.mass,nb.mass,(kx,ky,kz))
print mat
print mat.shape
