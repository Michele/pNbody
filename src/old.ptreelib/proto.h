#ifndef PROTO 
#define PROTO

#include "ptreelib.h"

void domain_allocate(Tree *self);
void domain_deallocate(Tree *self);
void domain_Decomposition(Tree *self);
void domain_decompose(Tree *self);
int domain_findSplit(Tree *self, int cpustart, int ncpu, int first, int last);
void domain_shiftSplit(Tree *self);
void domain_findExchangeNumbers(Tree *self,int task, int partner, int sphflag, int *send, int *recv);
void domain_exchangeParticles(Tree *self,int partner, int sphflag, int send_count, int recv_count);
void domain_countToGo(Tree *self);
void domain_findExtent(Tree *self);
void domain_determineTopTree(Tree *self);
void domain_topsplit_local(Tree *self,int node, peanokey startkey);
void domain_topsplit(Tree *self,int node, peanokey startkey);
void domain_walktoptree(Tree *self,int no);
void domain_sumCost(Tree *self);
int domain_compare_toplist(const void *a, const void *b);
int domain_compare_key(const void *a, const void *b);

void force_treeallocate(Tree *self,int maxnodes, int maxpart);
void force_treefree(Tree *self);
int force_treebuild(Tree *self,int npart);
int force_treebuild_single(Tree *self,int npart);
void force_create_empty_nodes(Tree *self,int no, int topnode, int bits, int x, int y, int z, int *nodecount,int *nextfree);
void force_insert_pseudo_particles(Tree *self);
void force_update_node_recursive(Tree *self,int no, int sib, int father);
void force_update_pseudoparticles(Tree *self);
void force_exchange_pseudodata(Tree *self);
void force_treeupdate_pseudos(Tree *self);
void force_flag_localnodes(Tree *self);
int force_treeevaluate(Tree *self,int target, int mode, double *ewaldcountsum);
int force_treeevaluate_sub(Tree *self,int target, int mode, double *ewaldcountsum);
void force_treeevaluate_potential(Tree *self,int target, int mode);
void force_treeevaluate_potential_sub(Tree *self,int target, int mode);

void peano_hilbert_order(Tree *self);
int compare_key(const void *a, const void *b);
void reorder_gas(Tree *self);
void reorder_particles(Tree *self);
peanokey peano_hilbert_key(int x, int y, int z, int bits);


void ngb_treeallocate(Tree *self,int npart);
void ngb_treefree(Tree *self);

void gravity_tree(Tree *self);
void gravity_tree_sub(Tree *self);
void set_softenings(Tree *self);
int grav_tree_compare_key(const void *a, const void *b);


double dmax(double x, double y);
double dmin(double x, double y);
int imax(int x, int y);
int imin(int x, int y);

void compute_potential(Tree *self);
void compute_potential_sub(Tree *self);

#endif
