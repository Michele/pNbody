'''
 @package   pNbody
 @file      test2.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from mpi4py import MPI
import ptreelib

from pNbody import *

nb = Nbody('../../examples/disk.dat',ftype='gadget')
nb.Tree = ptreelib.Tree(npart=nb.npart,pos=nb.pos,vel=nb.vel,mass=nb.mass,num=nb.num,tpe=nb.tpe)

nb.ExchangeParticles()



nb.Tree.SetParticles(nb.npart,nb.pos,nb.vel,nb.mass,nb.num,nb.tpe) 


nb.Tree.BuildTree()


Pot = nb.Tree.Potential(array([[0,0,0]],float32),0.1) 
acc = nb.Tree.Acceleration(array([[0,0,0]],float32),0.1) 

print Pot
print acc
