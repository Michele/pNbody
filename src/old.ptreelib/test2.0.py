'''
 @package   pNbody
 @file      test2.0.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from mpi4py import MPI
import ptreelib

from pNbody import *

nb = Nbody('../../examples/disk.dat',ftype='gadget')



Pot = nb.TreePot(array([[0,0,0]],float32),0.1) 
acc = nb.TreeAccel(array([[0,0,0]],float32),0.1) 

print Pot
print acc
