#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_IsoContours.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import tessel

import Ptools as pt
import plot

import sys
import time

random.seed(0)			# 2 for two points

n = 1000
pos = random.random((n,3))
mass = random.random(n)*0.1 + pos[:,0]*pos[:,1]
val = mean(mass)

print "Start with Delaunay"
tessel.ConstructDelaunay(pos,mass)
print "Done with Delaunay"

print "Start with IsoContours"
x,y = tessel.ComputeIsoContours(val)
print "Done with IsoContours"

x = array(x)
y = array(y)

print len(x)


'''
TriangleList = tessel.GetTriangles()
i = 0
for Triangle in TriangleList:
  P1 = Triangle['coord'][0]
  P2 = Triangle['coord'][1]
  P3 = Triangle['coord'][2]
  plot.draw_triangle(P1,P2,P3)  
'''


#pt.scatter(pos[:,0],pos[:,1],c=mass,s=10)
#pt.scatter(x,y,c='r')

for i in arange(len(x)/2)*2:
  pt.plot(x[i:i+2],y[i:i+2],'r')

pt.plot(x[i:i+2],y[i:i+2],'r')




#pt.axis([0,2,0,2])
pt.show()


