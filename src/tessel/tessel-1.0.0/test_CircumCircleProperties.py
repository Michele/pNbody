#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_CircumCircleProperties.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import tessel

import Ptools as pt
import plot




P1 = array([0,0,0])
P2 = array([1,0,0])
P3 = array([1,1,0])


P1 = random.random(3)
P2 = random.random(3)
P3 = random.random(3)



#tessel.test()


r,xc,yc = tessel.CircumCircleProperties(P1,P2,P3)

print r,xc,yc


plot.draw_points([P1,P2,P3])
plot.draw_circle(r,xc,yc)

pt.show()




