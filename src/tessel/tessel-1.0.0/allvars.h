/*! \file allvars.h
 *  \brief declares global variables.
 *
 *  This file declares all global variables. Further variables should be added here, and declared as
 *  'extern'. The actual existence of these variables is provided by the file 'allvars.c'. To produce
 *  'allvars.c' from 'allvars.h', do the following:
 *
 *     - Erase all #define's, typedef's, and enum's
 *     - add #include "allvars.h", delete the #ifndef ALLVARS_H conditional
 *     - delete all keywords 'extern'
 *     - delete all struct definitions enclosed in {...}, e.g.
 *        "extern struct global_data_all_processes {....} All;"
 *        becomes "struct global_data_all_processes All;"
 */


#ifndef ALLVARS_H
#define ALLVARS_H



#define  MAX_REAL_NUMBER  1e+37
#define  MIN_REAL_NUMBER  1e-37


#define TO_DOUBLE(a)        ( (PyArrayObject*) PyArray_CastToType(a, PyArray_DescrFromType(NPY_DOUBLE)  ,0) )

#define MAXNUMTRIANGLES 10000000


#define PI 3.1415926535897931

struct global_data_all_processes
{
  int MaxPart;			/*!< This gives the maxmimum number of particles that can be stored on one processor. */
}
  All;

/*! This structure holds all the information that is
 * stored for each particle of the simulation.
 */

struct Point		/* struct particle_data */
  {
    double Pos[3];			/*!< particle position at its current time */
    double Mass;
    int    IsDone;
  }
 *P;              /*!< holds particle data on local processor */






struct Triangle
  {
    struct Point Pt1[3];
    struct Point Pt2[3];
    struct Point Pt3[3];
  };



struct TriangleInList
  {
    int    idx;            		/* index of current triangle (used for checks) */
    struct Point* P[3];              	/* pointers towards the 3  point */
    struct TriangleInList* T[3];      	/* pointers towards the 3  triangles */
    int    idxe[3];			/* index of point in the first  triangle, opposite to the common edge */
    struct Median* Med[3];
  };


struct Median
  {
    double    		a; 
    double    		b;
    double    		c;			     
    struct Point	Ps;	     /* starting point of a segment */
    struct Point	Pe;	     /* stopping point */
  };




/* some global varables */
extern int ThisTask;		/*!< the rank of the local processor */
extern int NTask;               /*!< number of processors */
extern int PTask;	        /*!< smallest integer such that NTask <= 2^PTask */

extern int NumPart;		/*!< number of particles on the LOCAL processor */
extern int nT;
extern int numTinStack;

struct TriangleInList Triangles[MAXNUMTRIANGLES];	/* list of triangles               */
struct TriangleInList *TStack[MAXNUMTRIANGLES];				/* index of triangles to check	   */
struct Median MediansList[MAXNUMTRIANGLES][3];


double domainRadius,domainCenter[3];


struct Point Pe[3];					/* edges */
	
	
	




#endif
