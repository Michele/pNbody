#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_InTriangleOrOutside.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import tessel

import Ptools as pt
import plot

import sys





P1 = random.random(3)
P2 = random.random(3)
P3 = random.random(3)
P4 = random.random(3)

b = tessel.InTriangleOrOutside(P1,P2,P3,P4)

print b
    


plot.draw_triangle(P1,P2,P3)
plot.draw_points([P1],'r')
plot.draw_points([P2],'g')
plot.draw_points([P3],'b')
plot.draw_points([P4])
pt.show()


