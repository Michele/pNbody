/*! \file allvars.c
 *  \brief provides instances of all global variables.
 */

#include <stdio.h>
#include "allvars.h"



/* some global varables */
int ThisTask;		/*!< the rank of the local processor */
int NTask;               /*!< number of processors */
int PTask;	        /*!< smallest integer such that NTask <= 2^PTask */

int NumPart;		/*!< number of particles on the LOCAL processor */
int nT;
int numTinStack;


double domainRadius,domainCenter[3];


