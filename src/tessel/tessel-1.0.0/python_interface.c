#include <Python.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <numpy/libnumarray.h>

#include "allvars.h"
#include "proto.h"




/************************************************************/
/*  PYTHON INTERFACE                                        */
/************************************************************/


static PyObject *
      tessel_TriangleMedians(self, args)
          PyObject *self;
          PyObject *args;
      {


        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	  
        struct Point Pt1,Pt2,Pt3;	
	struct Point Pmm1,Pmm2,Pmm3,Pme1,Pme2,Pme3;


        if (!PyArg_ParseTuple(args,"OOO",&p1,&p2,&p3))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) )
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) )
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     
	
        TriangleMedians(Pt1,Pt2,Pt3,&Pmm1,&Pmm2,&Pmm3,&Pme1,&Pme2,&Pme3);
	
	
	
	/* create the outputs */
	PyArrayObject *aPmm1,*aPmm2,*aPmm3,*aPme1,*aPme2,*aPme3;
	npy_intp   ld[1];     
	ld[0]=3;
	
	aPmm1 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	aPmm2 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	aPmm3 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	aPme1 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	aPme2 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	aPme3 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	
	
	*(double *) (aPmm1->data + 0*(aPmm1->strides[0])) = Pmm1.Pos[0];
	*(double *) (aPmm1->data + 1*(aPmm1->strides[0])) = Pmm1.Pos[1];
	*(double *) (aPmm1->data + 2*(aPmm1->strides[0])) = 0;

	*(double *) (aPmm2->data + 0*(aPmm2->strides[0])) = Pmm2.Pos[0];
	*(double *) (aPmm2->data + 1*(aPmm2->strides[0])) = Pmm2.Pos[1];
	*(double *) (aPmm2->data + 2*(aPmm2->strides[0])) = 0;
	
	*(double *) (aPmm3->data + 0*(aPmm3->strides[0])) = Pmm3.Pos[0];
	*(double *) (aPmm3->data + 1*(aPmm3->strides[0])) = Pmm3.Pos[1];
	*(double *) (aPmm3->data + 2*(aPmm3->strides[0])) = 0;
	
	
	*(double *) (aPme1->data + 0*(aPme1->strides[0])) = Pme1.Pos[0];
	*(double *) (aPme1->data + 1*(aPme1->strides[0])) = Pme1.Pos[1];
	*(double *) (aPme1->data + 2*(aPme1->strides[0])) = 0;

	*(double *) (aPme2->data + 0*(aPme2->strides[0])) = Pme2.Pos[0];
	*(double *) (aPme2->data + 1*(aPme2->strides[0])) = Pme2.Pos[1];
	*(double *) (aPme2->data + 2*(aPme2->strides[0])) = 0;
	
	*(double *) (aPme3->data + 0*(aPme3->strides[0])) = Pme3.Pos[0];
	*(double *) (aPme3->data + 1*(aPme3->strides[0])) = Pme3.Pos[1];
	*(double *) (aPme3->data + 2*(aPme3->strides[0])) = 0;	
	
	
	return Py_BuildValue("(OOOOOO)",aPmm1,aPmm2,aPmm3,aPme1,aPme2,aPme3);

      }               






static PyObject *
      tessel_CircumCircleProperties(self, args)
          PyObject *self;
          PyObject *args;
      {


        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	  
        struct Point Pt1,Pt2,Pt3;
	double xc,yc,r;


        if (!PyArg_ParseTuple(args,"OOO",&p1,&p2,&p3))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) )
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) )
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     


        r = CircumCircleProperties(Pt1,Pt2,Pt3,&xc,&yc);


        return Py_BuildValue("(ddd)",r,xc,yc);

      }               







static PyObject *
      tessel_InTriangle(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	PyArrayObject *p4 = NULL;
	  
        struct Point Pt1,Pt2,Pt3,Pt4;
	struct Triangle T;
	int b;


        if (!PyArg_ParseTuple(args,"OOOO",&p1,&p2,&p3,&p4))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3) && PyArray_Check(p4)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) || (p4->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) || (p4->dimensions[0]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     

	Pt4.Pos[0] = *(double *) (p4->data + 0*(p4->strides[0]));
	Pt4.Pos[1] = *(double *) (p4->data + 1*(p4->strides[0]));

        T = MakeTriangleFromPoints(Pt1,Pt2,Pt3);
	T = OrientTriangle(T);
        
	b = InTriangle(T,Pt4);
	


        return Py_BuildValue("i",b);

      }               



static PyObject *
      tessel_InTriangleOrOutside(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	PyArrayObject *p4 = NULL;
	  
        struct Point Pt1,Pt2,Pt3,Pt4;
	struct Triangle T;
	int b;


        if (!PyArg_ParseTuple(args,"OOOO",&p1,&p2,&p3,&p4))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3) && PyArray_Check(p4)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) || (p4->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) || (p4->dimensions[0]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     

	Pt4.Pos[0] = *(double *) (p4->data + 0*(p4->strides[0]));
	Pt4.Pos[1] = *(double *) (p4->data + 1*(p4->strides[0]));

        T = MakeTriangleFromPoints(Pt1,Pt2,Pt3);
	T = OrientTriangle(T);
        
	b = InTriangleOrOutside(T,Pt4);
	


        return Py_BuildValue("i",b);

      }               



static PyObject *
      tessel_InCircumCircle(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	PyArrayObject *p4 = NULL;
	  
        struct Point Pt1,Pt2,Pt3,Pt4;
	struct Triangle T;
	int b;


        if (!PyArg_ParseTuple(args,"OOOO",&p1,&p2,&p3,&p4))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3) && PyArray_Check(p4)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) || (p4->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) || (p4->dimensions[0]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     

	Pt4.Pos[0] = *(double *) (p4->data + 0*(p4->strides[0]));
	Pt4.Pos[1] = *(double *) (p4->data + 1*(p4->strides[0]));


        T = MakeTriangleFromPoints(Pt1,Pt2,Pt3);
	T = OrientTriangle(T);
        
	b = InCircumCircle(T,Pt4);
	


        return Py_BuildValue("i",b);

      }               



static PyObject *
      tessel_ConstructDelaunay(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *pos = NULL;
	PyArrayObject *mass = NULL;
	  
	int i,j;



        if (!PyArg_ParseTuple(args,"OO",&pos,&mass))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(pos)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments 1 must be array.");
	    return NULL;		  
	  } 

	/* check type */  
	if (!(PyArray_Check(mass)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments 2 must be array.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (pos->nd!=2))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of argument 1 must be 2.");
	    return NULL;		  
	  } 	  

	/* check dimension */	  
	if ( (mass->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of argument 2 must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (pos->dimensions[1]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"First size of argument must be 3.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (pos->dimensions[0]!=mass->dimensions[0]))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of argument 1 must be similar to argument 2.");
	    return NULL;		  
	  } 	  

	     			   			    
	/* ensure double */	    
	pos = TO_DOUBLE(pos);	
        mass = TO_DOUBLE(mass);	      
        NumPart = pos->dimensions[0];
	
	/* add first triangle */
	
        /* init */
	All.MaxPart = NumPart;
        
	
        /* allocate memory */
        allocate_memory();
       

        /* init P */
	/* loop over all points */

       for (i=0;i<NumPart;i++)
         {
   	   P[i].Pos[0] = *(double *) (pos->data + i*(pos->strides[0]) + 0*pos->strides[1]);
	   P[i].Pos[1] = *(double *) (pos->data + i*(pos->strides[0]) + 1*pos->strides[1]);
	   P[i].Pos[2] = *(double *) (pos->data + i*(pos->strides[0]) + 2*pos->strides[1]);
	   P[i].Mass   = *(double *) (mass->data + i*(mass->strides[0])                  );	   
	 }

        /* find domain extent */
	FindExtent();
	
        
	/* set edges */	
	for (j=0;j<3;j++)
	  {
	    Pe[j].Pos[0] = domainCenter[0] + domainRadius * cos(2./3.*PI*j);
	    Pe[j].Pos[1] = domainCenter[1] + domainRadius * sin(2./3.*PI*j);
	    Pe[j].Pos[2] = 0;
	    Pe[j].Mass   = 0;
	  }
	          
	/* Triangle list */
	Triangles[0].idx = 0;        
	Triangles[0].P[0]     = &Pe[0];
	Triangles[0].P[1]     = &Pe[1];
	Triangles[0].P[2]     = &Pe[2];
	Triangles[0].T[0]     = NULL;  
	Triangles[0].T[1]     = NULL;  
	Triangles[0].T[2]     = NULL;  
	Triangles[0].idxe[0]  = -1;  
	Triangles[0].idxe[1]  = 1;  
	Triangles[0].idxe[2]  = -1;  
	nT++;
	OrientTriangleInList(Triangles[0]);
	
	
        
	/* loop over all points */
        for (i=0;i<pos->dimensions[0];i++)
          {	     
	     AddPoint(&P[i]);  
	  }


        /* check */
        CheckTriangles();

		
        return Py_BuildValue("i",1);

      }               


static PyObject *
      tessel_GetTriangles(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyObject *OutputList;
	PyObject *OutputDict;
	PyArrayObject *tri = NULL;
	npy_intp dim[2];
	int iT;
	
	/* loop over all triangles */
	OutputList = PyList_New(0);

        	
	for (iT=0;iT<nT;iT++)	
	  {
	  
	    
	    /* 3x3 vector */
	    dim[0]=3;
	    dim[1]=3;
		  
	    tri = (PyArrayObject *) PyArray_SimpleNew(2,dim,PyArray_DOUBLE);
            

	    *(double *) (tri->data + 0*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].P[0]->Pos[0];
     	    *(double *) (tri->data + 0*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].P[0]->Pos[1];
	    *(double *) (tri->data + 0*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	     
	    *(double *) (tri->data + 1*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].P[1]->Pos[0];
     	    *(double *) (tri->data + 1*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].P[1]->Pos[1];
	    *(double *) (tri->data + 1*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	    
	    *(double *) (tri->data + 2*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].P[2]->Pos[0];
     	    *(double *) (tri->data + 2*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].P[2]->Pos[1];
	    *(double *) (tri->data + 2*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	    
	    	    
	    
	    OutputDict = PyDict_New();
	    PyDict_SetItem(OutputDict,PyString_FromString("id"),PyInt_FromLong(Triangles[iT].idx) );
	    PyDict_SetItem(OutputDict,PyString_FromString("coord"),(PyObject*)tri);
	    
	    //(PyObject*)tri
	    
	    PyList_Append(OutputList, OutputDict );
	    	    
	    
	    	
	  }
	

        return Py_BuildValue("O",OutputList);

      }               



static PyObject *
      tessel_ComputeIsoContours(self, args)
          PyObject *self;
          PyObject *args;
      {

        double val;
	int iT;
        
	struct Point P[3];
	int nP,iP;

        PyObject *OutputXList;
	PyObject *OutputYList;


        if (!PyArg_ParseTuple(args,"d",&val))
          return NULL;

        
	OutputXList = PyList_New(0);
	OutputYList = PyList_New(0);

	
    	/* find triangle containing the point */
    	for(iT=0;iT<nT;iT++)	    /* loop over all triangles */ 
    	  {
    	    nP = FindSegmentInTriangle(&Triangles[iT],val,P);	    
	    
	    if (nP>0)
	      
	      switch(nP)
	        {
	        case 1:
		  printf("we are in trouble here (ComputeIsoContours)\n");
		  exit(-1);
	          break;
	        case 2:
	      	    PyList_Append(OutputXList, PyFloat_FromDouble(P[0].Pos[0]));
	      	    PyList_Append(OutputXList, PyFloat_FromDouble(P[1].Pos[0]));
		    
		    PyList_Append(OutputYList, PyFloat_FromDouble(P[0].Pos[1]));
		    PyList_Append(OutputYList, PyFloat_FromDouble(P[1].Pos[1]));
		  break;
		case 3:
	      	    PyList_Append(OutputXList, PyFloat_FromDouble(P[0].Pos[0]));
	      	    PyList_Append(OutputXList, PyFloat_FromDouble(P[1].Pos[0]));
		    PyList_Append(OutputXList, PyFloat_FromDouble(P[2].Pos[0]));
                    PyList_Append(OutputXList, PyFloat_FromDouble(P[0].Pos[0]));
		    
		    PyList_Append(OutputYList, PyFloat_FromDouble(P[0].Pos[1]));
		    PyList_Append(OutputYList, PyFloat_FromDouble(P[1].Pos[1]));
		    PyList_Append(OutputYList, PyFloat_FromDouble(P[2].Pos[1]));
		    PyList_Append(OutputYList, PyFloat_FromDouble(P[0].Pos[1]));		  
		  break;    
	      
		

		
		}
	    
	    
	    
	    
    	  }
	    
	     			   			    
	
		
        return Py_BuildValue("(O,O)",OutputXList,OutputYList);

      }               







static PyObject *
      tessel_GetVoronoi(self, args)
          PyObject *self;
          PyObject *args;
      {

        double val;
	int iT;
	int Tloc;
        
	struct Point Pt1,Pt2,Pt3;
	int nP,iP;
	struct Point Pmm1,Pmm2,Pmm3,Pme1,Pme2,Pme3;


	PyArrayObject *aPmm1,*aPmm2,*aPmm3,*aPme1,*aPme2,*aPme3;
	npy_intp   ld[1];     


        PyObject *OutputList;
        PyObject *SegmentList;
        
	OutputList  = PyList_New(0);
	



	/* create the outputs */
	ld[0]=3;
	

	
        ComputeMediansProperties();
        ComputeMediansIntersections();


    	/* loop over all triangles */ 
    	for(iT=0;iT<nT;iT++)	    
    	  {	  		     
	  
	    if ( Triangles[iT].P[0]->IsDone==2)
	      {
	        //printf("T=%d P %d (%g %g) incomplete\n",Triangles[iT].idx,Triangles[iT].P[0]->Pos[0],Triangles[iT].P[0]->Pos[1]);
	        continue;
	      }
	    if ( Triangles[iT].P[1]->IsDone==2)
	      {
	        //printf("T=%d P %d (%g %g) incomplete\n",Triangles[iT].idx,Triangles[iT].P[1]->Pos[0],Triangles[iT].P[1]->Pos[1]);
	        continue;
	      }     
	    if ( Triangles[iT].P[2]->IsDone==2)
	      {
	        //printf("T=%d P %d (%g %g) incomplete\n",Triangles[iT].idx,Triangles[iT].P[2]->Pos[0],Triangles[iT].P[2]->Pos[1]);
	        continue;
	      }	      
	      
	      
	    aPmm1 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
            *(double *) (aPmm1->data + 0*(aPmm1->strides[0])) = Triangles[iT].Med[0]->Ps.Pos[0];
	    *(double *) (aPmm1->data + 1*(aPmm1->strides[0])) = Triangles[iT].Med[0]->Ps.Pos[1];
	    *(double *) (aPmm1->data + 2*(aPmm1->strides[0])) = 0;
	    
	    aPme1 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
            *(double *) (aPme1->data + 0*(aPme1->strides[0])) = Triangles[iT].Med[0]->Pe.Pos[0];
	    *(double *) (aPme1->data + 1*(aPme1->strides[0])) = Triangles[iT].Med[0]->Pe.Pos[1];
	    *(double *) (aPme1->data + 2*(aPme1->strides[0])) = 0;
	    	
	    SegmentList = PyList_New(0);
	    PyList_Append(SegmentList,(PyObject *)aPmm1);
   	    PyList_Append(SegmentList,(PyObject *)aPme1);
	    PyList_Append(OutputList,SegmentList );
	    
            aPmm2 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    *(double *) (aPmm2->data + 0*(aPmm2->strides[0])) = Triangles[iT].Med[1]->Ps.Pos[0];
	    *(double *) (aPmm2->data + 1*(aPmm2->strides[0])) = Triangles[iT].Med[1]->Ps.Pos[1];
	    *(double *) (aPmm2->data + 2*(aPmm2->strides[0])) = 0;

            aPme2 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    *(double *) (aPme2->data + 0*(aPme2->strides[0])) = Triangles[iT].Med[1]->Pe.Pos[0];
	    *(double *) (aPme2->data + 1*(aPme2->strides[0])) = Triangles[iT].Med[1]->Pe.Pos[1];
	    *(double *) (aPme2->data + 2*(aPme2->strides[0])) = 0;
	    	
	    SegmentList = PyList_New(0);
   	    PyList_Append(SegmentList,(PyObject *)aPmm2);
	    PyList_Append(SegmentList,(PyObject *)aPme2);
	    PyList_Append(OutputList,SegmentList );
	    	  
            aPmm3 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    *(double *) (aPmm3->data + 0*(aPmm3->strides[0])) = Triangles[iT].Med[2]->Ps.Pos[0];
	    *(double *) (aPmm3->data + 1*(aPmm3->strides[0])) = Triangles[iT].Med[2]->Ps.Pos[1];
	    *(double *) (aPmm3->data + 2*(aPmm3->strides[0])) = 0;

            aPme3 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    *(double *) (aPme3->data + 0*(aPme3->strides[0])) = Triangles[iT].Med[2]->Pe.Pos[0];
	    *(double *) (aPme3->data + 1*(aPme3->strides[0])) = Triangles[iT].Med[2]->Pe.Pos[1];
	    *(double *) (aPme3->data + 2*(aPme3->strides[0])) = 0;
	    	
	    SegmentList = PyList_New(0);
   	    PyList_Append(SegmentList,(PyObject *)aPmm3);
	    PyList_Append(SegmentList,(PyObject *)aPme3);
	    PyList_Append(OutputList,SegmentList );
		    
          }



        if (0)
	{
	
    	/* find triangle containing the point */
    	for(iT=0;iT<nT;iT++)	    /* loop over all triangles */ 
    	  {
	  	    
   	    
	    Pt1.Pos[0] = Triangles[iT].P[0]->Pos[0];
	    Pt1.Pos[1] = Triangles[iT].P[0]->Pos[1];
	
	    Pt2.Pos[0] = Triangles[iT].P[1]->Pos[0];
	    Pt2.Pos[1] = Triangles[iT].P[1]->Pos[1];  	   
	
	    Pt3.Pos[0] = Triangles[iT].P[2]->Pos[0];
	    Pt3.Pos[1] = Triangles[iT].P[2]->Pos[1];  	    
	
            TriangleMedians(Pt1,Pt2,Pt3,&Pmm1,&Pmm2,&Pmm3,&Pme1,&Pme2,&Pme3);
	    
	    
	      

	    aPmm1 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    aPmm2 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    aPmm3 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    aPme1 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    aPme2 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    aPme3 = (PyArrayObject *) PyArray_SimpleNew(1,ld,PyArray_DOUBLE);
	    		    
	    
	    *(double *) (aPmm1->data + 0*(aPmm1->strides[0])) = Pmm1.Pos[0];
	    *(double *) (aPmm1->data + 1*(aPmm1->strides[0])) = Pmm1.Pos[1];
	    *(double *) (aPmm1->data + 2*(aPmm1->strides[0])) = 0;
	    
	    *(double *) (aPmm2->data + 0*(aPmm2->strides[0])) = Pmm2.Pos[0];
	    *(double *) (aPmm2->data + 1*(aPmm2->strides[0])) = Pmm2.Pos[1];
	    *(double *) (aPmm2->data + 2*(aPmm2->strides[0])) = 0;
	    
	    *(double *) (aPmm3->data + 0*(aPmm3->strides[0])) = Pmm3.Pos[0];
	    *(double *) (aPmm3->data + 1*(aPmm3->strides[0])) = Pmm3.Pos[1];
	    *(double *) (aPmm3->data + 2*(aPmm3->strides[0])) = 0;
	    
	    
	    *(double *) (aPme1->data + 0*(aPme1->strides[0])) = Pme1.Pos[0];
	    *(double *) (aPme1->data + 1*(aPme1->strides[0])) = Pme1.Pos[1];
	    *(double *) (aPme1->data + 2*(aPme1->strides[0])) = 0;
	    
	    *(double *) (aPme2->data + 0*(aPme2->strides[0])) = Pme2.Pos[0];
	    *(double *) (aPme2->data + 1*(aPme2->strides[0])) = Pme2.Pos[1];
	    *(double *) (aPme2->data + 2*(aPme2->strides[0])) = 0;
	    
	    *(double *) (aPme3->data + 0*(aPme3->strides[0])) = Pme3.Pos[0];
	    *(double *) (aPme3->data + 1*(aPme3->strides[0])) = Pme3.Pos[1];
	    *(double *) (aPme3->data + 2*(aPme3->strides[0])) = 0;  
	    


	    /* check if the interesection is inside the triangle */	    
	    Tloc = InTriangleOrOutside(TriangleInList2Triangle( Triangles[iT] ),Pmm1);

      	    /*
      	    return 2;	       to triangle T[2] 
	    return 0;	       to triangle T[1] 
	    return 1;	       to triangle T[0] 
	    return -1;         the point is inside						   
	    */  


	    if (Tloc==-1)
	      {
		SegmentList = PyList_New(0);
   		PyList_Append(SegmentList,(PyObject *)aPmm1);
	    	PyList_Append(SegmentList,(PyObject *)aPme1);
	    	PyList_Append(OutputList,SegmentList );
	    
	    	SegmentList = PyList_New(0);
	    	PyList_Append(SegmentList,(PyObject *)aPmm2);
	    	PyList_Append(SegmentList,(PyObject *)aPme2);
	    	PyList_Append(OutputList,SegmentList );
	    
	    	SegmentList = PyList_New(0);
	    	PyList_Append(SegmentList,(PyObject *)aPmm3);
	    	PyList_Append(SegmentList,(PyObject *)aPme3);
	    	PyList_Append(OutputList,SegmentList ); 		
              }
	      
	    if (Tloc==0)
	      {


              }	      
	      
	    if (Tloc==1)
	      {
		SegmentList = PyList_New(0);
   		PyList_Append(SegmentList,(PyObject *)aPmm1);
	    	PyList_Append(SegmentList,(PyObject *)aPme1);
	    	PyList_Append(OutputList,SegmentList );
	    
	    	SegmentList = PyList_New(0);
	    	PyList_Append(SegmentList,(PyObject *)aPmm2);
	    	PyList_Append(SegmentList,(PyObject *)aPme2);
	    	PyList_Append(OutputList,SegmentList );
              }	      
	      
	    if (Tloc==2)
	      {
		SegmentList = PyList_New(0);
   		PyList_Append(SegmentList,(PyObject *)aPmm1);
	    	PyList_Append(SegmentList,(PyObject *)aPme1);
	    	PyList_Append(OutputList,SegmentList );
	    
	    	SegmentList = PyList_New(0);
	    	PyList_Append(SegmentList,(PyObject *)aPmm2);
	    	PyList_Append(SegmentList,(PyObject *)aPme2);
	    	PyList_Append(OutputList,SegmentList );
	    
	    	SegmentList = PyList_New(0);
	    	PyList_Append(SegmentList,(PyObject *)aPmm3);
	    	PyList_Append(SegmentList,(PyObject *)aPme3);
	    	PyList_Append(OutputList,SegmentList ); 
	    
              }	 	      
	      
		    
    	  }
	    
	}     			   			    
	
		
        return Py_BuildValue("O",OutputList);

      }               



static PyObject *
      tessel_info(self, args)
          PyObject *self;
          PyObject *args;
      {
	   
	
	int iT,iP,iTe;

	
    	/* find triangle containing the point */
    	for(iT=0;iT<nT;iT++)	    /* loop over all triangles */ 
    	  {
	  
	    printf("Triangle =%d\n",Triangles[iT].idx);
	    iP=0;
	    printf("  P=%d :%g %g\n",iP,Triangles[iT].P[iP]->Pos[0],Triangles[iT].P[iP]->Pos[1]);
	    iP=1;
	    printf("  P=%d :%g %g\n",iP,Triangles[iT].P[iP]->Pos[0],Triangles[iT].P[iP]->Pos[1]);
	    iP=2;
	    printf("  P=%d :%g %g\n",iP,Triangles[iT].P[iP]->Pos[0],Triangles[iT].P[iP]->Pos[1]);
	    
	    iTe=0;
	    if (Triangles[iT].T[iTe]!=NULL)
	      printf("  T=%d :%d\n",iTe,Triangles[iT].T[iTe]->idx);
	    else
	      printf("  T=%d :-\n",iTe);  
	    iTe=1;
	    if (Triangles[iT].T[iTe]!=NULL)
	      printf("  T=%d :%d\n",iTe,Triangles[iT].T[iTe]->idx);
	    else
	      printf("  T=%d :-\n",iTe);  
	    iTe=2;
	    if (Triangles[iT].T[iTe]!=NULL)
	      printf("  T=%d :%d\n",iTe,Triangles[iT].T[iTe]->idx);
	    else
	      printf("  T=%d :-\n",iTe);  ;
	    
	    iTe=0;
	    if (Triangles[iT].T[iTe]!=NULL)
	      printf("  Pe=%d :%d\n",iTe,Triangles[iT].idxe[iTe]);
	    else
	      printf("  Pe=%d :-\n",iTe);  
	    iTe=1;
	    if (Triangles[iT].T[iTe]!=NULL)
	      printf("  Pe=%d :%d\n",iTe,Triangles[iT].idxe[iTe]);
	    else
	      printf("  Pe=%d :-\n",iTe);  
	    iTe=2;
	    if (Triangles[iT].T[iTe]!=NULL)
	      printf("  Pe=%d :%d\n",iTe,Triangles[iT].idxe[iTe]);
	    else
	      printf("  Pe=%d :-\n",iTe);  ;	    
	    
	    printf("\n");
	    
	    
	  }	    
	
	
	
        return Py_BuildValue("i",1);

      }    



/*********************************/
/* test */
/*********************************/

static PyObject *
      tessel_test(self, args)
          PyObject *self;
          PyObject *args;
      {
	   
	
        return Py_BuildValue("i",1);

      }               


           
/* definition of the method table */      
      
static PyMethodDef tesselMethods[] = {


          {"test",  tessel_test, METH_VARARGS,
           "Simple Test"},


          {"info",  tessel_info, METH_VARARGS,
           "info on tesselation"},


          {"TriangleMedians",  tessel_TriangleMedians, METH_VARARGS,
           "Get Triangle Medians"},


          {"CircumCircleProperties",  tessel_CircumCircleProperties, METH_VARARGS,
           "Get Circum Circle Properties"},

          {"InTriangle",  tessel_InTriangle, METH_VARARGS,
           "Return if the triangle (P1,P2,P3) contains the point P4"},

          {"InTriangleOrOutside",  tessel_InTriangleOrOutside, METH_VARARGS,
           "Return if the triangle (P1,P2,P3) contains the point P4"},



          {"InCircumCircle",  tessel_InCircumCircle, METH_VARARGS,
           "Return if the circum circle of the triangle (P1,P2,P3) contains the point P4"},

          {"ConstructDelaunay",  tessel_ConstructDelaunay, METH_VARARGS,
           "Construct the Delaunay tesselation for a given sample of points"},
	   

          {"GetTriangles",  tessel_GetTriangles, METH_VARARGS,
           "Get the trianles in a list of 3x3 arrays."},

          {"ComputeIsoContours",  tessel_ComputeIsoContours, METH_VARARGS,
           "Compute iso-contours."},

          {"GetVoronoi",  tessel_GetVoronoi, METH_VARARGS,
           "Get a list of segements corresponding to the voronoi."},

	   	   	   	   
          {NULL, NULL, 0, NULL}        /* Sentinel */
      };      
      
      
      
void inittessel(void)
      {    
          (void) Py_InitModule("tessel", tesselMethods);	
	  
	  import_array();
      }      
      
