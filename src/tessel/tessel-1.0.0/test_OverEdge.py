#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_OverEdge.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import tessel

import Ptools as pt
import plot

import sys



#while 1:


P1 = random.random(3)
P2 = random.random(3)
P4 = random.random(3)



ps = (P2[0] - P1[0])*(P4[1]-P1[1]) - (P2[1] - P1[1])*(P4[0]-P1[0])

print ps

if ps > 0:
  print 1
else:
  print 0  



plot.draw_points([P1],'r')
plot.draw_points([P2],'g')
plot.draw_points([P4],'k')
pt.show()


