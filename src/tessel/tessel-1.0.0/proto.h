
/*! \file proto.h
 *  \brief this file contains all function prototypes of the code
 */

#ifndef ALLVARS_H
#include "allvars.h"
#endif


void   endrun(int);

void allocate_memory(void);
void lines_intersections(double a0, double b0, double c0, double a1, double b1, double c1, double *x, double *y);
struct Triangle TriangleInList2Triangle(struct TriangleInList Tl);
struct Triangle MakeTriangleFromPoints(struct Point Pt1,struct Point Pt2,struct Point Pt3);
void TriangleMedians(struct Point Pt1,struct Point Pt2,struct Point Pt3,struct Point *Pmm1,struct Point *Pmm2,struct Point *Pmm3,struct Point *Pme1,struct Point *Pme2,struct Point *Pme3);
double CircumCircleProperties(struct Point Pt1,struct Point Pt2,struct Point Pt3, double *xc, double *yc);
int InCircumCircle(struct Triangle T,struct Point Pt4);
int InTriangle(struct Triangle T,struct Point Pt4);
int InTriangleOrOutside(struct Triangle T,struct Point Pt4);
struct Triangle OrientTriangle(struct Triangle T);
struct TriangleInList OrientTriangleInList(struct TriangleInList T);
void FindExtent();
int FindSegmentInTriangle(struct TriangleInList *T,double v,struct Point P[3]);
void CheckTriangles(void);
void FlipTriangle(int i,struct TriangleInList *T,struct TriangleInList *Te,struct TriangleInList *T1,struct TriangleInList *T2);
void DoTrianglesInStack(void);
void Check(void);
void SplitTriangle(struct TriangleInList *pT,struct Point *Pt);
int FindTriangle(struct Point *Pt);
int NewFindTriangle(struct Point *Pt);
void AddPoint(struct Point *Pt);
void ComputeMediansProperties();
void ComputeMediansAroundPoint(struct TriangleInList *Tstart,int iPstart);
void ComputeMediansIntersections();

