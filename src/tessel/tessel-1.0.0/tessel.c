#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "allvars.h"
#include "proto.h"







/*! This routine allocates memory for particle storage, both the
 *  collisionless and the SPH particles.
 */
void allocate_memory(void)
{
  size_t bytes;
  double bytes_tot = 0;

  if(All.MaxPart > 0)
    {
      if(!(P = malloc(bytes = All.MaxPart * sizeof(struct Point))))
	{
	  printf("failed to allocate memory for `P' (%g MB).\n", bytes / (1024.0 * 1024.0));
	  endrun(1);
	}
      bytes_tot += bytes;

      printf("\nAllocated %g MByte for particle storage. %d\n\n", bytes_tot / (1024.0 * 1024.0), sizeof(struct Point));
    }

}


void lines_intersections(double a0, double b0, double c0, double a1, double b1, double c1, double *x, double *y)
  {
   
    *x = (c1*b0 - c0*b1)/(a0*b1 - a1*b0);
    *y = (c1*a0 - c0*a1)/(a1*b0 - a0*b1);   
    
  }


/*! 
 */

struct Triangle TriangleInList2Triangle(struct TriangleInList Tl)
  {
    struct Triangle T;
        
    T.Pt1->Pos[0] = Tl.P[0]->Pos[0];
    T.Pt1->Pos[1] = Tl.P[0]->Pos[1];
    
    T.Pt2->Pos[0] = Tl.P[1]->Pos[0];
    T.Pt2->Pos[1] = Tl.P[1]->Pos[1];
    
    T.Pt3->Pos[0] = Tl.P[2]->Pos[0];
    T.Pt3->Pos[1] = Tl.P[2]->Pos[1];
    	    
    return T;
  }






/*! For a set of three points, construct a triangle
 */

struct Triangle MakeTriangleFromPoints(struct Point Pt1,struct Point Pt2,struct Point Pt3)
  {
    struct Triangle T;
    T.Pt1->Pos[0] = Pt1.Pos[0]; 
    T.Pt1->Pos[1] = Pt1.Pos[1];
    
    T.Pt2->Pos[0] = Pt2.Pos[0]; 
    T.Pt2->Pos[1] = Pt2.Pos[1];
    
    T.Pt3->Pos[0] = Pt3.Pos[0]; 
    T.Pt3->Pos[1] = Pt3.Pos[1];
    
    return T;  
  }



/*! For a set of three points, this function computes the 3 medians.
 */

void TriangleMedians(struct Point Pt1,struct Point Pt2,struct Point Pt3,struct Point *Pmm1,struct Point *Pmm2,struct Point *Pmm3,struct Point *Pme1,struct Point *Pme2,struct Point *Pme3)
  {
    
      
    double ma1,mb1,mc1;
    double ma2,mb2,mc2;
    double ma3,mb3,mc3;
    

    /* median 0-1 */
    ma1 = 2*(Pt2.Pos[0] - Pt1.Pos[0]);
    mb1 = 2*(Pt2.Pos[1] - Pt1.Pos[1]);
    mc1 = (Pt1.Pos[0]*Pt1.Pos[0]) - (Pt2.Pos[0]*Pt2.Pos[0]) + (Pt1.Pos[1]*Pt1.Pos[1]) - (Pt2.Pos[1]*Pt2.Pos[1]); 
    
    /* median 1-2 */
    ma2 = 2*(Pt3.Pos[0] - Pt2.Pos[0]);
    mb2 = 2*(Pt3.Pos[1] - Pt2.Pos[1]);
    mc2 = (Pt2.Pos[0]*Pt2.Pos[0]) - (Pt3.Pos[0]*Pt3.Pos[0]) + (Pt2.Pos[1]*Pt2.Pos[1]) - (Pt3.Pos[1]*Pt3.Pos[1]); 
    
    /* median 2-0 */
    ma3 = 2*(Pt1.Pos[0] - Pt3.Pos[0]);
    mb3 = 2*(Pt1.Pos[1] - Pt3.Pos[1]);
    mc3 = (Pt3.Pos[0]*Pt3.Pos[0]) - (Pt1.Pos[0]*Pt1.Pos[0]) + (Pt3.Pos[1]*Pt3.Pos[1]) - (Pt1.Pos[1]*Pt1.Pos[1]); 

    

    /* intersection m0-1 -- m1-2 */  
    Pmm1->Pos[0] = (mc2*mb1 - mc1*mb2)/(ma1*mb2 - ma2*mb1);
    Pmm1->Pos[1] = (mc2*ma1 - mc1*ma2)/(ma2*mb1 - ma1*mb2);
  
    /* intersection m1-2 -- m2-0 */  
    Pmm2->Pos[0] = (mc2*mb1 - mc1*mb2)/(ma1*mb2 - ma2*mb1);
    Pmm2->Pos[1] = (mc2*ma1 - mc1*ma2)/(ma2*mb1 - ma1*mb2);
  
    /* intersection m2-0 -- m0-1 */  
    Pmm3->Pos[0] = (mc2*mb1 - mc1*mb2)/(ma1*mb2 - ma2*mb1);
    Pmm3->Pos[1] = (mc2*ma1 - mc1*ma2)/(ma2*mb1 - ma1*mb2);
  
  
  
  
  
   
    /* intersection m1-2 -- e1-2 */  
    Pme1->Pos[0] = 0.5*(Pt1.Pos[0] + Pt2.Pos[0]);
    Pme1->Pos[1] = 0.5*(Pt1.Pos[1] + Pt2.Pos[1]); 
  
    /* intersection m2-3 -- e3-1 */  
    Pme2->Pos[0] = 0.5*(Pt2.Pos[0] + Pt3.Pos[0]);
    Pme2->Pos[1] = 0.5*(Pt2.Pos[1] + Pt3.Pos[1]); 
    
    /* intersection m3-1 -- e1-2 */  
    Pme3->Pos[0] = 0.5*(Pt3.Pos[0] + Pt1.Pos[0]);
    Pme3->Pos[1] = 0.5*(Pt3.Pos[1] + Pt1.Pos[1]); 
  
  }





/*! For a set of three points, this function computes their cirum-circle.
 *  Its radius is return, while the center is return using pointers.
 */

double CircumCircleProperties(struct Point Pt1,struct Point Pt2,struct Point Pt3, double *xc, double *yc)
  {
    
      
    double r;
    double x21,x32,y21,y32;
    double x12mx22,y12my22,x22mx32,y22my32;
    double c1,c2;
    
    x21 = Pt2.Pos[0]-Pt1.Pos[0];
    x32 = Pt3.Pos[0]-Pt2.Pos[0];
    
    y21 = Pt2.Pos[1]-Pt1.Pos[1];
    y32 = Pt3.Pos[1]-Pt2.Pos[1];
    
    
    x12mx22 = (Pt1.Pos[0]*Pt1.Pos[0])-(Pt2.Pos[0]*Pt2.Pos[0]);
    y12my22 = (Pt1.Pos[1]*Pt1.Pos[1])-(Pt2.Pos[1]*Pt2.Pos[1]);
    x22mx32 = (Pt2.Pos[0]*Pt2.Pos[0])-(Pt3.Pos[0]*Pt3.Pos[0]);
    y22my32 = (Pt2.Pos[1]*Pt2.Pos[1])-(Pt3.Pos[1]*Pt3.Pos[1]);
    
    c1 = x12mx22 + y12my22;
    c2 = x22mx32 + y22my32;
    
    
    *xc = (y32*c1 -  y21*c2)/2.0/( x32*y21 - x21*y32 );
    *yc = (x32*c1 -  x21*c2)/2.0/( x21*y32 - x32*y21 );
    
    r = sqrt( (Pt1.Pos[0]-*xc)*(Pt1.Pos[0]-*xc) + (Pt1.Pos[1]-*yc)*(Pt1.Pos[1]-*yc) ) ;
    
    return r;
  
  }



/*! For a given triangle T, the routine tells if the point P4
    is in the circum circle of the triangle or not.
 */


int InCircumCircle(struct Triangle T,struct Point Pt4)
  {      
  
    double a,b,c;
    double d,e,f;
    double g,h,i;  
    double det;
    
    /*
    a = T.Pt1->Pos[0] - Pt4.Pos[0];
    b = T.Pt1->Pos[1] - Pt4.Pos[1];
    c = (T.Pt1->Pos[0]*T.Pt1->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt1->Pos[1]*T.Pt1->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);

    d = T.Pt2->Pos[0] - Pt4.Pos[0];
    e = T.Pt2->Pos[1] - Pt4.Pos[1];
    f = (T.Pt2->Pos[0]*T.Pt2->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt2->Pos[1]*T.Pt2->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);

    g = T.Pt3->Pos[0] - Pt4.Pos[0];
    h = T.Pt3->Pos[1] - Pt4.Pos[1];
    i = (T.Pt3->Pos[0]*T.Pt3->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt3->Pos[1]*T.Pt3->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);
    */
      
    /*    
    Volker Formula
    */
    a = T.Pt2->Pos[0] - T.Pt1->Pos[0];
    b = T.Pt2->Pos[1] - T.Pt1->Pos[1];
    c = a*a + b*b;

    d = T.Pt3->Pos[0] - T.Pt1->Pos[0];
    e = T.Pt3->Pos[1] - T.Pt1->Pos[1];
    f = d*d + e*e;

    g = Pt4.Pos[0] - T.Pt1->Pos[0];
    h = Pt4.Pos[1] - T.Pt1->Pos[1];
    i = g*g + h*h;
     

    
     
    det = a*e*i - a*f*h - b*d*i + b*f*g + c*d*h - c*e*g;
    
    
    if (det<0)  
      return 1;			/* inside */
    else
      return 0;			/* outside */  
  }



/*! For a given triangle T, the routine tells if the point P4
    lie inside the triangle or not.
 */


int InTriangle(struct Triangle T,struct Point Pt4)
  {      
  
    double c1,c2,c3;
    
    /* here, we use the cross product */
    c1 = (T.Pt2->Pos[0]-T.Pt1->Pos[0])*(Pt4.Pos[1]-T.Pt1->Pos[1]) - (T.Pt2->Pos[1]-T.Pt1->Pos[1])*(Pt4.Pos[0]-T.Pt1->Pos[0]);
    c2 = (T.Pt3->Pos[0]-T.Pt2->Pos[0])*(Pt4.Pos[1]-T.Pt2->Pos[1]) - (T.Pt3->Pos[1]-T.Pt2->Pos[1])*(Pt4.Pos[0]-T.Pt2->Pos[0]);
    c3 = (T.Pt1->Pos[0]-T.Pt3->Pos[0])*(Pt4.Pos[1]-T.Pt3->Pos[1]) - (T.Pt1->Pos[1]-T.Pt3->Pos[1])*(Pt4.Pos[0]-T.Pt3->Pos[0]);
    
    if ( (c1>0) && (c2>0) && (c3>0) )		/* inside */
      return 1;
    else
      return 0;
    
  }


int InTriangleOrOutside(struct Triangle T,struct Point Pt4)
  {      
  
    double c1,c2,c3;
    
    c1 = (T.Pt2->Pos[0]-T.Pt1->Pos[0])*(Pt4.Pos[1]-T.Pt1->Pos[1]) - (T.Pt2->Pos[1]-T.Pt1->Pos[1])*(Pt4.Pos[0]-T.Pt1->Pos[0]);
    if (c1<0)
      return 2;		/* to triangle T[2] */

    c2 = (T.Pt3->Pos[0]-T.Pt2->Pos[0])*(Pt4.Pos[1]-T.Pt2->Pos[1]) - (T.Pt3->Pos[1]-T.Pt2->Pos[1])*(Pt4.Pos[0]-T.Pt2->Pos[0]);
    if (c2<0)
      return 0;		/* to triangle T[1] */
 
    c3 = (T.Pt1->Pos[0]-T.Pt3->Pos[0])*(Pt4.Pos[1]-T.Pt3->Pos[1]) - (T.Pt1->Pos[1]-T.Pt3->Pos[1])*(Pt4.Pos[0]-T.Pt3->Pos[0]);
    if (c3<0)
      return 1;		/* to triangle T[0] */
    
    return -1;		/* the point is inside */
    
  }







/*! For a given triangle, orient it positively.
 */

struct Triangle OrientTriangle(struct Triangle T)
  {      
     double a,b,c,d;
     double det;
     struct Point Ptsto;
   
   
     a = T.Pt2->Pos[0] - T.Pt1->Pos[0]; 
     b = T.Pt2->Pos[1] - T.Pt1->Pos[1]; 
     c = T.Pt3->Pos[0] - T.Pt1->Pos[0];  
     d = T.Pt3->Pos[1] - T.Pt1->Pos[1];
     
     det = (a*d) - (b*c);
     
     if (det<0)
       {
	 Ptsto.Pos[0] = T.Pt1->Pos[0];
	 Ptsto.Pos[1] = T.Pt1->Pos[1];
	 
	 T.Pt1->Pos[0] = T.Pt3->Pos[0];
	 T.Pt1->Pos[1] = T.Pt3->Pos[1];
	 
	 T.Pt3->Pos[0] = Ptsto.Pos[0];  
	 T.Pt3->Pos[1] = Ptsto.Pos[1];  
	 
	 T = OrientTriangle(T);   
       }
            
     return T;
  }


/*! For a given triangle, orient it positively.
 */

struct TriangleInList OrientTriangleInList(struct TriangleInList T)
  {      
     double a,b,c,d;
     double det;
     struct Point Ptsto;
   
   
     a = T.P[1]->Pos[0] - T.P[0]->Pos[0]; 
     b = T.P[1]->Pos[1] - T.P[0]->Pos[1]; 
     c = T.P[2]->Pos[0] - T.P[0]->Pos[0];  
     d = T.P[2]->Pos[1] - T.P[0]->Pos[1];
     
     det = (a*d) - (b*c);
     
     if (det<0)
       {
	 Ptsto.Pos[0] = T.P[0]->Pos[0];
	 Ptsto.Pos[1] = T.P[0]->Pos[1];
	 
	 T.P[0]->Pos[0] = T.P[2]->Pos[0];
	 T.P[0]->Pos[1] = T.P[2]->Pos[1];
	 
	 T.P[2]->Pos[0] = Ptsto.Pos[0];  
	 T.P[2]->Pos[1] = Ptsto.Pos[1];  
	 
	 T = OrientTriangleInList(T);   
       }
            
     return T;
  }


void FindExtent()
  {
    
    int i,j;
    double xmin[3], xmax[3],len;

    /* determine local extension */
    for(j = 0; j < 3; j++)
      {
    	xmin[j] = MAX_REAL_NUMBER;
    	xmax[j] = -MAX_REAL_NUMBER;
      }      


    for(i = 0; i < NumPart; i++)
      {
    	for(j = 0; j < 3; j++)
    	  {
    	    if(xmin[j] > P[i].Pos[j])
    	      xmin[j] = P[i].Pos[j];

    	    if(xmax[j] < P[i].Pos[j])
    	      xmax[j] = P[i].Pos[j];
    	  }
      }


    len = 0;
    for(j = 0; j < 3; j++)
      {
        if(xmax[j] - xmin[j] > len)
    	  len = xmax[j] - xmin[j];
      }

    for(j = 0; j < 3; j++)
      domainCenter[j] = xmin[j] + len/2.;


    domainRadius = len*1.5;
    
    printf("domainRadius = %g\n",domainRadius);
    printf("domainCenter = (%g %g)\n",domainCenter[0],domainCenter[1]);
        
  }    






int FindSegmentInTriangle(struct TriangleInList *T,double v,struct Point P[3])
  {
    
    double v0,v1,v2;
    double x0,x1,x2;
    double y0,y1,y2;
    double f;
    double x,y;
    int iP;
       
    
    /* if the triangle as an edge point, do nothing */
    if ( (T->P[0]==&Pe[0]) || (T->P[1]==&Pe[0]) || (T->P[2]==&Pe[0]) )
      return 0;
    /* if the triangle as an edge point, do nothing */
    if ( (T->P[0]==&Pe[1]) || (T->P[1]==&Pe[1]) || (T->P[2]==&Pe[1]) )
      return 0;
    /* if the triangle as an edge point, do nothing */
    if ( (T->P[0]==&Pe[2]) || (T->P[1]==&Pe[2]) || (T->P[2]==&Pe[2]) )
      return 0;
    
    
    
    iP = 0;
    v0 = T->P[0]->Mass;
    v1 = T->P[1]->Mass;
    v2 = T->P[2]->Mass;
    
    //printf("Triangle %d : %g %g %g\n",T->idx,v0,v1,v2);
    
    
    /* we could also use the sign v-v0 * v-v1 ??? */
    
    if (( ((v>v0)&&(v<v1)) || ((v>v1)&&(v<v0)) )&& (v0 != v1))  /* in 0-1 */
      {
         x0 = T->P[0]->Pos[0];
	 y0 = T->P[0]->Pos[1];
         x1 = T->P[1]->Pos[0];
	 y1 = T->P[1]->Pos[1];
	 
	 f = (v-v0)/(v1-v0);
	 P[iP].Pos[0] = f*(x1-x0) + x0;
	 P[iP].Pos[1] = f*(y1-y0) + y0;
	 iP++;
	 
      }
       
    if (( ((v>v1)&&(v<v2)) || ((v>v2)&&(v<v1)) )&& (v1 != v2))  /* in 1-2 */
      {
         x0 = T->P[1]->Pos[0];
	 y0 = T->P[1]->Pos[1];
         x1 = T->P[2]->Pos[0];
	 y1 = T->P[2]->Pos[1];
	 	 
	 f = (v-v1)/(v2-v1);
	 P[iP].Pos[0] = f*(x1-x0) + x0;
	 P[iP].Pos[1] = f*(y1-y0) + y0;
	 iP++;
      }
      
    if (( ((v>v2)&&(v<v0)) || ((v>v0)&&(v<v2)) )&& (v2 != v0))  /* in 2-0 */
      {
         x0 = T->P[2]->Pos[0];
	 y0 = T->P[2]->Pos[1];
         x1 = T->P[0]->Pos[0];
	 y1 = T->P[0]->Pos[1];
	 
	 f = (v-v2)/(v0-v2);
	 P[iP].Pos[0] = f*(x1-x0) + x0;
	 P[iP].Pos[1] = f*(y1-y0) + y0;
	 iP++;
      }
    


    

        
    
    
    return iP;
  
  }
  

void CheckTriangles(void)
  {
    int iT;
    struct TriangleInList *T,*Te;
    
    for (iT=0;iT<nT;iT++)
      {
	 T = &Triangles[iT];
	 
	 Te = T->T[0];
	 if (Te!=NULL)	
           {
	    if ((Te->T[0]!=NULL)&&(Te->T[0] == T))
	      {
	      }
	    else  
	      if ((Te->T[1]!=NULL)&&(Te->T[1] == T))
	    	{
	    	}      
	      else	      
	    	if ((Te->T[2]!=NULL)&&(Te->T[2] == T))
	    	  {
	    	  }
	        else
	    	  {
	    	    printf("Triangle %d does not point towards %d, while T->T2=%d\n",Te->idx,T->idx,T->T[0]->idx);
		    exit(-1);
	    	  }			 
           }

	 Te = T->T[1];
	 if (Te!=NULL)	
           {
	    if ((Te->T[0]!=NULL)&&(Te->T[0] == T))
	      {
	      }
	    else  
	      if ((Te->T[1]!=NULL)&&(Te->T[1] == T))
	    	{
	    	}      
	      else	      
	    	if ((Te->T[2]!=NULL)&&(Te->T[2] == T))
	    	  {
	    	  }
	        else
	    	  {
	    	    printf("Triangle %d does not point towards %d, while T->T2=%d\n",Te->idx,T->idx,T->T[1]->idx);
		    exit(-1);
	    	  }			 
           }

	 Te = T->T[2];
	 if (Te!=NULL)	
           {
	    if ((Te->T[0]!=NULL)&&(Te->T[0] == T))
	      {
	      }
	    else  
	      if ((Te->T[1]!=NULL)&&(Te->T[1] == T))
	    	{
	    	}      
	      else	      
	    	if ((Te->T[2]!=NULL)&&(Te->T[2] == T))
	    	  {
	    	  }
	        else
	    	  {
	    	    printf("Triangle %d does not point towards %d, while T->T2=%d\n",Te->idx,T->idx,T->T[2]->idx);
		    exit(-1);
	    	  }			 
           }
	 
      }
  
  }
  
  

/*! Flip two triangles.
    Te = T.T[i]
 */

void FlipTriangle(int i,struct TriangleInList *T,struct TriangleInList *Te,struct TriangleInList *T1,struct TriangleInList *T2)
  {      
    struct TriangleInList Ts1,Ts2;
    int i0,i1,i2;
    int j0,j1,j2;    
    int j;    
    
    Ts1 = *T;		/* save the content of the pointed triangle */
    Ts2 = *Te;		/* save the content of the pointed triangle */
    
    j = T->idxe[i];	/* index of point opposite to i */
    
    
    i0= i;
    i1= (i+1) % 3;
    i2= (i+2) % 3;  

    j0= j;
    j1= (j+1) % 3;
    j2= (j+2) % 3;      
            
    /* triangle 1 */
    
    T1->P[0] = Ts1.P[i0];
    T1->P[1] = Ts1.P[i1];
    T1->P[2] = Ts2.P[j0];
    
    T1->T[0] = Ts2.T[j1];
    T1->T[1] = T2;
    T1->T[2] = Ts1.T[i2];

    T1->idxe[0] = Ts2.idxe[j1];
    T1->idxe[1] = 1;
    T1->idxe[2] = Ts1.idxe[i2]; 

    
    /* triangle 2 */
    
    T2->P[0] = Ts2.P[j0];
    T2->P[1] = Ts2.P[j1];
    T2->P[2] = Ts1.P[i0];
    
    T2->T[0] = Ts1.T[i1];
    T2->T[1] = T1;
    T2->T[2] = Ts2.T[j2];

    T2->idxe[0] = Ts1.idxe[i1];
    T2->idxe[1] = 1;
    T2->idxe[2] = Ts2.idxe[j2];     
    
    /* restore links with adjacents triangles */   
    if (Ts1.T[i1]!=NULL)
      {
        Ts1.T[i1]->T[    Ts1.idxe[i1] ] = T2;
        Ts1.T[i1]->idxe[ Ts1.idxe[i1] ] = 0;
      }
      
    if (Ts1.T[i2] !=NULL)
      {
        Ts1.T[i2]->T[    Ts1.idxe[i2] ] = T1;
        Ts1.T[i2]->idxe[ Ts1.idxe[i2] ] = 2;
      }
    
    if (Ts2.T[j1] !=NULL)
      {       
        Ts2.T[j1]->T[    Ts2.idxe[j1] ] = T1;
        Ts2.T[j1]->idxe[ Ts2.idxe[j1] ] = 0;
      }	

    if (Ts2.T[j2] !=NULL)
      {    
        Ts2.T[j2]->T[    Ts2.idxe[j2] ] = T2;
        Ts2.T[j2]->idxe[ Ts2.idxe[j2] ] = 2; 
      }	
           
  }



void DoTrianglesInStack(void)
  {
  
    struct TriangleInList *T,*Te,*T1,*T2,*Tee;	
    struct TriangleInList Ts1,Ts2;
    struct Point P;
    int istack;
    int idx1,idx2;
    int i;
    
    
    istack=0;
    while(numTinStack>0)
      {
        int insphere=0;	
	
        T = TStack[istack];      
	
	//printf(" DoInStack T=%d  (istack=%d, numTinStack=%d)\n",T->idx,istack,numTinStack);
      

        /* find the opposite point of the 3 adjacent triangles */
	
        /*******************/	
	/* triangle 1      */
	/*******************/	
	i = 0;
	Te = T->T[i];
	if (Te!=NULL)
	  {
	    /* index of opposite point */
	    P = *Te->P[T->idxe[i]];
	       
	    insphere = InCircumCircle(TriangleInList2Triangle(*T),P); 
	    if (insphere)
	      {
	        //printf("insphere (1)... %g %g %g in T=%d\n",P.Pos[0],P.Pos[1],P.Pos[2],T->idx);
		/* index of the new triangles */
    		idx1 = T->idx;
    		idx2 = Te->idx;	      
                
		T1 = &Triangles[idx1];
                T2 = &Triangles[idx2];
		
		FlipTriangle(i,T,Te,T1,T2);
		
	        /* add triangles in stack */
    		if (numTinStack+1>MAXNUMTRIANGLES)
    		  {
    		    printf("\nNo more memory !\n");
		    printf("numTinStack+1=%d > MAXNUMTRIANGLES=%d\n",numTinStack+1,MAXNUMTRIANGLES);
		    printf("You should increase MAXNUMTRIANGLES\n\n");
    		    exit(-1);
    		  }
		TStack[istack            ] = T1;
		TStack[istack+numTinStack] = T2;
		numTinStack++;
		continue;	 
	      }     
	  }


        /*******************/	
	/* triangle 2      */
	/*******************/
	i = 1;
	Te = T->T[i];
	if (Te!=NULL)
	  {
	    /* index of opposite point */
	    P = *Te->P[T->idxe[i]];
	       
	    insphere = InCircumCircle(TriangleInList2Triangle(*T),P); 
	    if (insphere)
	      {
	        //printf("insphere (2)... %g %g %g in T=%d\n",P.Pos[0],P.Pos[1],P.Pos[2],T->idx);
		/* index of the new triangles */
    		idx1 = T->idx;
    		idx2 = Te->idx;	      
                
		T1 = &Triangles[idx1];
                T2 = &Triangles[idx2];
		
		FlipTriangle(i,T,Te,T1,T2);
		
	        /* add triangles in stack */
    		if (numTinStack+1>MAXNUMTRIANGLES)
    		  {
    		    printf("\nNo more memory !\n");
		    printf("numTinStack+1=%d > MAXNUMTRIANGLES=%d\n",numTinStack+1,MAXNUMTRIANGLES);
		    printf("You should increase MAXNUMTRIANGLES\n\n");
    		    exit(-1);
    		  }
		TStack[istack            ] = T1;
		TStack[istack+numTinStack] = T2;
		numTinStack++;
		continue;	 
	      }     
	  }

        /*******************/	
	/* triangle 3      */
	/*******************/
	i = 2;
	Te = T->T[i];
	if (Te!=NULL)
	  {
	    /* index of opposite point */
	    P = *Te->P[T->idxe[i]];
	       
	    insphere = InCircumCircle(TriangleInList2Triangle(*T),P); 
	    if (insphere)
	      {
	        //printf("insphere (3)... %g %g %g in T=%d\n",P.Pos[0],P.Pos[1],P.Pos[2],T->idx);
		/* index of the new triangles */
    		idx1 = T->idx;
    		idx2 = Te->idx;	      
                
		T1 = &Triangles[idx1];
                T2 = &Triangles[idx2];
		
		FlipTriangle(i,T,Te,T1,T2);
		
	        /* add triangles in stack */
    		if (numTinStack+1>MAXNUMTRIANGLES)
    		  {
    		    printf("\nNo more memory !\n");
		    printf("numTinStack+1=%d > MAXNUMTRIANGLES=%d\n",numTinStack+1,MAXNUMTRIANGLES);
		    printf("You should increase MAXNUMTRIANGLES\n\n");
    		    exit(-1);
    		  }
		TStack[istack            ] = T1;
		TStack[istack+numTinStack] = T2;
		numTinStack++;
		continue;	 
	      }     
	  }
	  
  	
        numTinStack--;
	istack++;

        
	
        //printf("one triangle less...(istack=%d numTinStack=%d)\n",istack,numTinStack);

	
      }
    


  
  }


void Check(void)
  {
    
    int iT;
    
    printf("===========================\n");
    
    for(iT=0;iT<nT;iT++)
      {
        printf("* T %d\n",Triangles[iT].idx);
	printf("pt1    %g %g %g\n",Triangles[iT].P[0]->Pos[0],Triangles[iT].P[0]->Pos[1],Triangles[iT].P[0]->Pos[2]);
	printf("pt2    %g %g %g\n",Triangles[iT].P[1]->Pos[0],Triangles[iT].P[1]->Pos[1],Triangles[iT].P[1]->Pos[2]);
	printf("pt3    %g %g %g\n",Triangles[iT].P[2]->Pos[0],Triangles[iT].P[2]->Pos[1],Triangles[iT].P[2]->Pos[2]);
	if (Triangles[iT].T[0]!=NULL)
	  printf("T1     %d\n",Triangles[iT].T[0]->idx);
	else
	  printf("T1     x\n");  
	
	if (Triangles[iT].T[1]!=NULL)
	  printf("T2     %d\n",Triangles[iT].T[1]->idx);
	else
	  printf("T2     x\n");  
	
	if (Triangles[iT].T[2]!=NULL)
	  printf("T3     %d\n",Triangles[iT].T[2]->idx);
	else
	  printf("T3     x\n");  	  	
      }

    printf("===========================\n");  
  }



/*! Split a triangle in 3, using the point P inside it. 
    Update the global list.
 */

void SplitTriangle(struct TriangleInList *pT,struct Point *Pt)
  {      

    struct TriangleInList T,*T0,*T1,*T2,*Te;
    int idx,idx0,idx1,idx2;    
        
       
    T = *pT;		/* save the content of the pointed triangle */

    idx = T.idx;

    
    /* index of the new triangles */
    idx0 = idx;
    idx1 = nT;
    idx2 = nT+1;	
     
    /* increment counter */
    nT=nT+2;	
    
    /* check memory */
    if (nT>MAXNUMTRIANGLES)
      {
        printf("\nNo more memory !\n");
	printf("nT=%d > MAXNUMTRIANGLES=%d\n",nT,MAXNUMTRIANGLES);
	printf("You should increase MAXNUMTRIANGLES\n\n");
        exit(-1);
      }
    
         
    /* create pointers towards the triangles */
    T0 = &Triangles[idx0];
    T1 = &Triangles[idx1];
    T2 = &Triangles[idx2];
        
    
    /* first */
    T0->idx = idx0;
    
    T0->P[0] = T.P[0]; 
    T0->P[1] = T.P[1]; 
    T0->P[2] = Pt;
   
    /* second */
    T1->idx = idx1;
    
    T1->P[0] = T.P[1]; 
    T1->P[1] = T.P[2]; 
    T1->P[2] = Pt;	 

    /* third */
    T2->idx = idx2;
    
    T2->P[0] = T.P[2]; 
    T2->P[1] = T.P[0]; 
    T2->P[2] = Pt;    
        
	  
    /* add adjacents */	  
    T0->T[0] = T1; 
    T0->T[1] = T2; 
    T0->T[2] = T.T[2];       
     
    T1->T[0] = T2; 
    T1->T[1] = T0;
    T1->T[2] = T.T[0];	
    
    T2->T[0] = T0;
    T2->T[1] = T1;
    T2->T[2] = T.T[1];	 
    
    /* add ext point */
    T0->idxe[0] = 1;
    T0->idxe[1] = 0; 
    T0->idxe[2] = T.idxe[2]; 
     
    T1->idxe[0] = 1;
    T1->idxe[1] = 0; 
    T1->idxe[2] = T.idxe[0]; 
    
    T2->idxe[0] = 1; 
    T2->idxe[1] = 0; 
    T2->idxe[2] = T.idxe[1];      
        
    
    /* restore links with adgacents triangles */    
    Te = T0->T[2];
    if (Te!=NULL)
      {    	
	Te->T[   T0->idxe[2]] = T0;
        Te->idxe[T0->idxe[2]] = 2;
      }

    Te = T1->T[2];
    if (Te!=NULL)
      {    	
	Te->T[   T1->idxe[2]] = T1;
        Te->idxe[T1->idxe[2]] = 2;
      }
      
    Te = T2->T[2];
    if (Te!=NULL)
      {    	
	Te->T[   T2->idxe[2]] = T2;
        Te->idxe[T2->idxe[2]] = 2;
      }      


     /* add the new triangles in the stack */
     TStack[numTinStack] = T0;
     numTinStack++;
     
     TStack[numTinStack] = T1;
     numTinStack++;
     
     TStack[numTinStack] = T2;
     numTinStack++;

	  
     //printf("--> add in stack %d %d %d\n",T0->idx,T1->idx,T2->idx);	  
	  
  }





int FindTriangle(struct Point *Pt)
  {
    int iT;
        
    /* find triangle containing the point */
    for(iT=0;iT<nT;iT++)	/* loop over all triangles */ 
      {
      	if (InTriangle(TriangleInList2Triangle( Triangles[iT] ),*Pt))
          break;
      }
    
    return iT;
  }



int NewFindTriangle(struct Point *Pt)
  {
    int iT;
    struct TriangleInList *T;
    int e;
    
    iT = 0;		/* star with first triangle */
    T = &Triangles[iT];
    
    while (1)
      {
        /* test position of the point relative to the triangle */
	e = InTriangleOrOutside(TriangleInList2Triangle( *T ),*Pt);
	
	//printf("T=%d e=%d Te=%d\n",T->idx,e,T->T[e]->idx);
	
	
	if (e==-1)	/* the point is inside */
          break;
       
	    
        T = T->T[e];
	
	
	if (T==NULL)
	  {
            printf("point lie outside the limits.\n");
            exit(-1);
	  }
      }
      
    
    //printf("done with find triangle (T=%d)\n",T->idx);
            
    return T->idx;
  }




/*! Add a new point in the tesselation
 */

void AddPoint(struct Point *Pt)
  {

    int iT;
        
    /* find the triangle that contains the point P */
    //iT= FindTriangle(Pt);
    iT= NewFindTriangle(Pt);
                    
    /* create the new triangles */
    SplitTriangle(&Triangles[iT],Pt);	 
    
    /* test the new triangles and divide and modify if necessary */	    
    DoTrianglesInStack();

    /* check */
    //CheckTriangles();
  
  }



/*! Compute all medians properties (a,b,c)
 */

void ComputeMediansProperties()
  {
    int iT;
	
    /* loop over all triangles */ 
    for(iT=0;iT<nT;iT++)	
      {
      
        struct Point Pt0,Pt1,Pt2;
      

	Pt0.Pos[0] = Triangles[iT].P[0]->Pos[0];
	Pt0.Pos[1] = Triangles[iT].P[0]->Pos[1];
	
	Pt1.Pos[0] = Triangles[iT].P[1]->Pos[0];
	Pt1.Pos[1] = Triangles[iT].P[1]->Pos[1];       
	
	Pt2.Pos[0] = Triangles[iT].P[2]->Pos[0];
	Pt2.Pos[1] = Triangles[iT].P[2]->Pos[1]; 


	/* median 0-1 */
	MediansList[iT][2].a = 2*(Pt1.Pos[0] - Pt0.Pos[0]);
    	MediansList[iT][2].b = 2*(Pt1.Pos[1] - Pt0.Pos[1]);
    	MediansList[iT][2].c = (Pt0.Pos[0]*Pt0.Pos[0]) - (Pt1.Pos[0]*Pt1.Pos[0]) + (Pt0.Pos[1]*Pt0.Pos[1]) - (Pt1.Pos[1]*Pt1.Pos[1]); 
    
    	/* median 1-2 */
    	MediansList[iT][0].a = 2*(Pt2.Pos[0] - Pt1.Pos[0]);
    	MediansList[iT][0].b = 2*(Pt2.Pos[1] - Pt1.Pos[1]);
    	MediansList[iT][0].c = (Pt1.Pos[0]*Pt1.Pos[0]) - (Pt2.Pos[0]*Pt2.Pos[0]) + (Pt1.Pos[1]*Pt1.Pos[1]) - (Pt2.Pos[1]*Pt2.Pos[1]); 
    
    	/* median 2-0 */
    	MediansList[iT][1].a = 2*(Pt0.Pos[0] - Pt2.Pos[0]);
    	MediansList[iT][1].b = 2*(Pt0.Pos[1] - Pt2.Pos[1]);
    	MediansList[iT][1].c = (Pt2.Pos[0]*Pt2.Pos[0]) - (Pt0.Pos[0]*Pt0.Pos[0]) + (Pt2.Pos[1]*Pt2.Pos[1]) - (Pt0.Pos[1]*Pt0.Pos[1]); 
    

    	/* link The triangle with the MediansList */
    	Triangles[iT].Med[0] = &MediansList[iT][0]; 
    	Triangles[iT].Med[1] = &MediansList[iT][1];   
    	Triangles[iT].Med[2] = &MediansList[iT][2];   
      }

  }


/*
  compute the intersetions of medians around a point of index p (index of the point in the triangle T)
*/
void ComputeMediansAroundPoint(struct TriangleInList *Tstart,int iPstart)
  {
  
     /*
         
        Tstart  : pointer to first triangle
	iPstart : index of master point relative to triangle Tstart 	 
       
     
        if p = 0:
	  T1 = T0->T[iTn];	pn=1

        if p = 1:
	  T1 = T0->T[iTn];	pn=2
	  
        if p = 0:
	  T1 = T0->T[iTn];	pn=3	  	  
	 
	 iTn = (p+1) % 3; 
     
     */
     
     double x,y;
     struct TriangleInList *T0,*T1;
     int iP0,iP1;
     int iT1;
     struct Point *initialPoint;
     int iM0,iM1;
     
     T0 = Tstart;
     iP0 = iPstart;
     
     initialPoint = T0->P[iP0];
          
     
     //printf("\n--> rotating around T=%d p=%d\n",T0->idx,iP0);
     
     
     /* rotate around the point */
     while (1)
       {
         	    
         /* next triangle */
	 iT1= (iP0+1) % 3; 
         T1 = T0->T[iT1];
	 
	 if (T1==NULL)
	   {
	     //printf("reach an edge\n");
	     T0->P[iP0]->IsDone=2;
	     //printf("%g %g\n",T0->P[iP0]->Pos[0],T0->P[iP0]->Pos[1]);
	     return;
	   }
	   
	 //printf("    next triangle = %d\n",T1->idx);
	 
	 
         /* index of point in the triangle */
	 iP1 = T0->idxe[iT1];		/* index of point opposite to iTn */
	 iP1 = (iP1+1) % 3;		/* next index of point opposite to iTn */
	 
         //printf("    initial point=%g %g current point =%g %g  iP1=%d\n",initialPoint->Pos[0],initialPoint->Pos[1],T1->P[iP1]->Pos[0],T1->P[iP1]->Pos[1],iP1);
	 
	 /* check */
	 if (initialPoint!=T1->P[iP1])
	   {
	     printf("    problem : initial point=%g %g current point =%g %g  iP1=%d\n",initialPoint->Pos[0],initialPoint->Pos[1],T1->P[iP1]->Pos[0],T1->P[iP1]->Pos[1],iP1);
	     exit(-1);
	   }
	      
         /* compute the intersection of the two medians */
	 
	 iM0 = (iP0+1) % 3;
	 iM1 = (iP1+1) % 3;
	 lines_intersections(T0->Med[iM0]->a,T0->Med[iM0]->b,T0->Med[iM0]->c,T1->Med[iM1]->a,T1->Med[iM1]->b,T1->Med[iM1]->c,&x,&y);	
	 	 
	 /* end point for T0 */
	 T0->Med[iM0]->Pe.Pos[0] = x;
	 T0->Med[iM0]->Pe.Pos[1] = y;
	 /* start point for T0 */
	 T1->Med[iM1]->Ps.Pos[0] = x;
	 T1->Med[iM1]->Ps.Pos[1] = y;	 
	 
	 //printf("    --> T0=%d iM0=%d   T1=%d iM1=%d : (%g %g)\n",T0->idx,iM0,T1->idx,iM1,x,y);
	 
	 
	 
	 if (T1==Tstart)		/* end of loop */
	   {
	     //printf("    end of loop\n");
	     break;
	   }
	 
       
         T0  = T1;
         iP0 = iP1; 
       }
     
  }



/*! Compute all medians intersections and define Ps and Pe
 */

void ComputeMediansIntersections()
  {
    int i,p,iT;

    for (i=0;i<NumPart;i++)
     	P[i].IsDone = 0;	
	
    /* loop over all triangles */ 
    for(iT=0;iT<nT;iT++)	
      {
	/* loop over points in triangle */ 
        for(p=0;p<3;p++)	
          {
            if (!(Triangles[iT].P[p]->IsDone))
	      {
	        //printf("in Triangle T %d do point %d\n",iT,p);
		Triangles[iT].P[p]->IsDone = 1;
		ComputeMediansAroundPoint(&Triangles[iT],p);
		
	      }
	  }	 
      }

  }








