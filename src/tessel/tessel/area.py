'''
 @package   pNbody
 @file      area.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *

def area(p):
    return 0.5 * abs(sum(x0*y1 - x1*y0  for ((x0, y0), (x1, y1)) in segments(p)))

def segments(p):
    return zip(p, p[1:] + [p[0]])



p = [(1.,1.),(3.,1.),(6.,2.0),(8.,2.),(5.,5.),(3.,5.)]

#p = [(0,0),(0,4),(4,4),(4,0),(0,0)]


print area(p)




'''
the loop :
  for ((x0, y0), (x1, y1)) in segments(p)))

loops over;

  x0   y0     x1   y1
((1.0, 1.0), (3.0, 1.0))
((3.0, 1.0), (6.0, 2.0))
((6.0, 2.0), (8.0, 2.0))
((8.0, 2.0), (5.0, 5.0))
((5.0, 5.0), (3.0, 5.0))
((3.0, 5.0), (1.0, 1.0))

-> 
   s = sum(x0*y1 - x1*y0)
   area = 0.5*abs(s)


'''




