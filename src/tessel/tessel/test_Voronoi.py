#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_Voronoi.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import ic
from pNbody import libutil

from numpy import *
import tessel

import Ptools as pt
import plot

import sys
import time

import copy

random.seed(0)			# 2 for two points

n = 100

#nb = ic.plummer(n,1,1,1,eps=0.1,rmax=1.,ftype='gadget')
nb = ic.box(n,1,1,1)

nb1 = copy.deepcopy(nb)
nb1.translate(array([ 2, 0,0]))
nb2 = copy.deepcopy(nb)
nb2.translate(array([-2, 0,0]))
nb3 = copy.deepcopy(nb)
nb3.translate(array([ 0, 2,0]))
nb4 = copy.deepcopy(nb)
nb4.translate(array([ 0,-2,0]))

nb5 = copy.deepcopy(nb)
nb5.translate(array([ 2,2,0]))
nb6 = copy.deepcopy(nb)
nb6.translate(array([ -2,2,0]))
nb7 = copy.deepcopy(nb)
nb7.translate(array([ 2,-2,0]))
nb8 = copy.deepcopy(nb)
nb8.translate(array([ -2,-2,0]))


nb = nb + nb1+nb2+nb3+nb4 + nb5+nb6+nb7+nb8


nb.pos[:,2] = nb.pos[:,2]*0 
pos = nb.pos
mass = nb.mass

#pos = random.random((n,3))
#mass = pos[:,0]+pos[:,1]

#pos[0] = [0.5,0.5,0]
#pos[1] = [0.5,0.25,0]
#pos[2] = [0.25,0.5,0]
#pos[3] = [0.75,1.0,0]

t1 = time.time()
tessel.ConstructDelaunay(pos,mass)
t2 = time.time()

#tessel.info()



TriangleList = tessel.GetTriangles()


print len(TriangleList),"triangles","in",(t2-t1),"s"




t1 = time.time()
segments = tessel.GetVoronoi()
t2 = time.time()
print len(segments),"segments","in",(t2-t1),"s"
#sys.exit()



vpos = tessel.get_vPoints()
rho = tessel.get_AllDensities()
rsp = tessel.get_AllVolumes()





# save model
nb.rho = rho
nb.rsp = rsp
nb.u   = ones(nb.nbody)
nb.rename('snap.dat')
nb.write()



rho,mn,mx,cd = libutil.set_ranges(rho,scale='lin')
rsp,mn,mx,cd = libutil.set_ranges(rsp,scale='lin')

rho = rho/255.
rsp = rsp/255.


'''
for segment in segments:
  #print segment[0],segment[1]
  plot.draw_line(segment[0],segment[1],'r')
'''

#pt.scatter(vpos[:,0],vpos[:,1])
pt.scatter(nb.pos[:,0],nb.pos[:,1],lw=0,s=10)

cmap = pt.GetColormap('rainbow4',revesed=False)

# single point with vpoints
for i in xrange(nb.nbody):
  vpos = tessel.get_vPointsForOnePoint(i)
  #pt.plot(vpos[:,0],vpos[:,1],'g',lw=5)
  #pt.scatter(nb.pos[i,0],nb.pos[i,1],lw=0,s=50)

  plot.draw_cell(vpos,color=[rho[i],rho[i],rho[i]],alpha=0.8)


'''
# draw the triangles
i = 0
for Triangle in TriangleList:
  P1 = Triangle['coord'][0]
  P2 = Triangle['coord'][1]
  P3 = Triangle['coord'][2]
  plot.draw_triangle(P1,P2,P3,c='r')

  #cm = 1/3.*(P1+P2+P3)
  #pt.text(cm[0],cm[1],Triangle['id'],fontsize=12,horizontalalignment='center',verticalalignment='center')
'''


"""
for P in pos:
  plot.draw_points([P],'b')
"""



# draw a box
plot.draw_box(x=array([0,1,1,0])*2 - 1,y=array([0,0,1,1])*2-1)



pt.axis([-1.8,1.8,-1.8,1.8])
pt.show()

