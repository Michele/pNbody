#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_Medians.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import tessel

import Ptools as pt
import plot

import sys



P1 = random.random(3)
P2 = random.random(3)
P3 = random.random(3)

'''
P1[0] = 0
P1[1] = 0

P2[0] = 1
P2[1] = 0

P3[0] = 0
P3[1] = 1
'''


points = tessel.TriangleMedians(P1,P2,P3)

pmm1 = points[0]
pmm2 = points[1]
pmm3 = points[2]
pme1 = points[3]
pme2 = points[4]
pme3 = points[5]


print pmm1
print pmm2
print pmm3
 

plot.draw_triangle(P1,P2,P3)
plot.draw_points([P1])
plot.draw_points([P2])
plot.draw_points([P3])


plot.draw_points([pmm1,pme1,pme2,pme3])


plot.draw_line(pme1,pmm1)
plot.draw_line(pme2,pmm1)
plot.draw_line(pme3,pmm1)


pt.axis([0,1,0,1])
pt.show()


