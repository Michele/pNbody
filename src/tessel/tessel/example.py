#!/usr/bin/env python
'''
 @package   pNbody
 @file      example.py
 @brief     Example
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from pNbody import *
import tessel
import sys
import Ptools as pt
import plot

file = sys.argv[1]
nb = Nbody(file,ftype='gadget')
#nb = nb.reduc(4)
nb = nb.selectc(nb.mass<1)
print nb.nbody

nb.pos[:,1] = nb.pos[:,2]

print "Start with Delaunay"
tessel.ConstructDelaunay(nb.pos,nb.mass)
print "Done with Delaunay"



vals = [0.2,0.7]
cs = ['r','b']
j = 0
for val in vals:
  print "Start with IsoContours"
  x,y = tessel.ComputeIsoContours(val)
  print "Done with IsoContours"
  x = array(x)
  y = array(y)
  print len(x)

  for i in arange(len(x)/2)*2:
    pt.plot(x[i:i+2],y[i:i+2],c=cs[j])
  j = j + 1  


#pt.scatter(nb.x(),nb.y())



'''
TriangleList = tessel.GetTriangles()
i = 0
for Triangle in TriangleList:
  P1 = Triangle['coord'][0]
  P2 = Triangle['coord'][1]
  P3 = Triangle['coord'][2]
  plot.draw_triangle(P1,P2,P3)  
'''



#pt.axis([0,2,0,2])
pt.show()

