#include <Python.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <numpy/libnumarray.h>

#define TO_DOUBLE(a)        ( (PyArrayObject*) PyArray_CastToType(a, PyArray_DescrFromType(NPY_DOUBLE)  ,0) )

#define MAXNUMTRIANGLES 1000000
#define MAXPART 1000000




struct global_data_all_processes
{
  int MaxPart;			/*!< This gives the maxmimum number of particles that can be stored on one processor. */
}
  All;

/*! This structure holds all the information that is
 * stored for each particle of the simulation.
 */

struct Point		/* struct particle_data */
  {
    double Pos[3];			/*!< particle position at its current time */
  }
 *P;              /*!< holds particle data on local processor */






struct Triangle
  {
    struct Point Pt1[3];
    struct Point Pt2[3];
    struct Point Pt3[3];
  };



struct TriangleInList
  {

    int idx;            		/* index of current triangle */
     
    struct Point* Pt1;              	/* pointer towards the first  point */
    struct Point* Pt2;              	/* pointer towards the second point */
    struct Point* Pt3;	            	/* pointer towards the third  point */
    
    struct TriangleInList* T1;      	/* index of first  triangle */
    struct TriangleInList* T2;      	/* index of second triangle */
    struct TriangleInList* T3;      	/* index of third  triangle */
 
  };




/* some global varables */

int nT=0,numTinStack=0;					/* number of triangles in the list */
struct TriangleInList Triangles[MAXNUMTRIANGLES];	/* list of triangles               */
struct TriangleInList *TStack[MAXNUMTRIANGLES];				/* index of triangles to check	   */
int NumPart;

struct Point Pe[3];					/* edges */
	

void endrun(int ierr)
{
  
  int ThisTask=0;
  
  if(ierr)
    {
      printf("task %d: endrun called with an error level of %d\n\n\n", ThisTask, ierr);
      fflush(stdout);
      exit(0);
    }

  exit(0);
}



/*! This routine allocates memory for particle storage, both the
 *  collisionless and the SPH particles.
 */
void allocate_memory(void)
{
  size_t bytes;
  double bytes_tot = 0;

  if(All.MaxPart > 0)
    {
      if(!(P = malloc(bytes = All.MaxPart * sizeof(struct Point))))
	{
	  printf("failed to allocate memory for `P' (%g MB).\n", bytes / (1024.0 * 1024.0));
	  endrun(1);
	}
      bytes_tot += bytes;

      printf("\nAllocated %g MByte for particle storage. %d\n\n", bytes_tot / (1024.0 * 1024.0), sizeof(struct Point));
    }

}





/*! 
 */

struct Triangle TriangleInList2Triangle(struct TriangleInList Tl)
  {
    struct Triangle T;
        
    T.Pt1->Pos[0] = Tl.Pt1->Pos[0];
    T.Pt1->Pos[1] = Tl.Pt1->Pos[1];
    
    T.Pt2->Pos[0] = Tl.Pt2->Pos[0];
    T.Pt2->Pos[1] = Tl.Pt2->Pos[1];
    
    T.Pt3->Pos[0] = Tl.Pt3->Pos[0];
    T.Pt3->Pos[1] = Tl.Pt3->Pos[1];
    	    
    return T;
  }






/*! For a set of three points, construct a triangle
 */

struct Triangle MakeTriangleFromPoints(struct Point Pt1,struct Point Pt2,struct Point Pt3)
  {
    struct Triangle T;
    T.Pt1->Pos[0] = Pt1.Pos[0]; 
    T.Pt1->Pos[1] = Pt1.Pos[1];
    
    T.Pt2->Pos[0] = Pt2.Pos[0]; 
    T.Pt2->Pos[1] = Pt2.Pos[1];
    
    T.Pt3->Pos[0] = Pt3.Pos[0]; 
    T.Pt3->Pos[1] = Pt3.Pos[1];
    
    return T;  
  }





/*! For a set of three points, this function computes their cirum-circle.
 *  Its radius is return, while the center is return using pointers.
 */

double CircumCircleProperties(struct Point Pt1,struct Point Pt2,struct Point Pt3, double *xc, double *yc)
  {
    
      
    double r;
    double x21,x32,y21,y32;
    double x12mx22,y12my22,x22mx32,y22my32;
    double c1,c2;
    
    x21 = Pt2.Pos[0]-Pt1.Pos[0];
    x32 = Pt3.Pos[0]-Pt2.Pos[0];
    
    y21 = Pt2.Pos[1]-Pt1.Pos[1];
    y32 = Pt3.Pos[1]-Pt2.Pos[1];
    
    
    x12mx22 = (Pt1.Pos[0]*Pt1.Pos[0])-(Pt2.Pos[0]*Pt2.Pos[0]);
    y12my22 = (Pt1.Pos[1]*Pt1.Pos[1])-(Pt2.Pos[1]*Pt2.Pos[1]);
    x22mx32 = (Pt2.Pos[0]*Pt2.Pos[0])-(Pt3.Pos[0]*Pt3.Pos[0]);
    y22my32 = (Pt2.Pos[1]*Pt2.Pos[1])-(Pt3.Pos[1]*Pt3.Pos[1]);
    
    c1 = x12mx22 + y12my22;
    c2 = x22mx32 + y22my32;
    
    
    *xc = (y32*c1 -  y21*c2)/2.0/( x32*y21 - x21*y32 );
    *yc = (x32*c1 -  x21*c2)/2.0/( x21*y32 - x32*y21 );
    
    r = sqrt( (Pt1.Pos[0]-*xc)*(Pt1.Pos[0]-*xc) + (Pt1.Pos[1]-*yc)*(Pt1.Pos[1]-*yc) ) ;
    
    return r;
  
  }



/*! For a given triangle T, the routine tells if the point P4
    is in the circum circle of the triangle or not.
 */


int InCircumCircle(struct Triangle T,struct Point Pt4)
  {      
  
    double a,b,c;
    double d,e,f;
    double g,h,i;  
    double det;
    
    /*
    a = T.Pt1->Pos[0] - Pt4.Pos[0];
    b = T.Pt1->Pos[1] - Pt4.Pos[1];
    c = (T.Pt1->Pos[0]*T.Pt1->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt1->Pos[1]*T.Pt1->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);

    d = T.Pt2->Pos[0] - Pt4.Pos[0];
    e = T.Pt2->Pos[1] - Pt4.Pos[1];
    f = (T.Pt2->Pos[0]*T.Pt2->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt2->Pos[1]*T.Pt2->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);

    g = T.Pt3->Pos[0] - Pt4.Pos[0];
    h = T.Pt3->Pos[1] - Pt4.Pos[1];
    i = (T.Pt3->Pos[0]*T.Pt3->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt3->Pos[1]*T.Pt3->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);
    */
      
    /*    
    Volker Formula
    */
    a = T.Pt2->Pos[0] - T.Pt1->Pos[0];
    b = T.Pt2->Pos[1] - T.Pt1->Pos[1];
    c = a*a + b*b;

    d = T.Pt3->Pos[0] - T.Pt1->Pos[0];
    e = T.Pt3->Pos[1] - T.Pt1->Pos[1];
    f = d*d + e*e;

    g = Pt4.Pos[0] - T.Pt1->Pos[0];
    h = Pt4.Pos[1] - T.Pt1->Pos[1];
    i = g*g + h*h;
     

    
     
    det = a*e*i - a*f*h - b*d*i + b*f*g + c*d*h - c*e*g;
    
    
    if (det<0)  
      return 1;			/* inside */
    else
      return 0;			/* outside */  
  }



/*! For a given triangle T, the routine tells if the point P4
    lie inside the triangle or not.
 */


int InTriangle(struct Triangle T,struct Point Pt4)
  {      
  
    double c1,c2,c3;
    
    /* here, we use the cross product */
    c1 = (T.Pt2->Pos[0]-T.Pt1->Pos[0])*(Pt4.Pos[1]-T.Pt1->Pos[1]) - (T.Pt2->Pos[1]-T.Pt1->Pos[1])*(Pt4.Pos[0]-T.Pt1->Pos[0]);
    c2 = (T.Pt3->Pos[0]-T.Pt2->Pos[0])*(Pt4.Pos[1]-T.Pt2->Pos[1]) - (T.Pt3->Pos[1]-T.Pt2->Pos[1])*(Pt4.Pos[0]-T.Pt2->Pos[0]);
    c3 = (T.Pt1->Pos[0]-T.Pt3->Pos[0])*(Pt4.Pos[1]-T.Pt3->Pos[1]) - (T.Pt1->Pos[1]-T.Pt3->Pos[1])*(Pt4.Pos[0]-T.Pt3->Pos[0]);
    
    if ( (c1>0) && (c2>0) && (c3>0) )		/* inside */
      return 1;
    else
      return 0;
    
  }



/*! For a given triangle, orient it positively.
 */

struct Triangle OrientTriangle(struct Triangle T)
  {      
     double a,b,c,d;
     double det;
     struct Point Ptsto;
   
   
     a = T.Pt2->Pos[0] - T.Pt1->Pos[0]; 
     b = T.Pt2->Pos[1] - T.Pt1->Pos[1]; 
     c = T.Pt3->Pos[0] - T.Pt1->Pos[0];  
     d = T.Pt3->Pos[1] - T.Pt1->Pos[1];
     
     det = (a*d) - (b*c);
     
     if (det<0)
       {
	 Ptsto.Pos[0] = T.Pt1->Pos[0];
	 Ptsto.Pos[1] = T.Pt1->Pos[1];
	 
	 T.Pt1->Pos[0] = T.Pt3->Pos[0];
	 T.Pt1->Pos[1] = T.Pt3->Pos[1];
	 
	 T.Pt3->Pos[0] = Ptsto.Pos[0];  
	 T.Pt3->Pos[1] = Ptsto.Pos[1];  
	 
	 T = OrientTriangle(T);   
       }
            
     return T;
  }


/*! For a given triangle, orient it positively.
 */

struct TriangleInList OrientTriangleInList(struct TriangleInList T)
  {      
     double a,b,c,d;
     double det;
     struct Point Ptsto;
   
   
     a = T.Pt2->Pos[0] - T.Pt1->Pos[0]; 
     b = T.Pt2->Pos[1] - T.Pt1->Pos[1]; 
     c = T.Pt3->Pos[0] - T.Pt1->Pos[0];  
     d = T.Pt3->Pos[1] - T.Pt1->Pos[1];
     
     det = (a*d) - (b*c);
     
     if (det<0)
       {
	 Ptsto.Pos[0] = T.Pt1->Pos[0];
	 Ptsto.Pos[1] = T.Pt1->Pos[1];
	 
	 T.Pt1->Pos[0] = T.Pt3->Pos[0];
	 T.Pt1->Pos[1] = T.Pt3->Pos[1];
	 
	 T.Pt3->Pos[0] = Ptsto.Pos[0];  
	 T.Pt3->Pos[1] = Ptsto.Pos[1];  
	 
	 T = OrientTriangleInList(T);   
       }
            
     return T;
  }



void CheckTriangles(void)
  {
    int iT;
    struct TriangleInList *T,*Te;
    
    for (iT=0;iT<nT;iT++)
      {
	 T = &Triangles[iT];
	 
	 Te = T->T1;
	 if (Te!=NULL)	
           {
	    if ((Te->T1!=NULL)&&(Te->T1 == T))
	      {
	      }
	    else  
	      if ((Te->T2!=NULL)&&(Te->T2 == T))
	    	{
	    	}      
	      else	      
	    	if ((Te->T3!=NULL)&&(Te->T3 == T))
	    	  {
	    	  }
	        else
	    	  {
	    	    printf("Triangle %d does not point towards %d, while T->T2=%d\n",Te->idx,T->idx,T->T1->idx);
		    exit(-1);
	    	  }			 
           }

	 Te = T->T2;
	 if (Te!=NULL)	
           {
	    if ((Te->T1!=NULL)&&(Te->T1 == T))
	      {
	      }
	    else  
	      if ((Te->T2!=NULL)&&(Te->T2 == T))
	    	{
	    	}      
	      else	      
	    	if ((Te->T3!=NULL)&&(Te->T3 == T))
	    	  {
	    	  }
	        else
	    	  {
	    	    printf("Triangle %d does not point towards %d, while T->T2=%d\n",Te->idx,T->idx,T->T2->idx);
		    exit(-1);
	    	  }			 
           }

	 Te = T->T3;
	 if (Te!=NULL)	
           {
	    if ((Te->T1!=NULL)&&(Te->T1 == T))
	      {
	      }
	    else  
	      if ((Te->T2!=NULL)&&(Te->T2 == T))
	    	{
	    	}      
	      else	      
	    	if ((Te->T3!=NULL)&&(Te->T3 == T))
	    	  {
	    	  }
	        else
	    	  {
	    	    printf("Triangle %d does not point towards %d, while T->T2=%d\n",Te->idx,T->idx,T->T3->idx);
		    exit(-1);
	    	  }			 
           }
	 
      }
  
  }
  
  


void DoTrianglesInStack(void)
  {
  
    struct TriangleInList *T,*Te,*T1,*T2,*Tee;	
    struct TriangleInList Ts1,Ts2;
    struct Point P;
    int istack;
    int idx1,idx2;
    
    
    istack=0;
    while(numTinStack>0)
      {
        int insphere=0;	
	
        T = TStack[istack];      
	
	//printf(" DoInStack T=%d  (istack=%d, numTinStack=%d)\n",T->idx,istack,numTinStack);
      

        /* find the opposite point of the 3 adjacent triangles */
	
        /*******************/	
	/* triangle 1      */
	/*******************/
	Te = T->T1;
	if (Te!=NULL)
	  {
	    if ((Te->T1!=NULL)&&(Te->T1 == T))
	      {
	    	P = *Te->Pt1;
	      }
	    else  
	      if ((Te->T2!=NULL)&&(Te->T2 == T))
	    	{
	    	  P = *Te->Pt2;
	    	}      
	      else	      
	    	if ((Te->T3!=NULL)&&(Te->T3 == T))
	    	  {
	    	    P = *Te->Pt3;
	    	  }
	        else
	    	  {
	    	    printf("we are in trouble here... (DoTrianglesInStack 2)\n");
	    	  }			 
	       
	    insphere = InCircumCircle(TriangleInList2Triangle(*T),P); 
	    if (insphere)
	      {
	      	//printf("insphere  (1)... %g %g %g in T=%d\n",P.Pos[0],P.Pos[1],P.Pos[2],T->idx);
	      
	      	if (Te->T1 == T)
	      	  {
		    //printf("insphere 11\n");
		    
		    Ts1 = *T;		  /* save the content of the pointed triangle */
		    Ts2 = *Te;  	  /* save the content of the pointed triangle */
		  
    		    /* index of the new triangles */
    		    idx1 = T->idx;
    		    idx2 = Te->idx;
 

  		    /* create pointers towards the triangles */
    		    T1 = &Triangles[idx1];
    		    T2 = &Triangles[idx2];


    		    /* first */
    		    T1->idx = idx1;
    
    		    T1->Pt1 = Ts1.Pt1; 
    		    T1->Pt2 = Ts1.Pt2; 
    		    T1->Pt3 = Ts2.Pt1;
    
   		    /* second */
    		    T2->idx = idx2;
    
    		    T2->Pt1 = Ts2.Pt1; 
    		    T2->Pt2 = Ts2.Pt2; 
    		    T2->Pt3 = Ts1.Pt1;      
        			  
		    /* add adjacents */   
    		    T1->T1 = Ts2.T2; 
    		    T1->T2 =	 T2; 
    		    T1->T3 = Ts1.T3;	 
    		     
    		    T2->T1 = Ts1.T2; 
    		    T2->T2 =	 T1;
    		    T2->T3 = Ts2.T3;	 
        

    			/* restore links with adgacents triangles */
    
    			Tee = T1->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==T)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 12)\n");
				  }    
    			  }


    			Tee = T1->T1;
    			if (Tee!=NULL)
    			  {    
                            if (Tee->T1==Te)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==Te)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 11)\n");
				  }    
    			  }


    			Tee = T2->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==Te)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==Te)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 23)\n");
				  }			    
    			  }
			  
    			Tee = T2->T1;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==T)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 21)\n");
				  }			    
    			  }	
	      	  }
	      	else  
	      	  if (Te->T2 == T)
	      	    {
		      //printf("insphere 12\n");
		      
		      Ts1 = *T; 	    /* save the content of the pointed triangle */
		      Ts2 = *Te;	    /* save the content of the pointed triangle */
	
		      /* index of the new triangles */
		      idx1 = T->idx;
		      idx2 = Te->idx;
    		    
    		    
    		      /* create pointers towards the triangles */
 		      T1 = &Triangles[idx1];
		      T2 = &Triangles[idx2];
  		    
    		    
    		      /* first */
		      T1->idx = idx1;

    		      T1->Pt1 = Ts1.Pt1; 
    		      T1->Pt2 = Ts1.Pt2; 
    		      T1->Pt3 = Ts2.Pt2;
    		    
    		      /* second */
    		      T2->idx = idx2;
    
   		      T2->Pt1 = Ts2.Pt2; 
    		      T2->Pt2 = Ts2.Pt3; 
    		      T2->Pt3 = Ts1.Pt1;      
    				    
    		      /* add adjacents */   
    		      T1->T1 = Ts2.T3; 
        	      T1->T2 =     T2; 
		      T1->T3 = Ts1.T3;     
    		       
    		      T2->T1 = Ts1.T2; 
    		      T2->T2 =     T1;
    		      T2->T3 = Ts2.T1;     
    		    
    		    
    			/* restore links with adgacents triangles */
    
    			Tee = T1->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==T)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 12)\n");
				  }    
    			  }


    			Tee = T1->T1;
    			if (Tee!=NULL)
    			  {    
                            if (Tee->T1==Te)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==Te)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 11)\n");
				  }    
    			  }


    			Tee = T2->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==Te)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==Te)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 23)\n");
				  }			    
    			  }
			  
    			Tee = T2->T1;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==T)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 21)\n");
				  }			    
    			  }	
	      	    }	   
	      	  else  	  
	      	    if (Te->T3 == T)
	      	      {
		        //printf("insphere 13\n");			
			
			Ts1 = *T;	      /* save the content of the pointed triangle */
			Ts2 = *Te;	      /* save the content of the pointed triangle */
		  
    			/* index of the new triangles */
    			idx1 = T->idx;
    			idx2 = Te->idx;
 

  			/* create pointers towards the triangles */
    			T1 = &Triangles[idx1];
    			T2 = &Triangles[idx2];


    			/* first */
    			T1->idx = idx1;
    
    			T1->Pt1 = Ts1.Pt1; 
    			T1->Pt2 = Ts1.Pt2; 
    			T1->Pt3 = Ts2.Pt3;
    
   			/* second */
    			T2->idx = idx2;
    
    			T2->Pt1 = Ts2.Pt3; 
    			T2->Pt2 = Ts2.Pt1; 
    			T2->Pt3 = Ts1.Pt1;	
        			      
			/* add adjacents */   
    			T1->T1 = Ts2.T1; 
    			T1->T2 =     T2; 
    			T1->T3 = Ts1.T3;     
    			 
    			T2->T1 = Ts1.T2; 
    			T2->T2 =     T1;
    			T2->T3 = Ts2.T2;     
        

    			/* restore links with adgacents triangles */
    
    			Tee = T1->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==T)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 12)\n");
				  }    
    			  }


    			Tee = T1->T1;
    			if (Tee!=NULL)
    			  {    
                            if (Tee->T1==Te)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==Te)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 11)\n");
				  }    
    			  }


    			Tee = T2->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==Te)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==Te)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 23)\n");
				  }			    
    			  }
			  
    			Tee = T2->T1;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==T)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 21)\n");
				  }			    
    			  }		      		
	      	      }
	      	    else
	      	      {
	      		printf("we are in trouble here... (DoTrianglesInStack 3)\n");
	      	      } 		     
	      	 
	      
	      
	        /* add triangles in stack */
		//printf("add triangle %d (i=%d) and %d (i=%d) in stack\n",T1->idx,istack,T2->idx,numTinStack+1);
		TStack[istack            ] = T1;
		TStack[istack+numTinStack] = T2;
		numTinStack++;
		continue;	 
	      
	      }     
	  }


        /*******************/	
	/* triangle 2      */
	/*******************/
	Te = T->T2;
	if (Te!=NULL)
	  {	    
	    if ((Te->T1!=NULL)&&(Te->T1 == T))
	      {
	    	P = *Te->Pt1;
	      }
	    else  
	      if ((Te->T2!=NULL)&&(Te->T2 == T))
	    	{
	    	  P = *Te->Pt2;
	    	}      
	      else	      
	    	if ((Te->T3!=NULL)&&(Te->T3 == T))
	    	  {
	    	    P = *Te->Pt3;
	    	  }
	        else
	    	  {
	    	    printf("we are in trouble here... (DoTrianglesInStack 2)\n");
	    	  }			 
	    	       
	    insphere = InCircumCircle(TriangleInList2Triangle(*T),P); 
	    if (insphere)
	      {
	      	//printf("insphere  (2)... %g %g %g in T=%d\n",P.Pos[0],P.Pos[1],P.Pos[2],T->idx);
	      
	      	if (Te->T1 == T)
	      	  {
		    //printf("insphere 21\n");
		  
		    Ts1 = *T;		  /* save the content of the pointed triangle */
		    Ts2 = *Te;  	  /* save the content of the pointed triangle */
		  
    		    /* index of the new triangles */
    		    idx1 = T->idx;
    		    idx2 = Te->idx;
 

  		    /* create pointers towards the triangles */
    		    T1 = &Triangles[idx1];
    		    T2 = &Triangles[idx2];


    		    /* first */
    		    T1->idx = idx1;
    
    		    T1->Pt1 = Ts1.Pt2; 
    		    T1->Pt2 = Ts1.Pt3; 
    		    T1->Pt3 = Ts2.Pt1;
    
   		    /* second */
    		    T2->idx = idx2;
    
    		    T2->Pt1 = Ts2.Pt1; 
    		    T2->Pt2 = Ts2.Pt2; 
    		    T2->Pt3 = Ts1.Pt2;      
        			  
		    /* add adjacents */   
    		    T1->T1 = Ts2.T2; 
    		    T1->T2 =	 T2; 
    		    T1->T3 = Ts1.T1;	 
    		     
    		    T2->T1 = Ts1.T3; 
    		    T2->T2 =	 T1;
    		    T2->T3 = Ts2.T3;	 
        

    		    /* restore links with adgacents triangles */
    
    		    Tee = T1->T3;
    		    if (Tee!=NULL)
    		      {    
    		    	if (Tee->T1==T)
    		    	  {
    		    	    Tee->T1=T1;
    		    	  }
		    	else  
    		    	  if (Tee->T2==T)
    		    	    {
    		    	      Tee->T2=T1;
		    	    }	 
    		    	  else
		    	    if (Tee->T3==T)
		    	      {
    		    		Tee->T3=T1;
    		    	      } 
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 12)\n");
		    	      }    
    		      }


    		    Tee = T1->T1;
    		    if (Tee!=NULL)
    		      {    
                    	if (Tee->T1==Te)
    		    	  {
    		    	    Tee->T1=T1;
    		    	  }
		    	else  
    		    	  if (Tee->T2==Te)
    		    	    {
    		    	      Tee->T2=T1;
		    	    }	 
    		    	  else
		    	    if (Tee->T3==Te)
		    	      {
    		    		Tee->T3=T1;
    		    	      } 
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 11)\n");
		    	      }    
    		      }


    		    Tee = T2->T3;
    		    if (Tee!=NULL)
    		      {    
    		    	if (Tee->T1==Te)
    		    	  {
    		    	    Tee->T1=T2;
    		    	  }
		    	else  
    		    	  if (Tee->T2==Te)
    		    	    {
    		    	      Tee->T2=T2;
		    	    }	 
    		    	  else  
    		    	    if (Tee->T3==Te)
    		    	      {
    		    		Tee->T3=T2;
		    	      }
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 23)\n");
		    	      } 			
    		      }
		      
    		    Tee = T2->T1;
    		    if (Tee!=NULL)
    		      {    
    		    	if (Tee->T1==T)
    		    	  {
    		    	    Tee->T1=T2;
    		    	  }
		    	else  
    		    	  if (Tee->T2==T)
    		    	    {
    		    	      Tee->T2=T2;
		    	    }	 
    		    	  else  
    		    	    if (Tee->T3==T)
    		    	      {
    		    		Tee->T3=T2;
		    	      }
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 21)\n");
		    	      } 			
    		      }

	      	  }
	      	else  
	      	  if (Te->T2 == T)
	      	    {
		      //printf("insphere 22\n");
		      
		      Ts1 = *T; 	    /* save the content of the pointed triangle */
		      Ts2 = *Te;	    /* save the content of the pointed triangle */
	
		      /* index of the new triangles */
		      idx1 = T->idx;
		      idx2 = Te->idx;
    		    
    		    
    		      /* create pointers towards the triangles */
 		      T1 = &Triangles[idx1];
		      T2 = &Triangles[idx2];
  		    
    		    
    		      /* first */
		      T1->idx = idx1;

    		      T1->Pt1 = Ts1.Pt2; 
    		      T1->Pt2 = Ts1.Pt3; 
    		      T1->Pt3 = Ts2.Pt2;
    		    
    		      /* second */
    		      T2->idx = idx2;
    
   		      T2->Pt1 = Ts2.Pt2; 
    		      T2->Pt2 = Ts2.Pt3; 
    		      T2->Pt3 = Ts1.Pt2;      
    				    
    		      /* add adjacents */   
    		      T1->T1 = Ts2.T3; 
        	      T1->T2 =     T2; 
		      T1->T3 = Ts1.T1;     
    		       
    		      T2->T1 = Ts1.T3; 
    		      T2->T2 =     T1;
    		      T2->T3 = Ts2.T1;     
    		    
    		    
    		      /* restore links with adgacents triangles */
    
    		      Tee = T1->T3;
    		      if (Tee!=NULL)
    		        {    
    		          if (Tee->T1==T)
    		            {
    		              Tee->T1=T1;
    		            }
		          else  
    		            if (Tee->T2==T)
    		              {
    		        	Tee->T2=T1;
		              }    
    		            else
		              if (Tee->T3==T)
		        	{
    		        	  Tee->T3=T1;
    		        	} 
		              else
		        	{
		        	  printf("we are in trouble here... (split 12)\n");
		        	}    
    		        }


    		      Tee = T1->T1;
    		      if (Tee!=NULL)
    		        {    
                          if (Tee->T1==Te)
    		            {
    		              Tee->T1=T1;
    		            }
		          else  
    		            if (Tee->T2==Te)
    		              {
    		        	Tee->T2=T1;
		              }    
    		            else
		              if (Tee->T3==Te)
		        	{
    		        	  Tee->T3=T1;
    		        	} 
		              else
		        	{
		        	  printf("we are in trouble here... (split 11)\n");
		        	}    
    		        }


    		      Tee = T2->T3;
    		      if (Tee!=NULL)
    		        {    
    		          if (Tee->T1==Te)
    		            {
    		              Tee->T1=T2;
    		            }
		          else  
    		            if (Tee->T2==Te)
    		              {
    		        	Tee->T2=T2;
		              }    
    		            else  
    		              if (Tee->T3==Te)
    		        	{
    		        	  Tee->T3=T2;
		        	}
		              else
		        	{
		        	  printf("we are in trouble here... (split 23)\n");
		        	}			  
    		        }
		        
    		      Tee = T2->T1;
    		      if (Tee!=NULL)
    		        {    
    		          if (Tee->T1==T)
    		            {
    		              Tee->T1=T2;
    		            }
		          else  
    		            if (Tee->T2==T)
    		              {
    		        	Tee->T2=T2;
		              }    
    		            else  
    		              if (Tee->T3==T)
    		        	{
    		        	  Tee->T3=T2;
		        	}
		              else
		        	{
		        	  printf("we are in trouble here... (split 21)\n");
		        	}			  
    		        }

	      	    }	   
	      	  else  	  
	      	    if (Te->T3 == T)
	      	      {
		        //printf("insphere 23\n");
			
			Ts1 = *T;	      /* save the content of the pointed triangle */
			Ts2 = *Te;	      /* save the content of the pointed triangle */
		  
    			/* index of the new triangles */
    			idx1 = T->idx;
    			idx2 = Te->idx;
 

  			/* create pointers towards the triangles */
    			T1 = &Triangles[idx1];
    			T2 = &Triangles[idx2];


    			/* first */
    			T1->idx = idx1;
    
    			T1->Pt1 = Ts1.Pt2; 
    			T1->Pt2 = Ts1.Pt3; 
    			T1->Pt3 = Ts2.Pt3;
    
   			/* second */
    			T2->idx = idx2;
    
    			T2->Pt1 = Ts2.Pt3; 
    			T2->Pt2 = Ts2.Pt1; 
    			T2->Pt3 = Ts1.Pt2;	
        			      
			/* add adjacents */   
    			T1->T1 = Ts2.T1; 
    			T1->T2 =     T2; 
    			T1->T3 = Ts1.T1;     
    			 
    			T2->T1 = Ts1.T3; 
    			T2->T2 =     T1;
    			T2->T3 = Ts2.T2;     
        

    			/* restore links with adgacents triangles */
    
    			Tee = T1->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==T)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 12)\n");
				  }    
    			  }


    			Tee = T1->T1;
    			if (Tee!=NULL)
    			  {    
                            if (Tee->T1==Te)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==Te)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 11)\n");
				  }    
    			  }


    			Tee = T2->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==Te)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==Te)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 23)\n");
				  }			    
    			  }
			  
    			Tee = T2->T1;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==T)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 21)\n");
				  }			    
    			  }
	      	      }
	      	    else
	      	      {
	      		printf("we are in trouble here... (DoTrianglesInStack 3)\n");
	      	      } 		     
	      	 
	      
	        /* add triangles in stack */
		//printf("add triangle %d (i=%d) and %d (i=%d) in stack\n",T1->idx,istack,T2->idx,numTinStack+1);
		TStack[istack            ] = T1;
		TStack[istack+numTinStack] = T2;
		numTinStack++;
		continue;	 
	      	      
	      }     
	  }


        /*******************/	
	/* triangle 3      */
	/*******************/
	Te = T->T3;
	if (Te!=NULL)
	  {
	    if ((Te->T1!=NULL)&&(Te->T1 == T))
	      {
	    	P = *Te->Pt1;
	      }
	    else  
	      if ((Te->T2!=NULL)&&(Te->T2 == T))
	    	{
	    	  P = *Te->Pt2;
	    	}      
	      else	      
	    	if ((Te->T3!=NULL)&&(Te->T3 == T))
	    	  {
	    	    P = *Te->Pt3;
	    	  }
	        else
	    	  {
	    	    printf("we are in trouble here... (DoTrianglesInStack 2)\n");
	    	  }			 
	       
	    insphere = InCircumCircle(TriangleInList2Triangle(*T),P); 
	    if (insphere)
	      {
	      	//printf("insphere (3)... %g %g %g in T=%d\n",P.Pos[0],P.Pos[1],P.Pos[2],T->idx);
	      
	      	if (Te->T1 == T)
	      	  {
		    //printf("insphere 31\n");
		    
		    Ts1 = *T;		  /* save the content of the pointed triangle */
		    Ts2 = *Te;  	  /* save the content of the pointed triangle */
		  
    		    /* index of the new triangles */
    		    idx1 = T->idx;
    		    idx2 = Te->idx;
 

  		    /* create pointers towards the triangles */
    		    T1 = &Triangles[idx1];
    		    T2 = &Triangles[idx2];


    		    /* first */
    		    T1->idx = idx1;
    
    		    T1->Pt1 = Ts1.Pt3; 
    		    T1->Pt2 = Ts1.Pt1; 
    		    T1->Pt3 = Ts2.Pt1;
    
   		    /* second */
    		    T2->idx = idx2;
    
    		    T2->Pt1 = Ts2.Pt1; 
    		    T2->Pt2 = Ts2.Pt2; 
    		    T2->Pt3 = Ts1.Pt3;      
        			  
		    /* add adjacents */   
    		    T1->T1 = Ts2.T2; 
    		    T1->T2 =	 T2; 
    		    T1->T3 = Ts1.T2;	 
    		     
    		    T2->T1 = Ts1.T1; 
    		    T2->T2 =	 T1;
    		    T2->T3 = Ts2.T3;	 
        

    		    /* restore links with adgacents triangles */
    
    		    Tee = T1->T3;
    		    if (Tee!=NULL)
    		      {    
    		    	if (Tee->T1==T)
    		    	  {
    		    	    Tee->T1=T1;
    		    	  }
		    	else  
    		    	  if (Tee->T2==T)
    		    	    {
    		    	      Tee->T2=T1;
		    	    }	 
    		    	  else
		    	    if (Tee->T3==T)
		    	      {
    		    		Tee->T3=T1;
    		    	      } 
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 12)\n");
		    	      }    
    		      }


    		    Tee = T1->T1;
    		    if (Tee!=NULL)
    		      {    
                    	if (Tee->T1==Te)
    		    	  {
    		    	    Tee->T1=T1;
    		    	  }
		    	else  
    		    	  if (Tee->T2==Te)
    		    	    {
    		    	      Tee->T2=T1;
		    	    }	 
    		    	  else
		    	    if (Tee->T3==Te)
		    	      {
    		    		Tee->T3=T1;
    		    	      } 
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 11)\n");
		    	      }    
    		      }


    		    Tee = T2->T3;
    		    if (Tee!=NULL)
    		      {    
    		    	if (Tee->T1==Te)
    		    	  {
    		    	    Tee->T1=T2;
    		    	  }
		    	else  
    		    	  if (Tee->T2==Te)
    		    	    {
    		    	      Tee->T2=T2;
		    	    }	 
    		    	  else  
    		    	    if (Tee->T3==Te)
    		    	      {
    		    		Tee->T3=T2;
		    	      }
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 23)\n");
		    	      } 			
    		      }
		      
    		    Tee = T2->T1;
    		    if (Tee!=NULL)
    		      {    
    		    	if (Tee->T1==T)
    		    	  {
    		    	    Tee->T1=T2;
    		    	  }
		    	else  
    		    	  if (Tee->T2==T)
    		    	    {
    		    	      Tee->T2=T2;
		    	    }	 
    		    	  else  
    		    	    if (Tee->T3==T)
    		    	      {
    		    		Tee->T3=T2;
		    	      }
		    	    else
		    	      {
		    		printf("we are in trouble here... (split 21)\n");
		    	      } 			
    		      }

	      	  }
	      	else  
	      	  if (Te->T2 == T)
	      	    {
		      //printf("insphere 32\n");
		      
		      Ts1 = *T; 	    /* save the content of the pointed triangle */
		      Ts2 = *Te;	    /* save the content of the pointed triangle */
	
		      /* index of the new triangles */
		      idx1 = T->idx;
		      idx2 = Te->idx;
    		    
    		    
    		      /* create pointers towards the triangles */
 		      T1 = &Triangles[idx1];
		      T2 = &Triangles[idx2];
  		    
    		    
    		      /* first */
		      T1->idx = idx1;

    		      T1->Pt1 = Ts1.Pt3; 
    		      T1->Pt2 = Ts1.Pt1; 
    		      T1->Pt3 = Ts2.Pt2;
    		    
    		      /* second */
    		      T2->idx = idx2;
    
   		      T2->Pt1 = Ts2.Pt2; 
    		      T2->Pt2 = Ts2.Pt3; 
    		      T2->Pt3 = Ts1.Pt3;      
    				    
    		      /* add adjacents */   
    		      T1->T1 = Ts2.T3; 
        	      T1->T2 =     T2; 
		      T1->T3 = Ts1.T2;     
    		       
    		      T2->T1 = Ts1.T1; 
    		      T2->T2 =     T1;
    		      T2->T3 = Ts2.T1;     
		          		    
    		    
    		      /* restore links with adgacents triangles */
		      
    		      Tee = T1->T3;
    		      if (Tee!=NULL)
    		        {    
    		          if (Tee->T1==T)
    		            {
    		              Tee->T1=T1;
    		            }
		          else  
    		            if (Tee->T2==T)
    		              {
    		        	Tee->T2=T1;
		              }    
    		            else
		              if (Tee->T3==T)
		        	{
    		        	  Tee->T3=T1;
    		        	} 
		              else
		        	{
		        	  printf("we are in trouble here... (split 12)\n");
		        	}    
    		        }


    		      Tee = T1->T1;
    		      if (Tee!=NULL)
    		        {    
                          if (Tee->T1==Te)
    		            {
    		              Tee->T1=T1;
    		            }
		          else  
    		            if (Tee->T2==Te)
    		              {
    		        	Tee->T2=T1;
		              }    
    		            else
		              if (Tee->T3==Te)
		        	{
    		        	  Tee->T3=T1;
    		        	} 
		              else
		        	{
		        	  printf("we are in trouble here... (split 11)\n");
		        	}    
    		        }


    		      Tee = T2->T3;
    		      if (Tee!=NULL)
    		        {    
    		          if (Tee->T1==Te)
    		            {
    		              Tee->T1=T2;
    		            }
		          else  
    		            if (Tee->T2==Te)
    		              {
    		        	Tee->T2=T2;
		              }    
    		            else  
    		              if (Tee->T3==Te)
    		        	{
    		        	  Tee->T3=T2;
		        	}
		              else
		        	{
		        	  printf("we are in trouble here... (split 23)\n");
		        	}			  
    		        }
		        
    		      Tee = T2->T1;
    		      if (Tee!=NULL)
    		        {    
    		          if (Tee->T1==T)
    		            {
    		              Tee->T1=T2;
    		            }
		          else  
    		            if (Tee->T2==T)
    		              {
    		        	Tee->T2=T2;
		              }    
    		            else  
    		              if (Tee->T3==T)
    		        	{
    		        	  Tee->T3=T2;
		        	}
		              else
		        	{
		        	  printf("we are in trouble here... (split 21)\n");
		        	}			  
    		        }

	      	    }	   
	      	  else  	  
	      	    if (Te->T3 == T)
	      	      {
		        //printf("insphere 33\n");
			
			Ts1 = *T;	      /* save the content of the pointed triangle */
			Ts2 = *Te;	      /* save the content of the pointed triangle */
		  
    			/* index of the new triangles */
    			idx1 = T->idx;
    			idx2 = Te->idx;

  			/* create pointers towards the triangles */
    			T1 = &Triangles[idx1];
    			T2 = &Triangles[idx2];

    			/* first */
    			T1->idx = idx1;
    
    			T1->Pt1 = Ts1.Pt3; 
    			T1->Pt2 = Ts1.Pt1; 
    			T1->Pt3 = Ts2.Pt3;
    
   			/* second */
    			T2->idx = idx2;
    
    			T2->Pt1 = Ts2.Pt3; 
    			T2->Pt2 = Ts2.Pt1; 
    			T2->Pt3 = Ts1.Pt3;	
        			      
			/* add adjacents */   
    			T1->T1 = Ts2.T1; 
    			T1->T2 =     T2; 
    			T1->T3 = Ts1.T2;     
    			 
    			T2->T1 = Ts1.T1; 
    			T2->T2 =     T1;
    			T2->T3 = Ts2.T2;     
			

    			/* restore links with adgacents triangles */
    
    			Tee = T1->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==T)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 12)\n");
				  }    
    			  }


    			Tee = T1->T1;
    			if (Tee!=NULL)
    			  {    
                            if (Tee->T1==Te)
    			      {
    				Tee->T1=T1;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T1;
				}    
    			      else
				if (Tee->T3==Te)
				  {
    				    Tee->T3=T1;
    				  } 
				else
				  {
				    printf("we are in trouble here... (split 11)\n");
				  }    
    			  }


    			Tee = T2->T3;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==Te)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==Te)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==Te)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 23)\n");
				  }			    
    			  }
			  
    			Tee = T2->T1;
    			if (Tee!=NULL)
    			  {    
    			    if (Tee->T1==T)
    			      {
    				Tee->T1=T2;
    			      }
			    else  
    			      if (Tee->T2==T)
    				{
    				  Tee->T2=T2;
				}    
    			      else  
    				if (Tee->T3==T)
    				  {
    				    Tee->T3=T2;
				  }
				else
				  {
				    printf("we are in trouble here... (split 21)\n");
				  }			    
    			  }			  
	      	      }
	      	    else
	      	      {
	      		printf("we are in trouble here... (DoTrianglesInStack 3)\n");
	      	      } 		     
	      		      
	        /* add triangles in stack */
		//printf("add triangle %d (i=%d) and %d (i=%d) in stack\n",T1->idx,istack,T2->idx,numTinStack+1);
		TStack[istack            ] = T1;
		TStack[istack+numTinStack] = T2;
		numTinStack++;
		continue;	      
	      
	      
	      }     
	  }

  	
        numTinStack--;
	istack++;
	
        //printf("one triangle less...(istack=%d numTinStack=%d)\n",istack,numTinStack);

	
      }
    


  
  }


void Check(void)
  {
    
    int iT;
    
    printf("===========================\n");
    
    for(iT=0;iT<nT;iT++)
      {
        printf("* T %d\n",Triangles[iT].idx);
	printf("pt1    %g %g %g\n",Triangles[iT].Pt1->Pos[0],Triangles[iT].Pt1->Pos[1],Triangles[iT].Pt1->Pos[2]);
	printf("pt2    %g %g %g\n",Triangles[iT].Pt2->Pos[0],Triangles[iT].Pt2->Pos[1],Triangles[iT].Pt2->Pos[2]);
	printf("pt3    %g %g %g\n",Triangles[iT].Pt3->Pos[0],Triangles[iT].Pt3->Pos[1],Triangles[iT].Pt3->Pos[2]);
	if (Triangles[iT].T1!=NULL)
	  printf("T1     %d\n",Triangles[iT].T1->idx);
	else
	  printf("T1     x\n");  
	
	if (Triangles[iT].T2!=NULL)
	  printf("T2     %d\n",Triangles[iT].T2->idx);
	else
	  printf("T2     x\n");  
	
	if (Triangles[iT].T3!=NULL)
	  printf("T3     %d\n",Triangles[iT].T3->idx);
	else
	  printf("T3     x\n");  	  	
      }

    printf("===========================\n");  
  }



/*! Split a triangle in 3, using the point P inside it. 
    Update the global list.
 */

void SplitTriangle(struct TriangleInList *pT,struct Point *Pt)
  {      

    struct TriangleInList T,*T1,*T2,*T3,*Te;
    int idx0,idx1,idx2,idx3;
    
    
       
    T = *pT;		/* save the content of the pointed triangle */

    idx0 = T.idx;

    
    /* index of the new triangles */
    idx1 = idx0;
    idx2 = nT;
    idx3 = nT+1;	
 
    /* increment counter */
    nT=nT+2;	
         
    /* create pointers towards the triangles */
    T1 = &Triangles[idx1];
    T2 = &Triangles[idx2];
    T3 = &Triangles[idx3];
        
    
    /* first */
    T1->idx = idx1;
    
    T1->Pt1 = T.Pt1; 
    T1->Pt2 = T.Pt2; 
    T1->Pt3 = Pt;
   
    /* second */
    T2->idx = idx2;
    
    T2->Pt1 = T.Pt2; 
    T2->Pt2 = T.Pt3; 
    T2->Pt3 = Pt;       

    /* third */
    T3->idx = idx3;
    
    T3->Pt1 = T.Pt3; 
    T3->Pt2 = T.Pt1; 
    T3->Pt3 = Pt;    
        
	  
    /* add adjacents */	  
    T1->T1 = T2; 
    T1->T2 = T3; 
    T1->T3 = T.T3;       
     
    T2->T1 = T3; 
    T2->T2 = T1;
    T2->T3 = T.T1;     
    
    T3->T1 = T1;
    T3->T2 = T2;
    T3->T3 = T.T2;	

       
    
    /* restore links with adgacents triangles */
    
    Te = T1->T3;
    if (Te!=NULL)
      {    
    	if (Te->T1==pT)
    	  {
    	    Te->T1=T1;
    	  }
	else  
    	  if (Te->T2==pT)
    	    {
    	      Te->T2=T1;
	    }	 
    	  else
	    if (Te->T3==pT)
	      {
    	    	Te->T3=T1;
    	      } 
	    else
	      {
	        printf("we are in trouble here... (split 1)\n");
	      }    
      }

    Te = T2->T3;
    if (Te!=NULL)
      {    
    	if (Te->T1==pT)
    	  {
    	    Te->T1=T2;
    	  }
	else  
    	  if (Te->T2==pT)
    	    {
    	      Te->T2=T2;
	    }	 
    	  else  
    	    if (Te->T3==pT)
    	      {
    	    	Te->T3=T2;
	      }
	    else
	      {
	        printf("we are in trouble here... (split 2)\n");
	      }  	                
      }
      
    Te = T3->T3;
    if (Te!=NULL)
      {
    	if (Te->T1==pT)
    	  {
    	    Te->T1=T3;
    	  }
	else  
    	  if (Te->T2==pT)
    	    {
    	      Te->T2=T3;
	    }
	  else  	 
    	    if (Te->T3==pT)
    	      {
    	    	Te->T3=T3;
    	      } 
	    else
	      {
	        printf("we are in trouble here... (split 3)\n");
	      }  	             
      }



     /* add the new triangles in the stack */
     TStack[numTinStack] = T1;
     numTinStack++;
     
     TStack[numTinStack] = T2;
     numTinStack++;
     
     TStack[numTinStack] = T3;
     numTinStack++;

     
     
     //printf("--> add in stack %d %d %d\n",T1->idx,T2->idx,T3->idx);     	  
   
	  
	  
  }





int FindTriangle(struct Point *Pt)
  {
    int iT;
        
    /* find triangle containing the point */
    for(iT=0;iT<nT;iT++)	/* loop over all triangles */ 
      {
      	if (InTriangle(TriangleInList2Triangle( Triangles[iT] ),*Pt))
          break;
      }
    
    return iT;
  }



/*! Add a new point in the tesselation
 */

void AddPoint(struct Point *Pt)
  {

    int iT;
    
    /* find the triangle that contains the point P */
    iT= FindTriangle(Pt);
    //printf("\n---> point in triangle %d\n",iT);
    //printf(" T1=%g %g %g\n",Triangles[iT].Pt1->Pos[0],Triangles[iT].Pt1->Pos[1],Triangles[iT].Pt1->Pos[2]);
    //printf(" T2=%g %g %g\n",Triangles[iT].Pt2->Pos[0],Triangles[iT].Pt2->Pos[1],Triangles[iT].Pt2->Pos[2]);
    //printf(" T3=%g %g %g\n",Triangles[iT].Pt3->Pos[0],Triangles[iT].Pt3->Pos[1],Triangles[iT].Pt3->Pos[2]);
    
    //printf("add point in triangle %d (%g %g %g)\n",Triangles[iT].idx,Pt->Pos[0],Pt->Pos[1],Pt->Pos[2]);
                    
    /* create the new triangles */
    SplitTriangle(&Triangles[iT],Pt);	 
    
    /* test the new triangles and divide and modify if necessary */	    
    DoTrianglesInStack();

    /* check */
    //CheckTriangles();
  
  }




/************************************************************/
/*  PYTHON INTERFACE                                        */
/************************************************************/







static PyObject *
      tessel_CircumCircleProperties(self, args)
          PyObject *self;
          PyObject *args;
      {


        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	  
        struct Point Pt1,Pt2,Pt3;
	double xc,yc,r;


        if (!PyArg_ParseTuple(args,"OOO",&p1,&p2,&p3))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) )
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) )
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     


        r = CircumCircleProperties(Pt1,Pt2,Pt3,&xc,&yc);


        return Py_BuildValue("(ddd)",r,xc,yc);

      }               







static PyObject *
      tessel_InTriangle(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	PyArrayObject *p4 = NULL;
	  
        struct Point Pt1,Pt2,Pt3,Pt4;
	struct Triangle T;
	int b;


        if (!PyArg_ParseTuple(args,"OOOO",&p1,&p2,&p3,&p4))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3) && PyArray_Check(p4)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) || (p4->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) || (p4->dimensions[0]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     

	Pt4.Pos[0] = *(double *) (p4->data + 0*(p4->strides[0]));
	Pt4.Pos[1] = *(double *) (p4->data + 1*(p4->strides[0]));

        T = MakeTriangleFromPoints(Pt1,Pt2,Pt3);
	T = OrientTriangle(T);
        
	b = InTriangle(T,Pt4);
	


        return Py_BuildValue("i",b);

      }               






static PyObject *
      tessel_InCircumCircle(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	PyArrayObject *p4 = NULL;
	  
        struct Point Pt1,Pt2,Pt3,Pt4;
	struct Triangle T;
	int b;


        if (!PyArg_ParseTuple(args,"OOOO",&p1,&p2,&p3,&p4))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3) && PyArray_Check(p4)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) || (p4->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) || (p4->dimensions[0]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);
	p3 = TO_DOUBLE(p3);		  
	    
	      
	Pt1.Pos[0] = *(double *) (p1->data + 0*(p1->strides[0]));
	Pt1.Pos[1] = *(double *) (p1->data + 1*(p1->strides[0]));
	
	Pt2.Pos[0] = *(double *) (p2->data + 0*(p2->strides[0]));
	Pt2.Pos[1] = *(double *) (p2->data + 1*(p2->strides[0]));	    
	
	Pt3.Pos[0] = *(double *) (p3->data + 0*(p3->strides[0]));
	Pt3.Pos[1] = *(double *) (p3->data + 1*(p3->strides[0]));	     

	Pt4.Pos[0] = *(double *) (p4->data + 0*(p4->strides[0]));
	Pt4.Pos[1] = *(double *) (p4->data + 1*(p4->strides[0]));


        T = MakeTriangleFromPoints(Pt1,Pt2,Pt3);
	T = OrientTriangle(T);
        
	b = InCircumCircle(T,Pt4);
	


        return Py_BuildValue("i",b);

      }               



static PyObject *
      tessel_ConstructDelaunay(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *pos = NULL;
	  
	int i;



        if (!PyArg_ParseTuple(args,"O",&pos))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(pos)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments must be array.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (pos->nd!=2))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of argument must be 2.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (pos->dimensions[1]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"First size of argument must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	pos = TO_DOUBLE(pos);		      
        NumPart = pos->dimensions[0];
	
	/* add first triangle */
	
        /* init */
	All.MaxPart = NumPart;
        
	
        /* allocate memory */
        allocate_memory();
       

        /* init P */
	/* loop over all points */

       for (i=0;i<NumPart;i++)
         {
   	   P[i].Pos[0] = *(double *) (pos->data + i*(pos->strides[0]) + 0*pos->strides[1]);
	   P[i].Pos[1] = *(double *) (pos->data + i*(pos->strides[0]) + 1*pos->strides[1]);
	   P[i].Pos[2] = *(double *) (pos->data + i*(pos->strides[0]) + 2*pos->strides[1]);
	 }


        /* edges */
	Pe[0].Pos[0] = 0;
	Pe[0].Pos[1] = 0;
	Pe[0].Pos[2] = 0;
	
	Pe[1].Pos[0] = 2;
	Pe[1].Pos[1] = 0;
	Pe[1].Pos[2] = 0;
	
	Pe[2].Pos[0] = 0;
	Pe[2].Pos[1] = 2;
	Pe[2].Pos[2] = 0;
	
        
	/* Triangle list */
	Triangles[0].idx = 0;        
	Triangles[0].Pt1 =  &Pe[0];
	Triangles[0].Pt2 =  &Pe[1];
	Triangles[0].Pt3 =  &Pe[2];
	Triangles[0].T1 = NULL;  
	Triangles[0].T2 = NULL;  
	Triangles[0].T3 = NULL;  
	
	
	/* check that it is oriented */
	OrientTriangleInList(Triangles[0]);


	nT++;
        
	/* loop over all points */
        for (i=0;i<pos->dimensions[0];i++)
          {	     
	     AddPoint(&P[i]);  
	  }


        /* check */
        CheckTriangles();

		
        return Py_BuildValue("i",1);

      }               


static PyObject *
      tessel_GetTriangles(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyObject *OutputList;
	PyObject *OutputDict;
	PyArrayObject *tri = NULL;
	npy_intp dim[2];
	int iT;
	
	/* loop over all triangles */
	OutputList = PyList_New(0);

        	
	for (iT=0;iT<nT;iT++)	
	  {
	  
	    
	    /* 3x3 vector */
	    dim[0]=3;
	    dim[1]=3;
		  
	    tri = (PyArrayObject *) PyArray_SimpleNew(2,dim,PyArray_DOUBLE);
            

	    *(double *) (tri->data + 0*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].Pt1->Pos[0];
     	    *(double *) (tri->data + 0*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].Pt1->Pos[1];
	    *(double *) (tri->data + 0*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	     
	    *(double *) (tri->data + 1*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].Pt2->Pos[0];
     	    *(double *) (tri->data + 1*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].Pt2->Pos[1];
	    *(double *) (tri->data + 1*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	    
	    *(double *) (tri->data + 2*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].Pt3->Pos[0];
     	    *(double *) (tri->data + 2*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].Pt3->Pos[1];
	    *(double *) (tri->data + 2*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	    
	    	    
	    
	    OutputDict = PyDict_New();
	    PyDict_SetItem(OutputDict,PyString_FromString("id"),PyInt_FromLong(Triangles[iT].idx) );
	    PyDict_SetItem(OutputDict,PyString_FromString("coord"),(PyObject*)tri);
	    
	    //(PyObject*)tri
	    
	    PyList_Append(OutputList, OutputDict );
	    	    
	    
	    	
	  }
	
	      


        return Py_BuildValue("O",OutputList);

      }               



/*********************************/
/* test */
/*********************************/

static PyObject *
      tessel_test(self, args)
          PyObject *self;
          PyObject *args;
      {
	   
	
        return Py_BuildValue("i",1);

      }               


           
/* definition of the method table */      
      
static PyMethodDef tesselMethods[] = {


          {"test",  tessel_test, METH_VARARGS,
           "Simple Test"},

          {"CircumCircleProperties",  tessel_CircumCircleProperties, METH_VARARGS,
           "Get Circum Circle Properties"},

          {"InTriangle",  tessel_InTriangle, METH_VARARGS,
           "Return if the triangle (P1,P2,P3) contains the point P4"},


          {"InCircumCircle",  tessel_InCircumCircle, METH_VARARGS,
           "Return if the circum circle of the triangle (P1,P2,P3) contains the point P4"},

          {"ConstructDelaunay",  tessel_ConstructDelaunay, METH_VARARGS,
           "Construct the Delaunay tesselation for a given sample of points"},
	   

          {"GetTriangles",  tessel_GetTriangles, METH_VARARGS,
           "Get the trianles in a list of 3x3 arrays."},



	   	   	   	   
          {NULL, NULL, 0, NULL}        /* Sentinel */
      };      
      
      
      
void inittessel(void)
      {    
          (void) Py_InitModule("tessel", tesselMethods);	
	  
	  import_array();
      }      
      
