#include <Python.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <numpy/libnumarray.h>

#define TO_DOUBLE(a)        ( (PyArrayObject*) PyArray_CastToType(a, PyArray_DescrFromType(NPY_DOUBLE)  ,0) )

#define MAXNUMTRIANGLES 1000








struct Point
  {
    double x;
    double y;
    double z;
  };

struct Triangle
  {
    struct Point P1;
    struct Point P2;
    struct Point P3;
  };


struct TriangleIndex
  {
    int p1;		/* index of first  point */
    int p2;		/* index of second point */
    int p3;		/* index of third  point */
    int t1;		/* index of first  triangle */
    int t2;		/* index of second triangle */
    int t3;		/* index of third  triangle */
  };


struct TriangleInList
  {
    struct Triangle T;	/* triangle */
    int idx;            /* index of current triangle */
    int t1;		/* index of first  triangle */
    int t2;		/* index of second triangle */
    int t3;		/* index of third  triangle */
    /* nex and prev are useless, as the triangles must keep their order once created... */
    int next;		/* next triangle in the list */
    int prev;		/* prev triangle in the list */
  };






/* some global varables */

int nT=0,numTinStack=0;						/* number of triangles in the list */
struct TriangleInList Triangles[MAXNUMTRIANGLES];	/* list of triangles               */
int TStack[MAXNUMTRIANGLES];				/* index of triangles to check	   */

double testValue;











/*! For a set of three points, construct a triangle
 */

struct Triangle MakeTriangleFromPoints(struct Point P1,struct Point P2,struct Point P3)
  {
    struct Triangle T;
    T.P1 = P1; 
    T.P2 = P2; 
    T.P3 = P3; 
    return T;  
  }





/*! For a set of three points, this function computes their cirum-circle.
 *  Its radius is return, while the center is return using pointers.
 */

double CircumCircleProperties(struct Point P1,struct Point P2,struct Point P3, double *xc, double *yc)
  {
    
      
    double r;
    double x21,x32,y21,y32;
    double x12mx22,y12my22,x22mx32,y22my32;
    double c1,c2;
    
    x21 = P2.x-P1.x;
    x32 = P3.x-P2.x;
    
    y21 = P2.y-P1.y;
    y32 = P3.y-P2.y;
    
    
    x12mx22 = (P1.x*P1.x)-(P2.x*P2.x);
    y12my22 = (P1.y*P1.y)-(P2.y*P2.y);
    x22mx32 = (P2.x*P2.x)-(P3.x*P3.x);
    y22my32 = (P2.y*P2.y)-(P3.y*P3.y);
    
    c1 = x12mx22 + y12my22;
    c2 = x22mx32 + y22my32;
    
    
    *xc = (y32*c1 -  y21*c2)/2.0/( x32*y21 - x21*y32 );
    *yc = (x32*c1 -  x21*c2)/2.0/( x21*y32 - x32*y21 );
    
    r = sqrt( (P1.x-*xc)*(P1.x-*xc) + (P1.y-*yc)*(P1.y-*yc) ) ;
    
    return r;
  
  }



/*! For a given triangle T, the routine tells if the point P4
    is in the circum circle of the triangle or not.
 */


int InCircumCircle(struct Triangle T,struct Point P4)
  {      
  
    double a,b,c;
    double d,e,f;
    double g,h,i;  
    double det;
    
    
    a = T.P1.x - P4.x;
    b = T.P1.y - P4.y;
    c = (T.P1.x*T.P1.x - P4.x*P4.x) + (T.P1.y*T.P1.y - P4.y*P4.y)  ;

    d = T.P2.x - P4.x;
    e = T.P2.y - P4.y;
    f = (T.P2.x*T.P2.x - P4.x*P4.x) + (T.P2.y*T.P2.y - P4.y*P4.y);

    g = T.P3.x - P4.x;
    h = T.P3.y - P4.y;
    i = (T.P3.x*T.P3.x - P4.x*P4.x) + (T.P3.y*T.P3.y - P4.y*P4.y);
    
    det = a*e*i - a*f*h - b*d*i + b*f*g + c*d*h - c*e*g;
    
    
    if (det>0)  
      return 1;			/* inside */
    else
      return 0;			/* outside */  
  }



/*! For a given triangle T, the routine tells if the point P4
    lie inside the triangle or not.
 */


int InTriangle(struct Triangle T,struct Point P4)
  {      
  
    double c1,c2,c3;
    
    /* here, we use the cross product */
    c1 = (T.P2.x-T.P1.x)*(P4.y-T.P1.y) - (T.P2.y-T.P1.y)*(P4.x-T.P1.x);
    c2 = (T.P3.x-T.P2.x)*(P4.y-T.P2.y) - (T.P3.y-T.P2.y)*(P4.x-T.P2.x);
    c3 = (T.P1.x-T.P3.x)*(P4.y-T.P3.y) - (T.P1.y-T.P3.y)*(P4.x-T.P3.x);
    
    if ( (c1>0) && (c2>0) && (c3>0) )		/* inside */
      return 1;
    else
      return 0;
    
  }



/*! For a given triangle, orient it positively.
 */

struct Triangle OrientTriangle(struct Triangle T)
  {      
     double a,b,c,d;
     double det;
     struct Point Psto;
   
   
     a = T.P2.x - T.P1.x; 
     b = T.P2.y - T.P1.y; 
     c = T.P3.x - T.P1.x;  
     d = T.P3.y - T.P1.y;
     
     det = (a*d) - (b*c);
     
     if (det<0)
       {
	 Psto = T.P1;
	 T.P1 = T.P3;
	 T.P3 = Psto;    
	 T = OrientTriangle(T);   
       }
            
     return T;
  }





void DoTrianglesInStack(void)
  {
    struct TriangleInList T,Tnext;
    struct Point P;
    int istack;
    int insphere=0;
    
    istack = 0;
        
    printf("numTinStack=%d\n",numTinStack);
    
    
    while(numTinStack>0)
      {
	
	T =  Triangles[TStack[istack]];
	
	/* find the oposite point of the 3 adjacent triangles */
	
	if (T.t1!=-1)
	  {
	    Tnext = Triangles[T.t1];
	  
	    if (Tnext.t1 == T.idx)
	      P = Tnext.T.P1;
	    else
	      if (Tnext.t2 == T.idx) 
	    	P = Tnext.T.P2; 
	      else
	    	 if (Tnext.t3 == T.idx) 
	    	   P = Tnext.T.P2;   
	    	 else
	    	   printf("we are in trouble here... (71) %d %d %d %d %d\n",T.idx,Tnext.idx,Tnext.t1,Tnext.t2,Tnext.t3);  
	    insphere= InCircumCircle(T.T,P);   
	    
	    if (insphere)
	      {
	        /* flip the triangles T and Tnext */
		if (Tnext.t1 == T.idx)
		  {
		    
		    /* change the indexes that points towards T */
		    
		    
		    
		    /* correct the triangles */
		    
		    
		  
		  }
		
		
		continue;
	        
	      }
	    
	  }


	      
	if (T.t2!=-1)
	  {
	    Tnext = Triangles[T.t2];
	
	    if (Tnext.t1 == T.idx)
	      P = Tnext.T.P1;
	    else
	      if (Tnext.t2 == T.idx) 
	    	P = Tnext.T.P2; 
	      else
	    	 if (Tnext.t3 == T.idx) 
	    	   P = Tnext.T.P2;   
	    	 else
	    	   printf("we are in trouble here... (72) %d %d %d %d %d\n",T.idx,Tnext.idx,Tnext.t1,Tnext.t2,Tnext.t3);
	    insphere= InCircumCircle(T.T,P);	   
	  }
	
	if (T.t3!=-1)
	  {
	    Tnext = Triangles[T.t3];
	
	    if (Tnext.t1 == T.idx)
	      P = Tnext.T.P1;
	    else
	      if (Tnext.t2 == T.idx) 
	    	P = Tnext.T.P2; 
	      else
	    	 if (Tnext.t3 == T.idx) 
	    	   P = Tnext.T.P2;   
	    	 else
	    	   printf("we are in trouble here... (73) %d %d %d %d %d\n",T.idx,Tnext.idx,Tnext.t1,Tnext.t2,Tnext.t3);
	    insphere= InCircumCircle(T.T,P);	   
	  }
		
         


	
	/* test if the point are in the circle */
	
	  /* yes */
	     /* split the triangles and add to the stack list */
	
	  /* no  */
	    /* do nothing */
	
	
	
	
        numTinStack--;
	istack++;
      }
  
  }






/*! Split a triangle in 3, using the point P inside it. 
    Update the global list.
 */

void SplitTriangle(struct TriangleInList Tl,struct Point P)
  {      

    struct Triangle T1,T2,T3;
    int idx;
    int idx0;
    int idx1,idx2,idx3;
    
    idx0 = Tl.idx;
     
     /* first triangle */
     
     T1.P1.x = Tl.T.P1.x;
     T1.P1.y = Tl.T.P1.y;
     
     T1.P2.x = Tl.T.P2.x;
     T1.P2.y = Tl.T.P2.y;
     
     T1.P3.x = P.x;
     T1.P3.y = P.y;   

     /* second triangle */

     T2.P1.x = Tl.T.P2.x;
     T2.P1.y = Tl.T.P2.y;
     
     T2.P2.x = Tl.T.P3.x;
     T2.P2.y = Tl.T.P3.y;
     
     T2.P3.x = P.x;
     T2.P3.y = P.y;	

     /* third triangle */

     T3.P1.x = Tl.T.P3.x;
     T3.P1.y = Tl.T.P3.y;
     
     T3.P2.x = Tl.T.P1.x;
     T3.P2.y = Tl.T.P1.y;
     
     T3.P3.x = P.x;
     T3.P3.y = P.y;	
 
     
     /* index of the new triangles */
     idx1 = idx0;
     idx2 = nT;
     idx3 = nT+1;


    /* change the indexes that points towards T */
    idx = Triangles[idx0].t1;
    if (idx != -1) 
      {	
        /* find which face of the triangle in in common */
        if( Triangles[idx].t1 == idx0 )
	    Triangles[idx].t1 = idx1;
	else    
	   if( Triangles[idx].t2 == idx0 )
	       Triangles[idx].t2 = idx1;
	   else    
             if( Triangles[idx].t3 == idx0 ) 
	     	 Triangles[idx].t3 = idx1;
	     else
	       printf("we are in trouble here...(91)\n");
      }

    idx = Triangles[idx0].t2;
    if (idx != -1) 
      {	
        /* find which face of the triangle in in common */
        if( Triangles[idx].t1 == idx0 )
	    Triangles[idx].t1 = idx2;
        else
	  if( Triangles[idx].t2 == idx0 ) 
              Triangles[idx].t2 = idx2;
	  else    
	    if( Triangles[idx].t3 == idx0 ) 
	    	Triangles[idx].t3 = idx2;
	    else
	       printf("we are in trouble here...(92)\n");
      }

    idx = Triangles[idx0].t3;
    if (idx != -1) 
      {	
        /* find which face of the triangle in in common */
        if( Triangles[idx].t1 == idx0 )
	    Triangles[idx].t1 = idx3;
        else
	  if( Triangles[idx].t2 == idx0 ) 
              Triangles[idx].t2 = idx3;
          else 
	    if( Triangles[idx].t3 == idx0 ) 
	    	Triangles[idx].t3 = idx3;
	    else
	       printf("we are in trouble here...(93)\n");        
      }
      

     /* add the triangles to the global list */
     Triangles[idx1].T   = T1;
     Triangles[idx1].t1  = Tl.t1;
     Triangles[idx1].t2  = idx2;
     Triangles[idx1].t3  = idx3;
     Triangles[idx1].idx = idx1;
          
     Triangles[idx2].T   = T2;
     Triangles[idx2].t1  = Tl.t2;
     Triangles[idx2].t2  = idx3;
     Triangles[idx2].t3  = idx1;
     Triangles[idx2].idx = idx2;
          
     Triangles[idx3].T = T3;
     Triangles[idx3].t1  = Tl.t3;
     Triangles[idx3].t2  = idx1;
     Triangles[idx3].t3  = idx2;
     Triangles[idx3].idx = idx3;



     /* nex and prev are useless, as the triangles must keep their order once created... */
     Triangles[nT-1].next = nT;
     Triangles[nT  ].next = nT+1;
     Triangles[nT+1].next = -1;

     
     /* add the new triangles in the stack */
     TStack[numTinStack] = idx1;
     numTinStack++;
     
     TStack[numTinStack] = idx2;
     numTinStack++;
     
     TStack[numTinStack] = idx3;
     numTinStack++;
     
     /* increment counter */
     nT=nT+2;	     
          
  }


int FindTriangle(struct Point P)
  {
    struct Triangle T;
    int iT;
    
    /* find triangle containing the point */
    iT = 0;
    while(iT>=0)	 
      {
    	T =  Triangles[iT].T;

    	if (InTriangle(T,P))
          break;
    	
	iT = Triangles[iT].next;      
      }
    
    return iT;
  }



/*! Add a new point in the tesselation
 */

void AddPoint(struct Point P)
  {

    struct TriangleInList T;
    int iT;

    /* find the triangle that contains the point P */
    iT= FindTriangle(P);
    T =  Triangles[iT];
    
    /* create the new triangles */
    SplitTriangle(T,P);	 

    /* test the new triangles and divide and modify if necessary */	    
    DoTrianglesInStack();

  
  
  }




/************************************************************/
/*  PYTHON INTERFACE                                        */
/************************************************************/







static PyObject *
      tessel_CircumCircleProperties(self, args)
          PyObject *self;
          PyObject *args;
      {


        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	  
        struct Point P1,P2,P3;
	double xc,yc,r;


        if (!PyArg_ParseTuple(args,"OOO",&p1,&p2,&p3))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) )
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) )
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);		  
	    
	      
	P1.x = *(double *) (p1->data + 0*(p1->strides[0]));
	P1.y = *(double *) (p1->data + 1*(p1->strides[0]));
	
	P2.x = *(double *) (p2->data + 0*(p2->strides[0]));
	P2.y = *(double *) (p2->data + 1*(p2->strides[0]));	
	
	P3.x = *(double *) (p3->data + 0*(p3->strides[0]));
	P3.y = *(double *) (p3->data + 1*(p3->strides[0]));	 


        r = CircumCircleProperties(P1,P2,P3,&xc,&yc);


        return Py_BuildValue("(ddd)",r,xc,yc);

      }               







static PyObject *
      tessel_InTriangle(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	PyArrayObject *p4 = NULL;
	  
        struct Point P1,P2,P3,P4;
	struct Triangle T;
	int b;


        if (!PyArg_ParseTuple(args,"OOOO",&p1,&p2,&p3,&p4))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3) && PyArray_Check(p4)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) || (p4->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) || (p4->dimensions[0]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);
	p3 = TO_DOUBLE(p3);		  
	    
	      
	P1.x = *(double *) (p1->data + 0*(p1->strides[0]));
	P1.y = *(double *) (p1->data + 1*(p1->strides[0]));
	
	P2.x = *(double *) (p2->data + 0*(p2->strides[0]));
	P2.y = *(double *) (p2->data + 1*(p2->strides[0]));	
	
	P3.x = *(double *) (p3->data + 0*(p3->strides[0]));
	P3.y = *(double *) (p3->data + 1*(p3->strides[0]));	 

	P4.x = *(double *) (p4->data + 0*(p4->strides[0]));
	P4.y = *(double *) (p4->data + 1*(p4->strides[0]));


        T = MakeTriangleFromPoints(P1,P2,P3);
	T = OrientTriangle(T);
        
	b = InTriangle(T,P4);
	


        return Py_BuildValue("i",b);

      }               






static PyObject *
      tessel_InCircumCircle(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *p1 = NULL;
	PyArrayObject *p2 = NULL;
	PyArrayObject *p3 = NULL;
	PyArrayObject *p4 = NULL;
	  
        struct Point P1,P2,P3,P4;
	struct Triangle T;
	int b;


        if (!PyArg_ParseTuple(args,"OOOO",&p1,&p2,&p3,&p4))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(p1) && PyArray_Check(p2) && PyArray_Check(p3) && PyArray_Check(p4)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments are not all arrays.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (p1->nd!=1) || (p2->nd!=1) || (p3->nd!=1) || (p4->nd!=1))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of arguments must be 1.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (p1->dimensions[0]!=3) || (p2->dimensions[0]!=3) || (p3->dimensions[0]!=3) || (p4->dimensions[0]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"Size of arguments must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	p1 = TO_DOUBLE(p1);		  
	p2 = TO_DOUBLE(p2);		  
	p3 = TO_DOUBLE(p3);
	p3 = TO_DOUBLE(p3);		  
	    
	      
	P1.x = *(double *) (p1->data + 0*(p1->strides[0]));
	P1.y = *(double *) (p1->data + 1*(p1->strides[0]));
	
	P2.x = *(double *) (p2->data + 0*(p2->strides[0]));
	P2.y = *(double *) (p2->data + 1*(p2->strides[0]));	
	
	P3.x = *(double *) (p3->data + 0*(p3->strides[0]));
	P3.y = *(double *) (p3->data + 1*(p3->strides[0]));	 

	P4.x = *(double *) (p4->data + 0*(p4->strides[0]));
	P4.y = *(double *) (p4->data + 1*(p4->strides[0]));


        T = MakeTriangleFromPoints(P1,P2,P3);
	T = OrientTriangle(T);
        
	b = InCircumCircle(T,P4);
	


        return Py_BuildValue("i",b);

      }               



static PyObject *
      tessel_ConstructDelaunay(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyArrayObject *pos = NULL;
	  
        struct Triangle T;
	struct Point    P;
	int i;


        if (!PyArg_ParseTuple(args,"O",&pos))
          return NULL;

	/* check type */  
	if (!(PyArray_Check(pos)))
          {
	    PyErr_SetString(PyExc_ValueError,"aruments must be array.");
	    return NULL;		  
	  } 
	  
	/* check dimension */	  
	if ( (pos->nd!=2))
          {
	    PyErr_SetString(PyExc_ValueError,"Dimension of argument must be 2.");
	    return NULL;		  
	  } 	  
	  
	/* check size */	  
	if ( (pos->dimensions[1]!=3))
          {
	    PyErr_SetString(PyExc_ValueError,"First size of argument must be 3.");
	    return NULL;		  
	  } 	  
	  
	     			   			    
	/* ensure double */	    
	pos = TO_DOUBLE(pos);		  
	    

        /* create first base triangle */
        T.P1.x = 0;
	T.P1.y = 0;
	T.P1.z = 0;

        T.P2.x = 2;
	T.P2.y = 0;
	T.P2.z = 0;    

        T.P3.x = 0;
	T.P3.y = 2;
	T.P3.z = 0;
        
	/* Triangle list */
        Triangles[0].T = T;
	Triangles[0].idx = 0;
	Triangles[0].t1 = -1;  
	Triangles[0].t2 = -1;  
	Triangles[0].t3 = -1;  
	Triangles[0].next = -1;
        Triangles[0].prev = -1;
	nT++;
        
	/* loop over all points */

       for (i=0;i<pos->dimensions[0];i++)
         {

   	   P.x = *(double *) (pos->data + i*(pos->strides[0]) + 0*pos->strides[1]);
	   P.y = *(double *) (pos->data + i*(pos->strides[0]) + 1*pos->strides[1]);
	   P.z = *(double *) (pos->data + i*(pos->strides[0]) + 2*pos->strides[1]);
	     
	   printf("add point %d\n",i);   
	   AddPoint(P);  
	     
	     

	 

	 }


		
        return Py_BuildValue("i",1);

      }               


static PyObject *
      tessel_GetTriangles(self, args)
          PyObject *self;
          PyObject *args;
      {

        PyObject *OutputList;
	PyArrayObject *tri = NULL;
	npy_intp dim[2];
	int iT;
	
	/* loop over all triangles */
	OutputList = PyList_New(0);
	
	
	iT=0;
        while(iT>=0)		
	  {
	  
	    
	    /* 3x3 vector */
	    dim[0]=3;
	    dim[1]=3;
		  
	    tri = (PyArrayObject *) PyArray_SimpleNew(2,dim,PyArray_DOUBLE);
            
	    *(double *) (tri->data + 0*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].T.P1.x;
     	    *(double *) (tri->data + 0*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].T.P1.y;
	    *(double *) (tri->data + 0*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	     
	    *(double *) (tri->data + 1*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].T.P2.x;
     	    *(double *) (tri->data + 1*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].T.P2.y;
	    *(double *) (tri->data + 1*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	    
	    *(double *) (tri->data + 2*(tri->strides[0]) + 0*tri->strides[1]) = Triangles[iT].T.P3.x;
     	    *(double *) (tri->data + 2*(tri->strides[0]) + 1*tri->strides[1]) = Triangles[iT].T.P3.y;
	    *(double *) (tri->data + 2*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	    
	    //printf("--- %d ---\n",iT);
	    //printf("%g %g\n", Triangles[iT].T.P1.x,Triangles[iT].T.P1.y);
	    //printf("%g %g\n", Triangles[iT].T.P2.x,Triangles[iT].T.P2.y);
	    //printf("%g %g\n", Triangles[iT].T.P3.x,Triangles[iT].T.P3.y);
	    
	    PyList_Append(OutputList, (PyObject*)tri );
	    
	    
	    iT = Triangles[iT].next;
	    
	    
	    	
	  }
	
	      


        return Py_BuildValue("O",OutputList);

      }               



/*********************************/
/* test */
/*********************************/

static PyObject *
      tessel_test(self, args)
          PyObject *self;
          PyObject *args;
      {
	   
        testValue = 7;
	
        return Py_BuildValue("i",1);

      }               


static PyObject *
      tessel_test2(self, args)
          PyObject *self;
          PyObject *args;
      {
	   

        return Py_BuildValue("d",testValue);

      }               

            
/* definition of the method table */      
      
static PyMethodDef tesselMethods[] = {


          {"test",  tessel_test, METH_VARARGS,
           "Simple Test"},

          {"test2",  tessel_test2, METH_VARARGS,
           "Simple Test"},

          {"CircumCircleProperties",  tessel_CircumCircleProperties, METH_VARARGS,
           "Get Circum Circle Properties"},

          {"InTriangle",  tessel_InTriangle, METH_VARARGS,
           "Return if the triangle (P1,P2,P3) contains the point P4"},


          {"InCircumCircle",  tessel_InCircumCircle, METH_VARARGS,
           "Return if the circum circle of the triangle (P1,P2,P3) contains the point P4"},

          {"ConstructDelaunay",  tessel_ConstructDelaunay, METH_VARARGS,
           "Construct the Delaunay tesselation for a given sample of points"},
	   

          {"GetTriangles",  tessel_GetTriangles, METH_VARARGS,
           "Get the trianles in a list of 3x3 arrays."},



	   	   	   	   
          {NULL, NULL, 0, NULL}        /* Sentinel */
      };      
      
      
      
void inittessel(void)
      {    
          (void) Py_InitModule("tessel", tesselMethods);	
	  
	  import_array();
      }      
      
