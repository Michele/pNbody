#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_Tesselation.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import tessel

import Ptools as pt
import plot

import sys
import time

random.seed(4)			# 2 for two points

n = 50000
pos = random.random((n,3))

#pos[0] = [0.5,0.5,0]
#pos[1] = [0.5,0.25,0]
#pos[2] = [0.25,0.5,0]
#pos[3] = [0.75,1.0,0]

t1 = time.time()
tessel.ConstructDelaunay(pos)
t2 = time.time()

TriangleList = tessel.GetTriangles()

print len(TriangleList),"triangles","in",(t2-t1),"s"
sys.exit()

i = 0
for Triangle in TriangleList:
  #print i
  #i = i+1
  #print Triangle

  P1 = Triangle['coord'][0]
  P2 = Triangle['coord'][1]
  P3 = Triangle['coord'][2]
  plot.draw_triangle(P1,P2,P3)

  #cm = 1/3.*(P1+P2+P3)
  #pt.text(cm[0],cm[1],Triangle['id'],fontsize=12,horizontalalignment='center',verticalalignment='center')

  
  #if Triangle['id']==16:
  #  r,xc,yc = tessel.CircumCircleProperties(P1,P2,P3)
  #  plot.draw_circle(r,xc,yc)



for P in pos:
  plot.draw_points([P],'b')

pt.axis([0,2,0,2])
pt.show()

