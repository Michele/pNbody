#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import teslib

from numpy import *

import Ptools as pt
import plot

random.seed(2)

n = 2
pos  = random.random((n,3))


tt = teslib.TES(pos=pos)


TriangleList = tt.GetTriangles()



for Triangle in TriangleList:
  #print Triangle

  P1 = Triangle[0]
  P2 = Triangle[1]
  P3 = Triangle[2]
  plot.draw_triangle(P1,P2,P3)



for P in pos:
  plot.draw_points([P],'b')

pt.axis([0,2,0,2])
pt.show()

