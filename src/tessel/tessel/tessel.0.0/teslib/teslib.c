#include <Python.h>
#include <numpy/arrayobject.h>
#include "structmember.h"

#define TO_DOUBLE(a)        ( (PyArrayObject*) PyArray_CastToType(a, PyArray_DescrFromType(NPY_DOUBLE)  ,0) )


#define MAXNUMTRIANGLES 1000
#define MAXPART 1000

/******************************************************************************

SYSTEM

*******************************************************************************/


/*! returns the maximum of two double
 */
double dmax(double x, double y)
{
  if(x > y)
    return x;
  else
    return y;
}

/*! returns the minimum of two double
 */
double dmin(double x, double y)
{
  if(x < y)
    return x;
  else
    return y;
}

/*! returns the maximum of two integers
 */
int imax(int x, int y)
{
  if(x > y)
    return x;
  else
    return y;
}

/*! returns the minimum of two integers
 */
int imin(int x, int y)
{
  if(x < y)
    return x;
  else
    return y;
}



/******************************************************************************

TES STRUCTURE

*******************************************************************************/


struct global_data_all_processes
  {
    int MaxPart;			/*!< This gives the maxmimum number of particles that can be stored on one processor. */
  };

/*! This structure holds all the information that is
 * stored for each particle of the simulation.
 */

struct Point		/* struct particle_data */
  {
    double Pos[3];			/*!< particle position at its current time */
  };

struct Triangle
  {
    struct Point Pt1[3];
    struct Point Pt2[3];
    struct Point Pt3[3];
  };



struct TriangleInList
  {

    int idx;            		/* index of current triangle */
     
    struct Point* Pt1;              	/* pointer towards the first  point */
    struct Point* Pt2;              	/* pointer towards the second point */
    struct Point* Pt3;	            	/* pointer towards the third  point */
    
    struct TriangleInList* T1;      	/* index of first  triangle */
    struct TriangleInList* T2;      	/* index of second triangle */
    struct TriangleInList* T3;      	/* index of third  triangle */
 
  };



/******************************************************************************

TES Object

*******************************************************************************/





typedef struct {
    
    /* Here we define variables liked to the TESObject */
    
    PyObject_HEAD
    
    
    int ThisTask;	/*!< the rank of the local processor */
    int NTask;		/*!< number of processors */
    int PTask;		/*!< smallest integer such that NTask <= 2^PTask */

    
    
    
    struct global_data_all_processes All;
    struct Point *P; 
    
    struct Point Pe[3];			/* edges */


    /* some global varables */

    int nT;
    int numTinStack;					/* number of triangles in the list */
    struct TriangleInList Triangles[MAXNUMTRIANGLES];	    /* list of triangles	       */
    int TStack[MAXNUMTRIANGLES];			    /* index of triangles to check     */
    int NumPart;

      
} TES;




static void
TES_dealloc(TES* self)
{

    self->ob_type->tp_free((PyObject*)self);
}

static PyObject *
TES_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    TES *self;

    self = (TES *)type->tp_alloc(type, 0);
    if (self != NULL) 
      {
	
        /* not clear what I have to put here */
	
	
      }

    return (PyObject *)self;
}




static PyMemberDef TES_members[] = {

     
    {NULL}  /* Sentinel */
};


/******************************************************************************

routines not linked to TES

*******************************************************************************/


/*! 
 */

struct Triangle TriangleInList2Triangle(struct TriangleInList Tl)
  {
    struct Triangle T;
    
    
    
    T.Pt1->Pos[0] = Tl.Pt1->Pos[0];
    T.Pt1->Pos[1] = Tl.Pt1->Pos[1];
    
    T.Pt2->Pos[0] = Tl.Pt2->Pos[0];
    T.Pt2->Pos[1] = Tl.Pt2->Pos[1];
    
    T.Pt3->Pos[0] = Tl.Pt3->Pos[0];
    T.Pt3->Pos[1] = Tl.Pt3->Pos[1];
            
    
    	    
    return T;
  }


/*! For a set of three points, construct a triangle
 */

struct Triangle MakeTriangleFromPoints(struct Point Pt1,struct Point Pt2,struct Point Pt3)
  {
    struct Triangle T;
    T.Pt1->Pos[0] = Pt1.Pos[0]; 
    T.Pt1->Pos[1] = Pt1.Pos[1];
    
    T.Pt2->Pos[0] = Pt2.Pos[0]; 
    T.Pt2->Pos[1] = Pt2.Pos[1];
    
    T.Pt3->Pos[0] = Pt3.Pos[0]; 
    T.Pt3->Pos[1] = Pt3.Pos[1];
    
    return T;  
  }


/*! For a set of three points, this function computes their cirum-circle.
 *  Its radius is return, while the center is return using pointers.
 */

double CircumCircleProperties(struct Point Pt1,struct Point Pt2,struct Point Pt3, double *xc, double *yc)
  {
    
      
    double r;
    double x21,x32,y21,y32;
    double x12mx22,y12my22,x22mx32,y22my32;
    double c1,c2;
    
    x21 = Pt2.Pos[0]-Pt1.Pos[0];
    x32 = Pt3.Pos[0]-Pt2.Pos[0];
    
    y21 = Pt2.Pos[1]-Pt1.Pos[1];
    y32 = Pt3.Pos[1]-Pt2.Pos[1];
    
    
    x12mx22 = (Pt1.Pos[0]*Pt1.Pos[0])-(Pt2.Pos[0]*Pt2.Pos[0]);
    y12my22 = (Pt1.Pos[1]*Pt1.Pos[1])-(Pt2.Pos[1]*Pt2.Pos[1]);
    x22mx32 = (Pt2.Pos[0]*Pt2.Pos[0])-(Pt3.Pos[0]*Pt3.Pos[0]);
    y22my32 = (Pt2.Pos[1]*Pt2.Pos[1])-(Pt3.Pos[1]*Pt3.Pos[1]);
    
    c1 = x12mx22 + y12my22;
    c2 = x22mx32 + y22my32;
    
    
    *xc = (y32*c1 -  y21*c2)/2.0/( x32*y21 - x21*y32 );
    *yc = (x32*c1 -  x21*c2)/2.0/( x21*y32 - x32*y21 );
    
    r = sqrt( (Pt1.Pos[0]-*xc)*(Pt1.Pos[0]-*xc) + (Pt1.Pos[1]-*yc)*(Pt1.Pos[1]-*yc) ) ;
    
    return r;
  
  }



/*! For a given triangle T, the routine tells if the point P4
    is in the circum circle of the triangle or not.
 */


int InCircumCircle(struct Triangle T,struct Point Pt4)
  {      
  
    double a,b,c;
    double d,e,f;
    double g,h,i;  
    double det;
    
    
    a = T.Pt1->Pos[0] - Pt4.Pos[0];
    b = T.Pt1->Pos[1] - Pt4.Pos[1];
    c = (T.Pt1->Pos[0]*T.Pt1->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt1->Pos[1]*T.Pt1->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);

    d = T.Pt2->Pos[0] - Pt4.Pos[0];
    e = T.Pt2->Pos[1] - Pt4.Pos[1];
    f = (T.Pt2->Pos[0]*T.Pt2->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt2->Pos[1]*T.Pt2->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);

    g = T.Pt3->Pos[0] - Pt4.Pos[0];
    h = T.Pt3->Pos[1] - Pt4.Pos[1];
    i = (T.Pt3->Pos[0]*T.Pt3->Pos[0] - Pt4.Pos[0]*Pt4.Pos[0]) + (T.Pt3->Pos[1]*T.Pt3->Pos[1] - Pt4.Pos[1]*Pt4.Pos[1]);
    
    det = a*e*i - a*f*h - b*d*i + b*f*g + c*d*h - c*e*g;
    
    
    if (det>0)  
      return 1;			/* inside */
    else
      return 0;			/* outside */  
  }



/*! For a given triangle T, the routine tells if the point P4
    lie inside the triangle or not.
 */


int InTriangle(struct Triangle T,struct Point Pt4)
  {      
  
    double c1,c2,c3;
    
    /* here, we use the cross product */
    c1 = (T.Pt2->Pos[0]-T.Pt1->Pos[0])*(Pt4.Pos[1]-T.Pt1->Pos[1]) - (T.Pt2->Pos[1]-T.Pt1->Pos[1])*(Pt4.Pos[0]-T.Pt1->Pos[0]);
    c2 = (T.Pt3->Pos[0]-T.Pt2->Pos[0])*(Pt4.Pos[1]-T.Pt2->Pos[1]) - (T.Pt3->Pos[1]-T.Pt2->Pos[1])*(Pt4.Pos[0]-T.Pt2->Pos[0]);
    c3 = (T.Pt1->Pos[0]-T.Pt3->Pos[0])*(Pt4.Pos[1]-T.Pt3->Pos[1]) - (T.Pt1->Pos[1]-T.Pt3->Pos[1])*(Pt4.Pos[0]-T.Pt3->Pos[0]);
    
    if ( (c1>0) && (c2>0) && (c3>0) )		/* inside */
      return 1;
    else
      return 0;
    
  }



/*! For a given triangle, orient it positively.
 */

struct Triangle OrientTriangle(struct Triangle T)
  {      
     double a,b,c,d;
     double det;
     struct Point Ptsto;
   
   
     a = T.Pt2->Pos[0] - T.Pt1->Pos[0]; 
     b = T.Pt2->Pos[1] - T.Pt1->Pos[1]; 
     c = T.Pt3->Pos[0] - T.Pt1->Pos[0];  
     d = T.Pt3->Pos[1] - T.Pt1->Pos[1];
     
     det = (a*d) - (b*c);
     
     if (det<0)
       {
	 Ptsto.Pos[0] = T.Pt1->Pos[0];
	 Ptsto.Pos[1] = T.Pt1->Pos[1];
	 
	 T.Pt1->Pos[0] = T.Pt3->Pos[0];
	 T.Pt1->Pos[1] = T.Pt3->Pos[1];
	 
	 T.Pt3->Pos[0] = Ptsto.Pos[0];  
	 T.Pt3->Pos[1] = Ptsto.Pos[1];  
	 
	 T = OrientTriangle(T);   
       }
            
     return T;
  }


/*! For a given triangle, orient it positively.
 */

struct TriangleInList OrientTriangleInList(struct TriangleInList T)
  {      
     double a,b,c,d;
     double det;
     struct Point Ptsto;
   
   
     a = T.Pt2->Pos[0] - T.Pt1->Pos[0]; 
     b = T.Pt2->Pos[1] - T.Pt1->Pos[1]; 
     c = T.Pt3->Pos[0] - T.Pt1->Pos[0];  
     d = T.Pt3->Pos[1] - T.Pt1->Pos[1];
     
     det = (a*d) - (b*c);
     
     if (det<0)
       {
	 Ptsto.Pos[0] = T.Pt1->Pos[0];
	 Ptsto.Pos[1] = T.Pt1->Pos[1];
	 
	 T.Pt1->Pos[0] = T.Pt3->Pos[0];
	 T.Pt1->Pos[1] = T.Pt3->Pos[1];
	 
	 T.Pt3->Pos[0] = Ptsto.Pos[0];  
	 T.Pt3->Pos[1] = Ptsto.Pos[1];  
	 
	 T = OrientTriangleInList(T);   
       }
            
     return T;
  }




/******************************************************************************

TES Routines

*******************************************************************************/

/*!  This function aborts the simulations. If a single processors wants an
 *   immediate termination, the function needs to be called with ierr>0. A
 *   bunch of MPI-error messages may also appear in this case.  For ierr=0,
 *   MPI is gracefully cleaned up, but this requires that all processors
 *   call endrun().
 */
void endrun(TES *self,int ierr)
{
  if(ierr)
    {
      printf("task %d: endrun called with an error level of %d\n\n\n", self->ThisTask, ierr);
      fflush(stdout);
      exit(0);
    }

  exit(0);
};



/*! This routine allocates memory for particle storage, both the
 *  collisionless and the SPH particles.
 */
void allocate_memory(TES *self)
{
  size_t bytes;
  double bytes_tot = 0;

  if(self->All.MaxPart > 0)
    {
      if(!(self->P = malloc(bytes = self->All.MaxPart * sizeof(struct Point))))
	{
	  printf("failed to allocate memory for `P' (%g MB).\n", bytes / (1024.0 * 1024.0));
	  endrun(self,1);
	}
      bytes_tot += bytes;

      printf("\nAllocated %g MByte for particle storage. %d\n\n", bytes_tot / (1024.0 * 1024.0), sizeof(struct Point));
    }

}





void DoTrianglesInStack(TES *self)
  {
	
  
  }


/*! Split a triangle in 3, using the point P inside it. 
    Update the global list.
 */

void SplitTriangle(TES *self,struct TriangleInList T,struct Point Pt)
  {      

    struct Triangle T1,T2,T3;


    //T1.Pt1 =  
    //T1.Pt1 = 
    //T1.Pt1 = 
    
  }


int FindTriangle(TES *self,struct Point Pt)
  {
    int iT;

  }

void AddPoint(TES *self,struct Point Pt)
  {
  }






/******************************************************************************

Interface

*******************************************************************************/


static PyObject *
TES_InitDefaultParameters(TES* self)
{
    /* list of Gadget parameters */

    //self->All.PartAllocFactor	  		= 2.0;

    
    return Py_BuildValue("i",1);
    
}


static PyObject *
TES_info(TES* self)
{
    printf("Hello world, my name is TES\n");
        
    printf("%g %g %g\n",self->Triangles[0].Pt1->Pos[0],self->Triangles[0].Pt1->Pos[1],self->Triangles[0].Pt1->Pos[2]);
    printf("%g %g %g\n",self->Triangles[0].Pt2->Pos[0],self->Triangles[0].Pt2->Pos[1],self->Triangles[0].Pt2->Pos[2]);
    printf("%g %g %g\n",self->Triangles[0].Pt3->Pos[0],self->Triangles[0].Pt3->Pos[1],self->Triangles[0].Pt3->Pos[2]);
    
    
    return Py_BuildValue("i",1);
}





static PyObject *
TES_GetParameters(TES* self)
{
    PyObject *dict;
    PyObject *key;
    PyObject *value;
    
    dict = PyDict_New();

    key   = PyString_FromString("ComovingIntegrationOn");
    //value = PyInt_FromLong(self->All.ComovingIntegrationOn);
    PyDict_SetItem(dict,key,value);
    
    return Py_BuildValue("O",dict);
}



static PyObject *
TES_SetParameters(TES* self, PyObject *args)
{
        
    PyObject *dict;
    PyObject *key;
    PyObject *value;
    int ivalue;
    float fvalue;
    double dvalue;
        	
    /* here, we can have either arguments or dict directly */

    if(PyDict_Check(args))
      {
        dict = args;
      }
    else
      {
        if (! PyArg_ParseTuple(args, "O",&dict))
          return NULL;
      }	

	
    /* check that it is a PyDictObject */
    if(!PyDict_Check(dict))
      {
        PyErr_SetString(PyExc_AttributeError, "argument is not a dictionary.");   
        return NULL;
      }
    
     Py_ssize_t pos=0;
     while(PyDict_Next(dict,&pos,&key,&value))
       {
         
	 if(PyString_Check(key))
	   {
	   	   
	     if(strcmp(PyString_AsString(key), "PeanoHilbertOrder")==0)
	       {
		 if(PyInt_Check(value)||PyLong_Check(value)||PyFloat_Check(value))
		   {
		     ivalue = PyInt_AsLong(value);
		     //Tree_SetPeanoHilbertOrder(self,ivalue);
		   }
	       }	
	       
	       
	              	       	       	       
	       	       	       	       
	   }
       }
	
    return Py_BuildValue("i",1);
}







static PyObject *
      TES_GetTriangles(TES* self, PyObject *args)
      {

        PyObject *OutputList;
	PyArrayObject *tri = NULL;
	npy_intp dim[2];
	int iT;
	
	/* loop over all triangles */
	OutputList = PyList_New(0);

	
	
	for (iT=0;iT<self->nT;iT++)	
	  {
	  
	    
	    /* 3x3 vector */
	    dim[0]=3;
	    dim[1]=3;
		  
	    tri = (PyArrayObject *) PyArray_SimpleNew(2,dim,PyArray_DOUBLE);
            

	    *(double *) (tri->data + 0*(tri->strides[0]) + 0*tri->strides[1]) = self->Triangles[iT].Pt1->Pos[0];
     	    *(double *) (tri->data + 0*(tri->strides[0]) + 1*tri->strides[1]) = self->Triangles[iT].Pt1->Pos[1];
	    *(double *) (tri->data + 0*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	     
	    *(double *) (tri->data + 1*(tri->strides[0]) + 0*tri->strides[1]) = self->Triangles[iT].Pt2->Pos[0];
     	    *(double *) (tri->data + 1*(tri->strides[0]) + 1*tri->strides[1]) = self->Triangles[iT].Pt2->Pos[1];
	    *(double *) (tri->data + 1*(tri->strides[0]) + 2*tri->strides[1]) = 0;
	    
	    *(double *) (tri->data + 2*(tri->strides[0]) + 0*tri->strides[1]) = self->Triangles[iT].Pt3->Pos[0];
     	    *(double *) (tri->data + 2*(tri->strides[0]) + 1*tri->strides[1]) = self->Triangles[iT].Pt3->Pos[1];
	    *(double *) (tri->data + 2*(tri->strides[0]) + 2*tri->strides[1]) = 0;


	    
	    //printf("--- %d ---\n",iT);
	    //printf("%g %g\n", Triangles[iT].T.P1[0],Triangles[iT].T.P1[1]);
	    //printf("%g %g\n", Triangles[iT].T.P2[0],Triangles[iT].T.P2[1]);
	    //printf("%g %g\n", Triangles[iT].T.P3[0],Triangles[iT].T.P3[1]);
	    
	    PyList_Append(OutputList, (PyObject*)tri );
	    	    
	    
	    	
	  }
	
	      


        return Py_BuildValue("O",OutputList);

      }               










static int
TES_init(TES *self, PyObject *args, PyObject *kwds)
{

    import_array();
        

    size_t bytes;

    PyArrayObject *pos;
    PyObject *params;
        
    static char *kwlist[] = {"pos","params", NULL};

    if (! PyArg_ParseTupleAndKeywords(args, kwds, "|OO",kwlist,&pos,&params))
        return -1; 


    int i;

    /* MPI init */
    //MPI_Comm_rank(MPI_COMM_WORLD, &self->ThisTask);
    //MPI_Comm_size(MPI_COMM_WORLD, &self->NTask);
        
    for(self->PTask = 0; self->NTask > (1 << self->PTask); self->PTask++);


    /* init parameters */
    TES_InitDefaultParameters(self);


    //if(PyDict_Check(params))
    //  TES_SetParameters(self,params);


    /*********************/
    /* create P          */
    /*********************/
        
    /* check type */  
    if (!(PyArray_Check(pos)))
      {
    	PyErr_SetString(PyExc_ValueError,"aruments must be array.");
    	return NULL;		      
      } 
      
    /* check dimension */     
    if ( (pos->nd!=2))
      {
    	PyErr_SetString(PyExc_ValueError,"Dimension of argument must be 2.");
    	return NULL;		      
      }       
      
    /* check size */	      
    if ( (pos->dimensions[1]!=3))
      {
    	PyErr_SetString(PyExc_ValueError,"First size of argument must be 3.");
    	return NULL;		      
      }       

    /* ensure double */ 	
    pos = TO_DOUBLE(pos);		  
    self->NumPart = pos->dimensions[0];
	
    /* init */
    self->All.MaxPart = self->NumPart;

    /* allocate memory */
    allocate_memory(self);
    
    /* init P */
    /* loop over all points */

    for (i=0;i<self->NumPart;i++)
     {
       self->P[i].Pos[0] = *(double *) (pos->data + i*(pos->strides[0]) + 0*pos->strides[1]);
       self->P[i].Pos[1] = *(double *) (pos->data + i*(pos->strides[0]) + 1*pos->strides[1]);
       self->P[i].Pos[2] = *(double *) (pos->data + i*(pos->strides[0]) + 2*pos->strides[1]);
     }
    


    /* edges */
    self->Pe[0].Pos[0] = 0;
    self->Pe[0].Pos[1] = 0;
    self->Pe[0].Pos[2] = 0;

    self->Pe[1].Pos[0] = 2;
    self->Pe[1].Pos[1] = 0;
    self->Pe[1].Pos[2] = 0;

    self->Pe[2].Pos[0] = 0;
    self->Pe[2].Pos[1] = 2;
    self->Pe[2].Pos[2] = 0;

    /* Triangle list */
    self->Triangles[0].idx = 0;	 
    self->Triangles[0].Pt1 =  &self->Pe[0];
    self->Triangles[0].Pt2 =  &self->Pe[1];
    self->Triangles[0].Pt3 =  &self->Pe[2];
    self->Triangles[0].T1 = NULL;  
    self->Triangles[0].T2 = NULL;  
    self->Triangles[0].T3 = NULL;  

    /* check that it is oriented */
    OrientTriangleInList(self->Triangles[0]);


    self->nT++;
    
    /* loop over all points */
    for (i=0;i<self->NumPart;i++)
      {  	 
        //AddPoint(P[i]);  
      }

    return 0;

}


















/******************************************************************************

class definition

*******************************************************************************/



static PyMethodDef TES_methods[] = {

    {"info", (PyCFunction)TES_info, METH_NOARGS,
     "Return info"
    },

    {"SetParameters", (PyCFunction)TES_SetParameters, METH_VARARGS,
    "Set TES parameters"
    },    

    {"GetParameters", (PyCFunction)TES_GetParameters, METH_NOARGS,
    "Get TES parameters"
    },        

    {"GetTriangles", (PyCFunction)TES_GetTriangles, METH_NOARGS,
    "Get the trianles in a list of 3x3 arrays."
    },        


                
    {NULL}  /* Sentinel */
};


static PyTypeObject TESType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "TES.TES",               /*tp_name*/
    sizeof(TES),              /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)TES_dealloc,  /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "TES objects",            /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    TES_methods,              /* tp_methods */
    TES_members,              /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)TES_init,       /* tp_init */
    0,                         /* tp_alloc */
    TES_new,                  /* tp_new */
};

static PyMethodDef module_methods[] = {
    {NULL}  /* Sentinel */
};

#ifndef PyMODINIT_FUNC	/* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
initteslib(void) 
{
    PyObject* m;

    if (PyType_Ready(&TESType) < 0)
        return;

    m = Py_InitModule3("teslib", module_methods,
                       "Example module that creates an extension type.");

    if (m == NULL)
      return;

    Py_INCREF(&TESType);
    PyModule_AddObject(m, "TES", (PyObject *)&TESType);
}
