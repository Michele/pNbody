#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_InCircumCircle.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import tessel

import Ptools as pt
import plot

import sys



#while 1:


P1 = random.random(3)
P2 = random.random(3)
P3 = random.random(3)
P4 = random.random(3)

b = tessel.InCircumCircle(P1,P2,P3,P4)
r,xc,yc = tessel.CircumCircleProperties(P1,P2,P3)

print b
  
#  if b==1:
#    break


plot.draw_triangle(P1,P2,P3)
plot.draw_points([P1])
plot.draw_points([P2])
plot.draw_points([P3])
plot.draw_points([P4])
plot.draw_circle(r,xc,yc)
pt.show()


