'''
 @package   pNbody
 @file      plot.py
 @brief     plot
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
import Ptools as pt
from numpy import *
import matplotlib.patches as mpatches


def draw_points(Ps,color=None):

  for P in Ps:
    pt.scatter([P[0]],[P[1]],color=color,s=10)


def draw_circle(r,xc,yc):
  
  n = 100
  t = arange(n+1)/float(n)*(2*pi)
  
  x = r*cos(t) + xc
  y = r*sin(t) + yc
  
  pt.plot(x,y)
  
  
def draw_box(x,y): 
  
  x = concatenate((x,[x[0]]))
  y = concatenate((y,[y[0]]))
  pt.plot(x,y)
  

def draw_triangle(P1,P2,P3,c='k'):
  
  x = [P1[0],P2[0]]
  y = [P1[1],P2[1]]
  pt.plot(x,y,c)

  x = [P2[0],P3[0]]
  y = [P2[1],P3[1]]
  pt.plot(x,y,c)

  x = [P3[0],P1[0]]
  y = [P3[1],P1[1]]
  pt.plot(x,y,c)

def draw_line(P1,P2,color='k'):
  
  x = [P1[0],P2[0]]
  y = [P1[1],P2[1]]
  pt.plot(x,y,color)


def draw_cell(vpos,color='gray',alpha=None):
  x = vpos[:,0]
  y = vpos[:,1]
  vectors = transpose( array([x,y]) )
  polygon = mpatches.Polygon(array(vectors),alpha=alpha,zorder=-1,color=color)
  ax=pt.gca()
  ax.add_artist(polygon)
