#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *

pos=array([[0.5,0.5,0]])

nb = Nbody(status='new',pos=pos,ftype='gadget')

#nb.rename('qq.dat')
#nb.write()

nb.rsp = array([10],float32)

import mapping



mat = mapping.mkmap2dncub(nb.pos,nb.mass,ones(nb.nbody,float32),nb.rsp,(512,512))
print sum(nb.mass)
print sum(ravel(mat))

libutil.mplot(mat,save='qq.fits')

