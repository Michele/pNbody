'''
 @package   pNbody
 @file      examples.py
 @brief     Examples
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import io
import mapping

nb = Nbody('../../examples/snap.dat',ftype='gadget')



mx = max(nb.rxyz())

kx = 64
ky = 64
kz = 64

pos = ( nb.pos + [mx,mx,mx] )/(2*mx)		# between 0,1
pos = pos.astype(float32)

rsp = ones(nb.nbody)/1000.
rsp = rsp.astype(float32)


print "mkmap1dn"
mat = mapping.mkmap1dn(pos[0,:],nb.mass,nb.mass,(kx,))
print "mkmap2dn"
mat = mapping.mkmap2dn(pos,nb.mass,nb.mass,(kx,ky))
print "mkmap3dn"
mat = mapping.mkmap3dn(pos,nb.mass,nb.mass,(kx,ky,kz))


print "mkmap2dnsph"
mat = mapping.mkmap2dnsph(pos,nb.mass,nb.mass,rsp,(kx,ky))


print "mkmap1dw"
mat = mapping.mkmap1dw(pos[0,:],nb.mass,nb.mass,(kx,))
print "mkmap2dw"
mat = mapping.mkmap2dw(pos,nb.mass,nb.mass,(kx,ky))
print "mkmap3dw"
mat = mapping.mkmap3dw(pos,nb.mass,nb.mass,(kx,ky,kz))




#mat = mapzero
#mat = mapzerosph
#mat = mapone
#mat = mapn


#mat = mapping.mkmap3dslicesph()
#mat = mapping.mkmap3dsortedsph()
