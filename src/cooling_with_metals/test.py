#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import cooling_with_metals as cooling
import sys

params = {}
params["UnitLength_in_cm"]	   = 3.085e+21
params["UnitMass_in_g"]	           = 1.989e+43
params["UnitVelocity_in_cm_per_s"] = 20725573.785998672
params["CoolingFile"]              = "cooling.dat"


cooling.init_cooling(params)
cooling.PrintParameters(params)


rho = 3.0364363e-06
u   = 0.0015258887
fe  = 0.
l = cooling.get_lambda_from_Density_EnergyInt_FeH(rho,u,fe)

dudt = l/rho
dt   = u/dudt
print 
print "cooling time = ",dt

sys.exit()


nb = Nbody("snapshot_0000",ftype='gadget')
nb = nb.select('gas')
nb = nb.sub(0,5)

rho = nb.rho[0]
u   = nb.u[0]
fe  = nb.metals[0][0]

l = cooling.get_lambda_from_Density_EnergyInt_FeH(rho,u,fe)

dudt = l/rho
dt   = u/dudt

print dt


l = cooling.get_lambda_from_Density_EnergyInt_FeH(nb.rho,nb.u,nb.metals[:,0])

dudt = l/rho
dt   = u/dudt

print dt


l = cooling.get_lambda_from_Density_Temperature_FeH(nb.rho[0],1e4,nb.metals[0,0])

dudt = l/rho
dt   = u/dudt

print dt
