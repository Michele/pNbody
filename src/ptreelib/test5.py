#/usr/bin/env python
'''
 @package   pNbody
 @file      test5.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import ptreelib

from Gtools import io

params = io.read_params("params")
params['OutputInfo'] = 1
params['ComovingIntegrationOn'] = 1
params['PeriodicBoundariesOn'] = 1
params['BoxSize'] = 20000

nb = Nbody('lcdm.dat',ftype='gadget')
nb = nb.select('gas')


nb.Tree = ptreelib.Tree(nb.npart,nb.pos,nb.vel,nb.mass,nb.num,nb.tpe,params)
nb.ExchangeParticles()
nb.Tree.SetParticles(nb.npart,nb.pos,nb.vel,nb.mass,nb.num,nb.tpe)
nb.Tree.BuildTree()

'''

newgrid = 0
if newgrid:
  # create the grid
  nx = 512
  ny = 512
  xmin = 0
  xmax= 20000
  ymin = 0
  ymax =  20000
  pos = libgrid.get_Points_On_Carthesian_2d_Grid(nx,ny,xmin,xmax,ymin,ymax,offx=0,offy=0)
  gr = Nbody(pos=pos,status='new',ftype='gadget')		 # this should be //
  gr.translate([0,0,10000])
else:
  # open grid
  gr = Nbody('grid512.dat',ftype='gadget')
  nx = int(sqrt(gr.nbody))
  ny = nx
  

observable = nb.rho

# compute density
if newgrid:
  hsml = gr.mass/gr.mass*100
  den,hsml = nb.Tree.Density(gr.pos,hsml)
else:
  hsml = gr.rsp  

observable = nb.Tree.SphEvaluate(gr.pos,hsml,nb.rho,observable)

# compute potential
#pot = nb.Tree.Potential(gr.pos,8.0)

observable = observable



observable.shape = (nx,ny)
mat = observable

if mpi.mpi_IsMaster():
  libutil.mplot(mat,save='map.fits')


# save grid
if newgrid:
  gr.u   = 0*den.astype(Float32)
  gr.rho = den.astype(Float32)
  gr.rsp = hsml.astype(Float32)
  gr.rename('grid512.dat')
  gr.write()
  
  
'''
