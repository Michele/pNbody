'''
 @package   pNbody
 @file      test2.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from mpi4py import MPI

from pNbody import *

import ptreelib


nb = Nbody('../../examples/disk.dat',ftype='gadget')
nb.cmcenter()
nb.Tree = ptreelib.Tree(npart=nb.npart,pos=nb.pos,vel=nb.vel,mass=nb.mass,num=nb.num,tpe=nb.tpe)


nb.ExchangeParticles()
nb.Tree.SetParticles(nb.npart,nb.pos,nb.vel,nb.mass,nb.num,nb.tpe) 
nb.Tree.BuildTree()


#Pot = nb.Tree.Potential(array([[0,0,0]],float32),0.1) 
#print Pot

#acc = nb.Tree.Acceleration(array([[0,0,0]],float32),0.1) 
#print ac


nb.Tree.AllPotential()
pot = nb.Tree.GetAllPotential()



'''

nb.Tree.AllDensity()



pos = array([[0,0,0]],float32)
hsml = array([0.1],float32)
#obs = nb.rho


dens,hsml=nb.Tree.Density(pos,hsml)

obs = nb.Tree.SphEvaluate(pos,hsml,nb.rho,nb.pos[:,0])


print dens,hsml
print obs
'''
