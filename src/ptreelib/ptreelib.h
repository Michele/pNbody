#ifndef PTREELIB 
#define PTREELIB

typedef struct {
    
    /* Here we define variables liked to the TreeObject */
    
    PyObject_HEAD
    
    
    int ThisTask;	/*!< the rank of the local processor */
    int NTask;		/*!< number of processors */
    int PTask;		/*!< smallest integer such that NTask <= 2^PTask */

    int Numnodestree;
    int MaxNodes;
    int NumPart;
    int NumSphUpdate;
    int N_gas;
    long long Ntype[6];
    
    struct global_data_all_processes All;
    
    struct particle_data *P; 
    struct particle_data *DomainPartBuf;

    struct sph_particle_data *SphP; 
    struct sph_particle_data *DomainSphBuf;  
  
    void *CommBuffer;
    peanokey *DomainKeyBuf;
    
  
    /* allvars.c */    
    int NtypeLocal[6];
    
    
    
    
    struct topnode_data *TopNodes;
        
    peanokey *Key;	
    peanokey *KeySorted;
    
    
    /* domain.c */
    int *toGo, *toGoSph;
    int *local_toGo, *local_toGoSph;
    int *list_NumPart;
    int *list_N_gas;
    int *list_load;
    int *list_loadsph;
    double *list_work;    
   
    struct topnode_exchange *toplist, *toplist_local;
    
    double DomainCorner[3]; 
    double DomainCenter[3]; 
    double DomainLen;	
    double DomainFac;	

    int    DomainMyStart;
    int    DomainMyLast;     
    double *DomainWork;
    int	*DomainCount;	
    int	*DomainCountSph;
    long long maxload;
    long long maxloadsph;
    
    int    *DomainStartList;
    int    *DomainEndList;  
    int    *DomainTask;
    
    int    *DomainNodeIndex;  
    FLOAT  *DomainTreeNodeLen;
    FLOAT  *DomainHmax; 	
    
    struct DomainNODE  *DomainMoment; 
    /* yr */
    struct particle_IdProc *DomainIdProc;
    int NSend;
    int NRecv;
    
    int NTopnodes;
    int NTopleaves;
   
    int *Nextnode;
    int *Father;   
    struct extNODE *Extnodes_base;      
    struct extNODE *Extnodes;    
    
    
    struct NODE *Nodes_base;
    struct NODE *Nodes;
     
    double TimeOfLastTreeConstruction;
    
    char *Exportflag;
    
    struct gravdata_index *GravDataIndexTable;
    struct gravdata_in *GravDataGet, *GravDataIn, *GravDataOut, *GravDataResult;
    
    struct particle_data *Q;
    struct sph_particle_data *SphQ;
    int NumPartQ;
    int N_gasQ;
    int NumSphUpdateQ;
    double ForceSofteningQ;
    
    /* peano.c */
    struct peano_hilbert_data *mp;
    int *Id;
    
    /* forcetree.c */
    int first_flag;
    float tabfac;
    float shortrange_table[NTAB];
    float shortrange_table_potential[NTAB];
    int last;
 
    FLOAT fcorrx[EN + 1][EN + 1][EN + 1];
    FLOAT fcorry[EN + 1][EN + 1][EN + 1];
    FLOAT fcorrz[EN + 1][EN + 1][EN + 1];
    FLOAT potcorr[EN + 1][EN + 1][EN + 1];
    double fac_intp;
        
    
    /* ngb */
    int  *Ngblist; 
        	
    /* density */		
    struct densdata_in *DensDataIn,*DensDataGet; 
    struct densdata_out  *DensDataResult,*DensDataPartialResult;    
      
} Tree;

#endif
