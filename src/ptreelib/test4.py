#/usr/bin/env python
'''
 @package   pNbody
 @file      test4.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import ptreelib

from Gtools import io

params = io.read_params("params")
params['OutputInfo'] = 1
params['ComovingIntegrationOn'] = 1
params['PeriodicBoundariesOn'] = 1
params['Boxsize'] = 20000

nb = Nbody('disk.dat',ftype='gadget')
Tree = ptreelib.Tree(nb.npart,nb.pos,nb.vel,nb.mass,nb.num,nb.tpe,params)

# set parameters
#params = io.read_params("params")
#Tree.SetParameters(params)

#params = {'DesNumNgb':64}
#Tree.SetParameters(params)


params =  Tree.GetParameters()
print params['SofteningBndryMaxPhys']
print params['DesNumNgb']
print params['TypeOfOpeningCriterion']
