#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_Parameters.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from mpi4py import MPI

from pNbody import ic

from numpy import *
from PyGadget import gadget

import Ptools as pt

import sys
import time

random.seed(MPI.COMM_WORLD.Get_rank())			# 2 for two points

n = 100

nb = ic.plummer(n,1,1,1,eps=0.1,rmax=1.)


gadget.InitMPI()			# init MPI
gadget.InitDefaultParameters()		# init default parameters
gadget.Info()

params = gadget.GetParameters()


keys = ['TimeLimitCPU',		      
'ResubmitOn',		       
'ICFormat',		 
'SnapFormat',		       
'ComovingIntegrationOn',         
'TypeOfTimestepCriterion', 
'OutputListOn',		       
'PeriodicBoundariesOn',	 
'TimeBegin',		 
'TimeMax',		 
'Omega0', 		 
'OmegaLambda',		       
'OmegaBaryon',		       
'HubbleParam',		       
'BoxSize',		 
'TimeBetSnapshot',	 
'TimeOfFirstSnapshot',	       
'CpuTimeBetRestartFile',         
'TimeBetStatistics',	 
'NumFilesPerSnapshot',	       
'NumFilesWrittenInParallel',
'ErrTolIntAccuracy',	 
'MaxRMSDisplacementFac',   
'CourantFac',		       
'MaxSizeTimestep',	 
'MinSizeTimestep',	 
'ErrTolTheta',		       
'TypeOfOpeningCriterion',  
'ErrTolForceAcc', 	 
'TreeDomainUpdateFrequency',     
'DesNumNgb',		 
'MaxNumNgbDeviation',	       
'ArtBulkViscConst',	 
'InitGasTemp',		       
'MinGasTemp',		       
'PartAllocFactor',	 
'TreeAllocFactor',	 
'BufferSize',		       
'UnitLength_in_cm',	 
'UnitMass_in_g', 	       
'UnitVelocity_in_cm_per_s',
'GravityConstantInternal', 
'MinGasHsmlFractional',	       
'SofteningGas',		       
'SofteningHalo',  	       
'SofteningDisk',  	       
'SofteningBulge', 	 
'SofteningStars', 	 
'SofteningBndry', 	 
'SofteningGasMaxPhys',	       
'SofteningHaloMaxPhys',	       
'SofteningDiskMaxPhys',	       
'SofteningBulgeMaxPhys',         
'SofteningStarsMaxPhys',         
'SofteningBndryMaxPhys']         

for key in keys:
  print key,params[key]

i=0
for key in keys:
  params[key] = i
  i = i + 1
  
print  
  
for key in keys:
  print key,params[key]


gadget.SetParameters(params)
params = gadget.GetParameters()



#gadget.LoadParticles(array(nb.npart),nb.pos,nb.vel,nb.mass,nb.num,nb.tpe)
