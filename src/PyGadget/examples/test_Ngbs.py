#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_Ngbs.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from mpi4py import MPI

from pNbody import ic

from numpy import *
from PyGadget import gadget

#import Ptools as pt

import sys
import time

random.seed(MPI.COMM_WORLD.Get_rank())			# 2 for two points

n = 10000
rc = 2
rmax = 100

nb = ic.plummer(n,1,1,1,eps=rc,rmax=rmax,ftype='gadget')
nb.rename('plummer.dat')
nb.write()

gadget.InitMPI()			# init MPI
gadget.InitDefaultParameters()		# init default parameters
gadget.Info()


params = {}

params['ErrTolTheta']			= 0.7

params['DesNumNgb']			= 50		 
params['MaxNumNgbDeviation']		= 1	       

params['UnitLength_in_cm']		= 3.085e+21
params['UnitMass_in_g']			= 4.435693e+44	       
params['UnitVelocity_in_cm_per_s']	= 97824708.2699


eps = 0.1

params['SofteningGas']			= eps	
params['SofteningHalo']			= eps      
params['SofteningDisk']  		= eps	
params['SofteningBulge']		= eps	
params['SofteningStars']		= eps	
params['SofteningBndry'] 		= eps	    
params['SofteningGasMaxPhys']		= eps		  
params['SofteningHaloMaxPhys']		= eps		  
params['SofteningDiskMaxPhys']		= eps		  
params['SofteningBulgeMaxPhys']		= eps	       
params['SofteningStarsMaxPhys']		= eps	   
params['SofteningBndryMaxPhys']		= eps   

gadget.SetParameters(params)



params = gadget.GetParameters()


gadget.LoadParticles(array(nb.npart),nb.pos,nb.vel,nb.mass,nb.num,nb.tpe)


pos = array([0,0,0],float)
eps = 0.5

pos = gadget.Ngbs(pos,eps)

print pos



