#!/usr/bin/env python
'''
 @package   pNbody
 @file      test_AllPotential.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from mpi4py import MPI

from pNbody import ic

from numpy import *
from PyGadget import gadget

import Ptools as pt

import sys
import time

random.seed(MPI.COMM_WORLD.Get_rank())			# 2 for two points

n = 10000
rc = 2
rmax = 100

nb = ic.plummer(n,1,1,1,eps=rc,rmax=rmax,ftype='gadget')
nb.rename('plummer.dat')
nb.write()

gadget.InitMPI()			# init MPI
gadget.InitDefaultParameters()		# init default parameters
gadget.Info()


params = {}

params['ErrTolTheta']			= 0.7

params['DesNumNgb']			= 50		 
params['MaxNumNgbDeviation']		= 1	       

params['UnitLength_in_cm']		= 3.085e+21
params['UnitMass_in_g']			= 4.435693e+44	       
params['UnitVelocity_in_cm_per_s']	= 97824708.2699

rc = 1000.

params['SofteningGas']			= rc/100.	
params['SofteningHalo']			= rc/100.	      
params['SofteningDisk']  		= rc/100.	
params['SofteningBulge']		= rc/100.	
params['SofteningStars']		= rc/100.	
params['SofteningBndry'] 		= rc/100.	
params['SofteningGasMaxPhys']		= rc/100.	      
params['SofteningHaloMaxPhys']		= rc/100.	      
params['SofteningDiskMaxPhys']		= rc/100.	      
params['SofteningBulgeMaxPhys']		= rc/100.	   
params['SofteningStarsMaxPhys']		= rc/100.	   
params['SofteningBndryMaxPhys']		= rc/100.   

gadget.SetParameters(params)



params = gadget.GetParameters()



gadget.LoadParticles(array(nb.npart),nb.pos,nb.vel,nb.mass,nb.num,nb.tpe)

gadget.AllPotential()
nb.pot = gadget.GetAllPotential()
nb.pos = gadget.GetAllPositions()


###############################
# plot
###############################

r = nb.rxyz()
val = nb.pot


pt.scatter(r,val,s=1)
pt.show()

