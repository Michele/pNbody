#!/usr/bin/env python
'''
 @package   pNbody
 @file      tipsy2gadget.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
from pNbody import libtipsy

import argparse

description=None
epilog=None

parser = argparse.ArgumentParser(description=description,epilog=epilog)



parser.add_argument("-o", 
                  "--outputfile",
                  action="store", 
		  dest="outputfile", 
                  help="name of the output file",
		  metavar="FILE")

parser.add_argument('files', metavar='FILE', type=str,
                   nargs=1,
                   help='name of the file to convert')


#####################################
# main
#####################################

opt = parser.parse_args()

file = opt.files[0]

if not os.path.isfile(file):
  print "file %s does not exists."%file
  sys.exit()

header,pos,vel,mass,eps,phi = libtipsy.read(file)

pos = pos.astype(float32)
vel = vel.astype(float32)
mass = mass.astype(float32)


if opt.outputfile!=None:

  nb = Nbody(status='new',p_name=opt.outputfile,pos=pos,vel=vel,mass=mass,ftype='gadget')
  nb.set_tpe(2)
  nb.write()
