'''
 @package   pNbody
 @file      test03.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
import time
import treelib

import Ptools as pt

ErrTolTheta = 0.8
eps = 0.1

# open a model
nb = Nbody("../../examples/snap.dat",ftype='gadget')
#nb = nb.selectc(nb.rxyz()<2)

# create a tree object
MyTree = treelib.Tree(npart=array(nb.npart),pos=nb.pos,vel=nb.vel,mass=nb.mass,ErrTolTheta=ErrTolTheta)


R = arange(-20,20,0.1)
print len(R)
pos = concatenate((R,zeros(len(R)),zeros(len(R))))
pos.shape = (3,len(R))
pos = transpose(pos)
pos = pos.astype(float32)



print "init hsml"
hsml = MyTree.InitHsml(33,3)
print "compute density"
density,hsml = MyTree.Density(pos,hsml,33,3)

pt.plot(R,density)
pt.show()
