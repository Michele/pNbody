#from Nbody import *
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from myNumeric import *
from numarray import *
from numarray import random_array as rand

import sys


vec = rand.random((int(1e6),3))
lst = [(0,1,30),(0,1,30),(0,1,30)]
lst = [(0,1,10),(0,1,10),(0,1,10)]

#vec = rand.random((10)) 
#lst = [(0,1,10)]

h = hnd(lst,vec)

print h.shape
print h
print sum(ravel(h))






sys.exit()
r = arange(11).astype(Float)
x = array([-1,1.1,100,9.9,9],Float)
m = array([1 ,  1,  0.1,  1,1],Float)

#print x
#print r

print whistogram(x,m,r)
print histogram(x,r).astype(Float)
