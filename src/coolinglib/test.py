#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test cooling library
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


import sys
import coolinglib
from numarray import *

Redshift = 0.

Egyspec = array([0.4,0.4,0.4],Float)
Density = array([0.4,0.4,0.4],Float)
Mu,Lambda = coolinglib.cooling(Egyspec,Density,0.76,Redshift)

print Mu
print Lambda

Temperature = array([10,100,1000],Float)
HydrogenDensity = array([1,1,1],Float)
Mu,Ne,Lambda = coolinglib.cooling_from_nH_and_T(Temperature,HydrogenDensity,0.76,Redshift)

print
print Mu
print Ne
print Lambda
