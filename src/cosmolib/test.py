#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import sys
import cosmolib
from numpy import *

LITTLEH = 0.73

Omega0=0.24
OmegaLambda=0.76
Hubble = 0.1

'''
as = array([0.,0.1,0.2,0.5,1.])

print Omega0,OmegaLambda,Hubble
'''

as = array([0],float)
print cosmolib.Age_a(as,Omega0,OmegaLambda,Hubble)  *0.978/LITTLEH


