CC              = mpicc
PGM 		= pmlib
#INCLUDEPY	= /home/revaz/local/include/python2.4
INCLUDEPY	= /usr/include/python2.5/
INCLUDENUMPY    = /usr/lib/python2.5/site-packages/numpy/core/include/
INCLUDENUMARRAY = /usr/lib/python2.5/site-packages/numpy/numarray/

FFTW_INCL       = /home/epfl/revaz/local/include
FFTW_LIB        = -L/home/epfl/revaz/local/lib -lsrfftw_mpi -lsfftw_mpi -lsrfftw -lsfftw

MPI_LIB         = -L/home/epfl/revaz/local/openmpi/lib/ -lmpi 
#MPI_INC         = /home/epfl/revaz/local/openmpi/include/


CFLAGS= -fPIC -I$(INCLUDEPY) -I$(INCLUDENUMARRAY) -I$(INCLUDENUMPY) -I$(FFTW_INCL) -I$(MPI_INC)
LIBS   = -g  $(MPI_LIB) $(FFTW_LIB)

$(PGM).so : $(PGM).o
	$(CC) -shared  $(PGM).o $(LIBS)  -o $(PGM).so 

$(PGM).o :  $(PGM).c
	$(CC) $(CFLAGS) -c $(PGM).c  
        
		 
clean:
	rm -f *.o *.exe	*.so 	 






 
