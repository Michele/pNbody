#include <Python.h>
#include <numpy/arrayobject.h>
#include "structmember.h"
#include     <srfftw_mpi.h>


#define FLOAT float
typedef  long long  peanokey;


#define  MAXTOPNODES     200000
#define  MAX_REAL_NUMBER  1e37
#define  BITS_PER_DIMENSION 18
#define  PEANOCELLS (((peanokey)1)<<(3*BITS_PER_DIMENSION))

#ifndef  TWODIMS
#define  NUMDIMS 3                                      /*!< For 3D-normalized kernel */
#define  KERNEL_COEFF_1  2.546479089470                 /*!< Coefficients for SPH spline kernel and its derivative */ 
#define  KERNEL_COEFF_2  15.278874536822
#define  KERNEL_COEFF_3  45.836623610466
#define  KERNEL_COEFF_4  30.557749073644
#define  KERNEL_COEFF_5  5.092958178941
#define  KERNEL_COEFF_6  (-15.278874536822)
#define  NORM_COEFF      4.188790204786                 /*!< Coefficient for kernel normalization. Note:  4.0/3 * PI = 4.188790204786 */ 
#else
#define  NUMDIMS 2                                      /*!< For 2D-normalized kernel */
#define  KERNEL_COEFF_1  (5.0/7*2.546479089470)         /*!< Coefficients for SPH spline kernel and its derivative */ 
#define  KERNEL_COEFF_2  (5.0/7*15.278874536822)
#define  KERNEL_COEFF_3  (5.0/7*45.836623610466)
#define  KERNEL_COEFF_4  (5.0/7*30.557749073644)
#define  KERNEL_COEFF_5  (5.0/7*5.092958178941)
#define  KERNEL_COEFF_6  (5.0/7*(-15.278874536822))
#define  NORM_COEFF      M_PI                           /*!< Coefficient for kernel normalization. */
#endif





/* some tags */


#define TAG_N             10      /*!< Various tags used for labelling MPI messages */ 
#define TAG_HEADER        11
#define TAG_PDATA         12
#define TAG_SPHDATA       13
#define TAG_KEY           14
#define TAG_DMOM          15
#define TAG_NODELEN       16
#define TAG_HMAX          17
#define TAG_GRAV_A        18
#define TAG_GRAV_B        19
#define TAG_DIRECT_A      20
#define TAG_DIRECT_B      21
#define TAG_HYDRO_A       22 
#define TAG_HYDRO_B       23
#define TAG_NFORTHISTASK  24
#define TAG_PERIODIC_A    25
#define TAG_PERIODIC_B    26
#define TAG_PERIODIC_C    27
#define TAG_PERIODIC_D    28
#define TAG_NONPERIOD_A   29 
#define TAG_NONPERIOD_B   30
#define TAG_NONPERIOD_C   31
#define TAG_NONPERIOD_D   32
#define TAG_POTENTIAL_A   33
#define TAG_POTENTIAL_B   34
#define TAG_DENS_A        35
#define TAG_DENS_B        36
#define TAG_LOCALN        37

/******************************************************************************

SYSTEM

*******************************************************************************/


/*! returns the maximum of two double
 */
double dmax(double x, double y)
{
  if(x > y)
    return x;
  else
    return y;
}

/*! returns the minimum of two double
 */
double dmin(double x, double y)
{
  if(x < y)
    return x;
  else
    return y;
}

/*! returns the maximum of two integers
 */
int imax(int x, int y)
{
  if(x > y)
    return x;
  else
    return y;
}

/*! returns the minimum of two integers
 */
int imin(int x, int y)
{
  if(x < y)
    return x;
  else
    return y;
}



/******************************************************************************

PM STRUCTURE

*******************************************************************************/

struct global_data_all_processes
{
  
  long long TotNumPart;
  long long TotN_gas;  
  
  int MaxPart;
  double SofteningTable[6];
  
  double PartAllocFactor;

  /* Placement of PM grids */
  double Asmth[2];              /*!< Gives the scale of the long-range/short-range split (in mesh-cells), both for the coarse and the high-res mesh */
  double Rcut[2];               /*!< Gives the maximum radius for which the short-range force is evaluated with the tree (in mesh-cells), both for the coarse and the high-res mesh */
  double Corner[2][3];          /*!< lower left corner of coarse and high-res PM-mesh */
  double UpperCorner[2][3];     /*!< upper right corner of coarse and high-res PM-mesh */
  double Xmintot[2][3];         /*!< minimum particle coordinates both for coarse and high-res PM-mesh */
  double Xmaxtot[2][3];         /*!< maximum particle coordinates both for coarse and high-res PM-mesh */
  double TotalMeshSize[2];      /*!< total extension of coarse and high-res PM-mesh */


  
  double G;
  
};

struct particle_data
{
  FLOAT Pos[3];			/*!< particle position at its current time */
  FLOAT Mass;			/*!< particle mass */
  FLOAT GravPM[3];		/*!< particle acceleration due to gravity */
  FLOAT Potential;		/*!< gravitational potential */
  int Type;		        /*!< flags particle type.  0=gas, 1=halo, 2=disk, 3=bulge, 4=stars, 5=bndry */
  unsigned int Active;
  unsigned int ID;
};





/******************************************************************************

PM Object

*******************************************************************************/





typedef struct {
    
    /* Here we define variables liked to the PMObject */
    
    PyObject_HEAD
    
    
    int ThisTask;	/*!< the rank of the local processor */
    int NTask;		/*!< number of processors */
    int PTask;		/*!< smallest integer such that NTask <= 2^PTask */

    int NumPart;
    long long Ntype[6];
    
    struct global_data_all_processes All;
    
    struct particle_data *P; 
      
} PM;




static void
PM_dealloc(PM* self)
{

    self->ob_type->tp_free((PyObject*)self);
}

static PyObject *
PM_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    PM *self;

    self = (PM *)type->tp_alloc(type, 0);
    if (self != NULL) 
      {
	
        /* not clear what I have to put here */
	
	
      }

    return (PyObject *)self;
}




static PyMemberDef PM_members[] = {

    //{"first", T_OBJECT_EX, offsetof(PM, first), 0,
    // "first name"},
    //{"list", T_OBJECT_EX, offsetof(PM, list), 0,
    // "list of"},     
    //{"number", T_INT, offsetof(PM, number), 0,
    // "PM number"},
     
    {NULL}  /* Sentinel */
};


/******************************************************************************

ENDRUN

*******************************************************************************/


/*!  This function aborts the simulations. If a single processors wants an
 *   immediate termination, the function needs to be called with ierr>0. A
 *   bunch of MPI-error messages may also appear in this case.  For ierr=0,
 *   MPI is gracefully cleaned up, but this requires that all processors
 *   call endrun().
 */
void endrun(PM *self,int ierr)
{
  if(ierr)
    {
      printf("task %d: endrun called with an error level of %d\n\n\n", self->ThisTask, ierr);
      fflush(stdout);
//#ifdef DEBUG
//      terminate_processes();
//      raise(SIGABRT);
//      sleep(60);
//#else
//      MPI_Abort(MPI_COMM_WORLD, ierr);
//#endif
      exit(0);
    }

//  MPI_Finalize();
  exit(0);
};


/******************************************************************************

PM Routines

*******************************************************************************/


#define  PMGRID 256
#define  GRID  (2*PMGRID)
#define  GRID2 (2*(GRID/2 + 1))

#define ASMTH 1.25
#define RCUT  4.5

static rfftwnd_mpi_plan fft_forward_plan, fft_inverse_plan;

static int slab_to_task[GRID];
static int *slabs_per_task;
static int *first_slab_of_task;

static int *meshmin_list, *meshmax_list;

static int slabstart_x, nslab_x, slabstart_y, nslab_y;

static int fftsize, maxfftsize;

static fftw_real *kernel[2], *rhogrid, *forcegrid, *workspace;
static fftw_complex *fft_of_kernel[2], *fft_of_rhogrid;




/*! This function frees the memory allocated for the non-periodic FFT
 *  computation. (With the exception of the Greens function(s), which are kept
 *  statically in memory for the next force computation.)
 */
void pm_init_nonperiodic_free(PM *self)
{
  /* deallocate memory */
  free(workspace);
  free(forcegrid);
  free(rhogrid);
}

/*! This function allocates the workspace needed for the non-periodic FFT
 *  algorithm. Three fields are used, one for the density/potential fields,
 *  one to hold the force field obtained by finite differencing, and finally
 *  an additional workspace which is used both in the parallel FFT itself, and
 *  as a buffer for the communication algorithm.
 */
void pm_init_nonperiodic_allocate(PM *self,int dimprod)
{
  static int first_alloc = 1;
  int dimprodmax;
  double bytes_tot = 0;
  size_t bytes;

  MPI_Allreduce(&dimprod, &dimprodmax, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);

  if(!(rhogrid = (fftw_real *) malloc(bytes = fftsize * sizeof(fftw_real))))
    {
      printf("failed to allocate memory for `FFT-rhogrid' (%g MB).\n", bytes / (1024.0 * 1024.0));
      endrun(self,1);
    }
  bytes_tot += bytes;

  fft_of_rhogrid = (fftw_complex *) rhogrid;

  if(!(forcegrid = (fftw_real *) malloc(bytes = imax(fftsize, dimprodmax) * sizeof(fftw_real))))
    {
      printf("failed to allocate memory for `FFT-forcegrid' (%g MB).\n", bytes / (1024.0 * 1024.0));
      endrun(self,1);
    }
  bytes_tot += bytes;

  if(!(workspace = (fftw_real *) malloc(bytes = imax(maxfftsize, dimprodmax) * sizeof(fftw_real))))
    {
      printf("failed to allocate memory for `FFT-workspace' (%g MB).\n", bytes / (1024.0 * 1024.0));
      endrun(self,1);
    }
  bytes_tot += bytes;

  if(first_alloc == 1)
    {
      first_alloc = 0;
      if(self->ThisTask == 0)
	printf("\nUsing %g MByte for non-periodic FFT computation.\n\n", bytes_tot / (1024.0 * 1024.0));
    }
}





/*! Calculates the long-range non-periodic forces using the PM method.  The
 *  potential is Gaussian filtered with Asmth, given in mesh-cell units. The
 *  potential is finite differenced using a 4-point finite differencing
 *  formula to obtain the force fields, which are then interpolated to the
 *  particle positions. We carry out a CIC charge assignment, and compute the
 *  potenial by Fourier transform methods. The CIC kernel is deconvolved.
 */
int pmforce_nonperiodic(PM *self,int grnr)
{
  double dx, dy, dz;
  double fac, to_slab_fac;
  double re, im, acc_dim;
  int i, j, slab, level, sendTask, recvTask, flag, flagsum;
  int x, y, z, xl, yl, zl, xr, yr, zr, xll, yll, zll, xrr, yrr, zrr, ip, dim;
  int slab_x, slab_y, slab_z;
  int slab_xx, slab_yy, slab_zz;
  int meshmin[3], meshmax[3], sendmin, sendmax, recvmin, recvmax;
  int dimx, dimy, dimz, recv_dimx, recv_dimy, recv_dimz;
  MPI_Status status;

  if(self->ThisTask == 0)
    printf("Starting non-periodic PM calculation (grid=%d).\n", grnr);

  fac = self->All.G / pow(self->All.TotalMeshSize[grnr], 4) * pow(self->All.TotalMeshSize[grnr] / GRID, 3);	/* to get potential */
  fac *= 1 / (2 * self->All.TotalMeshSize[grnr] / GRID);	/* for finite differencing */

  to_slab_fac = GRID / self->All.TotalMeshSize[grnr];
  
  /* first, establish the extension of the local patch in GRID (for binning) */

  for(j = 0; j < 3; j++)
    {
      meshmin[j] = GRID;
      meshmax[j] = 0;
    }

  for(i = 0, flag = 0; i < self->NumPart; i++)
    {
#ifdef PLACEHIGHRESREGION
      if(grnr == 0 || (grnr == 1 && ((1 << self->P[i].Type) & (PLACEHIGHRESREGION))))
#endif
	{
	  for(j = 0; j < 3; j++)
	    {
	      if(self->P[i].Pos[j] < self->All.Xmintot[grnr][j] || self->P[i].Pos[j] > self->All.Xmaxtot[grnr][j])
		{
		  if(flag == 0)
		    {
		      printf
			("Particle Id=%d on task=%d with coordinates (%g|%g|%g) lies outside PM mesh.\nStopping\n",
			 (int)self->P[i].ID, self->ThisTask, self->P[i].Pos[0], self->P[i].Pos[1], self->P[i].Pos[2]);
		      fflush(stdout);
		    }
		  flag++;
		  break;
		}
	    }
	}

      if(flag > 0)
	continue;

      if(self->P[i].Pos[0] >= self->All.Corner[grnr][0] && self->P[i].Pos[0] < self->All.UpperCorner[grnr][0])
	if(self->P[i].Pos[1] >= self->All.Corner[grnr][1] && self->P[i].Pos[1] < self->All.UpperCorner[grnr][1])
	  if(self->P[i].Pos[2] >= self->All.Corner[grnr][2] && self->P[i].Pos[2] < self->All.UpperCorner[grnr][2])
	    {
	      for(j = 0; j < 3; j++)
		{
		  slab = to_slab_fac * (self->P[i].Pos[j] - self->All.Corner[grnr][j]);

		  if(slab < meshmin[j])
		    meshmin[j] = slab;

		  if(slab > meshmax[j])
		    meshmax[j] = slab;
		}
	    }
    }


  MPI_Allreduce(&flag, &flagsum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  if(flagsum > 0)
    {
      if(self->ThisTask == 0)
	{
	  printf("In total %d particles were outside allowed range.\n", flagsum);
	  fflush(stdout);
	}
      return 1;			/* error - need to return because particle were outside allowed range */
    }

  MPI_Allgather(meshmin, 3, MPI_INT, meshmin_list, 3, MPI_INT, MPI_COMM_WORLD);
  MPI_Allgather(meshmax, 3, MPI_INT, meshmax_list, 3, MPI_INT, MPI_COMM_WORLD);

  dimx = meshmax[0] - meshmin[0] + 2;
  dimy = meshmax[1] - meshmin[1] + 2;
  dimz = meshmax[2] - meshmin[2] + 2;


  //force_treefree();

  pm_init_nonperiodic_allocate(self,(dimx + 4) * (dimy + 4) * (dimz + 4));

  for(i = 0; i < dimx * dimy * dimz; i++)
    workspace[i] = 0;

  for(i = 0; i < self->NumPart; i++)
    {
      if(self->P[i].Pos[0] < self->All.Corner[grnr][0] || self->P[i].Pos[0] >= self->All.UpperCorner[grnr][0])
	continue;
      if(self->P[i].Pos[1] < self->All.Corner[grnr][1] || self->P[i].Pos[1] >= self->All.UpperCorner[grnr][1])
	continue;
      if(self->P[i].Pos[2] < self->All.Corner[grnr][2] || self->P[i].Pos[2] >= self->All.UpperCorner[grnr][2])
	continue;

      slab_x = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]);
      dx = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]) - slab_x;
      slab_x -= meshmin[0];
      slab_xx = slab_x + 1;

      slab_y = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]);
      dy = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]) - slab_y;
      slab_y -= meshmin[1];
      slab_yy = slab_y + 1;

      slab_z = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]);
      dz = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]) - slab_z;
      slab_z -= meshmin[2];
      slab_zz = slab_z + 1;

      workspace[(slab_x * dimy + slab_y) * dimz + slab_z] += self->P[i].Mass * (1.0 - dx) * (1.0 - dy) * (1.0 - dz);
      workspace[(slab_x * dimy + slab_yy) * dimz + slab_z] += self->P[i].Mass * (1.0 - dx) * dy * (1.0 - dz);
      workspace[(slab_x * dimy + slab_y) * dimz + slab_zz] += self->P[i].Mass * (1.0 - dx) * (1.0 - dy) * dz;
      workspace[(slab_x * dimy + slab_yy) * dimz + slab_zz] += self->P[i].Mass * (1.0 - dx) * dy * dz;

      workspace[(slab_xx * dimy + slab_y) * dimz + slab_z] += self->P[i].Mass * (dx) * (1.0 - dy) * (1.0 - dz);
      workspace[(slab_xx * dimy + slab_yy) * dimz + slab_z] += self->P[i].Mass * (dx) * dy * (1.0 - dz);
      workspace[(slab_xx * dimy + slab_y) * dimz + slab_zz] += self->P[i].Mass * (dx) * (1.0 - dy) * dz;
      workspace[(slab_xx * dimy + slab_yy) * dimz + slab_zz] += self->P[i].Mass * (dx) * dy * dz;
    }


  for(i = 0; i < fftsize; i++)	/* clear local density field */
    rhogrid[i] = 0;

  for(level = 0; level < (1 << self->PTask); level++)	/* note: for level=0, target is the same task */
    {
      sendTask = self->ThisTask;
      recvTask = self->ThisTask ^ level;
      if(recvTask < self->NTask)
	{
	  /* check how much we have to send */
	  sendmin = 2 * GRID;
	  sendmax = -1;
	  for(slab_x = meshmin[0]; slab_x < meshmax[0] + 2; slab_x++)
	    if(slab_to_task[slab_x] == recvTask)
	      {
		if(slab_x < sendmin)
		  sendmin = slab_x;
		if(slab_x > sendmax)
		  sendmax = slab_x;
	      }
	  if(sendmax == -1)
	    sendmin = 0;

	  /* check how much we have to receive */
	  recvmin = 2 * GRID;
	  recvmax = -1;
	  for(slab_x = meshmin_list[3 * recvTask]; slab_x < meshmax_list[3 * recvTask] + 2; slab_x++)
	    if(slab_to_task[slab_x] == sendTask)
	      {
		if(slab_x < recvmin)
		  recvmin = slab_x;
		if(slab_x > recvmax)
		  recvmax = slab_x;
	      }
	  if(recvmax == -1)
	    recvmin = 0;

	  if((recvmax - recvmin) >= 0 || (sendmax - sendmin) >= 0)	/* ok, we have a contribution to the slab */
	    {
	      recv_dimx = meshmax_list[3 * recvTask + 0] - meshmin_list[3 * recvTask + 0] + 2;
	      recv_dimy = meshmax_list[3 * recvTask + 1] - meshmin_list[3 * recvTask + 1] + 2;
	      recv_dimz = meshmax_list[3 * recvTask + 2] - meshmin_list[3 * recvTask + 2] + 2;

	      if(level > 0)
		{
		  MPI_Sendrecv(workspace + (sendmin - meshmin[0]) * dimy * dimz,
			       (sendmax - sendmin + 1) * dimy * dimz * sizeof(fftw_real), MPI_BYTE, recvTask,
			       TAG_NONPERIOD_A, forcegrid,
			       (recvmax - recvmin + 1) * recv_dimy * recv_dimz * sizeof(fftw_real), MPI_BYTE,
			       recvTask, TAG_NONPERIOD_A, MPI_COMM_WORLD, &status);
		}
	      else
		{
		  memcpy(forcegrid, workspace + (sendmin - meshmin[0]) * dimy * dimz,
			 (sendmax - sendmin + 1) * dimy * dimz * sizeof(fftw_real));
		}

	      for(slab_x = recvmin; slab_x <= recvmax; slab_x++)
		{
		  slab_xx = slab_x - first_slab_of_task[self->ThisTask];

		  if(slab_xx >= 0 && slab_xx < slabs_per_task[self->ThisTask])
		    {
		      for(slab_y = meshmin_list[3 * recvTask + 1];
			  slab_y <= meshmax_list[3 * recvTask + 1] + 1; slab_y++)
			{
			  slab_yy = slab_y;

			  for(slab_z = meshmin_list[3 * recvTask + 2];
			      slab_z <= meshmax_list[3 * recvTask + 2] + 1; slab_z++)
			    {
			      slab_zz = slab_z;

			      rhogrid[GRID * GRID2 * slab_xx + GRID2 * slab_yy + slab_zz] +=
				forcegrid[((slab_x - recvmin) * recv_dimy +
					   (slab_y - meshmin_list[3 * recvTask + 1])) * recv_dimz +
					  (slab_z - meshmin_list[3 * recvTask + 2])];
			    }
			}
		    }
		}
	    }
	}
    }


  /* Do the FFT of the density field */

  rfftwnd_mpi(fft_forward_plan, 1, rhogrid, workspace, FFTW_TRANSPOSED_ORDER);


  /* multiply with the Fourier transform of the Green's function (kernel) */

  for(y = 0; y < nslab_y; y++)
    for(x = 0; x < GRID; x++)
      for(z = 0; z < GRID / 2 + 1; z++)
	{
	  ip = GRID * (GRID / 2 + 1) * y + (GRID / 2 + 1) * x + z;

	  re =
	    fft_of_rhogrid[ip].re * fft_of_kernel[grnr][ip].re -
	    fft_of_rhogrid[ip].im * fft_of_kernel[grnr][ip].im;

	  im =
	    fft_of_rhogrid[ip].re * fft_of_kernel[grnr][ip].im +
	    fft_of_rhogrid[ip].im * fft_of_kernel[grnr][ip].re;

	  fft_of_rhogrid[ip].re = re;
	  fft_of_rhogrid[ip].im = im;
	}

  /* get the potential by inverse FFT */

  rfftwnd_mpi(fft_inverse_plan, 1, rhogrid, workspace, FFTW_TRANSPOSED_ORDER);

  /* Now rhogrid holds the potential */
  /* construct the potential for the local patch */


  /* if we have a high-res mesh, establish the extension of the local patch in GRID (for reading out the
   * forces) 
   */

#ifdef PLACEHIGHRESREGION
  if(grnr == 1)
    {
      for(j = 0; j < 3; j++)
	{
	  meshmin[j] = GRID;
	  meshmax[j] = 0;
	}

      for(i = 0; i < self->NumPart; i++)
	{
	  if(!((1 << P[i].Type) & (PLACEHIGHRESREGION)))
	    continue;


	  if(self->P[i].Pos[0] >= self->All.Corner[grnr][0] && self->P[i].Pos[0] < self->All.UpperCorner[grnr][0])
	    if(self->P[i].Pos[1] >= self->All.Corner[grnr][1] && self->P[i].Pos[1] < self->All.UpperCorner[grnr][1])
	      if(self->P[i].Pos[2] >= self->All.Corner[grnr][2] && self->P[i].Pos[2] < self->All.UpperCorner[grnr][2])
		{
		  for(j = 0; j < 3; j++)
		    {
		      slab = to_slab_fac * (self->P[i].Pos[j] - self->All.Corner[grnr][j]);

		      if(slab < meshmin[j])
			meshmin[j] = slab;

		      if(slab > meshmax[j])
			meshmax[j] = slab;
		    }
		}
	}

      MPI_Allgather(meshmin, 3, MPI_INT, meshmin_list, 3, MPI_INT, MPI_COMM_WORLD);
      MPI_Allgather(meshmax, 3, MPI_INT, meshmax_list, 3, MPI_INT, MPI_COMM_WORLD);
    }
#endif

  dimx = meshmax[0] - meshmin[0] + 6;
  dimy = meshmax[1] - meshmin[1] + 6;
  dimz = meshmax[2] - meshmin[2] + 6;

  for(j = 0; j < 3; j++)
    {
      if(meshmin[j] < 2)
	endrun(self,131231);
      if(meshmax[j] > GRID / 2 - 3)
	endrun(self,131288);
    }

  for(level = 0; level < (1 << self->PTask); level++)	/* note: for level=0, target is the same task */
    {
      sendTask = self->ThisTask;
      recvTask = self->ThisTask ^ level;

      if(recvTask < self->NTask)
	{
	  /* check how much we have to send */
	  sendmin = 2 * GRID;
	  sendmax = -GRID;
	  for(slab_x = meshmin_list[3 * recvTask] - 2; slab_x < meshmax_list[3 * recvTask] + 4; slab_x++)
	    if(slab_to_task[slab_x] == sendTask)
	      {
		if(slab_x < sendmin)
		  sendmin = slab_x;
		if(slab_x > sendmax)
		  sendmax = slab_x;
	      }
	  if(sendmax == -GRID)
	    sendmin = sendmax + 1;


	  /* check how much we have to receive */
	  recvmin = 2 * GRID;
	  recvmax = -GRID;
	  for(slab_x = meshmin[0] - 2; slab_x < meshmax[0] + 4; slab_x++)
	    if(slab_to_task[slab_x] == recvTask)
	      {
		if(slab_x < recvmin)
		  recvmin = slab_x;
		if(slab_x > recvmax)
		  recvmax = slab_x;
	      }
	  if(recvmax == -GRID)
	    recvmin = recvmax + 1;

	  if((recvmax - recvmin) >= 0 || (sendmax - sendmin) >= 0)	/* ok, we have a contribution to the slab */
	    {
	      recv_dimx = meshmax_list[3 * recvTask + 0] - meshmin_list[3 * recvTask + 0] + 6;
	      recv_dimy = meshmax_list[3 * recvTask + 1] - meshmin_list[3 * recvTask + 1] + 6;
	      recv_dimz = meshmax_list[3 * recvTask + 2] - meshmin_list[3 * recvTask + 2] + 6;

	      /* prepare what we want to send */
	      if(sendmax - sendmin >= 0)
		{
		  for(slab_x = sendmin; slab_x <= sendmax; slab_x++)
		    {
		      slab_xx = slab_x - first_slab_of_task[self->ThisTask];

		      for(slab_y = meshmin_list[3 * recvTask + 1] - 2;
			  slab_y < meshmax_list[3 * recvTask + 1] + 4; slab_y++)
			{
			  slab_yy = slab_y;

			  for(slab_z = meshmin_list[3 * recvTask + 2] - 2;
			      slab_z < meshmax_list[3 * recvTask + 2] + 4; slab_z++)
			    {
			      slab_zz = slab_z;

			      forcegrid[((slab_x - sendmin) * recv_dimy +
					 (slab_y - (meshmin_list[3 * recvTask + 1] - 2))) * recv_dimz +
					slab_z - (meshmin_list[3 * recvTask + 2] - 2)] =
				rhogrid[GRID * GRID2 * slab_xx + GRID2 * slab_yy + slab_zz];
			    }
			}
		    }
		}

	      if(level > 0)
		{
		  MPI_Sendrecv(forcegrid,
			       (sendmax - sendmin + 1) * recv_dimy * recv_dimz * sizeof(fftw_real),
			       MPI_BYTE, recvTask, TAG_NONPERIOD_B,
			       workspace + (recvmin - (meshmin[0] - 2)) * dimy * dimz,
			       (recvmax - recvmin + 1) * dimy * dimz * sizeof(fftw_real), MPI_BYTE,
			       recvTask, TAG_NONPERIOD_B, MPI_COMM_WORLD, &status);
		}
	      else
		{
		  memcpy(workspace + (recvmin - (meshmin[0] - 2)) * dimy * dimz,
			 forcegrid, (recvmax - recvmin + 1) * dimy * dimz * sizeof(fftw_real));
		}
	    }
	}
    }

  dimx = meshmax[0] - meshmin[0] + 2;
  dimy = meshmax[1] - meshmin[1] + 2;
  dimz = meshmax[2] - meshmin[2] + 2;

  recv_dimx = meshmax[0] - meshmin[0] + 6;
  recv_dimy = meshmax[1] - meshmin[1] + 6;
  recv_dimz = meshmax[2] - meshmin[2] + 6;


  for(dim = 0; dim < 3; dim++)	/* Calculate each component of the force. */
    {
      /* get the force component by finite differencing the potential */
      /* note: "workspace" now contains the potential for the local patch, plus a suffiently large buffer region */

      for(x = 0; x < meshmax[0] - meshmin[0] + 2; x++)
	for(y = 0; y < meshmax[1] - meshmin[1] + 2; y++)
	  for(z = 0; z < meshmax[2] - meshmin[2] + 2; z++)
	    {
	      xrr = xll = xr = xl = x;
	      yrr = yll = yr = yl = y;
	      zrr = zll = zr = zl = z;

	      switch (dim)
		{
		case 0:
		  xr = x + 1;
		  xrr = x + 2;
		  xl = x - 1;
		  xll = x - 2;
		  break;
		case 1:
		  yr = y + 1;
		  yl = y - 1;
		  yrr = y + 2;
		  yll = y - 2;
		  break;
		case 2:
		  zr = z + 1;
		  zl = z - 1;
		  zrr = z + 2;
		  zll = z - 2;
		  break;
		}

	      forcegrid[(x * dimy + y) * dimz + z]
		=
		fac * ((4.0 / 3) *
		       (workspace[((xl + 2) * recv_dimy + (yl + 2)) * recv_dimz + (zl + 2)]
			- workspace[((xr + 2) * recv_dimy + (yr + 2)) * recv_dimz + (zr + 2)]) -
		       (1.0 / 6) *
		       (workspace[((xll + 2) * recv_dimy + (yll + 2)) * recv_dimz + (zll + 2)] -
			workspace[((xrr + 2) * recv_dimy + (yrr + 2)) * recv_dimz + (zrr + 2)]));
	    }


      /* read out the forces */

      for(i = 0; i < self->NumPart; i++)
	{
#ifdef PLACEHIGHRESREGION
	  if(grnr == 1)
	    if(!((1 << P[i].Type) & (PLACEHIGHRESREGION)))
	      continue;
#endif
	  slab_x = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]);
	  dx = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]) - slab_x;
	  slab_x -= meshmin[0];
	  slab_xx = slab_x + 1;

	  slab_y = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]);
	  dy = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]) - slab_y;
	  slab_y -= meshmin[1];
	  slab_yy = slab_y + 1;

	  slab_z = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]);
	  dz = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]) - slab_z;
	  slab_z -= meshmin[2];
	  slab_zz = slab_z + 1;

	  acc_dim =
	    forcegrid[(slab_x * dimy + slab_y) * dimz + slab_z] * (1.0 - dx) * (1.0 - dy) * (1.0 - dz);
	  acc_dim += forcegrid[(slab_x * dimy + slab_yy) * dimz + slab_z] * (1.0 - dx) * dy * (1.0 - dz);
	  acc_dim += forcegrid[(slab_x * dimy + slab_y) * dimz + slab_zz] * (1.0 - dx) * (1.0 - dy) * dz;
	  acc_dim += forcegrid[(slab_x * dimy + slab_yy) * dimz + slab_zz] * (1.0 - dx) * dy * dz;

	  acc_dim += forcegrid[(slab_xx * dimy + slab_y) * dimz + slab_z] * (dx) * (1.0 - dy) * (1.0 - dz);
	  acc_dim += forcegrid[(slab_xx * dimy + slab_yy) * dimz + slab_z] * (dx) * dy * (1.0 - dz);
	  acc_dim += forcegrid[(slab_xx * dimy + slab_y) * dimz + slab_zz] * (dx) * (1.0 - dy) * dz;
	  acc_dim += forcegrid[(slab_xx * dimy + slab_yy) * dimz + slab_zz] * (dx) * dy * dz;

	  self->P[i].GravPM[dim] += acc_dim;
	  
	}
    }

  pm_init_nonperiodic_free(self);
  //force_treeallocate(All.TreeAllocFactor * All.MaxPart, All.MaxPart);
  //All.NumForcesSinceLastDomainDecomp = 1 + All.TotNumPart * All.TreeDomainUpdateFrequency;

  if(self->ThisTask == 0)
    printf("done PM.\n");

  return 0;
}




/*! Calculates the long-range non-periodic potential using the PM method.  The
 *  potential is Gaussian filtered with Asmth, given in mesh-cell units.  We
 *  carry out a CIC charge assignment, and compute the potenial by Fourier
 *  transform methods. The CIC kernel is deconvolved.
 */
int pmpotential_nonperiodic(PM *self,int grnr)
{
  double dx, dy, dz;
  double fac, to_slab_fac;
  double re, im, pot;
  int i, j, slab, level, sendTask, recvTask, flag, flagsum;
  int x, y, z, ip;
  int slab_x, slab_y, slab_z;
  int slab_xx, slab_yy, slab_zz;
  int meshmin[3], meshmax[3], sendmin, sendmax, recvmin, recvmax;
  int dimx, dimy, dimz, recv_dimx, recv_dimy, recv_dimz;
  MPI_Status status;


  if(self->ThisTask == 0)
    printf("Starting non-periodic PM-potential calculation.\n");

  fac = self->All.G / pow(self->All.TotalMeshSize[grnr], 4) * pow(self->All.TotalMeshSize[grnr] / GRID, 3);	/* to get potential */

  to_slab_fac = GRID / self->All.TotalMeshSize[grnr];

  /* first, establish the extension of the local patch in GRID (for binning) */

  for(j = 0; j < 3; j++)
    {
      meshmin[j] = GRID;
      meshmax[j] = 0;
    }

  for(i = 0, flag = 0; i < self->NumPart; i++)
    {
#ifdef PLACEHIGHRESREGION
      if(grnr == 0 || (grnr == 1 && ((1 << self->P[i].Type) & (PLACEHIGHRESREGION))))
#endif
	{
	  for(j = 0; j < 3; j++)
	    {
	      if(self->P[i].Pos[j] < self->All.Xmintot[grnr][j] || self->P[i].Pos[j] > self->All.Xmaxtot[grnr][j])
		{
		  if(flag == 0)
		    {
		      printf
			("Particle Id=%d on task=%d with coordinates (%g|%g|%g) lies outside PM mesh.\nStopping\n",
			 (int)self->P[i].ID, self->ThisTask, self->P[i].Pos[0], self->P[i].Pos[1], self->P[i].Pos[2]);
		      fflush(stdout);
		    }
		  flag++;
		  break;
		}
	    }
	}

      if(flag > 0)
	continue;

      if(self->P[i].Pos[0] >= self->All.Corner[grnr][0] && self->P[i].Pos[0] < self->All.UpperCorner[grnr][0])
	if(self->P[i].Pos[1] >= self->All.Corner[grnr][1] && self->P[i].Pos[1] < self->All.UpperCorner[grnr][1])
	  if(self->P[i].Pos[2] >= self->All.Corner[grnr][2] && self->P[i].Pos[2] < self->All.UpperCorner[grnr][2])
	    {
	      for(j = 0; j < 3; j++)
		{
		  slab = to_slab_fac * (self->P[i].Pos[j] - self->All.Corner[grnr][j]);

		  if(slab < meshmin[j])
		    meshmin[j] = slab;

		  if(slab > meshmax[j])
		    meshmax[j] = slab;
		}
	    }
    }


  MPI_Allreduce(&flag, &flagsum, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  if(flagsum > 0) 
    {
      if(self->ThisTask == 0)
	{
	  printf("In total %d particles were outside allowed range.\n", flagsum);
	  fflush(stdout);
	}
      return 1;			/* error - need to return because particle were outside allowed range */
    }



  MPI_Allgather(meshmin, 3, MPI_INT, meshmin_list, 3, MPI_INT, MPI_COMM_WORLD);
  MPI_Allgather(meshmax, 3, MPI_INT, meshmax_list, 3, MPI_INT, MPI_COMM_WORLD);

  dimx = meshmax[0] - meshmin[0] + 2;
  dimy = meshmax[1] - meshmin[1] + 2;
  dimz = meshmax[2] - meshmin[2] + 2;


  //force_treefree();

  pm_init_nonperiodic_allocate(self,(dimx + 4) * (dimy + 4) * (dimz + 4));

  for(i = 0; i < dimx * dimy * dimz; i++)
    workspace[i] = 0;

  for(i = 0; i < self->NumPart; i++)
    {
      if(self->P[i].Pos[0] < self->All.Corner[grnr][0] || self->P[i].Pos[0] >= self->All.UpperCorner[grnr][0])
	continue;
      if(self->P[i].Pos[1] < self->All.Corner[grnr][1] || self->P[i].Pos[1] >= self->All.UpperCorner[grnr][1])
	continue;
      if(self->P[i].Pos[2] < self->All.Corner[grnr][2] || self->P[i].Pos[2] >= self->All.UpperCorner[grnr][2])
	continue;

      slab_x = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]);
      dx = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]) - slab_x;
      slab_x -= meshmin[0];
      slab_xx = slab_x + 1;

      slab_y = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]);
      dy = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]) - slab_y;
      slab_y -= meshmin[1];
      slab_yy = slab_y + 1;

      slab_z = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]);
      dz = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]) - slab_z;
      slab_z -= meshmin[2];
      slab_zz = slab_z + 1;

      workspace[(slab_x * dimy + slab_y) * dimz + slab_z] += self->P[i].Mass * (1.0 - dx) * (1.0 - dy) * (1.0 - dz);
      workspace[(slab_x * dimy + slab_yy) * dimz + slab_z] += self->P[i].Mass * (1.0 - dx) * dy * (1.0 - dz);
      workspace[(slab_x * dimy + slab_y) * dimz + slab_zz] += self->P[i].Mass * (1.0 - dx) * (1.0 - dy) * dz;
      workspace[(slab_x * dimy + slab_yy) * dimz + slab_zz] += self->P[i].Mass * (1.0 - dx) * dy * dz;

      workspace[(slab_xx * dimy + slab_y) * dimz + slab_z] += self->P[i].Mass * (dx) * (1.0 - dy) * (1.0 - dz);
      workspace[(slab_xx * dimy + slab_yy) * dimz + slab_z] += self->P[i].Mass * (dx) * dy * (1.0 - dz);
      workspace[(slab_xx * dimy + slab_y) * dimz + slab_zz] += self->P[i].Mass * (dx) * (1.0 - dy) * dz;
      workspace[(slab_xx * dimy + slab_yy) * dimz + slab_zz] += self->P[i].Mass * (dx) * dy * dz;
    }


  for(i = 0; i < fftsize; i++)	/* clear local density field */
    rhogrid[i] = 0;

  for(level = 0; level < (1 << self->PTask); level++)	/* note: for level=0, target is the same task */
    {
      sendTask = self->ThisTask;
      recvTask = self->ThisTask ^ level;
      if(recvTask < self->NTask)
	{
	  /* check how much we have to send */
	  sendmin = 2 * GRID;
	  sendmax = -1;
	  for(slab_x = meshmin[0]; slab_x < meshmax[0] + 2; slab_x++)
	    if(slab_to_task[slab_x] == recvTask)
	      {
		if(slab_x < sendmin)
		  sendmin = slab_x;
		if(slab_x > sendmax)
		  sendmax = slab_x;
	      }
	  if(sendmax == -1)
	    sendmin = 0;

	  /* check how much we have to receive */
	  recvmin = 2 * GRID;
	  recvmax = -1;
	  for(slab_x = meshmin_list[3 * recvTask]; slab_x < meshmax_list[3 * recvTask] + 2; slab_x++)
	    if(slab_to_task[slab_x] == sendTask)
	      {
		if(slab_x < recvmin)
		  recvmin = slab_x;
		if(slab_x > recvmax)
		  recvmax = slab_x;
	      }
	  if(recvmax == -1)
	    recvmin = 0;

	  if((recvmax - recvmin) >= 0 || (sendmax - sendmin) >= 0)	/* ok, we have a contribution to the slab */
	    {
	      recv_dimx = meshmax_list[3 * recvTask + 0] - meshmin_list[3 * recvTask + 0] + 2;
	      recv_dimy = meshmax_list[3 * recvTask + 1] - meshmin_list[3 * recvTask + 1] + 2;
	      recv_dimz = meshmax_list[3 * recvTask + 2] - meshmin_list[3 * recvTask + 2] + 2;

	      if(level > 0)
		{
		  MPI_Sendrecv(workspace + (sendmin - meshmin[0]) * dimy * dimz,
			       (sendmax - sendmin + 1) * dimy * dimz * sizeof(fftw_real), MPI_BYTE, recvTask,
			       TAG_NONPERIOD_C, forcegrid,
			       (recvmax - recvmin + 1) * recv_dimy * recv_dimz * sizeof(fftw_real), MPI_BYTE,
			       recvTask, TAG_NONPERIOD_C, MPI_COMM_WORLD, &status);
		}
	      else
		{
		  memcpy(forcegrid, workspace + (sendmin - meshmin[0]) * dimy * dimz,
			 (sendmax - sendmin + 1) * dimy * dimz * sizeof(fftw_real));
		}

	      for(slab_x = recvmin; slab_x <= recvmax; slab_x++)
		{
		  slab_xx = slab_x - first_slab_of_task[self->ThisTask];

		  if(slab_xx >= 0 && slab_xx < slabs_per_task[self->ThisTask])
		    {
		      for(slab_y = meshmin_list[3 * recvTask + 1];
			  slab_y <= meshmax_list[3 * recvTask + 1] + 1; slab_y++)
			{
			  slab_yy = slab_y;

			  for(slab_z = meshmin_list[3 * recvTask + 2];
			      slab_z <= meshmax_list[3 * recvTask + 2] + 1; slab_z++)
			    {
			      slab_zz = slab_z;

			      rhogrid[GRID * GRID2 * slab_xx + GRID2 * slab_yy + slab_zz] +=
				forcegrid[((slab_x - recvmin) * recv_dimy +
					   (slab_y - meshmin_list[3 * recvTask + 1])) * recv_dimz +
					  (slab_z - meshmin_list[3 * recvTask + 2])];
			    }
			}
		    }
		}
	    }
	}
    }


  /* Do the FFT of the density field */

  rfftwnd_mpi(fft_forward_plan, 1, rhogrid, workspace, FFTW_TRANSPOSED_ORDER);


  /* multiply with the Fourier transform of the Green's function (kernel) */

  for(y = 0; y < nslab_y; y++)
    for(x = 0; x < GRID; x++)
      for(z = 0; z < GRID / 2 + 1; z++)
	{
	  ip = GRID * (GRID / 2 + 1) * y + (GRID / 2 + 1) * x + z;

	  re =
	    fft_of_rhogrid[ip].re * fft_of_kernel[grnr][ip].re -
	    fft_of_rhogrid[ip].im * fft_of_kernel[grnr][ip].im;

	  im =
	    fft_of_rhogrid[ip].re * fft_of_kernel[grnr][ip].im +
	    fft_of_rhogrid[ip].im * fft_of_kernel[grnr][ip].re;

	  fft_of_rhogrid[ip].re = fac * re;
	  fft_of_rhogrid[ip].im = fac * im;
	}

  /* get the potential by inverse FFT */

  rfftwnd_mpi(fft_inverse_plan, 1, rhogrid, workspace, FFTW_TRANSPOSED_ORDER);

  /* Now rhogrid holds the potential */
  /* construct the potential for the local patch */


  /* if we have a high-res mesh, establish the extension of the local patch in GRID (for reading out the
   * forces) 
   */

#ifdef PLACEHIGHRESREGION
  if(grnr == 1)
    {
      for(j = 0; j < 3; j++)
	{
	  meshmin[j] = GRID;
	  meshmax[j] = 0;
	}

      for(i = 0; i < self->NumPart; i++)
	{
	  if(!((1 << self->P[i].Type) & (PLACEHIGHRESREGION)))
	    continue;


	  if(self->P[i].Pos[0] >= self->All.Corner[grnr][0] && self->P[i].Pos[0] < self->All.UpperCorner[grnr][0])
	    if(self->P[i].Pos[1] >= self->All.Corner[grnr][1] && self->P[i].Pos[1] < self->All.UpperCorner[grnr][1])
	      if(self->P[i].Pos[2] >= self->All.Corner[grnr][2] && self->P[i].Pos[2] < self->All.UpperCorner[grnr][2])
		{
		  for(j = 0; j < 3; j++)
		    {
		      slab = to_slab_fac * (self->P[i].Pos[j] - self->All.Corner[grnr][j]);

		      if(slab < meshmin[j])
			meshmin[j] = slab;

		      if(slab > meshmax[j])
			meshmax[j] = slab;
		    }
		}
	}

      MPI_Allgather(meshmin, 3, MPI_INT, meshmin_list, 3, MPI_INT, MPI_COMM_WORLD);
      MPI_Allgather(meshmax, 3, MPI_INT, meshmax_list, 3, MPI_INT, MPI_COMM_WORLD);
    }
#endif

  dimx = meshmax[0] - meshmin[0] + 6;
  dimy = meshmax[1] - meshmin[1] + 6;
  dimz = meshmax[2] - meshmin[2] + 6;

  for(j = 0; j < 3; j++)
    {
      if(meshmin[j] < 2)
	endrun(self,131231);
      if(meshmax[j] > GRID / 2 - 3)
	endrun(self,131288);
    }

  for(level = 0; level < (1 << self->PTask); level++)	/* note: for level=0, target is the same task */
    {
      sendTask = self->ThisTask;
      recvTask = self->ThisTask ^ level;

      if(recvTask < self->NTask)
	{
	  /* check how much we have to send */
	  sendmin = 2 * GRID;
	  sendmax = -GRID;
	  for(slab_x = meshmin_list[3 * recvTask] - 2; slab_x < meshmax_list[3 * recvTask] + 4; slab_x++)
	    if(slab_to_task[slab_x] == sendTask)
	      {
		if(slab_x < sendmin)
		  sendmin = slab_x;
		if(slab_x > sendmax)
		  sendmax = slab_x;
	      }
	  if(sendmax == -GRID)
	    sendmin = sendmax + 1;


	  /* check how much we have to receive */
	  recvmin = 2 * GRID;
	  recvmax = -GRID;
	  for(slab_x = meshmin[0] - 2; slab_x < meshmax[0] + 4; slab_x++)
	    if(slab_to_task[slab_x] == recvTask)
	      {
		if(slab_x < recvmin)
		  recvmin = slab_x;
		if(slab_x > recvmax)
		  recvmax = slab_x;
	      }
	  if(recvmax == -GRID)
	    recvmin = recvmax + 1;

	  if((recvmax - recvmin) >= 0 || (sendmax - sendmin) >= 0)	/* ok, we have a contribution to the slab */
	    {
	      recv_dimx = meshmax_list[3 * recvTask + 0] - meshmin_list[3 * recvTask + 0] + 6;
	      recv_dimy = meshmax_list[3 * recvTask + 1] - meshmin_list[3 * recvTask + 1] + 6;
	      recv_dimz = meshmax_list[3 * recvTask + 2] - meshmin_list[3 * recvTask + 2] + 6;

	      /* prepare what we want to send */
	      if(sendmax - sendmin >= 0)
		{
		  for(slab_x = sendmin; slab_x <= sendmax; slab_x++)
		    {
		      slab_xx = slab_x - first_slab_of_task[self->ThisTask];

		      for(slab_y = meshmin_list[3 * recvTask + 1] - 2;
			  slab_y < meshmax_list[3 * recvTask + 1] + 4; slab_y++)
			{
			  slab_yy = slab_y;

			  for(slab_z = meshmin_list[3 * recvTask + 2] - 2;
			      slab_z < meshmax_list[3 * recvTask + 2] + 4; slab_z++)
			    {
			      slab_zz = slab_z;

			      forcegrid[((slab_x - sendmin) * recv_dimy +
					 (slab_y - (meshmin_list[3 * recvTask + 1] - 2))) * recv_dimz +
					slab_z - (meshmin_list[3 * recvTask + 2] - 2)] =
				rhogrid[GRID * GRID2 * slab_xx + GRID2 * slab_yy + slab_zz];
			    }
			}
		    }
		}

	      if(level > 0)
		{
		  MPI_Sendrecv(forcegrid,
			       (sendmax - sendmin + 1) * recv_dimy * recv_dimz * sizeof(fftw_real),
			       MPI_BYTE, recvTask, TAG_NONPERIOD_D,
			       workspace + (recvmin - (meshmin[0] - 2)) * dimy * dimz,
			       (recvmax - recvmin + 1) * dimy * dimz * sizeof(fftw_real), MPI_BYTE,
			       recvTask, TAG_NONPERIOD_D, MPI_COMM_WORLD, &status);
		}
	      else
		{
		  memcpy(workspace + (recvmin - (meshmin[0] - 2)) * dimy * dimz,
			 forcegrid, (recvmax - recvmin + 1) * dimy * dimz * sizeof(fftw_real));
		}
	    }
	}
    }

  dimx = meshmax[0] - meshmin[0] + 2;
  dimy = meshmax[1] - meshmin[1] + 2;
  dimz = meshmax[2] - meshmin[2] + 2;

  recv_dimx = meshmax[0] - meshmin[0] + 6;
  recv_dimy = meshmax[1] - meshmin[1] + 6;
  recv_dimz = meshmax[2] - meshmin[2] + 6;


  for(x = 0; x < meshmax[0] - meshmin[0] + 2; x++)
    for(y = 0; y < meshmax[1] - meshmin[1] + 2; y++)
      for(z = 0; z < meshmax[2] - meshmin[2] + 2; z++)
	{
	  forcegrid[(x * dimy + y) * dimz + z]
	    = workspace[((x + 2) * recv_dimy + (y + 2)) * recv_dimz + (z + 2)];
	}


  /* read out the potential */

  for(i = 0; i < self->NumPart; i++)
    {
#ifdef PLACEHIGHRESREGION
      if(grnr == 1)
	if(!((1 << self->P[i].Type) & (PLACEHIGHRESREGION)))
	  continue;
#endif
      slab_x = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]);
      dx = to_slab_fac * (self->P[i].Pos[0] - self->All.Corner[grnr][0]) - slab_x;
      slab_x -= meshmin[0];
      slab_xx = slab_x + 1;

      slab_y = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]);
      dy = to_slab_fac * (self->P[i].Pos[1] - self->All.Corner[grnr][1]) - slab_y;
      slab_y -= meshmin[1];
      slab_yy = slab_y + 1;

      slab_z = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]);
      dz = to_slab_fac * (self->P[i].Pos[2] - self->All.Corner[grnr][2]) - slab_z;
      slab_z -= meshmin[2];
      slab_zz = slab_z + 1;

      pot = forcegrid[(slab_x * dimy + slab_y) * dimz + slab_z] * (1.0 - dx) * (1.0 - dy) * (1.0 - dz);
      pot += forcegrid[(slab_x * dimy + slab_yy) * dimz + slab_z] * (1.0 - dx) * dy * (1.0 - dz);
      pot += forcegrid[(slab_x * dimy + slab_y) * dimz + slab_zz] * (1.0 - dx) * (1.0 - dy) * dz;
      pot += forcegrid[(slab_x * dimy + slab_yy) * dimz + slab_zz] * (1.0 - dx) * dy * dz;

      pot += forcegrid[(slab_xx * dimy + slab_y) * dimz + slab_z] * (dx) * (1.0 - dy) * (1.0 - dz);
      pot += forcegrid[(slab_xx * dimy + slab_yy) * dimz + slab_z] * (dx) * dy * (1.0 - dz);
      pot += forcegrid[(slab_xx * dimy + slab_y) * dimz + slab_zz] * (dx) * (1.0 - dy) * dz;
      pot += forcegrid[(slab_xx * dimy + slab_yy) * dimz + slab_zz] * (dx) * dy * dz;

      self->P[i].Potential += pot;
    }

  pm_init_nonperiodic_free(self);
  //force_treeallocate(self->All.TreeAllocFactor * All.MaxPart, All.MaxPart);
  //All.NumForcesSinceLastDomainDecomp = 1 + All.TotNumPart * All.TreeDomainUpdateFrequency;

  if(self->ThisTask == 0)
    printf("done PM-potential.\n");

  return 0;
}








/*! This function sets-up the Greens function for the non-periodic potential
 *  in real space, and then converts it to Fourier space by means of a FFT.
 */
void pm_setup_nonperiodic_kernel(PM *self)
{
  int i, j, k;
  double x, y, z, r, u, fac;
  double kx, ky, kz, k2, fx, fy, fz, ff;
  int ip;

  /* now set up kernel and its Fourier transform */

  pm_init_nonperiodic_allocate(self,0);

#if !defined(PERIODIC)
  for(i = 0; i < fftsize; i++)	/* clear local density field */
    kernel[0][i] = 0;

  for(i = slabstart_x; i < (slabstart_x + nslab_x); i++)
    for(j = 0; j < GRID; j++)
      for(k = 0; k < GRID; k++)
	{
	  x = ((double) i) / GRID;
	  y = ((double) j) / GRID;
	  z = ((double) k) / GRID;

	  if(x >= 0.5)
	    x -= 1.0;
	  if(y >= 0.5)
	    y -= 1.0;
	  if(z >= 0.5)
	    z -= 1.0;

	  r = sqrt(x * x + y * y + z * z);

	  u = 0.5 * r / (((double) ASMTH) / GRID);

	  fac = 1 - erfc(u);

	  if(r > 0)
	    kernel[0][GRID * GRID2 * (i - slabstart_x) + GRID2 * j + k] = -fac / r;
	  else
	    kernel[0][GRID * GRID2 * (i - slabstart_x) + GRID2 * j + k] =
	      -1 / (sqrt(M_PI) * (((double) ASMTH) / GRID));
	}

  /* do the forward transform of the kernel */

  rfftwnd_mpi(fft_forward_plan, 1, kernel[0], workspace, FFTW_TRANSPOSED_ORDER);
#endif


#if defined(PLACEHIGHRESREGION)
  for(i = 0; i < fftsize; i++)	/* clear local density field */
    kernel[1][i] = 0;

  for(i = slabstart_x; i < (slabstart_x + nslab_x); i++)
    for(j = 0; j < GRID; j++)
      for(k = 0; k < GRID; k++)
	{
	  x = ((double) i) / GRID;
	  y = ((double) j) / GRID;
	  z = ((double) k) / GRID;

	  if(x >= 0.5)
	    x -= 1.0;
	  if(y >= 0.5)
	    y -= 1.0;
	  if(z >= 0.5)
	    z -= 1.0;

	  r = sqrt(x * x + y * y + z * z);

	  u = 0.5 * r / (((double) ASMTH) / GRID);

	  fac = erfc(u * self->All.Asmth[1] / self->All.Asmth[0]) - erfc(u);

	  if(r > 0)
	    kernel[1][GRID * GRID2 * (i - slabstart_x) + GRID2 * j + k] = -fac / r;
	  else
	    {
	      fac = 1 - self->All.Asmth[1] / self->All.Asmth[0];
	      kernel[1][GRID * GRID2 * (i - slabstart_x) + GRID2 * j + k] =
		-fac / (sqrt(M_PI) * (((double) ASMTH) / GRID));
	    }
	}

  /* do the forward transform of the kernel */

  rfftwnd_mpi(fft_forward_plan, 1, kernel[1], workspace, FFTW_TRANSPOSED_ORDER);
#endif

  /* deconvolve the Greens function twice with the CIC kernel */

  for(y = slabstart_y; y < slabstart_y + nslab_y; y++)
    for(x = 0; x < GRID; x++)
      for(z = 0; z < GRID / 2 + 1; z++)
	{
	  if(x > GRID / 2)
	    kx = x - GRID;
	  else
	    kx = x;
	  if(y > GRID / 2)
	    ky = y - GRID;
	  else
	    ky = y;
	  if(z > GRID / 2)
	    kz = z - GRID;
	  else
	    kz = z;

	  k2 = kx * kx + ky * ky + kz * kz;

	  if(k2 > 0)
	    {
	      fx = fy = fz = 1;
	      if(kx != 0)
		{
		  fx = (M_PI * kx) / GRID;
		  fx = sin(fx) / fx;
		}
	      if(ky != 0)
		{
		  fy = (M_PI * ky) / GRID;
		  fy = sin(fy) / fy;
		}
	      if(kz != 0)
		{
		  fz = (M_PI * kz) / GRID;
		  fz = sin(fz) / fz;
		}
	      ff = 1 / (fx * fy * fz);
	      ff = ff * ff * ff * ff;

	      ip = GRID * (GRID / 2 + 1) * (y - slabstart_y) + (GRID / 2 + 1) * x + z;
#if !defined(PERIODIC)
	      fft_of_kernel[0][ip].re *= ff;
	      fft_of_kernel[0][ip].im *= ff;
#endif
#if defined(PLACEHIGHRESREGION)
	      fft_of_kernel[1][ip].re *= ff;
	      fft_of_kernel[1][ip].im *= ff;
#endif
	    }
	}
  /* end deconvolution */

  pm_init_nonperiodic_free(self);
}




/*! This function determines the particle extension of all particles, and for
 *  those types selected with PLACEHIGHRESREGION if this is used, and then
 *  determines the boundaries of the non-periodic FFT-mesh that can be placed
 *  on this region. Note that a sufficient buffer region at the rim of the
 *  occupied part of the mesh needs to be reserved in order to allow a correct
 *  finite differencing using a 4-point formula. In addition, to allow
 *  non-periodic boundaries, the actual FFT mesh used is twice as large in
 *  each dimension compared with PMGRID.
 */
void pm_init_regionsize(PM *self)
{
  double meshinner[2], xmin[2][3], xmax[2][3];
  int i, j, t;

  /* find enclosing rectangle */

  for(j = 0; j < 3; j++)
    {
      xmin[0][j] = xmin[1][j] = 1.0e36;
      xmax[0][j] = xmax[1][j] = -1.0e36;
    }

  for(i = 0; i < self->NumPart; i++)
    for(j = 0; j < 3; j++)
      {
	t = 0;
#ifdef PLACEHIGHRESREGION
	if(((1 << self->P[i].Type) & (PLACEHIGHRESREGION)))
	  t = 1;
#endif
	if(self->P[i].Pos[j] > xmax[t][j])
	  xmax[t][j] = self->P[i].Pos[j];
	if(self->P[i].Pos[j] < xmin[t][j])
	  xmin[t][j] = self->P[i].Pos[j];
      }

  MPI_Allreduce(xmin, self->All.Xmintot, 6, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  MPI_Allreduce(xmax, self->All.Xmaxtot, 6, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

  for(j = 0; j < 2; j++)
    {
      self->All.TotalMeshSize[j] = self->All.Xmaxtot[j][0] - self->All.Xmintot[j][0];
      self->All.TotalMeshSize[j] = dmax(self->All.TotalMeshSize[j], self->All.Xmaxtot[j][1] - self->All.Xmintot[j][1]);
      self->All.TotalMeshSize[j] = dmax(self->All.TotalMeshSize[j], self->All.Xmaxtot[j][2] - self->All.Xmintot[j][2]);
#ifdef ENLARGEREGION
      self->All.TotalMeshSize[j] *= ENLARGEREGION;
#endif

      /* symmetrize the box onto the center */
      for(i = 0; i < 3; i++)
	{
	  self->All.Xmintot[j][i] = (self->All.Xmintot[j][i] + self->All.Xmaxtot[j][i]) / 2 - self->All.TotalMeshSize[j] / 2;
	  self->All.Xmaxtot[j][i] =  self->All.Xmintot[j][i] + self->All.TotalMeshSize[j];
	}
    }

  /* this will produce enough room for zero-padding and buffer region to
     allow finite differencing of the potential  */

  for(j = 0; j < 2; j++)
    {
      meshinner[j] = self->All.TotalMeshSize[j];
      self->All.TotalMeshSize[j] *= 2.001 * (GRID) / ((double) (GRID - 2 - 8));
    }

  /* move lower left corner by two cells to allow finite differencing of the potential by a 4-point function */

  for(j = 0; j < 2; j++)
    for(i = 0; i < 3; i++)
      {
	self->All.Corner[j][i] = self->All.Xmintot[j][i] - 2.0005 * self->All.TotalMeshSize[j] / GRID;
	self->All.UpperCorner[j][i] = self->All.Corner[j][i] + (GRID / 2 - 1) * (self->All.TotalMeshSize[j] / GRID);
      }


#ifndef PERIODIC
  self->All.Asmth[0] = ASMTH * self->All.TotalMeshSize[0] / GRID;
  self->All.Rcut[0] = RCUT * self->All.Asmth[0];
#endif

#ifdef PLACEHIGHRESREGION
  self->All.Asmth[1] = ASMTH * self->All.TotalMeshSize[1] / GRID;
  self->All.Rcut[1] = RCUT * self->All.Asmth[1];
#endif

#ifdef PLACEHIGHRESREGION
  if(2 * self->All.TotalMeshSize[1] / GRID < self->All.Rcut[0])
    {
      self->All.TotalMeshSize[1] = 2 * (meshinner[1] + 2 * self->All.Rcut[0]) * (GRID) / ((double) (GRID - 2));

      for(i = 0; i < 3; i++)
	{
	  self->All.Corner[1][i] = All.Xmintot[1][i] - 1.0001 * self->All.Rcut[0];
	  self->All.UpperCorner[1][i] = self->All.Corner[1][i] + (GRID / 2 - 1) * (self->All.TotalMeshSize[1] / GRID);
	}

      if(2 * self->All.TotalMeshSize[1] / GRID > self->All.Rcut[0])
	{
	  self->All.TotalMeshSize[1] = 2 * (meshinner[1] + 2 * self->All.Rcut[0]) * (GRID) / ((double) (GRID - 10));

	  for(i = 0; i < 3; i++)
	    {
	      self->All.Corner[1][i] = self->All.Xmintot[1][i] - 1.0001 * (self->All.Rcut[0] + 2 * self->All.TotalMeshSize[j] / GRID);
	      self->All.UpperCorner[1][i] = self->All.Corner[1][i] + (GRID / 2 - 1) * (self->All.TotalMeshSize[1] / GRID);
	    }
	}

      self->All.Asmth[1] = ASMTH * self->All.TotalMeshSize[1] / GRID;
      self->All.Rcut[1] = RCUT * self->All.Asmth[1];
    }
#endif

  if(self->ThisTask == 0)
    {
#ifndef PERIODIC
      printf("\nAllowed region for isolated PM mesh (coarse):\n");
      printf("(%g|%g|%g)  -> (%g|%g|%g)   ext=%g  totmeshsize=%g  meshsize=%g\n\n",
	     self->All.Xmintot[0][0], self->All.Xmintot[0][1], self->All.Xmintot[0][2],
	     self->All.Xmaxtot[0][0], self->All.Xmaxtot[0][1], self->All.Xmaxtot[0][2], meshinner[0], self->All.TotalMeshSize[0],
	     self->All.TotalMeshSize[0] / GRID);
#endif
#ifdef PLACEHIGHRESREGION
      printf("\nAllowed region for isolated PM mesh (high-res):\n");
      printf("(%g|%g|%g)  -> (%g|%g|%g)   ext=%g  totmeshsize=%g  meshsize=%g\n\n",
	     self->All.Xmintot[1][0], self->All.Xmintot[1][1], self->All.Xmintot[1][2],
	     self->All.Xmaxtot[1][0], self->All.Xmaxtot[1][1], self->All.Xmaxtot[1][2],
	     meshinner[1], self->All.TotalMeshSize[1], self->All.TotalMeshSize[1] / GRID);
#endif
    }

}





/*! Initialization of the non-periodic PM routines. The plan-files for FFTW
 *  are created. Finally, the routine to set-up the non-periodic Greens
 *  function is called.
 */
void pm_init_nonperiodic(PM *self)
{
  int i, slab_to_task_local[GRID];
  double bytes_tot = 0;
  size_t bytes;


  /* Set up the FFTW plan files. */

  fft_forward_plan = rfftw3d_mpi_create_plan(MPI_COMM_WORLD, GRID, GRID, GRID,
					     FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE | FFTW_IN_PLACE);
  fft_inverse_plan = rfftw3d_mpi_create_plan(MPI_COMM_WORLD, GRID, GRID, GRID,
					     FFTW_COMPLEX_TO_REAL, FFTW_ESTIMATE | FFTW_IN_PLACE);

  /* Workspace out the ranges on each processor. */

  rfftwnd_mpi_local_sizes(fft_forward_plan, &nslab_x, &slabstart_x, &nslab_y, &slabstart_y, &fftsize);


  for(i = 0; i < GRID; i++)
    slab_to_task_local[i] = 0;

  for(i = 0; i < nslab_x; i++)
    slab_to_task_local[slabstart_x + i] = self->ThisTask;

  MPI_Allreduce(slab_to_task_local, slab_to_task, GRID, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

  slabs_per_task = malloc(self->NTask * sizeof(int));
  MPI_Allgather(&nslab_x, 1, MPI_INT, slabs_per_task, 1, MPI_INT, MPI_COMM_WORLD);

#ifndef PERIODIC
  if(self->ThisTask == 0)
    {
      for(i = 0; i < self->NTask; i++)
	printf("Task=%d  FFT-Slabs=%d\n", i, slabs_per_task[i]);
    }
#endif

  first_slab_of_task = malloc(self->NTask * sizeof(int));
  MPI_Allgather(&slabstart_x, 1, MPI_INT, first_slab_of_task, 1, MPI_INT, MPI_COMM_WORLD);

  meshmin_list = malloc(3 * self->NTask * sizeof(int));
  meshmax_list = malloc(3 * self->NTask * sizeof(int));

  MPI_Allreduce(&fftsize, &maxfftsize, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);

  /* now allocate memory to hold the FFT fields */

#if !defined(PERIODIC)
  if(!(kernel[0] = (fftw_real *) malloc(bytes = fftsize * sizeof(fftw_real))))
    {
      printf("failed to allocate memory for `FFT-kernel[0]' (%g MB).\n", bytes / (1024.0 * 1024.0));
      endrun(self,1);
    }
  bytes_tot += bytes;
  fft_of_kernel[0] = (fftw_complex *) kernel[0];
#endif

#if defined(PLACEHIGHRESREGION)
  if(!(kernel[1] = (fftw_real *) malloc(bytes = fftsize * sizeof(fftw_real))))
    {
      printf("failed to allocate memory for `FFT-kernel[1]' (%g MB).\n", bytes / (1024.0 * 1024.0));
      endrun(self,1);
    }
  bytes_tot += bytes;
  fft_of_kernel[1] = (fftw_complex *) kernel[1];
#endif

  if(self->ThisTask == 0)
    printf("\nAllocated %g MByte for FFT kernel(s).\n\n", bytes_tot / (1024.0 * 1024.0));




}












/******************************************************************************

Interface

*******************************************************************************/


static PyObject *
PM_InitDefaultParameters(PM* self)
{
    /* list of Gadget parameters */

    self->All.PartAllocFactor	  		= 2.0;

    self->All.G					= 1;  		
    
    return Py_BuildValue("i",1);
    
}



static PyObject *
PM_info(PM* self)
{

    //static PyObject *format = NULL;
    //PyObject *args, *result;

   
    printf("Hello world, my name is PM\n");


	
    return Py_BuildValue("i",1);
}





static PyObject *
PM_GetParameters(PM* self)
{

    PyObject *dict;
    PyObject *key;
    PyObject *value;
    
    dict = PyDict_New();

    key   = PyString_FromString("ComovingIntegrationOn");
    //value = PyInt_FromLong(self->All.ComovingIntegrationOn);
    PyDict_SetItem(dict,key,value);
    
           
    return Py_BuildValue("O",dict);
    

}



static PyObject *
PM_SetParameters(PM* self, PyObject *args)
{
        
    PyObject *dict;
    PyObject *key;
    PyObject *value;
    int ivalue;
    float fvalue;
    double dvalue;
        	
    /* here, we can have either arguments or dict directly */

    if(PyDict_Check(args))
      {
        dict = args;
      }
    else
      {
        if (! PyArg_ParseTuple(args, "O",&dict))
          return NULL;
      }	

	
    /* check that it is a PyDictObject */
    if(!PyDict_Check(dict))
      {
        PyErr_SetString(PyExc_AttributeError, "argument is not a dictionary.");   
        return NULL;
      }
    
     Py_ssize_t pos=0;
     while(PyDict_Next(dict,&pos,&key,&value))
       {
         
	 if(PyString_Check(key))
	   {
	   	   
	     if(strcmp(PyString_AsString(key), "PeanoHilbertOrder")==0)
	       {
		 if(PyInt_Check(value)||PyLong_Check(value)||PyFloat_Check(value))
		   {
		     ivalue = PyInt_AsLong(value);
		     //Tree_SetPeanoHilbertOrder(self,ivalue);
		   }
	       }	
	       
	       
	              	       	       	       
	       	       	       	       
	   }
       }
	
    return Py_BuildValue("i",1);
}



static PyObject *
PM_AllPotential(PM* self, PyObject *args)
{


        
    PyArrayObject *pot;
    int i;
    npy_intp   ld[1];
    int input_dimension;
    


    /* compute potential */
    pmpotential_nonperiodic(self,0);
    
    /* create a NumPy object */	  
    ld[0]=self->NumPart;
    pot = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_FLOAT);  
            
    for (i = 0; i < self->NumPart; i++) 
      {	
	*(float *)(pot->data + i*(pot->strides[0]))  = self->P[i].Potential;
      }
    
    return PyArray_Return(pot);
}


static PyObject *
PM_AllAcceleration(PM* self, PyObject *args)
{


        
    PyArrayObject *acc;
    int i;
    npy_intp   ld[2];
    


    /* compute potential */    
    i = pmforce_nonperiodic(self,0);
    if(i == 1)  		  /* this is returned if a particle lied outside allowed range */
      {
    	pm_init_regionsize(self);
    	pm_setup_nonperiodic_kernel(self);
    	i = pmforce_nonperiodic(self,0);	  /* try again */
      }
    if(i == 1)
      endrun(self,68687);


    
    ld[0]=self->NumPart;
    ld[1]=3;
    /* there is a kind of bug here ! I cannt replace ld by pos->dimensions */
    acc = (PyArrayObject *) PyArray_SimpleNew(2,ld,NPY_FLOAT);  
    

    for (i = 0; i < self->NumPart; i++) 
      {

	*(float *)(acc->data + i*(acc->strides[0]) + 0*acc->strides[1]) = self->P[i].GravPM[0];
        *(float *)(acc->data + i*(acc->strides[0]) + 1*acc->strides[1]) = self->P[i].GravPM[1];
        *(float *)(acc->data + i*(acc->strides[0]) + 2*acc->strides[1]) = self->P[i].GravPM[2];
		
      }
    
    return PyArray_Return(acc);
}

static PyObject *
PM_Potential(PM* self, PyObject *args)
{


    PyArrayObject *pos;
    float eps;

    if (! PyArg_ParseTuple(args, "Of",&pos,&eps))
        return PyString_FromString("error");
        
    PyArrayObject *pot;
    int i;
    double x,y,z,lpot;
    npy_intp   ld[1];
    int input_dimension;
    
    input_dimension =pos->nd;
    
    if (input_dimension != 2)
      PyErr_SetString(PyExc_ValueError,"dimension of first argument must be 2");

    if (pos->descr->type_num != PyArray_FLOAT)
      PyErr_SetString(PyExc_ValueError,"argument 1 must be of type Float32");

    
    /* create a NumPy object */	  
    ld[0]=pos->dimensions[0];
    pot = (PyArrayObject *) PyArray_SimpleNew(1,ld,NPY_FLOAT);  
            
    for (i = 0; i < pos->dimensions[0]; i++) 
      {
        x = *(float *) (pos->data + i*(pos->strides[0]) + 0*pos->strides[1]);
        y = *(float *) (pos->data + i*(pos->strides[0]) + 1*pos->strides[1]);
        z = *(float *) (pos->data + i*(pos->strides[0]) + 2*pos->strides[1]);	
		
	//lpot = force_treeevaluate_local_potential(self,x,y,z,(double)eps);	
	lpot = 0;
	
	*(float *)(pot->data + i*(pot->strides[0]))  = lpot;
      }
    
    return PyArray_Return(pot);
}



static PyObject *
PM_Acceleration(PM* self, PyObject *args)
{


    PyArrayObject *pos;
    float eps;

    if (! PyArg_ParseTuple(args, "Of",&pos,&eps))
        return PyString_FromString("error");
        
    PyArrayObject *acc;
    int i;
    double x,y,z,ax,ay,az;
    int input_dimension;
    
    
    input_dimension =pos->nd;
    
    if (input_dimension != 2)
      PyErr_SetString(PyExc_ValueError,"dimension of first argument must be 2");

    if (pos->descr->type_num != PyArray_FLOAT)
      PyErr_SetString(PyExc_ValueError,"argument 1 must be of type Float32");

    
    /* create a NumPy object */	  
    //acc = (PyArrayObject *) PyArray_SimpleNew(pos->nd,pos->dimensions,pos->descr->type_num);

    npy_intp   ld[2];
    ld[0]=pos->dimensions[0];
    ld[1]=pos->dimensions[1];
    /* there is a kind of bug here ! I cannt replace ld by pos->dimensions */
    acc = (PyArrayObject *) PyArray_SimpleNew(pos->nd,ld,pos->descr->type_num);  
    

    for (i = 0; i < pos->dimensions[0]; i++) 
      {
        x = *(float *) (pos->data + i*(pos->strides[0]) + 0*pos->strides[1]);
        y = *(float *) (pos->data + i*(pos->strides[0]) + 1*pos->strides[1]);
        z = *(float *) (pos->data + i*(pos->strides[0]) + 2*pos->strides[1]);	
	
	//force_treeevaluate_local(self,x,y,z,(double)eps,&ax,&ay,&az);
		
	*(float *)(acc->data + i*(acc->strides[0]) + 0*acc->strides[1]) = ax;
        *(float *)(acc->data + i*(acc->strides[0]) + 1*acc->strides[1]) = ay;
        *(float *)(acc->data + i*(acc->strides[0]) + 2*acc->strides[1]) = az;
		
      }
    
    return PyArray_Return(acc);
}





static int
PM_init(PM *self, PyObject *args, PyObject *kwds)
{

    import_array();
        

    size_t bytes;

    PyArrayObject *pos,*mass;
    PyObject *params;
        
    static char *kwlist[] = {"pos","mass","params", NULL};

    if (! PyArg_ParseTupleAndKeywords(args, kwds, "|OOO",kwlist,&pos,&mass,&params))
        return -1; 


    int i;


    /* MPI init */
    MPI_Comm_rank(MPI_COMM_WORLD, &self->ThisTask);
    MPI_Comm_size(MPI_COMM_WORLD, &self->NTask);
        
    for(self->PTask = 0; self->NTask > (1 << self->PTask); self->PTask++);


    /* init parameters */
    PM_InitDefaultParameters(self);

    if(PyDict_Check(params))
      PM_SetParameters(self,params);

    /* count number of particles */
    self->NumPart = pos->dimensions[0];
    self->All.TotNumPart = self->NumPart;
        
    /* update parameters */    
    self->All.MaxPart	 = self->All.PartAllocFactor * (self->All.TotNumPart);   



    /*********************/
    /* create P          */
    /*********************/
    
    if(!(self->P = malloc(bytes = self->All.MaxPart * sizeof(struct particle_data))))
      {
        printf("failed to allocate memory for `P' (%g MB).\n", bytes / (1024.0 * 1024.0));
        endrun(self,1);
      }    


    /*********************/
    /* init P            */
    /*********************/

    for (i = 0; i < pos->dimensions[0]; i++) 
      {
        self->P[i].Pos[0] = *(float *) (pos->data + i*(pos->strides[0]) + 0*pos->strides[1]);
        self->P[i].Pos[1] = *(float *) (pos->data + i*(pos->strides[0]) + 1*pos->strides[1]);
        self->P[i].Pos[2] = *(float *) (pos->data + i*(pos->strides[0]) + 2*pos->strides[1]);
        self->P[i].Mass   = *(float *) (mass->data + i*(mass->strides[0]));
	self->P[i].ID     =  (unsigned int *) i;
	//self->P[i].ID     = *(unsigned int *) (num->data + i*(num->strides[0]));
	self->P[i].Active = 1;
      }



    /***************************************
     * init pm   * 
    /***************************************/


    pm_init_nonperiodic(self);

    pm_init_regionsize(self);
    pm_setup_nonperiodic_kernel(self);
    


    return 0;

}


/******************************************************************************

class definition

*******************************************************************************/



static PyMethodDef PM_methods[] = {

    {"info", (PyCFunction)PM_info, METH_NOARGS,
     "Return info"
    },


    {"SetParameters", (PyCFunction)PM_SetParameters, METH_VARARGS,
    "Set PM parameters"
    },    

    {"GetParameters", (PyCFunction)PM_GetParameters, METH_NOARGS,
    "Get PM parameters"
    },        

    {"AllPotential", (PyCFunction)PM_AllPotential, METH_NOARGS,
    "Get potential for all particles"
    },        

    {"AllAcceleration", (PyCFunction)PM_AllAcceleration, METH_NOARGS,
    "Get acceleration for all particles"
    },        

    {"Potential", (PyCFunction)PM_Potential, METH_VARARGS,
     "This function computes the potential at a given position using the PM"
    },    

    {"Acceleration", (PyCFunction)PM_Acceleration, METH_VARARGS,
     "This function computes the acceleration at a given position using the PM"
    },   

                
    {NULL}  /* Sentinel */
};


static PyTypeObject PMType = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "pm.PM",               /*tp_name*/
    sizeof(PM),              /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)PM_dealloc,  /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    "PM objects",            /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    PM_methods,              /* tp_methods */
    PM_members,              /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)PM_init,       /* tp_init */
    0,                         /* tp_alloc */
    PM_new,                  /* tp_new */
};

static PyMethodDef module_methods[] = {
    {NULL}  /* Sentinel */
};

#ifndef PyMODINIT_FUNC	/* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif
PyMODINIT_FUNC
initpmlib(void) 
{
    PyObject* m;

    if (PyType_Ready(&PMType) < 0)
        return;

    m = Py_InitModule3("pmlib", module_methods,
                       "Example module that creates an extension type.");

    if (m == NULL)
      return;

    Py_INCREF(&PMType);
    PyModule_AddObject(m, "PM", (PyObject *)&PMType);
}
