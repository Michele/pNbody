#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "allvars.h"
#include "proto.h"


#ifdef MULTIPHASE

static int Ncz;
static int Ncxy;
static int Ncxyz;



/*! \file sticky.c
 *  \brief Compute sticky collisions
 *
*/



/*! init sticky 
 *
 */
void init_sticky()
{
  
  int i;
    
  Ncz   = All.StickyGridNz;
  Ncxy  = All.StickyGridNx*All.StickyGridNy;
  Ncxyz = All.StickyGridNx*All.StickyGridNy*All.StickyGridNz;
  

}




/*! This function is the driver routine for the calculation of sticky collisions
 */
void sticky()
{

  double t0, t1;

  t0 = second();		/* measure the time for the full chimie computation */


  if((All.Time - All.StickyLastCollisionTime) >= All.StickyCollisionTime)
    { 
    
    
#ifndef NO_DENSITY_FOR_STICKY
      density();
      force_update_hmax();
#endif     
    
      sticky_compute_energy_kin(1);
      sticky_collisions();
      sticky_compute_energy_kin(2);
      
      All.StickyLastCollisionTime += All.StickyCollisionTime;

    }	   
  

  t1 = second();
  All.CPU_Sticky += timediff(t0, t1);


}






/*! This function is used as a comparison kernel in a sort routine. 
 */
int compare_sticky_index(const void *a, const void *b)
{
  if(((struct Sticky_index *) a)->CellIndex < (((struct Sticky_index *) b)->CellIndex))
    return -1;

  if(((struct Sticky_index *) a)->CellIndex > (((struct Sticky_index *) b)->CellIndex))
    return +1;

  return 0;
}



/*! This function returns the first particle that may sticky-interact with
 *  the given particle. This routine use a pseudo-grid to find pairs, based
 *  on the Bournaud technique
 */
#ifdef MULTIPHASE
int find_sticky_pairs(int ii)
{

  int jj;
  int ci,cj;
  
  int i,j;
  double dx,dy,dz,r2,r;
  double dvx,dvy,dvz,vdotr,vdotr2,dv;  
  
  if (ii>=N_gas)
    return -11;
  
  ci =  StickyIndex[ii].CellIndex;
  
  if (ci>=Ncxyz)
    return -12;  
    
  jj = ii;

  
  do 
    {    
    
      jj++; 
      
      if (jj>=N_gas)
        return -1;
      
      cj =  StickyIndex[jj].CellIndex;
      
      if (cj>=Ncxyz)
        return -2;
	
      if (ci!=cj)
        return -3;
      
      if (SphP[StickyIndex[jj].Index].StickyFlag)		/* if its free, return it, else, go to the next one */
        {
          /* here we check that we can interact */
      
         i = StickyIndex[ii].Index;
         j = StickyIndex[jj].Index;
	 
      
         dx = P[i].Pos[0] - P[j].Pos[0];
         dy = P[i].Pos[1] - P[j].Pos[1];
         dz = P[i].Pos[2] - P[j].Pos[2];
         r2 = dx * dx + dy * dy + dz * dz;
	 r = sqrt(r2);
	
         dvx = P[i].Vel[0] - P[j].Vel[0];
         dvy = P[i].Vel[1] - P[j].Vel[1];
         dvz = P[i].Vel[2] - P[j].Vel[2];
         vdotr = dx * dvx + dy * dvy + dz * dvz;		    
         vdotr2 = vdotr;
         dv = vdotr2/r;  
	 
	 if ( (vdotr2<0) && (-vdotr2>All.StickyMinVelocity))  
	   {
             printf("(1)%d %d\n",i,j);
	     return StickyIndex[jj].Index;			/* ok, we accept the collision */
	     
	   }		
	}
                
    
    }
  while(1);   

  
}
#endif







void sticky_compute_energy_kin(int mode)
{
  int i;
  double DeltaEgyKin;
  double Tot_DeltaEgyKin;

  DeltaEgyKin = 0;
  Tot_DeltaEgyKin = 0;
  
  if (mode==1)
    {
      LocalSysState.EnergyKin1 = 0;
      LocalSysState.EnergyKin2 = 0;
   }
    
    
  for(i = 0; i < N_gas; i++)
    {
      if (P[i].Type==0)
        {
    
      	  if (mode==1)
            LocalSysState.EnergyKin1 += 0.5 * P[i].Mass * (P[i].Vel[0]*P[i].Vel[0]+P[i].Vel[1]*P[i].Vel[1]+P[i].Vel[2]*P[i].Vel[2]);
      	  else
            LocalSysState.EnergyKin2 += 0.5 * P[i].Mass * (P[i].Vel[0]*P[i].Vel[0]+P[i].Vel[1]*P[i].Vel[1]+P[i].Vel[2]*P[i].Vel[2]);
        }
    }	
    
  if (mode==2)   
    {
      DeltaEgyKin = LocalSysState.EnergyKin2 - LocalSysState.EnergyKin1;
      MPI_Reduce(&DeltaEgyKin, &Tot_DeltaEgyKin, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD); 
      LocalSysState.EnergyRadSticky -=  DeltaEgyKin; 
    }
 
      
}










/*! compute sticky collisions
 *
 */
void sticky_collisions()
{

  int i,j,k;
  int startnode;
  FLOAT *pos, *vel;
  FLOAT mass, h_i;
  int phase;
  
  double vcm[3],newv1[3],newv2[3];
  double dv1[3],dv2[3];
  double dx, dy, dz, dvx, dvy, dvz;
  double r,r2;
  double vdotr, vdotr2;

  double dv;
  double beta_r,beta_t;
  
  double M1M,M2M,M12;
  double dbeta_dv,dbeta_dv_er_x,dbeta_dv_er_y,dbeta_dv_er_z;
  double dv_beta_t_x,dv_beta_t_y,dv_beta_t_z;  
  
  
  NumColLocal = 0;
  NumNoColLocal = 0;  

  beta_r = All.StickyBetaR;
  beta_t = All.StickyBetaT;  



  double xi,yi,zi;
  int ix,iy,iz;
  size_t bytes;
  
  if (ThisTask==0)
    printf("Start sticky collisions...\n");
  
  
  /*****************************/
  /* first, update StickyIndex */
  /*****************************/
  
  if (All.StickyUseGridForCollisions)
    {

      /* allocate memory */
      if(!(StickyIndex = malloc(bytes = N_gas * sizeof(struct Sticky_index))))
        {
          printf("failed to allocate memory for `StickyIndex' (%g MB).\n", bytes / (1024.0 * 1024.0));
          endrun(2233);
        }     


      /* loop gas particles */
      for(i = 0; i < N_gas; i++)
        {

          StickyIndex[i].Index = i;
          StickyIndex[i].CellIndex = Ncxyz;	
          StickyIndex[i].Flag = 1;     
          
        	
          if (SphP[i].StickyFlag)	    /* particle that may collide (set in phase.c) */
            {
        
          
              /* scale between 0 and nx,ny,nz */
              xi = All.StickyGridNx*(P[i].Pos[0]-All.StickyGridXmin)/(All.StickyGridXmax-All.StickyGridXmin);
              yi = All.StickyGridNy*(P[i].Pos[1]-All.StickyGridYmin)/(All.StickyGridYmax-All.StickyGridYmin);
              zi = All.StickyGridNz*(P[i].Pos[2]-All.StickyGridZmin)/(All.StickyGridZmax-All.StickyGridZmin);
        
              ix = (int)(xi);
              iy = (int)(yi);
              iz = (int)(zi);  
        	      
              
              if (ix >= 0 && ix < All.StickyGridNx)
        	if (iy >= 0 && iy < All.StickyGridNy)
        	  if (iz >= 0 && iz < All.StickyGridNz)
        	    {
        	      j = (ix)*(Ncxy) + (iy)*(Ncz) + (iz);
        	      StickyIndex[i].Index = i;
        	      StickyIndex[i].CellIndex = j;	    
        	    }
            }
        
        
      }
  
  
  
      /* sort particles */	
      qsort(StickyIndex, N_gas, sizeof(struct Sticky_index), compare_sticky_index);

  

  
  
      /* record index in SphP (not necessary, but simplifies) */
  
      /*  loop over cells  */
      for(i = 0; i < N_gas; i++)
        {
           //printf("%d %d\n",StickyIndex[i].CellIndex,StickyIndex[i].Index);
           j = StickyIndex[i].Index;	   
           SphP[j].StickyIndex  = i;	   
        }    


    }
    
    
  /*****************************/
  /*  loop over all particles  */
  /*****************************/

    
  for(i = 0; i < N_gas; i++)
    {
        
//#ifdef SFR   
//      if((P[i].Ti_endstep == All.Ti_Current) && (P[i].Type == 0))
//#else
//      if(P[i].Ti_endstep == All.Ti_Current)
//#endif     
        { 
	
      	  if(SphP[i].Phase == GAS_STICKY)
      	    {
      	      if(SphP[i].StickyFlag)
	  	{
	  	
      	  	  pos	= P[i].Pos;
      	  	  vel	= SphP[i].VelPred;	
		  mass  = P[i].Mass;     
	  	  h_i	= SphP[i].Hsml;
	  	  phase = SphP[i].Phase;
	  	  
	  	  startnode = All.MaxPart;
	  	  
		  if (All.StickyUseGridForCollisions)
		    j = find_sticky_pairs(SphP[i].StickyIndex);	
		  else	  
		    j = ngb_treefind_sticky_collisions(&pos[0], h_i, phase, &startnode);
		  		  
		  /* check that particles are in the same cell */
		  /*
		  if (j>=0 && j!=i)
		    {
		      int i1,i2;
		    
          	      xi = All.StickyGridNx*(P[i].Pos[0]-All.StickyGridXmin)/(All.StickyGridXmax-All.StickyGridXmin);
          	      yi = All.StickyGridNy*(P[i].Pos[1]-All.StickyGridYmin)/(All.StickyGridYmax-All.StickyGridYmin);
          	      zi = All.StickyGridNz*(P[i].Pos[2]-All.StickyGridZmin)/(All.StickyGridZmax-All.StickyGridZmin);
    
          	      ix = (int)(xi);
          	      iy = (int)(yi);
          	      iz = (int)(zi);	
		    
		      i1 = (ix)*(Ncxy) + (iy)*(Ncz) + (iz);	  


          	      xi = All.StickyGridNx*(P[i].Pos[0]-All.StickyGridXmin)/(All.StickyGridXmax-All.StickyGridXmin);
          	      yi = All.StickyGridNy*(P[i].Pos[1]-All.StickyGridYmin)/(All.StickyGridYmax-All.StickyGridYmin);
          	      zi = All.StickyGridNz*(P[i].Pos[2]-All.StickyGridZmin)/(All.StickyGridZmax-All.StickyGridZmin);
    
          	      ix = (int)(xi);
          	      iy = (int)(yi);
          	      iz = (int)(zi);	
		    
		      i2 = (ix)*(Ncxy) + (iy)*(Ncz) + (iz);	
		      
		      if (i1!=i2)
		        {
			  printf("\n\nSomething odd here!\n");
		          endrun("999888777");
		        }
			
		    }
		  */
		  

		    
	  	  
	  	  if (j>=0 && j!=i)		/* particles may collide */
	  	    {
	  	      
		      if (j>=N_gas)
		        {
			  printf("sticky_collisions : j=%d,N_gas=%d j>N_gas !!!",j,N_gas);
			  exit(-1);
			}
		      
        	      dx = pos[0] - P[j].Pos[0];
        	      dy = pos[1] - P[j].Pos[1];
        	      dz = pos[2] - P[j].Pos[2];
        	      r2 = dx * dx + dy * dy + dz * dz;
	  	      r = sqrt(r2);
		      
        	      dvx = vel[0] - SphP[j].VelPred[0];
        	      dvy = vel[1] - SphP[j].VelPred[1];
        	      dvz = vel[2] - SphP[j].VelPred[2];
        	      vdotr = dx * dvx + dy * dvy + dz * dvz;			 
                      vdotr2 = vdotr;
                      dv = vdotr2/r;	
		      
		      
		      
                      if (vdotr2<0)		/* particles approaches !!! */
                       {

		      	 /* compute new velocities */												        	  
		      
		     	 M12 = mass+P[j].Mass;
		      	 M1M = mass/M12;
		      	 M2M = P[j].Mass/M12;
                      
			 	   
		      	 /* compute velocity of the mass center */
                      	 for(k = 0; k < 3; k++)
        	      	   vcm[k]  = (mass*vel[k]+P[j].Mass*SphP[j].VelPred[k])/M12;
        	      
   
			 dbeta_dv = (beta_r-beta_t)*dv; 				 
		      	 dbeta_dv_er_x = -dbeta_dv *dx/r;
		      	 dbeta_dv_er_y = -dbeta_dv *dy/r;
		      	 dbeta_dv_er_z = -dbeta_dv *dz/r;
		      
		      	 dv_beta_t_x = dvx*beta_t;
		      	 dv_beta_t_y = dvy*beta_t;
		      	 dv_beta_t_z = dvz*beta_t;
		      
		      
		      	 newv1[0] = M2M * ( +dv_beta_t_x - dbeta_dv_er_x ) +  vcm[0];
		      	 newv1[1] = M2M * ( +dv_beta_t_y - dbeta_dv_er_y ) +  vcm[1];
		      	 newv1[2] = M2M * ( +dv_beta_t_z - dbeta_dv_er_z ) +  vcm[2];			
		      
		     	 newv2[0] = M1M * ( -dv_beta_t_x + dbeta_dv_er_x ) +  vcm[0];
		      	 newv2[1] = M1M * ( -dv_beta_t_y + dbeta_dv_er_y ) +  vcm[1];
		      	 newv2[2] = M1M * ( -dv_beta_t_z + dbeta_dv_er_z ) +  vcm[2];			 
		      	  	      
	  	     
			 /* new velocities */
	  	      	 for(k = 0; k < 3; k++)
      	  	      	   {
			   
			     dv1[k] = newv1[k] - SphP[i].VelPred[k];
			     dv2[k] = newv2[k] - SphP[j].VelPred[k];
			   
	  		     SphP[i].VelPred[k] = newv1[k];
	  		     SphP[j].VelPred[k] = newv2[k];
			     
	  		     P[i].Vel[k] += dv1[k];
	  		     P[j].Vel[k] += dv2[k];				 
			     
	  		   }  
	  	
	  	      
		    
	  	    	 /* set particles as non sticky-active */
	  	      	 SphP[i].StickyFlag = 0;
	  	      	 SphP[j].StickyFlag = 0;
			 SphP[i].StickyTime = All.Time + All.StickyIdleTime;
			 SphP[j].StickyTime = All.Time + All.StickyIdleTime;
			 
	  	      
	  	      	 /* record collision */
      	  	      	 NumColLocal+=2;
      	  	      
		       }
		      	  	    
	  	    }

	  	
	  	
	  	}	
	    }	 
	}       
    }




  /* write some statistics */

  MPI_Allreduce(&NumColLocal,   &NumCol,    1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&NumColPotLocal,&NumColPot, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&NumNoColLocal, &NumNoCol,  1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  
  if(ThisTask == 0)
    {
      fprintf(FdSticky, "Step %d, Time: %g NumColPot: %d NumCol: %d NumNoCol: %d\n", All.NumCurrentTiStep, All.Time,(int)NumColPot,(int)NumCol,(int)NumNoCol);
      fflush(FdSticky);
    }



  if (All.StickyUseGridForCollisions)
    free(StickyIndex);

  if (ThisTask==0)  
    printf("sticky collisions done.\n");


}









#endif






