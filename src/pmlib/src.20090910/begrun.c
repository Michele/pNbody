#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <sys/types.h>
#include <unistd.h>
#include <gsl/gsl_rng.h>

#include "allvars.h"
#include "proto.h"


/*! \file begrun.c
 *  \brief initial set-up of a simulation run
 *
 *  This file contains various functions to initialize a simulation run. In
 *  particular, the parameterfile is read in and parsed, the initial
 *  conditions or restart files are read, and global variables are
 *  initialized to their proper values.
 */


/*! This function performs the initial set-up of the simulation. First, the
 *  parameterfile is set, then routines for setting units, reading
 *  ICs/restart-files are called, auxialiary memory is allocated, etc.
 */
void begrun(void)
{
  struct global_data_all_processes all;  

  if(ThisTask == 0)
    {
      printf("\nThis is Gadget, version `%s'.\n", GADGETVERSION);
      printf("\nRunning on %d processors.\n", NTask);
    }

  read_parameter_file(ParameterFile);	/* ... read in parameters for this run */

  allocate_commbuffers();	/* ... allocate buffer-memory for particle 
				   exchange during force computation */
  set_units();
 

#if defined(PERIODIC) && (!defined(PMGRID) || defined(FORCETEST))
  ewald_init();
#endif

  open_outputfiles();

  random_generator = gsl_rng_alloc(gsl_rng_ranlxd1);
#ifdef RANDOMSEED_AS_PARAMETER   
  if(ThisTask == 0)
    printf("Using %d as initial random seed\n",All.RandomSeed);
  gsl_rng_set(random_generator, All.RandomSeed);	/* start-up seed */
#else
  printf("Using %d as initial random seed\n",42);
  gsl_rng_set(random_generator, 42);			/* start-up seed */
#endif

#ifdef PMGRID
  long_range_init();
#endif

  All.TimeLastRestartFile = CPUThisRun;

#ifdef MULTIPHASE
  All.StickyLastCollisionTime = -1;
#endif  

  if(RestartFlag == 0 || RestartFlag == 2)
    {
      set_random_numbers();

      init();			/* ... read in initial model */
      
      init_local_sys_state();
    }
  else
    {
      all = All;		/* save global variables. (will be read from restart file) */

      restart(RestartFlag);	/* ... read restart file. Note: This also resets 
				   all variables in the struct `All'. 
				   However, during the run, some variables in the parameter
				   file are allowed to be changed, if desired. These need to 
				   copied in the way below.
				   Note:  All.PartAllocFactor is treated in restart() separately.  
				 */
				 
				 /* yr
				 if we want a parameter to be taken as the one written in the parameter file,
				 we have to save it below,
				 instead, the value of the restart file will be taken.
				 
				 This is usefull, for example, if stop a run and want it to be restarted with
				 different parameters.
				 */
				 

      All.MinSizeTimestep = all.MinSizeTimestep;
      All.MaxSizeTimestep = all.MaxSizeTimestep;
      All.BufferSize = all.BufferSize;
      All.BunchSizeForce = all.BunchSizeForce;
      All.BunchSizeDensity = all.BunchSizeDensity;
      All.BunchSizeHydro = all.BunchSizeHydro;
      All.BunchSizeDomain = all.BunchSizeDomain;
#ifdef CHIMIE
      All.BunchSizeChimie = all.BunchSizeChimie;
#endif      

      All.TimeLimitCPU = all.TimeLimitCPU;
      All.ResubmitOn = all.ResubmitOn;
      All.TimeBetSnapshot = all.TimeBetSnapshot;
      All.TimeBetStatistics = all.TimeBetStatistics;
      All.CpuTimeBetRestartFile = all.CpuTimeBetRestartFile;
      All.ErrTolIntAccuracy = all.ErrTolIntAccuracy;
      All.MaxRMSDisplacementFac = all.MaxRMSDisplacementFac;

      All.ErrTolForceAcc = all.ErrTolForceAcc;

      All.TypeOfTimestepCriterion = all.TypeOfTimestepCriterion;
      All.TypeOfOpeningCriterion = all.TypeOfOpeningCriterion;
      All.NumFilesWrittenInParallel = all.NumFilesWrittenInParallel;
      All.TreeDomainUpdateFrequency = all.TreeDomainUpdateFrequency;

      All.SnapFormat = all.SnapFormat;
      All.NumFilesPerSnapshot = all.NumFilesPerSnapshot;
      All.MaxNumNgbDeviation = all.MaxNumNgbDeviation;
      All.ArtBulkViscConst = all.ArtBulkViscConst;


      All.OutputListOn = all.OutputListOn;
      All.CourantFac = all.CourantFac;

      All.OutputListLength = all.OutputListLength;
      memcpy(All.OutputListTimes, all.OutputListTimes, sizeof(double) * All.OutputListLength);

#ifdef RANDOMSEED_AS_PARAMETER
      All.RandomSeed = all.RandomSeed;
#endif

#ifdef MULTIPHASE
      All.CriticalTemperature = all.CriticalTemperature;
      All.CriticalNonCollisionalTemperature = all.CriticalNonCollisionalTemperature;
      
      All.StickyUseGridForCollisions = all.StickyUseGridForCollisions;
      All.StickyTime = all.StickyTime;
      All.StickyCollisionTime = all.StickyCollisionTime;
      All.StickyIdleTime = all.StickyIdleTime;
      All.StickyMinVelocity = all.StickyMinVelocity;
      All.StickyLambda = all.StickyLambda;
      All.StickyDensity = all.StickyDensity;
      All.StickyDensityPower = all.StickyDensityPower;
      All.StickyRsphFact = all.StickyRsphFact;
      All.StickyBetaR = all.StickyBetaR;
      All.StickyBetaT = all.StickyBetaT;
      All.StickyGridNx   =  all.StickyGridNx;
      All.StickyGridNy   =  all.StickyGridNy;
      All.StickyGridNz   =  all.StickyGridNz;
      All.StickyGridXmin =  all.StickyGridXmin;
      All.StickyGridXmax =  all.StickyGridXmax;
      All.StickyGridYmin =  all.StickyGridYmin;
      All.StickyGridYmax =  all.StickyGridYmax;
      All.StickyGridZmin =  all.StickyGridZmin;
      All.StickyGridZmax =  all.StickyGridZmax; 
#ifdef COLDGAS_CYCLE      
      All.ColdGasCycleTransitionTime = all.ColdGasCycleTransitionTime;
      All.ColdGasCycleTransitionParameter = all.ColdGasCycleTransitionParameter;
#endif    
#endif 

#ifdef OUTERPOTENTIAL

#ifdef NFW
      All.HaloConcentration = all.HaloConcentration;
      All.HaloMass = all.HaloMass;
      All.GasMassFraction = all.GasMassFraction;
#endif

#ifdef PLUMMER
      All.PlummerMass = all.PlummerMass;
      All.PlummerSoftenning = all.PlummerSoftenning;
#endif

#ifdef PISOTHERM
      All.Rho0 = all.Rho0;
      All.Rc = all.Rc;
      All.GasMassFraction = all.GasMassFraction;
#endif

#ifdef CORIOLIS
      All.CoriolisOmegaX0 = all.CoriolisOmegaX0;
      All.CoriolisOmegaY0 = all.CoriolisOmegaY0;
      All.CoriolisOmegaZ0 = all.CoriolisOmegaZ0;
#endif

#endif    

#ifdef SFR
      //All.StarFormationNStarsFromGas = all.StarFormationNStarsFromGas; /* do not change the param. if restarting, else, StarFormationStarMass will be wrong */
      //All.StarFormationStarMass = all.StarFormationStarMass;
      All.StarFormationMgMsFraction = all.StarFormationMgMsFraction;
      All.StarFormationType = all.StarFormationType;
      All.StarFormationCstar = all.StarFormationCstar;
      All.StarFormationTime = all.StarFormationTime;
      All.StarFormationDensity = all.StarFormationDensity;
      All.StarFormationTemperature = all.StarFormationTemperature;
      All.ThresholdDensity = all.ThresholdDensity;
#endif   

#ifdef COOLING
      All.CoolingType = all.CoolingType;
      All.CutofCoolingTemperature = all.CutofCoolingTemperature;
      All.InitGasMetallicity = all.InitGasMetallicity;
#endif    

#ifdef  CHIMIE
     All.ChimieSupernovaEnergy = all.ChimieSupernovaEnergy;		/* do not use this value, use the restartfile one */
     All.ChimieKineticFeedbackFraction = all.ChimieKineticFeedbackFraction;     
     All.ChimieWindSpeed = all.ChimieWindSpeed;  
     All.ChimieWindTime = all.ChimieWindTime;   
#endif 

#if defined (HEATING_PE)
      All.HeatingPeElectronFraction = all.HeatingPeElectronFraction;
#endif      
#if defined (HEATING_PE) || defined (STELLAR_FLUX) || defined (EXTERNAL_FLUX)      
      All.HeatingPeSolarEnergyDensity = all.HeatingPeSolarEnergyDensity;
#endif
#if defined (HEATING_PE) || defined (STELLAR_FLUX)      
      All.HeatingPeLMRatioGas   = all.HeatingPeLMRatioGas;
      All.HeatingPeLMRatioHalo  = all.HeatingPeLMRatioHalo;
      All.HeatingPeLMRatioDisk  = all.HeatingPeLMRatioDisk;
      All.HeatingPeLMRatioBulge = all.HeatingPeLMRatioBulge;
      All.HeatingPeLMRatioStars = all.HeatingPeLMRatioStars;
      All.HeatingPeLMRatioBndry = all.HeatingPeLMRatioBndry;
      All.HeatingPeLMRatio[0] = all.HeatingPeLMRatio[0];
      All.HeatingPeLMRatio[1] = all.HeatingPeLMRatio[1];
      All.HeatingPeLMRatio[2] = all.HeatingPeLMRatio[2];
      All.HeatingPeLMRatio[3] = all.HeatingPeLMRatio[3];
      All.HeatingPeLMRatio[4] = all.HeatingPeLMRatio[4];
      All.HeatingPeLMRatio[5] = all.HeatingPeLMRatio[5];
#endif

#ifdef  EXTERNAL_FLUX
     All.HeatingExternalFLuxEnergyDensity = all.HeatingExternalFLuxEnergyDensity;
#endif 

#ifdef FEEDBACK
      All.SupernovaEgySpecPerMassUnit = all.SupernovaEgySpecPerMassUnit;
      All.SupernovaFractionInEgyKin = all.SupernovaFractionInEgyKin;
      All.SupernovaTime = all.SupernovaTime;
      All.SupernovaWindParameter = all.SupernovaWindParameter;
#endif

#ifdef BUBBLES
      All.BubblesDelta = all.BubblesDelta;
      All.BubblesAlpha = all.BubblesAlpha;
      All.BubblesRadiusFactor = all.BubblesRadiusFactor;
      All.BubblesR = all.BubblesR;
#endif       
      
#ifdef AGN_HEATING
      All.AGNHeatingPower = all.AGNHeatingPower;
      All.AGNHeatingRmax = all.AGNHeatingRmax;
#endif

#ifdef AGN_ACCRETION
      All.TimeBetAccretion = all.TimeBetAccretion;
      All.AccretionRadius = all.AccretionRadius;
      All.AGNFactor = all.AGNFactor;
      All.MinMTotInRa = all.MinMTotInRa;
#endif

#ifdef BONDI_ACCRETION
      All.BondiEfficiency = all.BondiEfficiency;
      All.BondiBlackHoleMass = all.BondiBlackHoleMass;
      All.BondiHsmlFactor = all.BondiHsmlFactor;
      All.BondiPower = all.BondiPower;
      All.BondiTimeBet = all.BondiTimeBet;
#endif

      strcpy(All.ResubmitCommand, all.ResubmitCommand);
      strcpy(All.OutputListFilename, all.OutputListFilename);
      strcpy(All.OutputDir, all.OutputDir);
      strcpy(All.RestartFile, all.RestartFile);
      strcpy(All.EnergyFile, all.EnergyFile);
#ifdef SYSTEMSTATISTICS 
      strcpy(All.SystemFile, all.SystemFile);
#endif      
      strcpy(All.InfoFile, all.InfoFile);
      strcpy(All.CpuFile, all.CpuFile);
      strcpy(All.LogFile, all.LogFile);
#ifdef SFR  
      strcpy(All.SfrFile, all.SfrFile);
#endif 
#ifdef CHIMIE  
      strcpy(All.ChimieFile, all.ChimieFile);
#endif 
#ifdef MULTIPHASE  
      strcpy(All.PhaseFile, all.PhaseFile);
      strcpy(All.StickyFile, all.StickyFile);
#endif
#ifdef AGN_ACCRETION  
      strcpy(All.AccretionFile, all.AccretionFile);
#endif
#ifdef BONDI_ACCRETION  
      strcpy(All.BondiFile, all.BondiFile);
#endif
#ifdef BUBBLES
      strcpy(All.BubbleFile, all.BubbleFile);
#endif                   

      strcpy(All.TimingsFile, all.TimingsFile);
      strcpy(All.SnapshotFileBase, all.SnapshotFileBase);

      if(All.TimeMax != all.TimeMax)
	readjust_timebase(All.TimeMax, all.TimeMax);
    }

#ifdef PMGRID
  long_range_init_regionsize();
#endif

  if(All.ComovingIntegrationOn)
    init_drift_table();

  if(RestartFlag == 2)
    All.Ti_nextoutput = find_next_outputtime(All.Ti_Current + 1);
  else
    All.Ti_nextoutput = find_next_outputtime(All.Ti_Current);


  All.TimeLastRestartFile = CPUThisRun;


  /* other initialization for special behavior */


#ifdef SFR
  if (ThisTask == 0)
    printf("StarFormationStarMass = %g\n\n",All.StarFormationStarMass);
#endif



#ifdef COOLING
  if (All.CoolingType==0)	/* sutherland */
    {
      if(ThisTask == 0) printf("Initialize cooling function...\n");
      init_cooling(0);
      if(ThisTask == 0) printf("Initialize cooling function done.\n");  
    }  


  if (All.CoolingType==2)	/* cooling with metals */
    {
      if(ThisTask == 0) printf("Initialize cooling function...\n");
      init_cooling_with_metals();
      if(ThisTask == 0) printf("Initialize cooling function done.\n");  
    }  


#endif 


#ifdef CHIMIE
  if(ThisTask == 0) printf("Initialize chimie...\n");
  init_chimie();
  if(ThisTask == 0) printf("Initialize chimie done...\n");
#endif



#ifdef OUTERPOTENTIAL  
  if(ThisTask == 0) printf("Initialize outer potential...\n");
  init_outer_potential();
  if(ThisTask == 0) printf("Initialize outer potential done.\n");
#endif

  
#ifdef BUBBLES
  if(ThisTask == 0) printf("Initialize bubble function...\n");
  init_bubble();
  if(ThisTask == 0) printf("Initialize bubble function done.\n");  
#endif  


#ifdef MULTIPHASE 
  if(ThisTask == 0) printf("Initialize sticky...\n");
  header.critical_energy_spec = All.CriticalEgySpec;
  init_sticky();
  if(ThisTask == 0) printf("Initialize sticky done.\n");  
#endif


}




/*! Computes conversion factors between internal code units and the
 *  cgs-system.
 */
void set_units(void)
{
  double meanweight;

  All.UnitTime_in_s = All.UnitLength_in_cm / All.UnitVelocity_in_cm_per_s;
  All.UnitTime_in_Megayears = All.UnitTime_in_s / SEC_PER_MEGAYEAR;

  if(All.GravityConstantInternal == 0)
    All.G = GRAVITY / pow(All.UnitLength_in_cm, 3) * All.UnitMass_in_g * pow(All.UnitTime_in_s, 2);
  else
    All.G = All.GravityConstantInternal;

  All.UnitDensity_in_cgs = All.UnitMass_in_g / pow(All.UnitLength_in_cm, 3);
  All.UnitPressure_in_cgs = All.UnitMass_in_g / All.UnitLength_in_cm / pow(All.UnitTime_in_s, 2);
  All.UnitCoolingRate_in_cgs = All.UnitPressure_in_cgs / All.UnitTime_in_s;
  All.UnitEnergy_in_cgs = All.UnitMass_in_g * pow(All.UnitLength_in_cm, 2) / pow(All.UnitTime_in_s, 2);

  /* convert some physical input parameters to internal units */
  All.Hubble = HUBBLE * All.UnitTime_in_s;
 
  meanweight = 4.0 / (1 + 3 * HYDROGEN_MASSFRAC);		/* note: we assume neutral gas here */
  /*meanweight = 4 / (8 - 5 * (1 - HYDROGEN_MASSFRAC));*/	        /* note: we assume FULL ionized gas here */
  
  
  All.Boltzmann  = BOLTZMANN /All.UnitEnergy_in_cgs;
  All.ProtonMass = PROTONMASS/All.UnitMass_in_g;
  All.mumh       = All.ProtonMass*meanweight;


#ifdef MULTIPHASE

  if (All.ComovingIntegrationOn)
    All.StickyTime *= 3.1536e+13*All.HubbleParam/All.UnitTime_in_s;		/* Myr to code unit */
  else
    All.StickyTime *= 3.1536e+13/All.UnitTime_in_s;				/* Myr to code unit */  

  if (All.ComovingIntegrationOn)
    All.StickyCollisionTime *= 3.1536e+13*All.HubbleParam/All.UnitTime_in_s;	/* Myr to code unit */
  else
    All.StickyCollisionTime *= 3.1536e+13/All.UnitTime_in_s;			/* Myr to code unit */  

  if (All.ComovingIntegrationOn)
    All.StickyIdleTime *= 3.1536e+13*All.HubbleParam/All.UnitTime_in_s;		/* Myr to code unit */
  else
    All.StickyIdleTime *= 3.1536e+13/All.UnitTime_in_s;				/* Myr to code unit */  


  if (All.ComovingIntegrationOn)
    {
      All.StickyMinVelocity *= 10000/All.UnitTime_in_s;				/* km/s to code unit */
      printf("here, you must check the unit conversion !");
      endrun(9876);
    }  
  else
    All.StickyMinVelocity *= 10000/All.UnitVelocity_in_cm_per_s;		/* km/s to code unit */ 


  if (All.StickyTime==0)
    All.StickyLambda = 0;
  else
    All.StickyLambda = 1./All.StickyTime;  

  All.CriticalEgySpec 			= 1./GAMMA_MINUS1 * All.Boltzmann/All.mumh * All.CriticalTemperature;
  All.CriticalNonCollisionalEgySpec 	= 1./GAMMA_MINUS1 * All.Boltzmann/All.mumh * All.CriticalNonCollisionalTemperature;

  All.StickyDensity = All.StickyDensity/All.UnitDensity_in_cgs;


//  if((All.StickyLambda > 0.1/All.MaxSizeTimestep)&&(ThisTask==0))
//    {
//      printf("\nStickyLambda is to big and you may experiment numerical problems !\n");
//      printf("You should either decrease StickyLambda or decrease MaxSizeTimestep.\n");
//      printf("(StickyLambda=%g,maxStickyLambda=%g)\n",All.StickyLambda,0.01/All.MaxSizeTimestep);
//      printf("try \n");
//      printf("StickyLambda <= %g    or   MaxSizeTimestep <= %g \n",(0.01/All.MaxSizeTimestep),(0.01/All.StickyLambda));
//      fflush(stdout);
//      endrun(121212);
//    }
    

#ifdef COLDGAS_CYCLE  
  if (All.ComovingIntegrationOn)
    All.ColdGasCycleTransitionTime *= 3.1536e+13*All.HubbleParam/All.UnitTime_in_s;		/* Myr to code unit */
  else
    All.ColdGasCycleTransitionTime *= 3.1536e+13/All.UnitTime_in_s;				/* Myr to code unit */  
#endif
#endif

#ifdef SFR
  All.StarFormationTime		= All.StarFormationTime/All.UnitTime_in_s * 3.1536e16;
  All.StarFormationDensity	= All.StarFormationDensity/All.UnitDensity_in_cgs;  
#endif

#if defined (HEATING_PE) || defined (STELLAR_FLUX)
      All.HeatingPeLMRatio[0] = All.HeatingPeLMRatioGas;
      All.HeatingPeLMRatio[1] = All.HeatingPeLMRatioHalo;
      All.HeatingPeLMRatio[2] = All.HeatingPeLMRatioDisk;
      All.HeatingPeLMRatio[3] = All.HeatingPeLMRatioBulge;
      All.HeatingPeLMRatio[4] = All.HeatingPeLMRatioStars;
      All.HeatingPeLMRatio[5] = All.HeatingPeLMRatioBndry;
      
      int k;
      for (k=0;k<6;k++)
        { 
          All.HeatingPeLMRatio[k] *= 1./SOLAR_MASS;							/* erg/s/Msol to erg/s/g */
          All.HeatingPeLMRatio[k] *= All.UnitMass_in_g*All.UnitTime_in_s / All.UnitEnergy_in_cgs;	/* erg/s/g to code unit */
	}  
#endif

#ifdef FEEDBACK
  All.SupernovaEgySpecPerMassUnit *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;
  if (All.ComovingIntegrationOn)
    All.SupernovaTime *= 3.1536e+13*All.HubbleParam/All.UnitTime_in_s;		/* Myr to code unit */
  else
    All.SupernovaTime *= 3.1536e+13/All.UnitTime_in_s;				/* Myr to code unit */  
    
  All.SupernovaWindSpeed = sqrt( 2*All.SupernovaFractionInEgyKin * All.SupernovaEgySpecPerMassUnit / All.SupernovaWindParameter );  
#endif

#if defined (AGN_ACCRETION) || defined (BONDI_ACCRETION)
  All.LightSpeed = C/All.UnitVelocity_in_cm_per_s;
#endif


#ifdef CHIMIE
  All.ChimieSupernovaEnergy = All.ChimieSupernovaEnergy/All.UnitMass_in_g/pow(All.UnitVelocity_in_cm_per_s,2);
  All.ChimieWindSpeed       = All.ChimieWindSpeed*1e5/All.UnitVelocity_in_cm_per_s;
  All.ChimieWindTime        = All.ChimieWindTime*3.1536e13/All.UnitTime_in_s;  
#endif  

    

  if(ThisTask == 0)
    {
      printf("\nHubble (internal units) = %g\n", All.Hubble);
      printf("G (internal units) = %g\n", All.G);
      printf("Boltzmann = %g \n", All.Boltzmann);
      printf("ProtonMass = %g \n", All.ProtonMass);
      printf("mumh = %g \n", All.mumh);      
      printf("UnitMass_in_g = %g \n", All.UnitMass_in_g);
      printf("UnitTime_in_s = %g \n", All.UnitTime_in_s);
      printf("UnitVelocity_in_cm_per_s = %g \n", All.UnitVelocity_in_cm_per_s);
      printf("UnitDensity_in_cgs = %g \n", All.UnitDensity_in_cgs);
      printf("UnitEnergy_in_cgs = %g \n", All.UnitEnergy_in_cgs);
      printf("\n");
#ifdef SFR
      printf("StarFormationDensity            (internal units) = %g \n", All.StarFormationDensity);
      printf("StarFormationTime               (internal units) = %g \n", All.StarFormationTime);
#endif      
#ifdef FEEDBACK
      printf("SupernovaTime                   (internal units) = %g \n", All.SupernovaTime);
      printf("SupernovaEgySpecPerMassUnit     (internal units) = %g \n", All.SupernovaEgySpecPerMassUnit);
      printf("SupernovaEgySpecKinPerMassUnit  (internal units) = %g \n", pow(All.SupernovaWindSpeed,2)*0.5*All.SupernovaWindParameter);
      printf("SupernovaWindSpeed              (internal units) = %g \n", All.SupernovaWindSpeed);
#endif      

#ifdef MULTIPHASE
      printf("CriticalEgySpec (internal units)               = %g \n", All.CriticalEgySpec);
      printf("CriticalNonCollisionalEgySpec (internal units) = %g \n", All.CriticalNonCollisionalEgySpec);
      printf("StickyCollisionTime (internal units)           = %g \n", All.StickyCollisionTime);
      printf("StickyIdleTime (internal units)                = %g \n", All.StickyIdleTime);
      printf("StickyDensity (internal units)                 = %g \n", All.StickyDensity);
      printf("StickyTime (internal units)                    = %g \n", All.StickyTime);
      printf("StickyMinVelocity (internal units)             = %g \n", All.StickyMinVelocity);
#endif      

#ifdef COLDGAS_CYCLE
      printf("ColdGasCycleTransitionTime (internal units) = %g \n", All.ColdGasCycleTransitionTime);
#endif    

#ifdef CHIMIE
      printf("ChimieSupernovaEnergy (internal units) = %g \n", All.ChimieSupernovaEnergy);
      printf("ChimieWindSpeed       (internal units) = %g \n", All.ChimieWindSpeed);
      printf("ChimieWindTime        (internal units) = %g \n", All.ChimieWindTime);
#endif 

      printf("\n");
    }

#ifdef ISOTHERM_EQS
  All.MinEgySpec = 0;
#else
  All.MinEgySpec = 1 / meanweight * (1.0 / GAMMA_MINUS1) * (BOLTZMANN / PROTONMASS) * All.MinGasTemp;
  All.MinEgySpec *= All.UnitMass_in_g / All.UnitEnergy_in_cgs;
#endif

}





/*! Initialize local system state variables
 */
void init_local_sys_state(void)
{

#ifdef SFR 
  LocalSysState.StarEnergyInt = 0.;
#ifdef COOLING
  LocalSysState.RadiatedEnergy = 0.;
#endif       


#ifdef FEEDBACK
  LocalSysState.StarEnergyFeedback = 0.;
#endif 
#endif   


#ifdef CHIMIE_THERMAL_FEEDBACK   
  LocalSysState.EnergyThermalFeedback = 0.;
#endif 
#ifdef CHIMIE_KINETIC_FEEDBACK
  LocalSysState.EnergyKineticFeedback = 0.;
#endif

#ifdef MULTIPHASE
  LocalSysState.EnergyRadSticky = 0.;
#endif
}



/*!  This function opens various log-files that report on the status and
 *   performance of the simulstion. On restart from restart-files
 *   (start-option 1), the code will append to these files.
 */
void open_outputfiles(void)
{
  char mode[2], buf[200];

#ifdef ADVANCEDSTATISTICS
  int i=0;
#endif  

  if(ThisTask != 0)		/* only the root processor writes to the log files */
    return;

  if(RestartFlag == 0)
    strcpy(mode, "w");
  else
    strcpy(mode, "a");


  sprintf(buf, "%s%s", All.OutputDir, All.CpuFile);
  if(!(FdCPU = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
#ifdef ADVANCEDCPUSTATISTICS
  else
    {
    if(RestartFlag == 0)		/* write the header */
      {
        fprintf(FdCPU,"# Step ");
	fprintf(FdCPU,"Time ");
	fprintf(FdCPU,"nCPUs ");
        fprintf(FdCPU,"CPU_Total "); 
        fprintf(FdCPU,"CPU_Gravity "); 
 	fprintf(FdCPU,"CPU_Hydro ");
#ifdef SFR
        fprintf(FdCPU,"CPU_StarFormation ");
#endif 
#ifdef CHIMIE
        fprintf(FdCPU,"CPU_Chimie ");
#endif      	
#ifdef MULTIPHASE
        fprintf(FdCPU,"CPU_Sticky ");
#endif     
 	fprintf(FdCPU,"CPU_Domain ");
 	fprintf(FdCPU,"CPU_Potential ");
 	fprintf(FdCPU,"CPU_Predict  ");
 	fprintf(FdCPU,"CPU_TimeLine "); 
 	fprintf(FdCPU,"CPU_Snapshot "); 
 	fprintf(FdCPU,"CPU_TreeWalk "); 
 	fprintf(FdCPU,"CPU_TreeConstruction ");
 	fprintf(FdCPU,"CPU_CommSum "); 
 	fprintf(FdCPU,"CPU_Imbalance "); 
 	fprintf(FdCPU,"CPU_HydCompWalk "); 
 	fprintf(FdCPU,"CPU_HydCommSumm ");
 	fprintf(FdCPU,"CPU_HydImbalance "); 
 	fprintf(FdCPU,"CPU_EnsureNgb "); 
 	fprintf(FdCPU,"CPU_PM "); 
 	fprintf(FdCPU,"CPU_Peano ");	  
#ifdef DETAILED_CPU_DOMAIN   
        fprintf(FdCPU,"CPU_Domain_findExtend ");
        fprintf(FdCPU,"CPU_Domain_determineTopTree ");
        fprintf(FdCPU,"CPU_Domain_sumCost ");
        fprintf(FdCPU,"CPU_Domain_findSplit ");
        fprintf(FdCPU,"CPU_Domain_shiftSplit ");
        fprintf(FdCPU,"CPU_Domain_countToGo ");
        fprintf(FdCPU,"CPU_Domain_exchange ");  			       
#endif   
#ifdef DETAILED_CPU_GRAVITY   
        fprintf(FdCPU,"CPU_Gravity_TreeWalk1 ");
        fprintf(FdCPU,"CPU_Gravity_TreeWalk2 ");
        fprintf(FdCPU,"CPU_Gravity_CommSum1 ");
        fprintf(FdCPU,"CPU_Gravity_CommSum2 ");
        fprintf(FdCPU,"CPU_Gravity_Imbalance1 ");
        fprintf(FdCPU,"CPU_Gravity_Imbalance2 ");  			       
#endif   

        /* return */
        fprintf(FdCPU,"\n");
        fflush(FdCPU);
      }
    }  
#endif    





  sprintf(buf, "%s%s", All.OutputDir, All.InfoFile);
  if(!(FdInfo = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }

  sprintf(buf, "%s%s", All.OutputDir, All.LogFile);
  if(!(FdLog = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }

  sprintf(buf, "%s%s", All.OutputDir, All.EnergyFile);
  if(!(FdEnergy = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
#ifdef ADVANCEDSTATISTICS
  else
    {
    if(RestartFlag == 0)		/* write the header */
      {
        fprintf(FdEnergy,"# Time EnergyInt EnergyPot EnergyKin ");
#ifdef COOLING
        fprintf(FdEnergy,"EnergyRadSph ");
#endif
#ifdef AGN_HEATING
        fprintf(FdEnergy,"EnergyAGNHeat ");
#endif
#ifdef MULTIPHASE
        fprintf(FdEnergy,"EnergyRadSticky ");
#endif  
#ifdef FEEDBACK
        fprintf(FdEnergy,"EnergyFeedback ");
#endif    
#ifdef BUBBLES
        fprintf(FdEnergy,"EnergyBubbles ");
#endif    
#ifdef CHIMIE_THERMAL_FEEDBACK    
        fprintf(FdEnergy,"EnergyThermalFeedback ");
#endif   
#ifdef CHIMIE_KINETIC_FEEDBACK
        fprintf(FdEnergy,"EnergyKineticFeedback ");
#endif
        for (i=0;i<6;i++)
          {
            fprintf(FdEnergy,"EnergyIntComp%d EnergyPotComp%d EnergyKinComp%d ",i+1,i+1,i+1);    
#ifdef COOLING
            fprintf(FdEnergy,"EnergyRadSphComp%d ",i+1);
#endif
#ifdef MULTIPHASE
            fprintf(FdEnergy,"EnergyRadStickyComp%d ",i+1);
#endif  
#ifdef FEEDBACK
            fprintf(FdEnergy,"EnergyFeedbackComp%d ",i+1);
#endif 
#ifdef BUBBLES
            fprintf(FdEnergy,"EnergyBubblesComp%d ",i+1);
#endif   
#ifdef CHIMIE_THERMAL_FEEDBACK    
            fprintf(FdEnergy,"EnergyThermalFeedbackComp%d ",i+1);
#endif   
#ifdef CHIMIE_KINETIC_FEEDBACK
            fprintf(FdEnergy,"EnergyKineticFeedbackComp%d ",i+1);
#endif
          }

        for (i=0;i<6;i++)
          fprintf(FdEnergy,"MassComp%d ",i+1);    
      
        /* return */
        fprintf(FdEnergy,"\n");
        fflush(FdEnergy);
      }
    }  
#endif    

#ifdef SYSTEMSTATISTICS 
  sprintf(buf, "%s%s", All.OutputDir, All.SystemFile);
  if(!(FdSystem = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
#endif

  sprintf(buf, "%s%s", All.OutputDir, All.TimingsFile);
  if(!(FdTimings = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }

#ifdef FORCETEST
  if(RestartFlag == 0)
    {
      sprintf(buf, "%s%s", All.OutputDir, "forcetest.txt");
      if(!(FdForceTest = fopen(buf, "w")))
	{
	  printf("error in opening file '%s'\n", buf);
	  endrun(1);
	}
      fclose(FdForceTest);
    }
#endif

#ifdef SFR
  sprintf(buf, "%s%s", All.OutputDir, All.SfrFile);
  if(!(FdSfr = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
#endif

#ifdef CHIMIE
  sprintf(buf, "%s%s", All.OutputDir, All.ChimieFile);
  if(!(FdChimie = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
#endif

#ifdef MULTIPHASE
  sprintf(buf, "%s%s", All.OutputDir, All.PhaseFile);
  if(!(FdPhase = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
  sprintf(buf, "%s%s", All.OutputDir, All.StickyFile);
  if(!(FdSticky = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }    
#endif

#ifdef AGN_ACCRETION
  sprintf(buf, "%s%s", All.OutputDir, All.AccretionFile);
  if(!(FdAccretion = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
#endif

#ifdef BONDI_ACCRETION
  sprintf(buf, "%s%s", All.OutputDir, All.BondiFile);
  if(!(FdBondi = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }
#endif

#ifdef BUBBLES   
  sprintf(buf, "%s%s", All.OutputDir, All.BubbleFile);
  if(!(FdBubble = fopen(buf, mode)))
    {
      printf("error in opening file '%s'\n", buf);
      endrun(1);
    }    
#endif
}


/*!  This function closes the global log-files.
 */
void close_outputfiles(void)
{
  if(ThisTask != 0)		/* only the root processor writes to the log files */
    return;

  fclose(FdCPU);
  fclose(FdInfo);
  fclose(FdLog);
  fclose(FdEnergy);
#ifdef SYSTEMSTATISTICS 
  fclose(FdSystem);
#endif  
  fclose(FdTimings);
#ifdef FORCETEST
  fclose(FdForceTest);
#endif

#ifdef SFR
  fclose(FdSfr);
#endif

#ifdef MULTIPHASE
  fclose(FdPhase);
  fclose(FdSticky);
#endif

#ifdef AGN_ACCRETION
  fclose(FdAccretion);
#endif

#ifdef BONDI_ACCRETION
  fclose(FdBondi);
#endif
  
#ifdef BUBBLES  
  fclose(FdBubble);
#endif
}




/*! This function parses the parameterfile in a simple way.  Each paramater
 *  is defined by a keyword (`tag'), and can be either of type double, int,
 *  or character string.  The routine makes sure that each parameter
 *  appears exactly once in the parameterfile, otherwise error messages are
 *  produced that complain about the missing parameters.
 */
void read_parameter_file(char *fname)
{
#define DOUBLE 1
#define STRING 2
#define INT 3
#define MAXTAGS 300

  FILE *fd, *fdout;
  char buf[200], buf1[200], buf2[200], buf3[400];
  int i, j, nt;
  int id[MAXTAGS];
  void *addr[MAXTAGS];
  char tag[MAXTAGS][50];
  int  errorFlag = 0;
  
  if(sizeof(long long) != 8)
    {
      if(ThisTask == 0)
	printf("\nType `long long' is not 64 bit on this platform. Stopping.\n\n");
      endrun(0);
    }

  if(sizeof(int) != 4)
    {
      if(ThisTask == 0)
	printf("\nType `int' is not 32 bit on this platform. Stopping.\n\n");
      endrun(0);
    }

  if(sizeof(float) != 4)
    {
      if(ThisTask == 0)
	printf("\nType `float' is not 32 bit on this platform. Stopping.\n\n");
      endrun(0);
    }

  if(sizeof(double) != 8)
    {
      if(ThisTask == 0)
	printf("\nType `double' is not 64 bit on this platform. Stopping.\n\n");
      endrun(0);
    }


  if(ThisTask == 0)		/* read parameter file on process 0 */
    {
      nt = 0;

      strcpy(tag[nt], "InitCondFile");
      addr[nt] = All.InitCondFile;
      id[nt++] = STRING;

      strcpy(tag[nt], "OutputDir");
      addr[nt] = All.OutputDir;
      id[nt++] = STRING;

      strcpy(tag[nt], "SnapshotFileBase");
      addr[nt] = All.SnapshotFileBase;
      id[nt++] = STRING;

      strcpy(tag[nt], "EnergyFile");
      addr[nt] = All.EnergyFile;
      id[nt++] = STRING;
#ifdef SYSTEMSTATISTICS
      strcpy(tag[nt], "SystemFile");
      addr[nt] = All.SystemFile;
      id[nt++] = STRING;
#endif
      strcpy(tag[nt], "CpuFile");
      addr[nt] = All.CpuFile;
      id[nt++] = STRING;
#ifdef SFR      
      strcpy(tag[nt], "SfrFile");
      addr[nt] = All.SfrFile;
      id[nt++] = STRING;      
#endif
#ifdef CHIMIE      
      strcpy(tag[nt], "ChimieFile");
      addr[nt] = All.ChimieFile;
      id[nt++] = STRING;      
#endif
#ifdef MULTIPHASE      
      strcpy(tag[nt], "PhaseFile");
      addr[nt] = All.PhaseFile;
      id[nt++] = STRING;      
      strcpy(tag[nt], "StickyFile");
      addr[nt] = All.StickyFile;
      id[nt++] = STRING;           
#endif
#ifdef AGN_ACCRETION      
      strcpy(tag[nt], "AccretionFile");
      addr[nt] = All.AccretionFile;
      id[nt++] = STRING;  
#endif
#ifdef BONDI_ACCRETION      
      strcpy(tag[nt], "BondiFile");
      addr[nt] = All.BondiFile;
      id[nt++] = STRING;  
#endif
#ifdef BUBBLES      
      strcpy(tag[nt], "BubbleFile");
      addr[nt] = All.BubbleFile;
      id[nt++] = STRING;            
#endif
      strcpy(tag[nt], "InfoFile");
      addr[nt] = All.InfoFile;
      id[nt++] = STRING;

      strcpy(tag[nt], "LogFile");
      addr[nt] = All.LogFile;
      id[nt++] = STRING;

      strcpy(tag[nt], "TimingsFile");
      addr[nt] = All.TimingsFile;
      id[nt++] = STRING;

      strcpy(tag[nt], "RestartFile");
      addr[nt] = All.RestartFile;
      id[nt++] = STRING;

      strcpy(tag[nt], "ResubmitCommand");
      addr[nt] = All.ResubmitCommand;
      id[nt++] = STRING;

      strcpy(tag[nt], "OutputListFilename");
      addr[nt] = All.OutputListFilename;
      id[nt++] = STRING;

      strcpy(tag[nt], "OutputListOn");
      addr[nt] = &All.OutputListOn;
      id[nt++] = INT;

      strcpy(tag[nt], "Omega0");
      addr[nt] = &All.Omega0;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "OmegaBaryon");
      addr[nt] = &All.OmegaBaryon;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "OmegaLambda");
      addr[nt] = &All.OmegaLambda;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "HubbleParam");
      addr[nt] = &All.HubbleParam;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "BoxSize");
      addr[nt] = &All.BoxSize;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "PeriodicBoundariesOn");
      addr[nt] = &All.PeriodicBoundariesOn;
      id[nt++] = INT;

      strcpy(tag[nt], "TimeOfFirstSnapshot");
      addr[nt] = &All.TimeOfFirstSnapshot;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "CpuTimeBetRestartFile");
      addr[nt] = &All.CpuTimeBetRestartFile;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "TimeBetStatistics");
      addr[nt] = &All.TimeBetStatistics;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "TimeBegin");
      addr[nt] = &All.TimeBegin;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "TimeMax");
      addr[nt] = &All.TimeMax;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "TimeBetSnapshot");
      addr[nt] = &All.TimeBetSnapshot;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "UnitVelocity_in_cm_per_s");
      addr[nt] = &All.UnitVelocity_in_cm_per_s;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "UnitLength_in_cm");
      addr[nt] = &All.UnitLength_in_cm;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "UnitMass_in_g");
      addr[nt] = &All.UnitMass_in_g;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "TreeDomainUpdateFrequency");
      addr[nt] = &All.TreeDomainUpdateFrequency;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ErrTolIntAccuracy");
      addr[nt] = &All.ErrTolIntAccuracy;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ErrTolTheta");
      addr[nt] = &All.ErrTolTheta;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ErrTolForceAcc");
      addr[nt] = &All.ErrTolForceAcc;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "MinGasHsmlFractional");
      addr[nt] = &All.MinGasHsmlFractional;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "MaxSizeTimestep");
      addr[nt] = &All.MaxSizeTimestep;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "MinSizeTimestep");
      addr[nt] = &All.MinSizeTimestep;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "MaxRMSDisplacementFac");
      addr[nt] = &All.MaxRMSDisplacementFac;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ArtBulkViscConst");
      addr[nt] = &All.ArtBulkViscConst;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "CourantFac");
      addr[nt] = &All.CourantFac;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "DesNumNgb");
      addr[nt] = &All.DesNumNgb;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "MaxNumNgbDeviation");
      addr[nt] = &All.MaxNumNgbDeviation;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ComovingIntegrationOn");
      addr[nt] = &All.ComovingIntegrationOn;
      id[nt++] = INT;

      strcpy(tag[nt], "ICFormat");
      addr[nt] = &All.ICFormat;
      id[nt++] = INT;

      strcpy(tag[nt], "SnapFormat");
      addr[nt] = &All.SnapFormat;
      id[nt++] = INT;

      strcpy(tag[nt], "NumFilesPerSnapshot");
      addr[nt] = &All.NumFilesPerSnapshot;
      id[nt++] = INT;

      strcpy(tag[nt], "NumFilesWrittenInParallel");
      addr[nt] = &All.NumFilesWrittenInParallel;
      id[nt++] = INT;

      strcpy(tag[nt], "ResubmitOn");
      addr[nt] = &All.ResubmitOn;
      id[nt++] = INT;

      strcpy(tag[nt], "TypeOfTimestepCriterion");
      addr[nt] = &All.TypeOfTimestepCriterion;
      id[nt++] = INT;

      strcpy(tag[nt], "TypeOfOpeningCriterion");
      addr[nt] = &All.TypeOfOpeningCriterion;
      id[nt++] = INT;

      strcpy(tag[nt], "TimeLimitCPU");
      addr[nt] = &All.TimeLimitCPU;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningHalo");
      addr[nt] = &All.SofteningHalo;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningDisk");
      addr[nt] = &All.SofteningDisk;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningBulge");
      addr[nt] = &All.SofteningBulge;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningGas");
      addr[nt] = &All.SofteningGas;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningStars");
      addr[nt] = &All.SofteningStars;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningBndry");
      addr[nt] = &All.SofteningBndry;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningHaloMaxPhys");
      addr[nt] = &All.SofteningHaloMaxPhys;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningDiskMaxPhys");
      addr[nt] = &All.SofteningDiskMaxPhys;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningBulgeMaxPhys");
      addr[nt] = &All.SofteningBulgeMaxPhys;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningGasMaxPhys");
      addr[nt] = &All.SofteningGasMaxPhys;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningStarsMaxPhys");
      addr[nt] = &All.SofteningStarsMaxPhys;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "SofteningBndryMaxPhys");
      addr[nt] = &All.SofteningBndryMaxPhys;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "BufferSize");
      addr[nt] = &All.BufferSize;
      id[nt++] = INT;

      strcpy(tag[nt], "PartAllocFactor");
      addr[nt] = &All.PartAllocFactor;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "TreeAllocFactor");
      addr[nt] = &All.TreeAllocFactor;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "GravityConstantInternal");
      addr[nt] = &All.GravityConstantInternal;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "InitGasTemp");
      addr[nt] = &All.InitGasTemp;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "MinGasTemp");
      addr[nt] = &All.MinGasTemp;
      id[nt++] = DOUBLE;

#ifdef RANDOMSEED_AS_PARAMETER
      strcpy(tag[nt], "RandomSeed");
      addr[nt] = &All.RandomSeed;
      id[nt++] = INT;  
#endif
      
#ifdef COOLING
      strcpy(tag[nt], "CoolingFile");
      addr[nt] = All.CoolingFile;
      id[nt++] = STRING;
            
      strcpy(tag[nt], "CutofCoolingTemperature");
      addr[nt] = &All.CutofCoolingTemperature;
      id[nt++] = DOUBLE;      
      
      strcpy(tag[nt], "InitGasMetallicity");
      addr[nt] = &All.InitGasMetallicity;
      id[nt++] = DOUBLE;  
      
      strcpy(tag[nt], "CoolingType");
      addr[nt] = &All.CoolingType;
      id[nt++] = DOUBLE;                      
#endif

#ifdef CHIMIE
      strcpy(tag[nt], "ChimieParameterFile");
      addr[nt] = All.ChimieParameterFile;
      id[nt++] = STRING;

      strcpy(tag[nt], "ChimieSupernovaEnergy");
      addr[nt] = &All.ChimieSupernovaEnergy;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ChimieKineticFeedbackFraction");
      addr[nt] = &All.ChimieKineticFeedbackFraction;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ChimieWindSpeed");
      addr[nt] = &All.ChimieWindSpeed;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ChimieWindTime");
      addr[nt] = &All.ChimieWindTime;
      id[nt++] = DOUBLE;
#endif            


#if defined (HEATING_PE)
      strcpy(tag[nt], "HeatingPeElectronFraction");
      addr[nt] = &All.HeatingPeElectronFraction;
      id[nt++] = DOUBLE;
#endif
#if defined (HEATING_PE) || defined (STELLAR_FLUX) || defined (EXTERNAL_FLUX)            
      strcpy(tag[nt], "HeatingPeSolarEnergyDensity");
      addr[nt] = &All.HeatingPeSolarEnergyDensity;
      id[nt++] = DOUBLE;    
#endif
#if defined (HEATING_PE) || defined (STELLAR_FLUX)      
      strcpy(tag[nt], "HeatingPeLMRatioGas");
      addr[nt] = &All.HeatingPeLMRatioGas;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "HeatingPeLMRatioHalo");
      addr[nt] = &All.HeatingPeLMRatioHalo;
      id[nt++] = DOUBLE;       
      
      strcpy(tag[nt], "HeatingPeLMRatioDisk");
      addr[nt] = &All.HeatingPeLMRatioDisk;
      id[nt++] = DOUBLE;       
      
      strcpy(tag[nt], "HeatingPeLMRatioBulge");
      addr[nt] = &All.HeatingPeLMRatioBulge;
      id[nt++] = DOUBLE;       
      
      strcpy(tag[nt], "HeatingPeLMRatioStars");
      addr[nt] = &All.HeatingPeLMRatioStars;
      id[nt++] = DOUBLE;       
      
      strcpy(tag[nt], "HeatingPeLMRatioBndry");
      addr[nt] = &All.HeatingPeLMRatioBndry;
      id[nt++] = DOUBLE;                                             
#endif

#ifdef  EXTERNAL_FLUX
      strcpy(tag[nt], "HeatingExternalFLuxEnergyDensity");
      addr[nt] = &All.HeatingExternalFLuxEnergyDensity;
      id[nt++] = DOUBLE;         
#endif 

#ifdef MULTIPHASE
      strcpy(tag[nt], "CriticalTemperature");
      addr[nt] = &All.CriticalTemperature;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "CriticalNonCollisionalTemperature");
      addr[nt] = &All.CriticalNonCollisionalTemperature;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "StickyUseGridForCollisions");
      addr[nt] = &All.StickyUseGridForCollisions;
      id[nt++] = INT;
                  
      strcpy(tag[nt], "StickyTime");
      addr[nt] = &All.StickyTime;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyCollisionTime");
      addr[nt] = &All.StickyCollisionTime;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "StickyIdleTime");
      addr[nt] = &All.StickyIdleTime;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyMinVelocity");
      addr[nt] = &All.StickyMinVelocity;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyBetaR");
      addr[nt] = &All.StickyBetaR;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyBetaT");
      addr[nt] = &All.StickyBetaT;
      id[nt++] = DOUBLE;


      strcpy(tag[nt], "StickyGridNx");
      addr[nt] = &All.StickyGridNx;
      id[nt++] = INT;

      strcpy(tag[nt], "StickyGridNy");
      addr[nt] = &All.StickyGridNy;
      id[nt++] = INT;

      strcpy(tag[nt], "StickyGridNz");
      addr[nt] = &All.StickyGridNz;
      id[nt++] = INT;

      strcpy(tag[nt], "StickyGridXmin");
      addr[nt] = &All.StickyGridXmin;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyGridXmax");
      addr[nt] = &All.StickyGridXmax;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyGridYmin");
      addr[nt] = &All.StickyGridYmin;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyGridYmax");
      addr[nt] = &All.StickyGridYmax;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyGridZmin");
      addr[nt] = &All.StickyGridZmin;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyGridZmax");
      addr[nt] = &All.StickyGridZmax;
      id[nt++] = DOUBLE;
            
      
      strcpy(tag[nt], "StickyDensity");
      addr[nt] = &All.StickyDensity;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StickyDensityPower");
      addr[nt] = &All.StickyDensityPower;
      id[nt++] = DOUBLE;
                  
      strcpy(tag[nt], "StickyRsphFact");
      addr[nt] = &All.StickyRsphFact;
      id[nt++] = DOUBLE;
      
#ifdef COLDGAS_CYCLE
      strcpy(tag[nt], "ColdGasCycleTransitionTime");
      addr[nt] = &All.ColdGasCycleTransitionTime;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "ColdGasCycleTransitionParameter");
      addr[nt] = &All.ColdGasCycleTransitionParameter;
      id[nt++] = DOUBLE;
            
#endif      
#endif      

#ifdef OUTERPOTENTIAL

#ifdef NFW
      strcpy(tag[nt], "HaloConcentration");
      addr[nt] = &All.HaloConcentration;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "HaloMass");
      addr[nt] = &All.HaloMass;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "GasMassFraction");
      addr[nt] = &All.GasMassFraction;
      id[nt++] = DOUBLE; 
#endif

#ifdef PLUMMER
      strcpy(tag[nt], "PlummerMass");
      addr[nt] = &All.PlummerMass;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "PlummerSoftenning");
      addr[nt] = &All.PlummerSoftenning;
      id[nt++] = DOUBLE;
#endif

#ifdef PISOTHERM
      strcpy(tag[nt], "Rho0");
      addr[nt] = &All.Rho0;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "Rc");
      addr[nt] = &All.Rc;
      id[nt++] = DOUBLE; 

      strcpy(tag[nt], "GasMassFraction");
      addr[nt] = &All.GasMassFraction;
      id[nt++] = DOUBLE;       
#endif           

#ifdef CORIOLIS
      strcpy(tag[nt], "CoriolisOmegaX0");
      addr[nt] = &All.CoriolisOmegaX0;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "CoriolisOmegaY0");
      addr[nt] = &All.CoriolisOmegaY0;
      id[nt++] = DOUBLE;
      
      strcpy(tag[nt], "CoriolisOmegaZ0");
      addr[nt] = &All.CoriolisOmegaZ0;
      id[nt++] = DOUBLE;            
#endif

#endif  

#ifdef SFR
      strcpy(tag[nt], "StarFormationNStarsFromGas");
      addr[nt] = &All.StarFormationNStarsFromGas;
      id[nt++] = INT;

      strcpy(tag[nt], "StarFormationMgMsFraction");
      addr[nt] = &All.StarFormationMgMsFraction;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StarFormationStarMass");
      addr[nt] = &All.StarFormationStarMass;
      id[nt++] = DOUBLE;

      strcpy(tag[nt], "StarFormationType");
      addr[nt] = &All.StarFormationType;
      id[nt++] = INT;     

      strcpy(tag[nt], "StarFormationCstar");
      addr[nt] = &All.StarFormationCstar;
      id[nt++] = DOUBLE;  
      
      strcpy(tag[nt], "StarFormationTime");
      addr[nt] = &All.StarFormationTime;
      id[nt++] = DOUBLE;     
      
      strcpy(tag[nt], "StarFormationDensity");
      addr[nt] = &All.StarFormationDensity;
      id[nt++] = DOUBLE;     
      
      strcpy(tag[nt], "StarFormationTemperature");
      addr[nt] = &All.StarFormationTemperature;
      id[nt++] = DOUBLE;     
#endif   

#ifdef FEEDBACK
      strcpy(tag[nt], "SupernovaEgySpecPerMassUnit");
      addr[nt] = &All.SupernovaEgySpecPerMassUnit;
      id[nt++] = DOUBLE;   

      strcpy(tag[nt], "SupernovaFractionInEgyKin");
      addr[nt] = &All.SupernovaFractionInEgyKin;
      id[nt++] = DOUBLE;   
      
      strcpy(tag[nt], "SupernovaTime");
      addr[nt] = &All.SupernovaTime;
      id[nt++] = DOUBLE;    
      
      strcpy(tag[nt], "SupernovaWindParameter");
      addr[nt] = &All.SupernovaWindParameter;
      id[nt++] = DOUBLE;                 
#endif   

#ifdef AGN_ACCRETION
      strcpy(tag[nt], "TimeBetAccretion");
      addr[nt] = &All.TimeBetAccretion;
      id[nt++] = DOUBLE;     
      
      strcpy(tag[nt], "AccretionRadius");
      addr[nt] = &All.AccretionRadius;
      id[nt++] = DOUBLE; 
      
      strcpy(tag[nt], "AGNFactor");
      addr[nt] = &All.AGNFactor;
      id[nt++] = DOUBLE; 
            
      strcpy(tag[nt], "MinMTotInRa");
      addr[nt] = &All.MinMTotInRa;
      id[nt++] = DOUBLE;              
#endif  

#ifdef BUBBLES
      strcpy(tag[nt], "BubblesDelta");
      addr[nt] = &All.BubblesDelta;
      id[nt++] = DOUBLE; 

      strcpy(tag[nt], "BubblesAlpha");
      addr[nt] = &All.BubblesAlpha;
      id[nt++] = DOUBLE; 
            
      strcpy(tag[nt], "BubblesRadiusFactor");
      addr[nt] = &All.BubblesRadiusFactor;
      id[nt++] = DOUBLE;         
      
      strcpy(tag[nt], "BubblesInitFile");
      addr[nt] = All.BubblesInitFile;
      id[nt++] = STRING;
#endif

#ifdef AGN_HEATING
      strcpy(tag[nt], "AGNHeatingPower");
      addr[nt] = &All.AGNHeatingPower;
      id[nt++] = DOUBLE; 

      strcpy(tag[nt], "AGNHeatingRmax");
      addr[nt] = &All.AGNHeatingRmax;
      id[nt++] = DOUBLE; 
#endif

#ifdef BONDI_ACCRETION
      strcpy(tag[nt], "BondiEfficiency");
      addr[nt] = &All.BondiEfficiency;
      id[nt++] = DOUBLE; 

      strcpy(tag[nt], "BondiBlackHoleMass");
      addr[nt] = &All.BondiBlackHoleMass;
      id[nt++] = DOUBLE; 

      strcpy(tag[nt], "BondiHsmlFactor");
      addr[nt] = &All.BondiHsmlFactor;
      id[nt++] = DOUBLE; 
      
      strcpy(tag[nt], "BondiTimeBet");
      addr[nt] = &All.BondiTimeBet;
      id[nt++] = DOUBLE;           
#endif


      if((fd = fopen(fname, "r")))
	{
	  sprintf(buf, "%s%s", fname, "-usedvalues");
	  if(!(fdout = fopen(buf, "w")))
	    {
	      printf("error opening file '%s' \n", buf);
	      errorFlag = 1;
	    }
	  else
	    {
	      while(!feof(fd))
		{
		  *buf = 0;
		  fgets(buf, 200, fd);
		  if(sscanf(buf, "%s%s%s", buf1, buf2, buf3) < 2)
		    continue;

		  if(buf1[0] == '%')
		    continue;

		  for(i = 0, j = -1; i < nt; i++)
		    if(strcmp(buf1, tag[i]) == 0)
		      {
			j = i;
			tag[i][0] = 0;
			break;
		      }

		  if(j >= 0)
		    {
		      switch (id[j])
			{
			case DOUBLE:
			  *((double *) addr[j]) = atof(buf2);
			  fprintf(fdout, "%-35s%g\n", buf1, *((double *) addr[j]));
			  break;
			case STRING:
			  strcpy(addr[j], buf2);
			  fprintf(fdout, "%-35s%s\n", buf1, buf2);
			  break;
			case INT:
			  *((int *) addr[j]) = atoi(buf2);
			  fprintf(fdout, "%-35s%d\n", buf1, *((int *) addr[j]));
			  break;
			}
		    }
		  else
		    {
		      fprintf(stdout, "Error in file %s:   Tag '%s' not allowed or multiple defined.\n",
			      fname, buf1);
		      errorFlag = 1;
		    }
		}
	      fclose(fd);
	      fclose(fdout);

	      i = strlen(All.OutputDir);
	      if(i > 0)
		if(All.OutputDir[i - 1] != '/')
		  strcat(All.OutputDir, "/");


              /* copy parameters-usedvalues file*/
	      sprintf(buf1, "%s%s", fname, "-usedvalues");
	      sprintf(buf2, "%s%s", All.OutputDir, "parameters-usedvalues");
              
              fd = fopen(buf1,"r");
	      fdout = fopen(buf2,"w");

              while(1)
	        {
	          fgets(buf, 200, fd);
		  if (feof(fd)) break;
		  fprintf(fdout, buf, 200);
                }
		
  	      fclose(fd);
	      fclose(fdout);
 
  	      
	    }
	}
      else
	{
	  printf("\nParameter file %s not found.\n\n", fname);
	  errorFlag = 2;
	}

      if(errorFlag != 2)
	for(i = 0; i < nt; i++)
	  {
	    if(*tag[i])
	      {
		printf("Error. I miss a value for tag '%s' in parameter file '%s'.\n", tag[i], fname);
		errorFlag = 1;
	      }
	  }

      if(All.OutputListOn && errorFlag == 0)
	errorFlag += read_outputlist(All.OutputListFilename);
      else
	All.OutputListLength = 0;
    }

  MPI_Bcast(&errorFlag, 1, MPI_INT, 0, MPI_COMM_WORLD);

  if(errorFlag)
    {
      MPI_Finalize();
      exit(0);
    }

  /* now communicate the relevant parameters to the other processes */
  MPI_Bcast(&All, sizeof(struct global_data_all_processes), MPI_BYTE, 0, MPI_COMM_WORLD);


  if(All.NumFilesWrittenInParallel < 1)
    {
      if(ThisTask == 0)
	printf("NumFilesWrittenInParallel MUST be at least 1\n");
      endrun(0);
    }

  if(All.NumFilesWrittenInParallel > NTask)
    {
      if(ThisTask == 0)
	printf("NumFilesWrittenInParallel MUST be smaller than number of processors\n");
      endrun(0);
    }

#ifdef PERIODIC
  if(All.PeriodicBoundariesOn == 0)
    {
      if(ThisTask == 0)
	{
	  printf("Code was compiled with periodic boundary conditions switched on.\n");
	  printf("You must set `PeriodicBoundariesOn=1', or recompile the code.\n");
	}
      endrun(0);
    }
#else
  if(All.PeriodicBoundariesOn == 1)
    {
      if(ThisTask == 0)
	{
	  printf("Code was compiled with periodic boundary conditions switched off.\n");
	  printf("You must set `PeriodicBoundariesOn=0', or recompile the code.\n");
	}
      endrun(0);
    }
#endif


  if(All.TypeOfTimestepCriterion >= 1)
    {
      if(ThisTask == 0)
	{
	  printf("The specified timestep criterion\n");
	  printf("is not valid\n");
	}
      endrun(0);
    }

#if defined(LONG_X) ||  defined(LONG_Y) || defined(LONG_Z)
#ifndef NOGRAVITY
  if(ThisTask == 0)
    {
      printf("Code was compiled with LONG_X/Y/Z, but not with NOGRAVITY.\n");
      printf("Stretched periodic boxes are not implemented for gravity yet.\n");
    }
  endrun(0);
#endif
#endif

#undef DOUBLE
#undef STRING
#undef INT
#undef MAXTAGS
}


/*! this function reads a table with a list of desired output times. The
 *  table does not have to be ordered in any way, but may not contain more
 *  than MAXLEN_OUTPUTLIST entries.
 */
int read_outputlist(char *fname)
{
  FILE *fd;

  if(!(fd = fopen(fname, "r")))
    {
      printf("can't read output list in file '%s'\n", fname);
      return 1;
    }

  All.OutputListLength = 0;
  do
    {
      if(fscanf(fd, " %lg ", &All.OutputListTimes[All.OutputListLength]) == 1)
	All.OutputListLength++;
      else
	break;
    }
  while(All.OutputListLength < MAXLEN_OUTPUTLIST);

  fclose(fd);

  printf("\nfound %d times in output-list.\n", All.OutputListLength);

  return 0;
}


/*! If a restart from restart-files is carried out where the TimeMax
 *  variable is increased, then the integer timeline needs to be
 *  adjusted. The approach taken here is to reduce the resolution of the
 *  integer timeline by factors of 2 until the new final time can be
 *  reached within TIMEBASE.
 */
void readjust_timebase(double TimeMax_old, double TimeMax_new)
{
  int i;
  long long ti_end;

  if(ThisTask == 0)
    {
      printf("\nAll.TimeMax has been changed in the parameterfile\n");
      printf("Need to adjust integer timeline\n\n\n");
    }

  if(TimeMax_new < TimeMax_old)
    {
      if(ThisTask == 0)
	printf("\nIt is not allowed to reduce All.TimeMax\n\n");
      endrun(556);
    }

  if(All.ComovingIntegrationOn)
    ti_end = log(TimeMax_new / All.TimeBegin) / All.Timebase_interval;
  else
    ti_end = (TimeMax_new - All.TimeBegin) / All.Timebase_interval;

  while(ti_end > TIMEBASE)
    {
      All.Timebase_interval *= 2.0;

      ti_end /= 2;
      All.Ti_Current /= 2;

#ifdef PMGRID
      All.PM_Ti_begstep /= 2;
      All.PM_Ti_endstep /= 2;
#endif

      for(i = 0; i < NumPart; i++)
	{
	  P[i].Ti_begstep /= 2;
	  P[i].Ti_endstep /= 2;
	}
    }

  All.TimeMax = TimeMax_new;
}

