#!/usr/bin/env python
'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
import time
import pmlib

import Ptools as pt
import time
import sys

if len(sys.argv)==1:
  file = "snap.dat"
else:
  file = sys.argv[1]  

# open a model
nb = Nbody(file,ftype='gadget')

# create a tree object
PM = pmlib.PM(pos=nb.pos,mass=nb.mass)


#nb.potPM = PM.AllPotential()
t0 = time.time()
nb.accPM = PM.AllAcceleration()
t1 = time.time()
tPM = t1-t0

t0 = time.time()
#nb.pot   =  nb.TreePot(nb.pos,eps=1.3,Tree=None)
#nb.acc   =  nb.TreeAccel(nb.pos,eps=1.3,Tree=None)
t1 = time.time()
tTree = t1-t0

#pt.scatter(nb.rxyz(),nb.potPM,s=0.1,color='r')
#pt.scatter(nb.rxyz(),nb.pot  ,s=0.1,color='b')
#pt.show()

#pt.scatter(nb.rxyz(),nb.accPM[:,0],s=0.1,color='r')
#pt.scatter(nb.rxyz(),nb.acc[:,0]  ,s=0.1,color='b')
#pt.show()

print "Nbody = ",nb.nbody
print "tPM  =%g"%tPM
print "tTree=%g"%tTree
