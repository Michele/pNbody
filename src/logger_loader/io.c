#include "io.h"
#include "tools.h"
#include "header.h"

#include <stdlib.h>
#include <stdio.h>
#include <Python.h>

const size_t io_data_size[io_data_count] = {
  sizeof(int),
  sizeof(float),
  sizeof(double),
  sizeof(char)
};

int io_read_mask(struct header *h, FILE *f, size_t *offset, size_t *mask, size_t *prev_offset)
{
  size_t p = 0;
  fseek(f, *offset, SEEK_SET);
  int test = fread(&p, h->mask, 1, f);
  *offset += h->mask;
  if (test != 1)
    {
      error(test, "Unable to read mask");
    }

  *mask = p;
  p = 0;

  test = fread(&p, h->offset, 1, f);
  *offset += h->offset;
  if (test != 1)
    {
      error(test, "Unable to read offset");
    }

  *prev_offset = p;

  return 0;
}

int io_read_data(FILE *f, size_t size, void* p, size_t *offset)
{
  size_t test;
  fseek(f, *offset, SEEK_SET);
  test = fread(p, size, 1, f);
  *offset += size;

  if (test != 1)
    {
      error(EIO, "Unable to read data");
    }

  return 0;
};


void io_general_data_init(struct io_general_data *data)
{
  data->state = io_state_empty;
};

int io_general_data_read(struct io_general_data *data,
			 FILE *f,
			 struct header *h,
			 size_t *offset)
{
  if (data->state != io_state_empty)
    error(EFAULT, "Data not empty");
  
  int test;

  /* read name */
  char *name = malloc(sizeof(char) * h->name);
  test = io_read_data(f, h->name, name, offset);
  if (test != 0)
    return test;
  data->name = name;

  /* read data type */
  size_t data_type = 0;
  test = io_read_data(f, LOGGER_DATATYPE_SIZE, &data_type, offset);
  if (test != 0)
    return test;
  if (data_type >= io_data_count)
    error(ENOTSUP, "Data type %i not implemented");
  data->type = data_type;
      
  /* read value */
  void *p = malloc(io_data_size[data_type]);
  test = io_read_data(f, io_data_size[data_type], p, offset);
  if (test != 0)
    return test;
  data->p = p;

  data->state = io_state_full;

  return 0;
  
}

void io_general_data_clean(struct io_general_data *data)
{
  data->state = io_state_empty;
  free(data->name);
  free(data->p);
}
