#include "header.h"

#include "tools.h"
#include "io.h"

#include <Python.h>
#include <stdlib.h>
#include <stdio.h>

void header_print(struct header *h)
{
  printf("Version:      %s\n", h->version);
  printf("First Offset: %lu\n", h->offset_first);
  printf("Offset size:  %lu\n", h->offset);
  printf("Name size:    %lu\n", h->name);
  printf("Mask size:    %lu\n", h->mask);
  printf("Number size:  %lu\n", h->number);
  printf("Number masks: %lu\n", h->nber_mask);

  for(size_t i=0; i < h->nber_mask; i++)
    {
      printf("\tMask:  %s\n", h->masks_name[i]);
      printf("\tValue: %lu\n", h->masks[i]);
      printf("\tSize:  %lu\n", h->masks_size[i]);
      printf("\n");
    }

  /* mask contains... TODO */
  
  /* data */
  printf("TimeBase: %g\n", h->time_base);
  
  
};


void header_free(struct header *h)
{
  for(size_t i = 0; i < h->nber_mask; i++)
    {
      free(h->masks_name[i]);
    }
  free(h->masks_name);
  free(h->masks);
  free(h->masks_size);
};

int header_is_present_and_get_index(struct header *h, char *field, size_t *ind)
{
  for(size_t i=0; i < h->nber_mask; i++)
    {
      if (strcmp(h->masks_name[i], field) == 0)
	{
	  if (ind != NULL)
	    {
	      *ind = i;
	    }
	  return 1;
	}
    }

  return 0;
};

int header_is_present(struct header *h, char *field)
{
  return header_is_present_and_get_index(h, field, NULL);
};


int header_read(struct header *h, FILE *f) {
  size_t offset = 0;

  /* read version */
  io_read_data(f, LOGGER_VERSION_SIZE, &h->version, &offset);

  /* read offset size */
  h->offset = 0;
  io_read_data(f, LOGGER_OFFSET_SIZE, &h->offset, &offset);

  /* read offset to first data */
  h->offset_first = 0;
  io_read_data(f, h->offset, &h->offset_first, &offset);

  /* read name size */
  h->name = 0;
  io_read_data(f, LOGGER_NAME_SIZE, &h->name, &offset);

  /* check if value defined in this file is large enough */
  if (STRING_SIZE < h->name)
    {
      error(EOVERFLOW, "Name too large in dump file");      
    }

  /* read number size */
  h->number = 0;
  io_read_data(f, LOGGER_NBER_SIZE, &h->number, &offset);

  /* read mask size */
  h->mask = 0;
  io_read_data(f, LOGGER_MASK_SIZE, &h->mask, &offset);

  /* read number of masks */
  h->nber_mask = 0;
  io_read_data(f, h->number, &h->nber_mask, &offset);

  /* allocate memory */
  h->masks = malloc(sizeof(size_t) * h->nber_mask);
  h->masks_name = malloc(sizeof(void*) * h->nber_mask);
  h->masks_size = malloc(sizeof(size_t) * h->nber_mask);

  /* loop over all masks */
  for(size_t i = 0; i < h->nber_mask; i++)
    {
      /* read mask name */
      h->masks_name[i] = malloc(h->name);
      io_read_data(f, h->name, h->masks_name[i], &offset);
      
      /* read mask value */
      h->masks[i] = 0;
      io_read_data(f, h->mask, &h->masks[i], &offset);

      /* read mask data size */
      h->masks_size[i] = 0;
      io_read_data(f, h->number, &h->masks_size[i], &offset);
    }

  /* need to read data information */

  /* read attributes */
  struct io_general_data data;
  io_general_data_init(&data);
  
  int test;
  /* timeBase */
  test = io_general_data_read(&data, f, h, &offset);
  if (test != 0)
    return test;
  
  if (data.type != io_data_double)
    error(EIO, "timeBase should be a double");
  if (strcmp(data.name, "timeBase") != 0)
    error(EIO, "Expecting timeBase but received %s", data.name);

  h->time_base = *(double*)data.p;

  io_general_data_clean(&data);
  
  return 0;
};


size_t header_get_mask_size(struct header *h, size_t mask)
{
  size_t count = 0;
  for (size_t i=0; i < h->nber_mask; i++)
    {
      if (mask & h->masks[i])
	{
	  count += h->masks_size[i];
	}
    }
  return count;
}
