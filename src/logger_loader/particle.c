#include "header.h"
#include "tools.h"
#include "particle.h"
#include "io.h"

#include <stdlib.h>
#include <stdio.h>



void particle_print(struct particle* p)
{
  printf("ID:            %lu\n", p->id);
  printf("Mass:          %g\n", p->mass);
  printf("Cutoff Radius: %g\n", p->h);
  printf("Position:      (%g, %g, %g)\n", p->pos[0], p->pos[1], p->pos[2]);
  printf("Velocity:      (%g, %g, %g)\n", p->vel[0], p->vel[1], p->vel[2]);
  printf("Acceleration:  (%g, %g, %g)\n", p->acc[0], p->acc[1], p->acc[2]);
  printf("Entropy:       %g\n", p->entropy);
  printf("Density:       %g\n", p->density);
}

int particle_check_data_type(struct header *h)
{
  return 1;
}

int particle_init(struct particle *part)
{
  for(size_t k = 0; k < DIM; k++)
    {
      part->pos[k] = 0;
      part->vel[k] = 0;
      part->acc[k] = 0;
    }

  part->entropy = -1;
  part->density = -1;
  part->h = -1;
  part->mass = -1;
  part->id = SIZE_MAX;
}

int particle_read_field(struct particle *part, struct header *h, FILE *f, size_t *offset, char *field, size_t size)
{
  void *p = NULL;

  if (strcmp("position", field) == 0)
    {
      p = &part->pos;
    }
  else if (strcmp("velocity", field) == 0)
    {
      p = &part->vel;
    }
  else if (strcmp("acceleration", field) == 0)
    {
      p = &part->acc;
    }
  else if (strcmp("entropy", field) == 0)
    {
      p = &part->entropy;
    }
  else if (strcmp("cutoff radius", field) == 0)
    {
      p = &part->h;
    }
  else if (strcmp("density", field) == 0)
    {
      p = &part->density;
    }
  else if (strcmp("consts", field) == 0)
    {
      p = malloc(size);
    }
  else
    {
      error(ENOTSUP, "Type %s not defined", field);
    }

  int error_code = io_read_data(f, size, p, offset);

  if (strcmp("consts", field) == 0)
    {
      part->mass = 0;
      part->id = 0;
      memcpy(p, &part->mass, sizeof(float));
      p += sizeof(float);
      memcpy(p, &part->id, sizeof(size_t));
      p -= sizeof(float);
      free(p);
    }
  return error_code;

}

int particle_read(struct particle* part, struct header* h, FILE *f, size_t *offset, int reader)
{
  int error_code = 0;
  size_t mask = 0;
  size_t offset_to_prev = 0;
  
  particle_init(part);

  size_t tmp = 0;
  fseek(f, *offset, SEEK_SET);
  fread(&tmp, 8, 1, f);
  offset_to_prev = tmp & 0xffffffffffffffULL;
  mask = tmp >> 56;

  *offset += 8;

  for(size_t i = 0; i < h->nber_mask; i++)
    {
      if (mask & h->masks[i])
	{
	  error_code = particle_read_field(part, h, f, offset, h->masks_name[i], h->masks_size[i]);
	  if (error_code != 0)
	    return error_code;
	}
      
    }

  /* end of const case */
  if (reader == reader_const)
    return 0;

  struct particle part_prev;

  /* no previous part */
  if (offset_to_prev == 0)
    return 0;

  /* previous part exists */
  error_code = particle_read(&part_prev, h, f, &offset_to_prev, reader_const);
  if (error_code != 0)
    return error_code;

  //error_code = particle_interpolate(&part_prev, part, NULL);
  return 0;
}


int particle_interpolate(struct particle *part_prev,
			 struct particle *part_curr,
			 struct particle *part_next,
			 double time)
{
  int order = 0;
  if (part_prev != NULL)
    order += 1;
  if (part_next != NULL)
    order += 1;

  if (order > 2)
    error(ENOTSUP, "order > 2 Not Implemented");

  if (part_next != NULL)
    error(ENOTSUP, "Next particle not implemented");

  printf("Need to read time and need to finish interpolation!");
  double dt = part_curr->time - part_prev->time;

  double tmp;
  float ftmp;

  for (size_t i = 0; i < DIM; i++)
    {
      tmp = (part_curr->pos[i] - part_prev->pos[i]) / dt;
      part_curr->pos[i] += tmp * (time - part_curr->time);

      ftmp = (part_curr->vel[i] - part_prev->vel[i]) / dt;
      part_curr->vel[i] += ftmp * (time - part_curr->time);

      ftmp = (part_curr->acc[i] - part_prev->acc[i]) / dt;
      part_curr->acc[i] += ftmp * (time - part_curr->time);
    }

  ftmp = (part_curr->entropy - part_prev->entropy) / dt;
  part_curr->entropy += ftmp * (time - part_curr->time);  

  
  return 0;
}
