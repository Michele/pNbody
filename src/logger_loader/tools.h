#ifndef __TOOLS_H__
#define __TOOLS_H__


#include <stdlib.h>
#include <stdio.h>
#include <Python.h>

#define STRING_SIZE 200

#include "header.h"

/* Set the error message for python (still need to return NULL) */
#define error(err, s, ...)					\
  ({								\
    error_no_return(err, s, ##__VA_ARGS__);			\
    return err;							\
  })

#define error_no_return(err, s, ...)					\
  ({									\
    char error_msg[STRING_SIZE];					\
    sprintf(error_msg, "%s:%s():%i: " s  ": %s", __FILE__,		\
	    __FUNCTION__, __LINE__, ##__VA_ARGS__, strerror(err));	\
    PyErr_SetString(PyExc_RuntimeError, error_msg);			\
  })

int tools_get_next_chunk(struct header *h, FILE *f, size_t *offset);

  
#endif //__TOOLS_H__
