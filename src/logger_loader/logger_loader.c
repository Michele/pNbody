#include "header.h"
#include "particle.h"
#include "timeline.h"

#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

/**
 * @brief load data from the offset without any interpolation
 *
 * @param offset PyArrayObject list of offset for each particle
 * @param filename string filename of the dump file
 * @return dictionnary containing the data read
 */
static PyObject *loadFromIndex(PyObject *self, PyObject *args)
{

  struct header h;

  /* input */
  PyArrayObject *offset = NULL;
  char *filename = NULL;
  
  /* output */
  PyArrayObject *pos  = NULL;
  PyArrayObject *vel  = NULL;
  PyArrayObject *acc = NULL;
  PyArrayObject *entropy = NULL;
  PyArrayObject *h_sph = NULL;
  PyArrayObject *rho = NULL;
  PyArrayObject *mass = NULL;
  PyArrayObject *id = NULL;
 
  /* parse arguments */
  
  if (!PyArg_ParseTuple(args, "Os", &offset, &filename))
    return NULL;

  if (!PyArray_Check(offset))
    {
      error_no_return(ENOTSUP, "Offset is not a numpy array");
      return NULL;
    }
  if (PyArray_NDIM(offset) != 1)
    {
      error_no_return(ENOTSUP, "Offset is not a 1 dimensional array");
      return NULL;
    }
  if (PyArray_TYPE(offset) != NPY_UINT64)
    {
      error_no_return(ENOTSUP, "Offset does not contain unsigned int");
      return NULL;
    }

  FILE *f;
  f = fopen(filename, "rb");

  if (f==NULL)
    {
      error_no_return(errno, "Unable to open file %s", filename);
      return NULL;
    }

  if (header_read(&h, f) != 0)
    return NULL;

  struct time_array times;

  if (time_array_init(&times, &h, f) != 0)
    return NULL;
  
  header_print(&h);
  exit(1);

  /* init array */
  npy_intp dim[2];
  dim[0] = offset->dimensions[0];
  dim[1] = DIM;

  /* init output */
  if (header_is_present(&h, "position"))
    {
      pos = (PyArrayObject*) PyArray_SimpleNew(2, dim, NPY_DOUBLE);
    }

  if (header_is_present(&h, "velocity"))
    {
      vel = (PyArrayObject*) PyArray_SimpleNew(2, dim, NPY_FLOAT);
    }

  if (header_is_present(&h, "acceleration"))
    {
      acc = (PyArrayObject*) PyArray_SimpleNew(2, dim, NPY_FLOAT);
    }
   
  if (header_is_present(&h, "entropy"))
    {
      entropy = (PyArrayObject*) PyArray_SimpleNew(1, offset->dimensions, NPY_FLOAT);
    }

  if (header_is_present(&h, "h_sph"))
    {
      h_sph = (PyArrayObject*) PyArray_SimpleNew(1, offset->dimensions, NPY_FLOAT);
    }

  if (header_is_present(&h, "density"))
    {
      rho = (PyArrayObject*) PyArray_SimpleNew(1, offset->dimensions, NPY_FLOAT);
    }

  if (header_is_present(&h, "mass"))
    {
      mass = (PyArrayObject*) PyArray_SimpleNew(1, offset->dimensions, NPY_FLOAT);
      id = (PyArrayObject*) PyArray_SimpleNew(1, offset->dimensions, NPY_ULONG);
    }


  int error_code = 0;

  /* check data type in particles */
  if (!particle_check_data_type(&h))
    {
      error_no_return(ENOTSUP, "Particle data type are not compatible");
      return NULL;
    }

  /* loop over all particles */
  for (size_t i = 0; i < offset->dimensions[0]; i++) {
    struct particle part;

    size_t *offset_particle = (size_t*) PyArray_GETPTR1(offset, i);
    
    error_code = particle_read(&part, &h, f, offset_particle, reader_const);
    if (error_code != 0)
      return NULL;
    
    double *dtmp;
    float *ftmp;
    size_t *stmp;

    /* copy data */
    for (size_t k = 0; k < DIM; k++)
      {
	if (pos)
	  {
	    dtmp = PyArray_GETPTR2(pos, i, k);
	    *dtmp = part.pos[k];
	  }

	if (vel)
	  {
	    ftmp = PyArray_GETPTR2(vel, i, k);
	    *ftmp = part.vel[k];
	  }

	if (acc)
	  {
	    ftmp = PyArray_GETPTR2(acc, i, k);
	    *ftmp = part.acc[k];
	  }
      }

    if (entropy)
      {
	ftmp = PyArray_GETPTR1(entropy, i);
	*ftmp = part.entropy;
      }

    if (rho)
      {
	ftmp = PyArray_GETPTR1(rho, i);
	*ftmp = part.density;
      }

    
    if (h_sph)
      {
	ftmp = PyArray_GETPTR1(h_sph, i);
	*ftmp = part.h;
      }

    if (mass)
      {
	ftmp = PyArray_GETPTR1(mass, i);
	*ftmp = part.mass;
      }

    if (id)
      {
	stmp = PyArray_GETPTR1(id, i);
	*stmp = part.id;
      }

  }

  header_free(&h);

  PyObject *dict = PyDict_New();
  PyObject *key = PyString_FromString("position");
  PyDict_SetItem(dict, key, PyArray_Return(pos));
    
  return dict;

}

            
/* definition of the method table */      
      
static PyMethodDef loggerMethods[] = {
  {"loadFromIndex",  loadFromIndex, METH_VARARGS,
   "Load snapshot directly from the offset in an index file."},	   	       
  
  {NULL, NULL, 0, NULL}        /* Sentinel */
};


void initlogger_loader(void)
{    
  (void) Py_InitModule("logger_loader", loggerMethods);	
	  
  import_array();
}
      
