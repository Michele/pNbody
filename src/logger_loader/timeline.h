#ifndef __TIMELINE_H__
#define __TIMELINE_H__

#include "header.h"
#include "tools.h"

typedef char timebin_t;

struct time_array {
  void *next;
  void *prev;
  double time;
  size_t offset;
};

double time_convert_timestep(timebin_t bin, double timeBase);

int time_read(double *time, struct header *h, FILE *f, size_t *offset);

int time_array_init(struct time_array* t, struct header *h, FILE *f);

void time_array_free(struct time_array* t);

void time_array_print(struct time_array *t);

int time_array_count(struct time_array *t);

#endif // __TIMELINE_H__
