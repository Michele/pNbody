#ifndef __PARTICLE_H__
#define __PARTICLE_H__

#include "header.h"
#include "tools.h"

#include <stdlib.h>
#include <stdio.h>


#define DIM 3

struct particle {
  /* position */
  double pos[DIM];

  /* velocity */
  float vel[DIM];

  /* acceleration */
  float acc[DIM];

  /* entropy */
  float entropy;

  /* cutoff radius */
  float h;

  /* density */
  float density;

  /* mass */
  float mass;

  /* id */
  size_t id;

  /* time */
  double time;
};

enum reader_type {
  reader_const,
  reader_lin,
};


/**
 * @brief print a particle
 *
 */
void particle_print(struct particle* p);

/**
 * @brief read a particle in the dump file
 *
 * @param h #header structure of the file
 * @param f file where to read
 * @param offset offset of the chunk to read (update it to the end of the chunk)
 * @param reader #reader_type
 */
int particle_read(struct particle* part, struct header* h, FILE *f, size_t *offset,
		  int reader);


int particle_check_data_type(struct header *h);

int particle_init(struct particle *part);

int particle_read_field(struct particle *part, struct header *h, FILE *f, size_t *offset, char *field, size_t size);

int particle_interpolate(struct particle *part_prev,
			   struct particle *part_curr,
			   struct particle *part_next,
			   double time);

  
#endif //__PARTICLE_H__
