#ifndef __LOGGER_HEADER_H__
#define __LOGGER_HEADER_H__

#include "tools.h"

#include <Python.h>
#include <stdlib.h>
#include <stdio.h>

#define LOGGER_VERSION_SIZE 20
#define LOGGER_NAME_SIZE 2
#define LOGGER_MASK_SIZE 1
#define LOGGER_NBER_SIZE 1
#define LOGGER_OFFSET_SIZE 1
#define LOGGER_DATATYPE_SIZE 1

struct header {
  /* Logger version */
  char version[STRING_SIZE];

  /* number of bytes for an offset */
  size_t offset;

  /* number of bytes for a name */
  size_t name;

  /* offset of the first header */
  size_t offset_first;

  /* number of bytes for a mask */
  size_t mask;

  /* number of bytes for a number */
  size_t number;

  /* number of masks */
  size_t nber_mask;

  /* list of masks */
  size_t *masks;

  /* list of mask name */
  char **masks_name;

  /* size of data in a mask */
  size_t *masks_size;

  /* TimeBase */
  double time_base;
};


/**
 * @brief print a header struct
 */
void header_print(struct header *h);

/**
 * @brief free allocated memory
 */
void header_free(struct header *h);

/**
 * @brief check if field is present in header
 *
 * @param field name of the requested field
 * @param ind (return value) indice of the requested field
 */
int header_is_present_and_get_index(struct header *h, char *field, size_t *ind);

/**
 * @brief check if field is present in header
 * 
 * @param field name of the requested field
 */
int header_is_present(struct header *h, char *field);


/**
 * @brief read the logger header
 *
 * @param f file to read
 */
int header_read(struct header *h, FILE *f);

size_t header_get_mask_size(struct header *h, size_t mask);

#endif // __LOGGER_HEADER_H__
