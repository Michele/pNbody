#include "timeline.h"

double time_convert_timestep(timebin_t bin, double timeBase) {
  if (bin <= 0)
    return 0;

  long long tmp = 1LL << (bin + 1);
  return tmp * timeBase;
}

int time_read(double *time, struct header *h, FILE *f, size_t *offset)
{
  int error_code = 0;
  size_t mask = 0;
  size_t ind = 0;

  timebin_t tmp = 0;

  io_read_data(f, h->mask, &mask, offset);

  if (!header_is_present_and_get_index(h, "timestamp", &ind))
    error(EIO, "Not a timestamp");

  error_code = io_read_data(f, h->masks_size[ind], &tmp, offset);

  if (error_code != 0)
    return error_code;

  *time = time_convert_timestep(tmp, h->time_base);

  return 0;
}


int time_array_init(struct time_array* t, struct header *h, FILE *f)
{
  struct time_array *init = t;
  t->next = 0;
  
  size_t offset = h->offset_first;
  size_t old_offset = offset;
  
  double time = 0;
  int test;

  /* read initial time */
  test = time_read(&time, h, f, &offset);
  if (test != 0)
    return test;
  t->time = time;
  t->prev = NULL;
  t->next = NULL;
  t->offset = old_offset;

  while (1)
    {
      /* find next chunk */
      test = tools_get_next_chunk(h, f, &old_offset);
      /* deal with error/special case */
      if (test != 0)
	{
	  /* end of file */
	  if (test == EOF)
	    break;
	  else
	    return test;
	}

      offset = old_offset;
      /* read time */
      test = time_read(&time, h, f, &offset);
      if (test != 0)
	return test;

      /* update time_array */
      struct time_array *tmp = malloc(sizeof(struct time_array));
      /* deal with pointers */
      t->next = tmp;
      tmp->prev = t;
      /* update current pointer */
      t = tmp;
      /* set data */
      t->time = time;
      t->offset = old_offset;
      t->next = NULL;
      
      time_array_print(init);
    }



  return 0;
}


void time_array_free(struct time_array* t)
{
  struct time_array *tmp;
  while (t->next)
    {
      tmp = t->next;
      free(t);
      t = tmp;
    }

}


void time_array_print(struct time_array *t)
{
  printf("Times: [%g", t->time);

  while (t->next)
    {
      t = t->next;
      printf(", %g", t->time);
    }

  printf("]\n");
}

int time_array_count(struct time_array *t)
{
  int count = 1;
  while (t->next)
    {
      t = t->next;
      count += 1;
    }

  return count;
}
