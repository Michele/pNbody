#include "tools.h"
#include "header.h"

  
int tools_get_next_chunk(struct header *h, FILE *f, size_t *offset)
{
  size_t init_offset = *offset;
  size_t prev_offset = 0;
  size_t mask;
  int test = 0;
  
  while (init_offset != prev_offset)
    {
      test = io_read_mask(h, f, offset, &mask, &prev_offset);
      if (test != 0)
	return test;

      *offset += header_get_mask_size(h, mask);
    }

  return 0;
}
