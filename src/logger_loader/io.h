#ifndef __SWIFT_LOGGER_IO_H__
#define __SWIFT_LOGGER_IO_H__

#include "header.h"
#include "tools.h"

#include <stdlib.h>
#include <stdio.h>

enum io_data_type {
  io_data_int,
  io_data_float,
  io_data_double,
  io_data_char,
  io_data_count, /* should always be the last! */
};

enum io_state {
  io_state_empty,
  io_state_full
};


struct io_general_data {
  /* name of the object */
  char *name;

  /* data type */
  unsigned int type;

  /* pointer to value */
  void *p;

  /* state */
  int state;
};

/* data size of each type */
extern const size_t io_data_size[];


/**
 * @brief read a single value in a file
 *
 * @param f file to read
 * @param size size of the chunk to read
 * @param p pointer where to store the data
 * @param offset offset where to read file
 */
int io_read_data(FILE *f, size_t size, void* p, size_t *offset);

int io_general_data_read(struct io_general_data *data, FILE *f, struct header *h, size_t *offset);

void io_general_data_init(struct io_general_data *data);

void io_general_data_clean(struct io_general_data *data);

int io_read_mask(struct header *h, FILE *f, size_t *offset, size_t *mask, size_t *prev_offset);

#endif // __SWIFT_LOGGER_IO_H__
