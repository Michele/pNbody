'''
 @package   pNbody
 @file      test_acc.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from numarray import *
import SM
import treelib

from pNbody import *

import time



eps = 0.1

# open a model
nb = Nbody('disk.dat',ftype='gadget')
#nb = nb.selectc(nb.rxyz()<2)

# create a tree object
MyTree = nb.getTree()





R = arange(0,200,1.0)
print len(R)
pos = concatenate((R,zeros(len(R)),zeros(len(R))))
pos.shape = (3,len(R))
pos = transpose(pos)
pos = pos.astype(Float32)



acc = MyTree.Acceleration(pos,eps)
acc = acc.astype(Float32)



R = R.astype(Float32)

g = SM.plot()
g.limits(R,acc[:,0])
g.box()
g.connect(R,acc[:,0])
g.show()

