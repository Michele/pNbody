'''
 @package   pNbody
 @file      test02.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from pNbody import *
import time
import treelib

import Ptools as pt

ErrTolTheta = 0.8
eps = 0.1

params = {}
params['ErrTolTheta'] = ErrTolTheta

# open a model
nb = Nbody("../../examples/snap.dat",ftype='gadget')
#nb = nb.selectc(nb.rxyz()<2)

# create a tree object
nb.Tree = treelib.Tree(npart=array(nb.npart),pos=nb.pos,vel=nb.vel,mass=nb.mass,num=nb.num,tpe=nb.tpe,params=params)   
# make the tree
nb.Tree.BuildTree()








R = arange(-20,20,0.1)
print len(R)
pos = concatenate((R,zeros(len(R)),zeros(len(R))))
pos.shape = (3,len(R))
pos = transpose(pos)
pos = pos.astype(float32)



acc = nb.Tree.Acceleration(pos,eps)


pt.plot(R,acc[:,0])
pt.show()
