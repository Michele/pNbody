'''
 @package   pNbody
 @file      newsetup.py
 @brief     Setup
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from setuptools import setup, find_packages, Extension
import glob
import numpy


INCDIRS=['.']

##############################################################
# variables for numpy/numarray
##############################################################
INCDIRS.append(numpy.get_include())
INCDIRS.append(numpy.get_numarray_include())

NUMPY_INC = numpy.get_include()


##############################################################
# variables for ptree
##############################################################
MPICH_DIR = "/usr/local/lib/"
MPICH_DIR = "/home/revaz/local/mpich2-1.0.6shm/lib/"
MPICH_LIB = "mpich"
MPICH_INC = "/home/revaz/local/mpich2-1.0.6shm/include/"
##############################################################


setup(

    name	    = "pNbody",
    version	    = "4.0.0",
    author	    = "Revaz Yves",
    author_email    = "yves.revaz@epfl.ch",
    url 	    = "http://obswww.unige.ch/~revaz/pNbody",
    description     = "python pyrallel Nbody toolbox",

    packages = find_packages(),
   
    ext_modules     = [Extension("pNbody.nbodymodule",  		                
    				 ["src/nbodymodule/nbodymodule.c"],			
    				 include_dirs=INCDIRS), 				
    		       Extension("pNbody.myNumeric", 					
    				 ["src/myNumeric/myNumeric.c"], 			
    				 include_dirs=INCDIRS), 				
    		       Extension("pNbody.mapping", 					
    				 ["src/mapping/mapping.c"],				
    				 include_dirs=INCDIRS), 	  			
    		       Extension("pNbody.coolinglib", 					
    				 ["src/coolinglib/coolinglib.c"],			
    				 include_dirs=INCDIRS), 		    		 		      
    		       Extension("pNbody.cosmolib", 					
    				 ["src/cosmolib/cosmolib.c"],				
    				 include_dirs=INCDIRS),     				
    		       Extension("pNbody.iclib", 					
    				 ["src/iclib/iclib.c"], 				
    				 include_dirs=INCDIRS),  				
    		       Extension("pNbody.asciilib", 					
    				 ["src/asciilib/asciilib.c"],				
    				 include_dirs=INCDIRS), 		    		 									 
    		       Extension("pNbody.nbdrklib", 					
    				 ["src/nbdrklib/nbdrklib.c"],				
    				 include_dirs=INCDIRS),  				
    				    							
    		       Extension("pNbody.treelib", 					
    				 ["src/treelib/treelib.c"],				
    				 include_dirs=INCDIRS), 				

#   		       Extension("pNbody.ptreelib", 					
#   				 ["src/ptreelib/domain.c",				
#   				  "src/ptreelib/endrun.c",				
#   				  "src/ptreelib/forcetree.c",				
#   				  "src/ptreelib/ngb.c", 				
#   				  "src/ptreelib/peano.c",				
#   				  "src/ptreelib/ptreelib.c",				
#   				  "src/ptreelib/potential.c",				
#   				  "src/ptreelib/gravtree.c",				
#   				  "src/ptreelib/density.c",				
#   				  "src/ptreelib/sph.c", 				
#   				  "src/ptreelib/io.c",					
#   				  "src/ptreelib/system.c"],		     		
#   				  include_dirs=["src/ptreelib/",MPICH_INC,NUMPY_INC],		
#   				  library_dirs=[MPICH_DIR],				
#   				  libraries=[MPICH_LIB]),
				  		    		 			 
    		       Extension("pNbody.peanolib", 					
    				 ["src/peanolib/peanolib.c"],				
    				 include_dirs=INCDIRS)  		    		 		      
    		       ],								
    		        								
				          
    data_files=[('config',glob.glob('./config/*parameters')),
    		('config/rgb_tables',glob.glob('./config/rgb_tables/*')),
    		('config/formats',glob.glob('./config/formats/*')),
    		('plugins',glob.glob('./config/plugins/*')),
    		('fonts',glob.glob('./fonts/*')),
    		('tests',glob.glob('./tests/*')),
    		('examples',glob.glob('./examples/*.dat')),
    		('examples',glob.glob('./examples/*.py')),			
    		('examples/scripts',glob.glob('./examples/scripts/*')),
    		('examples/films',glob.glob('./examples/films/*')),
    		('doc',glob.glob('./doc/man.ps'))
    ],	
   
   
    
    scripts = glob.glob('scripts/*'),   
   
   
    #install_requires = ['numpy>=1.0.3'],
    #install_requires = ['matplotlib>=0.91.2'],
    dependency_links = [
        "http://obswww.unige.ch/~revaz/PyPI/"
    ],
    

    
)
