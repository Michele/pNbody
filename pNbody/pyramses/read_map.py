#!/usr/bin/env python

'''
 @package   pNbody
 @file      read_map.py
 @brief     Read a map
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import pyramses
import sys
from numpy import *


data = pyramses.read_map(sys.argv[1],verbose=True)

print data
print min(ravel(data)),max(ravel(data))

from pNbody import libutil
libutil.mplot(transpose(data))
