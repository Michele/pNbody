#!/usr/bin/env python

'''
 @package   pNbody
 @file      plot1d.py
 @brief     Do a plot
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt
import string
from numpy import *


class Rdata:
  def __init__(self,lines=None):
    if lines!=None:
      self.read(lines)
  
  
  def read(self,lines):
    
    ncells=int(string.split(lines[0])[1])
    self.ncells = ncells
    
    self.l = zeros(ncells)
    self.x = zeros(ncells)
    self.d = zeros(ncells)
    self.u = zeros(ncells)
    self.P = zeros(ncells)
    
    for i,line in enumerate(lines[3:ncells+3]):
      data = string.split(line)
      self.l[i] = int(data[0])
      self.x[i] = float(data[1])
      self.d[i] = float(data[2])
      self.u [i]= float(data[3])    
      self.P[i] = float(data[4])
    


def read_ramses(file):

  f=open(file)
  lines=f.readlines()
  f.close()
  
  idxs = []
  
  for i,line in enumerate(lines):
    if line.find('Output')==1:
      idxs.append(i)
  
  datas = []
  for idx in idxs:
    data = Rdata(lines[idx:])
    datas.append(data)
    
  
  return datas  
  
  

file = "tube1d.log"



datas = read_ramses(file)
print len(datas)

data = datas[1]
pt.plot(data.x,data.d)
pt.show()


