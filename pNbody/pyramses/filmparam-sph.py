'''
 @package   pNbody
 @file      filmparam-sph.py
 @brief     Movie parameter for Smooth Particles Hydrodynamics
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

# nombre de sous film horiz et vertical 			     
nh = 1  		# number of horizontal frame								     
nw = 1			# number of vertical frame	
# size of subfilms						     
width = 512							     
height = width					     
# size of the film						     
numByte = width * nw						     
numLine = height * nh						     						     
# init parameters						     
param = initparams(nh,nw)	 

# 1 vue totale
param[1]['ftype'] = 'gadget'
param[1]['size'] = (1.5,1.5)
param[1]['obs'] = None
param[1]['xp'] = None
param[1]['x0'] = None
param[1]['view'] = 'xy'
param[1]['frsp'] = 100
param[1]['rendering'] = 'map'
param[1]['mode'] = 'm'
param[1]['cd'] = 2.022e-05
param[1]['mn'] = 0
param[1]['mx'] = 4.574e-04
param[1]['exec'] = """nb.translate([-1.5,-1.5,0])"""
param[1]['tdir'] = None
param[1]['time'] = "nb.atime"
