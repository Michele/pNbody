'''
 @package   pNbody
 @file      plot.py
 @brief     do a plot
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import Ptools as pt

x = [ 1.00097656 , 1.00488281 , 1.00097656 , 1.00292969 , 1.00292969 , 1.00097656 ,1.00488281 , 1.00292969]

y = [ 1.00097656,  1.00097656 , 1.00488281 , 1.00097656,  1.00488281 , 1.00292969,  1.00292969 , 1.00292969]

pt.scatter(x,y)


pt.show()



