'''
 @package   pNbody
 @file      filmparam.py
 @brief     Movie parameters
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

# nombre de sous film horiz et vertical 			     
nh = 1  		# number of horizontal frame								     
nw = 1			# number of vertical frame	
# size of subfilms						     
width = 512							     
height = width					     
# size of the film						     
numByte = width * nw						     
numLine = height * nh						     						     
# init parameters						     
param = initparams(nh,nw)	 

# 1 vue totale
param[1]['ftype'] = 'gadget'
param[1]['size'] = (1.5,1.5)
param[1]['obs'] = None
param[1]['xp'] = None
param[1]['x0'] = None
param[1]['view'] = 'xy'
param[1]['frsp'] = 171
param[1]['rendering'] = 'mapcubic'
param[1]['mode'] = 'm'
param[1]['cd'] = 2.089e-05
param[1]['mn'] = 0
param[1]['mx'] = 4.358e-04
param[1]['exec'] = """nb.translate([-1.5,-1.5,0])"""
param[1]['tdir'] = None
param[1]['time'] = "nb.atime"
