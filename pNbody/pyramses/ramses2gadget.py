#!/usr/bin/env python

'''
 @package   pNbody
 @file      ramses2gadget.py
 @brief     Convert RAMSES to gadget format
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import pyramses
import sys

from pNbody import *

import argparse
import glob

description=None
epilog=None

parser = argparse.ArgumentParser(description=description,epilog=epilog)



parser.add_argument("-o", 
                  "--outputdirectory",
                  action="store", 
		  dest="outputdirectory", 
                  help="name of the output  directory",
		  metavar="FILE")

parser.add_argument('files', metavar='FILE', type=str,
                   nargs='+',
                   help='name of the file to convert')


#####################################
# main
#####################################


opt = parser.parse_args()
directories = opt.files



for i,directory in enumerate(directories):

  amr_file   = glob.glob(os.path.join(directory,"amr_*"))[0]
  hydro_file = glob.glob(os.path.join(directory,"hydro_*"))[0]
  
  ofile = os.path.join(opt.outputdirectory,"snapshot_%04d"%i)


  pos,rsp = pyramses.read_amr(amr_file,verbose=False)
  rho	  = pyramses.read_hydro(hydro_file,verbose=False)


  n = len(pos)
  if pos.shape[1]==2:
    
    newpos = zeros([n,3])
    
    newpos[:,0] = pos[:,0]
    newpos[:,1] = pos[:,1]


  pos = newpos

  nb = Nbody(status='new',p_name=ofile,pos=pos,ftype='gadget')
  nb.u=zeros(n)
  nb.rho=rho
  nb.rsp = rsp

  print len(nb.rho)
  print len(nb.pos)
  print len(nb.rsp)

  nb.mass = nb.rsp**2 *nb.rho

  nb.write()
