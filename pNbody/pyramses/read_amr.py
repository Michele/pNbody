#!/usr/bin/env python

'''
 @package   pNbody
 @file      read_amr.py
 @brief     Read RAMSES mesh
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import pyramses
import sys


pyramses.read_amr(sys.argv[1],verbose=False)
