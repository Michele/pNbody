'''
 @package   pNbody
 @file      pyramses.py
 @brief     RAMSES Interface
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import sys,types,os

SELECTED_LEVEL=0		
SELECTED_IBOUND=0

#################################
def readblock(f,data_type,shape=None,byteorder=sys.byteorder):
#################################
  '''
  data_type = int,float32,float
  or
  data_type = array
  
  shape	    = tuple
  '''  
  
  sh=8
  sht=int
  
  # compute the number of bytes that should be read
  nbytes_to_read=None
  if shape!=None:
    shape_a = array(shape)
    nelts_to_read = shape_a[0]
    for n in shape_a[1:]:
      nelts_to_read = nelts_to_read*n
    nbytes_to_read = nelts_to_read*dtype(data_type).itemsize
  
  try:
    nb1 = fromstring(f.read(sh),sht)
    if sys.byteorder != byteorder:
      nb1.byteswap(True)
    nb1 = nb1[0]  
   
    nbytes=nb1
   
    # check
    if nbytes_to_read:
      if nbytes_to_read!=nbytes:
        print "inconsistent block header, using nbytes=%d instead"%nbytes_to_read
        nbytes=nbytes_to_read
      
      
    
    
  except IndexError:
    raise "ReadBlockError" 
      
  if type(data_type) == types.TupleType:
       
    data = []
    for tpe in data_type:
        
      if type(tpe) == int:
        val =  f.read(tpe)
      else:
        bytes = dtype(tpe).itemsize
	val = fromstring(f.read(bytes),tpe)
        if sys.byteorder != byteorder:
          val.byteswap(True)  
	 
	val = val[0]  
      
      data.append(val)
        
  else:
    data  = fromstring(f.read(nbytes),data_type)   
    if sys.byteorder != byteorder:
      data.byteswap(True)  
      
  nb2 = fromstring(f.read(sh),sht)
  if sys.byteorder != byteorder:
    nb2.byteswap(True)
  nb2 = nb2[0]  

  if nb1 != nb2:
    #raise "ReadBlockError","nb1=%d nb2=%d"%(nb1,nb2)   
    print "ReadBlockError : nb1=%d nb2=%d"%(nb1,nb2)   
    sys.exit()
  
  # reshape if needed
  if shape != None:
    data.shape=shape

    
  return data




def read_int32(f,):
  
  
  nb1 = fromstring(f.read(8),int)
  if sys.byteorder != byteorder:
    nb1.byteswap(True)
  nb1 = nb1[0]
  print nb1
  
  nbytes=nb1
  
  data  = fromstring(f.read(nbytes),int32)  
  print data
  
  
  nb2 = fromstring(f.read(8),int)
  if sys.byteorder != byteorder:
    nb1.byteswap(True)  
  nb2 = nb2[0]
  print nb2  



def read_map(file,verbose=True):
  """
  read a ramses map file
  
  amr2map.f90
  """  
  
  f = open(file,'r')

  nx,ny          = readblock(f,(int32,int32))
  data           = readblock(f,float32,(nx,ny))
  
  xxmin,xxmax    = readblock(f,(float,float))
  yymin,yymax    = readblock(f,(float,float))

  if verbose:
   print "nx,ny          =",nx,ny 	
   print "xxmin,xxmax    =",xxmin,xxmax
   print "yymin,yymax    =",yymin,yymax	

  rsize = f.tell()
  isize = os.path.getsize(file)
  if isize!=rsize:
    print "isize (%d) != rsize (%d)"%(isize,rsize)
    sys.exit()	  
  
  return data   
    
    
def read_info(file,verbose=True):
  """
  read a ramses info file
  
  """  

  # need
  #
  # levelmin
  # t
  # ordering
  # i,bound_key(impi-1),bound_key(impi)

  pass

def read_amr(file,verbose=True):
  """
  read a ramses amr file
  
  amr/output_amr.f90  -> backup_amr
  """  
  
  
  f = open(file,'r')

  ncpu          = readblock(f,(int32))[0]
  ndim          = readblock(f,(int32))[0]
  nx,ny,nz      = readblock(f,(int32,int32,int32))  
  nlevelmax     = readblock(f,(int32))[0]
  ngridmax      = readblock(f,(int32))[0]
  nboundary     = readblock(f,(int32))[0]
  ngrid_current = readblock(f,(int32))[0]
  boxlen        = readblock(f,(float))[0]
  
  twondim=2*ndim		# number of neighboring
  twotondim=2**ndim		# number of sons
  
  
  # time variables
  noutput,iout,ifout                                       = readblock(f,(int32,int32,int32))  
  tout                                                     = readblock(f,float,(noutput,))
  aout                                                     = readblock(f,float,(noutput,))
  t                                                        = readblock(f,(float))[0]
  dtold                                                    = readblock(f,float,(nlevelmax,))					  
  dtnew                                                    = readblock(f,float,(nlevelmax,))					  
  nstep,nstep_coarse                                       = readblock(f,(int32,int32))
  const,mass_tot_0,rho_tot                                 = readblock(f,(float,float,float)) 
   
  omega_m,omega_l,omega_k,omega_b,h0,aexp_ini,boxlen_ini   = readblock(f,(float,float,float,float,float,float,float)) 
  aexp,hexp,aexp_old,epot_tot_int,epot_tot_old             = readblock(f,(float,float,float,float,float)) 
  mass_sph                                                 = readblock(f,(float))[0] 
  
  
  # levels variables
  headl                                                    = readblock(f,int32,(nlevelmax,ncpu))                                           	
  taill                                                    = readblock(f,int32,(nlevelmax,ncpu))                                           	
  numbl                                                    = readblock(f,int32,(nlevelmax,ncpu))                
  numbtot                                                  = readblock(f,int32,(nlevelmax,10))     
  
  simple_boundary = True
  if (simple_boundary):
    headb    						   = readblock(f,int32,(nlevelmax,nboundary))
    tailb    						   = readblock(f,int32,(nlevelmax,nboundary))
    numbb    						   = readblock(f,int32,(nlevelmax,nboundary))
  
  
  # free memory
  headf,tailf,numbf,used_mem,used_mem_tot                  = readblock(f,(int32,int32,int32,int32,int32)) 

  # cpu boundaries
  ordering                                                 = readblock(f,(128,))[0] 
  
  if ordering=="bisection":
    print "not tested !!!"
    bisec_wall                               		   = readblock(f,float,(nbinodes,))
    bisec_next                               		   = readblock(f,float,(2,nbinodes))
    bisec_indx                               		   = readblock(f,float,(nbinodes,))
    bisec_cpubox_min                         		   = readblock(f,float,(ndim,ncpu))
    bisec_cpubox_max                         		   = readblock(f,float,(ndim,ncpu))   
  else:
    # !!! bound_key(0:ndomain)
    # !!! ndomain is notdefined at this stage
    bound_key                                              = readblock(f,float)
    ndomain = len(bound_key)-1
  
  # coarse level 
  # !!! ncoarse is notdefined at this stage
  son                                                      = readblock(f,int32)   
  flag1                                                    = readblock(f,int32) 
  cpu_map                                                  = readblock(f,int32) 
  ncoarse = len(son)
  
  # fin level 
  
  pos  = zeros((0,ndim))
  rsp  = zeros((0,))
  
  for ilevel in xrange(nlevelmax):
    for ibound in xrange(nboundary+ncpu):
        	
      if ibound<ncpu:
      	ncache = numbl[ilevel,ibound]
        istart = headl[ilevel,ibound]
      else:
      	ncache = numbb[ilevel,ibound-ncpu]
        istart = headb[ilevel,ibound-ncpu]        

        
      if ncache>0:
      
        # grid index
      	ind_grid					     = readblock(f,int32,(ncache,))	       
        
	# next index
      	iig						     = readblock(f,int32,(ncache,))   
	# previous index 
      	iig						     = readblock(f,int32,(ncache,))    
      
        # grid center !!!
	posgc = zeros((ncache,ndim),float)
      	for idim in xrange(ndim):
      	  posgc[:,idim] = readblock(f,float,(ncache,)) 
	
	#if ilevel==SELECTED_LEVEL:  
	#  if ibound==SELECTED_IBOUND:
	pos  = concatenate((pos,posgc))   
	rsp  = concatenate((rsp, ones(ncache) * 1./2**(ilevel+1) ))
            
        # father index
      	iig						     = readblock(f,int32,(ncache,))    
      
        # nbor index
      	for ind in xrange(1,twondim+1):
      	  iig = readblock(f,int32,(ncache,))  

        # son index
      	for ind in xrange(1,twotondim+1):
      	  iig = readblock(f,int32,(ncache,))  

        # cpu map
      	for ind in xrange(1,twotondim+1):
      	  iig = readblock(f,int32,(ncache,))	 
  
        # ref map
      	for ind in xrange(1,twotondim+1):
      	  iig = readblock(f,int32,(ncache,))	    





  rsize = f.tell()
  isize = os.path.getsize(file)
  if isize!=rsize:
    print "isize (%d) != rsize (%d)"%(isize,rsize)
    sys.exit()	  

  
  if verbose:
   print "ncpu          =",ncpu 	
   print "ndim          =",ndim 	
   print "nx,ny,nz      =",nx,ny,nz	
   print "nlevelmax     =",nlevelmax	
   print "ngridmax      =",ngridmax	
   print "nboundary     =",nboundary	
   print "ngrid_current =",ngrid_current
   print "boxlen        =",boxlen	 
  
   print "noutput     =",noutput	
   print "iout        =",iout
   print "ifout       =",ifout 
   print "tout        =",tout
   print "aout        =",aout 
   print "dtold       =",dtold
   print "dtnew       =",dtnew
   print "nstep       =",nstep
   print "nstep_coarse=",nstep_coarse
   print "const       =",const
   print "mass_tot_0  =",mass_tot_0
   print "rho_tot     =",rho_tot

   print "omega_m     =",omega_m     
   print "omega_l     =",omega_l     
   print "omega_k     =",omega_k     
   print "omega_b     =",omega_b     
   print "h0          =",h0
   print "aexp_ini    =",aexp_ini
   print "boxlen_ini  =",boxlen_ini  
   print "aexp        =",aexp
   print "hexp        =",   hexp
   print "aexp_old    =",aexp_old    
   print "epot_tot_int=",epot_tot_int
   print "epot_tot_old=",epot_tot_old	    
   print "mass_sph    =",mass_sph	    

   print "headl	      =",headl
   print "taill	      =",taill
   print "numbl	      =",numbl
   print "numbtot     =",numbtot

   if (simple_boundary):
     print "headb       =",headb
     print "tailb       =",tailb
     print "numbb       =",numbb

   print "headf       =",headf       
   print "tailf       =",tailf       
   print "numbf       =",numbf       
   print "used_mem    =",used_mem    
   print "used_mem_tot=",used_mem_tot 

   print "ordering    =",ordering 

   if ordering=="bisection":
     print "bisec_wall    =",	    bisec_wall    
     print "bisec_next    =",	    bisec_next    
     print "bisec_indx    =",	    bisec_indx    
     print "bisec_cpubox_min	=", bisec_cpubox_min 
     print "bisec_cpubox_max	=", bisec_cpubox_max 
   else:
     print "bound_key     =",bound_key


   print "son        	=",son
   print "flag1      	=",flag1
   print "cpu_map    	=",cpu_map



  return pos,rsp



  
def read_hydro(file,verbose=True):
  """
  read a ramses hydro file
  
  hydro/output_hydro.f90  -> backup_hydro
  """


  f = open(file,'r')

  ncpu      = readblock(f,(int32))[0]
  nvar      = readblock(f,(int32))[0]
  ndim      = readblock(f,(int32))[0]
  nlevelmax = readblock(f,(int32))[0]
  nboundary = readblock(f,(int32))[0]
  gamma     = readblock(f,(float))[0]

  twotondim=2**ndim
  
  
  if verbose:
   print "ncpu     =",ncpu
   print "nvar     =",nvar
   print "ndim     =",ndim
   print "nlevelmax=",nlevelmax
   print "nboundary=",nboundary
   print "gamma    =",gamma

  rho  = zeros((0,))

  for ilevel in xrange(nlevelmax):
    for ibound in xrange(nboundary+ncpu):
  
      ilevel_r = readblock(f,(int32))[0]		# ok, need to record
      ncache = readblock(f,(int32))[0]			# ok, need to record
      


      
      if(ncache>0):
        # Loop over cells
        for ind in xrange(1,twotondim+1):
	  for ivar in xrange(1,nvar+1):			# loop over variables
	    xdp     = readblock(f,float,(ncache,))	# ok, need to record 
            
	    #if ilevel==SELECTED_LEVEL:
	    #  if ibound==SELECTED_IBOUND:
	    if ivar==1 and ind==1:		    # this is bad... !!!!
              rho  = concatenate((rho,xdp))   

  rsize = f.tell()
  isize = os.path.getsize(file)
  if isize!=rsize:
    print "isize (%d) != rsize (%d)"%(isize,rsize)
    sys.exit()	  
  
  return rho
