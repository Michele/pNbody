#!/usr/bin/env python

'''
 @package   pNbody
 @file      mkrmov.py
 @brief     Make a RAMSES movie
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''


import pyramses
import sys

from pNbody import *

import argparse
import glob
import shutil

description=None
epilog=None

parser = argparse.ArgumentParser(description=description,epilog=epilog)



parser.add_argument("-o", 
                  "--outputdirectory",
                  action="store", 
		  dest="outputdirectory", 
                  help="name of the output  directory",
		  metavar="FILE")

parser.add_argument('files', metavar='FILE', type=str,
                   nargs='+',
                   help='name of the file to convert')


#####################################
# main
#####################################

opt = parser.parse_args()
directories = opt.files



if os.path.isdir(opt.outputdirectory):
  shutil.rmtree(opt.outputdirectory)

os.mkdir(opt.outputdirectory)


for i,directory in enumerate(directories):
   
   print directory
   
   mapfile = "map%06d.dat"%i
   pngfile = "img%06d.png"%i
   
   mapfile = os.path.join(opt.outputdirectory,mapfile)
   pngfile = os.path.join(opt.outputdirectory,pngfile)
   
   # run a.out
   os.system("./a.out -out %s  -dir z  -lma 10 -typ 1  -inp  %s"%(mapfile,directory))


   # create png
   data = transpose(pyramses.read_map(mapfile,verbose=False))
   
   mplot(data,palette='rainbow4',save=pngfile)   














