#!/bin/bash

rm .coverage
rm .coverage.*

function test {
    $1
    CODE=$?
    if [[ $CODE -ne 0 ]]; then
	echo $1 " Exit Code " $CODE
	echo "Test failed"
	exit 1
    fi
}

test "python -m coverage run test/basic_test.py"
test "python -m coverage run test/io_test.py"
test "python -m coverage run test/format_test.py"
test "mpiexec --allow-run-as-root -n 2 python -m coverage run test/pio_test.py"
test "python -m coverage run test/grid_test.py"
#test "mpiexec -n 2 python -m coverage run test/format_test.py"

python -m coverage combine
python -m coverage report
python -m coverage html
