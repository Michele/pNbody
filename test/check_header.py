#! /usr/bin/python
'''
 @package   pNbody
 @file      check_header.py
 @brief     Compare header between two files
 @copyright GPLv3
 @author    Loic Hausammann <loic_hausammann@hotmail.com>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
 To call the script: ./check_header.py file1 file2

'''

import sys

error_msg = "%s not present!"

def stripline(line):
    line = line.replace(" ", "")
    line = line.replace("\r", "")
    line = line.replace("\n", "")
    return line

def getHeader(f):
    """
    Obtain the header from the files.
    Read the file 'f' and construct a list from the header.
    """
    header = []
    start = False
    for line in f.readlines():
        line = stripline(line)
        if '"""' in line or "'''" in line:
            if not start:
                start = True
            else:
                break;
        if start:
            header.append(line)
    return header

def checkParameter(h, line, word):
    if word == "FILE":
        tmp = f_argv.split("/")[-1]
        return tmp in h
    elif word == "EMAIL":
        return "@" in h
    else:
        return True
    
def checkIncompletLine(line, h_f, f):
    """
    Check a line from the header containing a parameter delimited by '\{' and '\}'.
    """
    f2 = line.find("\}")
    if f2 == -1:
        print "ERROR in the header file, \{ not closed"
        sys.exit(54)
    test = None
    for i in h_f:
        if line[:f] in i:
            ind = i.find(line[:f])
            test = i[ind:]
            break;
    if test is None:
        return 1
    else:
        if not checkParameter(i, line, line[f+2:f2]):
            return 4
        new_f = line[f2+2:].find("\{")
        if new_f == -1:
            if line[f2+2:].isspace():
                return 0
            else:
                return not line[f2+2:] in test[f:]
        else:
            return checkIncompletLine(line[f2+2:], [test], new_f)

def checkHeader(header, h_f):
    """
    Check the header.
    """
    for i in header:
        f = i.find("\{")
        # case where nothing is inserted
        if f == -1:
            if i not in h_f:
                print error_msg % i
                return 1
        else:
            test = checkIncompletLine(i, h_f, f)
            if test != 0:
                print error_msg % i
                return 2
            
    return 0
                

if __name__ == "__main__":
    # get filename
    ref_argv = str(sys.argv[1])
    f_argv = str(sys.argv[2])
    
    # open files
    ref = open(ref_argv, "r")
    f = open(f_argv, "r")

    # get header string
    h_ref = getHeader(ref)
    h_f = getHeader(f)

    sys.exit(checkHeader(h_ref, h_f))
