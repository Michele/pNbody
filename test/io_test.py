#! /usr/bin/env python
'''
 @package   pNbody
 @file      io_test.py
 @brief     Test reading and writing functions for a few pNbody format.
 @copyright GPLv3
 @author    Loic Hausammann <loic_hausammann@hotmail.com>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
 The test implemented (for each format) are:
   - Reading -> Writing -> Reading; diff input/output
   - Reading (format 1) -> Writing (format 2)
   - New empty file -> Writing -> Reading
'''

import filecmp
import sys
import pNbody as pn
from pNbody import mpi
import numpy as np
import os

data_dir = "test/data/"
tmpfile = data_dir + "tmp.dat"

def test_reading_writing(filename, ftype, paramsfile):
    """
    Read a file and then write it under tmp.dat name.
    :param string filename: File to test
    :param string ftype: Format of the file
    :param string paramsfile: Name of the params file
    :returns: 0 if ok, 1 if not ok
    """
    try:
        print "TEST reading and writing with %s format" % ftype
        print "Reading %s Format..." % ftype
        nb = pn.Nbody(filename, unitsfile=paramsfile, ftype=ftype)
        print "Reading Done."
        nb.rename(tmpfile)
        print "Writing %s Format..."  % ftype
        nb.write()
        print "Writing Done."
    except Exception as e:
        print "ERROR: Reading and Writing for %s is broken!" % ftype
        raise e
    mpi.mpi_barrier()
    if (ftype == "gh5"):
        err = os.system("h5diff -c %s %s/tmp.dat" % (filename, data_dir))
    else:
        err = not filecmp.cmp(filename, tmpfile)
    if err != 0:
        raise Exception("ERROR: File are not equal for %s format!" % ftype)
        
def test_change_format(in_filename, in_ftype, in_params, out_ftype):
    """
    Read a file and then write it in a different format.
    :param string in_filename: File to test
    :param string in_ftype: Format of the input file
    :param string in_params: Name of the input params file
    :param string out_filename: File to write
    :param string out_ftype: Format of the output file
    :returns: 0 if ok
    """
    try:
        print "TEST: Change format from %s to %s" % (in_ftype, out_ftype)
        print "Reading %s Format..." % in_ftype
        nb = pn.Nbody(in_filename, unitsfile=in_params, ftype=in_ftype)
        print "Reading Done."
        nb.rename(tmpfile)
        nb = nb.set_ftype(ftype=out_ftype)
        print "Writing %s Format..."  % out_ftype
        nb.write()
        print "Writing Done."
    except Exception as e:
        print "ERROR: Change format from %s to %s is broken!"  % (in_ftype, out_ftype)
        raise e

def test_new_file(filename, ftype):
    """
    Create an empty snapshot, then save and read it.
    :param string filename: File to test
    :param string ftype: Format of the file
    :returns: 0 if ok
    """
    try:
        print "TEST: Creating an empty file with %s format" % ftype
        nb = pn.Nbody(filename, status="new", ftype=ftype)
        print "Writing %s..." % ftype
        nb.write()
        print "Writing Done."
        print "Reading %s Format..." % ftype
        nb = pn.Nbody(filename, ftype=ftype)
        print "Reading Done."
    except Exception as e:
        print "ERROR: Creating an empty file for %s is broken!" % ftype
        raise e


def test_skip_vec(filename, ftype):
    """
    Read a snapshot and skip an array (vel).
    :param string filename: File to test
    :param string ftype: Format of the file
    """
    print "TEST: Reading snapshot with skipped data with %s format" % ftype
    nb = pn.Nbody(filename, ftype=ftype, skipped_io_blocks=['vel'])
    if np.sum(nb.vel) != 0:
        raise Exception("ERROR: skipped block seems to be broken with %s format!" % ftype)


if __name__ == "__main__":
#def main():
    gadget_file = data_dir + "snap.dat"
    files = ["snap.dat", "snap.hdf5"]
    params = ["params", None]
    ftype = ["gadget", "gh5"]

    N = len(files)
    print "#"*45
    print "#"*19 + " Init " + "#"*20
    print "#"*45
    
    for i in range(N):
        files[i] = data_dir + files[i]
        if params[i] is not None:
            params[i] = data_dir + params[i]

    print "#"*45
    print "#"*16 + " Read/Write " + "#"*17
    print "#"*45

    for i in range(N):
        test_reading_writing(files[i], ftype[i], params[i])
        
    print "#"*45
    print "#"*22 + " New File " + "#"*23
    print "#"*45

    for i in range(N):
        new_file = data_dir + "new.dat"
        test_new_file(new_file, ftype[i])

    print "#"*45
    print "#"*15 + " Change Format " + "#"*15
    print "#"*45

    for i in range(N):
        for j in range(N):
            if i != j:
                test_change_format(files[i], ftype[i], params[i], ftype[j])

    print "#"*45
    print "#"*17 + " Skip Data " + "#"*17
    print "#"*45

    for i in range(N):
        test_skip_vec(files[i], ftype[i])
