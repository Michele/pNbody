#!/bin/bash

status=0
for f in $(find ./examples ./MpiTest ./pNbody ./scripts ./src ./test -name '*.py');
do
    test/check_header.py test/data/header.txt $f
    isOk=$?
  if [[ $isOk -ne 0 ]]; then
      echo "$f has wrong header"
    status=1
  fi
done < <(cat)

exit $status
