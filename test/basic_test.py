#! /usr/bin/env python
'''
 @package   pNbody
 @file      io_test.py
 @brief     Test reading and writing functions for a few pNbody format.
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
 The test implemented (for each format) are:
   - Reading -> Writing -> Reading; diff input/output
   - Reading (format 1) -> Writing (format 2)
   - New empty file -> Writing -> Reading
'''

from pNbody import *

nb = Nbody()
nb.info()
