#! /usr/bin/env python
import numpy as np

import pNbody
from pNbody import libgrid

nbodyfile = "test/data/snap.dat"

def g(x):
    return np.log(x)

def gm(x):
    return np.exp(x)

def test_grid1d():
    nb = pNbody.Nbody(nbodyfile)
    rmin = nb.rxyz().max()
    rmax = nb.rxyz().min()
    nr = 32
    r = nb.rxyz()
    mass = nb.mass
    val = np.ones(len(nb.mass))

    grid = libgrid.Generic_1d_Grid(rmin,rmax,nr,g=g,gm=gm)

    grid.get_r()
    grid.get_Points()
    grid.get_MassMap(r,mass)
    grid.get_MeanMap(r,mass,val)
    grid.get_SigmaMap(r,mass,val)
    grid.get_LinearMap()
    grid.get_LinearDensityMap(r,mass)

    grid = libgrid.Spherical_1d_Grid(rmin,rmax,nr,g=g,gm=gm)
    
    grid.get_r()
    grid.get_Points()
    grid.get_MassMap(nb)
    grid.get_NumberMap(nb)
    grid.get_ValMap(nb,val)
    grid.get_MeanValMap(nb,val)
    grid.get_SigmaValMap(nb,val)
    grid.get_PotentialMap(nb,1e-6,UseTree=False)
    grid.get_SurfaceMap()
    grid.get_VolumeMap()
    grid.get_DensityMap(nb)
    grid.get_LinearDensityMap(nb)
    grid.get_AccumulatedMassMap(nb)
    #grid.get_Interpolation(nb.pos)
    grid.get_AccelerationMap(nb,1e-6,UseTree=False)

def test_grid2d():
    nb = pNbody.Nbody(nbodyfile)
    rmin = nb.rxy().max()
    rmax = nb.rxy().min()
    nr = 16
    r = nb.rxy()
    z = nb.z()
    zmin = z.min()
    zmax = z.max()
    nz = 16
    mass = nb.mass
    val = np.ones(len(nb.mass))

    grid = libgrid.Cylindrical_2drz_Grid(rmin,rmax,nr,zmin,zmax,nz,g=g,gm=gm)

    grid.get_rz()
    grid.get_Points()
    grid.get_MassMap(nb)
    grid.get_NumberMap(nb)
    grid.get_PotentialMap(nb,1e-6,UseTree=False)
    grid.get_AccelerationMap(nb,1e-6,UseTree=False)
    grid.get_VolumeMap()
    grid.get_SurfaceMap()
    grid.get_SurfaceDensityMap(nb)
    grid.get_DensityMap(nb)

    nt = 16
    t = nb.phi_xy()
    grid = libgrid.Cylindrical_2drt_Grid(rmin,rmax,nr,nt,g=g,gm=gm)

    grid.get_rt()
    grid.get_xyz()
    grid.get_Points()
    grid.get_MassMap(nb)
    grid.get_NumberMap(nb)
    grid.get_ValMap(nb,val)
    grid.get_MeanValMap(nb,val)
    grid.get_SigmaValMap(nb,val)
    grid.get_PotentialMap(nb,1e-6,UseTree=False)
    grid.get_AccelerationMap(nb,1e-6,UseTree=False)
    grid.get_SurfaceMap(nb)
    grid.get_SurfaceValMap(nb,val)
    grid.get_SurfaceDensityMap(nb)
    grid.get_ReducedSurfaceDensityMap(nb)

    x = nb.x()
    xmin = x.min()
    xmax = x.max()
    nx = 16
    y = nb.y()
    ymin = y.min()
    ymax = y.max()
    ny = 16
    grid = libgrid.Carthesian_2dxy_Grid(xmin,xmax,nx,ymin,ymax,ny,g=g,gm=gm)

    grid.get_xy()
    grid.get_Points()
    grid.get_MassMap(nb)
    grid.get_NumberMap(nb)
    #grid.get_PotentialMap(nb,1e-6,UseTree=False)
    #grid.get_AccelerationMap(nb,1e-6,UseTree=False)
    grid.get_SurfaceMap(nb)
    grid.get_ValMap(nb,val)
    grid.get_MeanValMap(nb,val)
    grid.get_SigmaValMap(nb,val)
    grid.get_SurfaceDensityMap(nb)


def test_grid3d():
    nb = pNbody.Nbody(nbodyfile)
    x = nb.x()
    xmin = x.max()
    xmax = x.min()
    nx = 16
    y = nb.y()
    ymin = y.max()
    ymax = y.min()
    ny = 16
    z = nb.z()
    zmin = z.min()
    zmax = z.max()
    nz = 16
    mass = nb.mass
    val = np.ones(len(nb.mass))

    grid = libgrid.Carthesian_3dxyz_Grid(xmin,xmax,nx,ymin,ymax,ny,zmin,zmax,nz,g=g,gm=gm)
    grid.get_xyz()
    grid.get_Points()
    grid.get_MassMap(nb)
    grid.get_NumberMap(nb)
    #grid.get_PotentialMap(nb,1e-6,UseTree=False)
    #grid.get_AccelerationMap(nb,1e-6,UseTree=False)
    grid.get_VolumeMap()
    grid.get_ValMap(nb,val)
    grid.get_MeanValMap(nb,val)
    grid.get_SigmaValMap(nb,val)
    grid.get_DensityMap(nb)

    
if __name__ == "__main__":
    test_grid1d()
    test_grid2d()
    test_grid3d()
