#! /usr/bin/env python

'''
 @package   pNbody
 @file      format_test.py
 @brief     Test a list of methods.
 @copyright GPLv3
 @author    Loic Hausammann <loic_hausammann@hotmail.com>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
 Test if the code works, not if the results are good.
'''

import filecmp
import sys
import pNbody as pn
import numpy as np
import os

data_dir = "test/data/"
tmpfile = data_dir + "tmp.dat"

def test_format_methods(filename, ftype, paramsfile):
    """
    Test some standards functions in config/format
    """
    nb = pn.Nbody(filename, unitsfile=paramsfile, ftype=ftype)
    nb.spec_info()
    nb.get_massarr_and_nzero()
    como = nb.isComovingIntegrationOn()
    
    nb.setComovingIntegrationOn()
    if not nb.isComovingIntegrationOn():
        return 1
 
    nb.setComovingIntegrationOff()
    if nb.isComovingIntegrationOn():
        return 1

    if como:
        nb.setComovingIntegrationOn()
    nb.ComovingIntegrationInfo()
    nb.Z()
    nb.Fe()
    nb.Mg()
    nb.O()
    nb.Ba()
    nb.MgFe()
    nb.BaFe()
    #nb.SiFe()
    nb.AbRatio("Mg", "Fe")
    nb.AbRatio("Mg", "H")
    nb.Luminosity()
    nb.LuminositySpec()
    nb.Age()
    nb.age()
    nb.RGB()
    nb.Rxyz()
    nb.Rxy()
    nb.SphRadius()
    nb.TotalMass()
    nb.Mass()
    nb.Rho()
    nb.T()
    nb.TJeans()
    nb.Told()
    nb.Pressure()
    nb.SoundSpeed()
    nb.CourantTimeStep()
    nb.StellarAge()
    nb.CosmicTime()
    nb.sfr(1e-7)
    #nb.TimeStepLevel()
    #nb.dLdt()
    nb.GetVirialRadius()
    
    nb.setComovingIntegrationOn()
    nb.Redshift()
    nb.toPhysicalUnits()
    return 0

def test_main_methods(filename, ftype, paramsfile):
    """
    Test some functions in pNbody/main.py
    """
    nb = pn.Nbody(filename, unitsfile=paramsfile, ftype=ftype)
    # main
    nb.info()
    nb.get_ftype()
    nb.get_ntype()
    nb.get_npart_all([100,100,100,100], 3)
    nb.get_mxntpe()
    nb.object_info()
    nb.nodes_info()
    nb.memory_info()
    nb.print_filenames()
    nb.get_list_of_vars()
    nb.get_list_of_array()
    nb.get_list_of_method()

    print "Position methods"
    nb.x()
    nb.x(center=[0,0.1,0.2])
    nb.y()
    nb.y(center=[0,0.1,0.2])
    nb.z()
    nb.z(center=[0,0.1,0.2])
    r = nb.rxyz()
    nb.rxyz(center=[0,0.1,0.2])
    nb.phi_xyz()
    nb.theta_xyz()
    nb.rxy()
    nb.phi_xy()
    nb.cart2sph()
    nb.sph2cart()

    print "Velocities methods"
    nb.vx()
    nb.vy()
    nb.vz()
    nb.vn()
    nb.vrxyz()
    nb.Vr()
    nb.Vt()
    nb.Vz()
    nb.vel_cyl2cart()
    nb.vel_cart2cyl()

    print "Get methods"
    nb.get_ns()
    nb.get_mass_tot()
    nb.size()

    print "Stat methods"
    nb.cm()
    nb.get_histocenter()
    nb.get_histocenter2()
    nb.cv()
    nb.minert()
    nb.inertial_tensor()
    nb.x_sigma()
    nb.v_sigma()
    nb.dx_mean()
    nb.dv_mean()

    print "Phys. quant. methods"
    nb.Ekin()
    nb.ekin()
    nb.Epot(1e-6)
    nb.epot(1e-6)
    nb.L()
    nb.l()
    nb.Ltot()
    nb.ltot()
    x = np.array([[0,0,0], [1.,1.,1.]])
    nb.Pot(x,0.1)
    #nb.TreePot(x,0.1)
    nb.Accel(x,0.1)
    #nb.TreeAccel(x,0.1)
    nb.tork(np.ones(nb.pos.shape))

    print "Density methods"
    nb.dens()
    nb.mdens()
    nb.mr()
    nb.Mr_Spherical()
    nb.sdens()
    nb.msdens()

    print "Stat. methods 2"
    nb.sigma_z()
    nb.sigma_vz()
    nb.zprof()
    nb.sigma()
    nb.histovel()
    nb.cmcenter()
    nb.zmodes()
    nb.dmodes()

    print "Cylindrical methods"
    nb.cmcenter()
    nb.getRadiusInCylindricalGrid(1, 15)
    nb.getAccelerationInCylindricalGrid(0.1, 1, 15)
    nb.getPotentialInCylindricalGrid(0.1, 1, 15)
    nb.getSurfaceDensityInCylindricalGrid(0.1, 1, 15)
    nb.getNumberParticlesInCylindricalGrid(0.1, 1, 15)
    nb.getRadialVelocityDispersionInCylindricalGrid(0.1, 1, 15)

    print "Global translation methods"
    nb.cvcenter()
    nb.histocenter()
    nb.histocenter2()
    nb.hdcenter()
    nb.translate([0.1,0.1,0.1])
    nb.rebox(mode="centred")
    nb.rebox()
    nb.rebox(mode=[0.1,0.2,0.4])
    nb.rotate_old()

    print "Rotation methods"
    nb.rotate()
    nb.get_rotation_matrix_to_align_with_main_axis()

    print "Alignment methods"
    nb.align_with_main_axis()
    nb.align([0.1,0.1,0.1])
    nb.align2()
    nb.spin(L=12,j=0.1,E=1.)

    print "Selection methods"
    nb.selectc(r < 5)
    nb.selecti(range(10))
    nb.select(0)
    nb.sub()
    nb.reduc(200)
    tmp = nb.selectp([50,124])

    print "Addition methods"
    nb.getindex([20])
    nb.append(tmp)
    nb = nb + tmp
    nb.take()

    print "Sorting methods"
    nb.sort()
    #nb.sort_type()

    print "MPI methods"
    nb.InitSphParameters()
    #nb.setTreeParameters()
    #nb.getTree()
    #nb.get_rsp_approximation()
    #nb.ComputeSph()
    #nb.ComputeDensityAndHsml()
    #nb.SphEvaluate()
    nb.weighted_numngb(50)
    nb.real_numngb(40)
    nb.usual_numngb([50])
    nb.redistribute()
    #nb.ExchangeParticles()
    nb.SendAllToAll()
    nb.gather_pos()
    nb.gather_vel()
    nb.gather_mass()
    nb.gather_num()
    nb.gather_vec(np.array([50]))


    print "Map methods"
    nb.Map()
    nb.CombiMap()
    #nb.ComputeMeanMap()
    #nb.ComputeSigmaMap()
    nb.ComputeMap()
    #nb.ComputeObjectMap()
    #nb.expose()
    #nb.Histo(r)
    #nb.CombiHisto(r)
    #nb.ComputeMeanHisto(r,None, None)
    #nb.ComputeSigmaHisto(r,None, None)
    #nb.ComputeHisto(r, None, None)

    print "Phys. quant. methods 2"
    #nb.Get_Velocities_From_Virial_Approximation()
    #nb.Get_Velocities_From_AdaptativeSpherical_Grid()
    #nb.Get_Velocities_From_Spherical_Grid()
    #nb.Get_Velocities_From_Cylindrical_Grid()
    #nb.IntegrateUsingRK()
    nb.U()
    nb.Rho()
    nb.T()
    #nb.MeanWeight()
    nb.Tmu()
    #nb.A()
    #nb.P()
    #nb.Tcool()
    nb.Ne()
    nb.S()
    #nb.Lum()

    print "Other methods"
    nb.find_vars()
    nb.check_arrays()
    nb.write_num("test/data/data.num")
    nb.set_npart(nb.npart)
    nb.set_tpe(2)

    # modify nb.pos
    nb.rotateR([0.1,0.1,0.1])
    
    return 0

if __name__ == "__main__":
#def main():
    files = ["snap.dat", "snap.hdf5"]
    params = ["params", None]
    ftype = ["gadget", "gh5"]

    N = len(files)
    for i in range(N):
        files[i] = data_dir + files[i]
        if params[i] is not None:
            params[i] = data_dir + params[i]

    for i in range(N):
        if test_format_methods(files[i], ftype[i], params[i]) != 0:
            print "Failed format methods test with " % ftype[i]
            exit(2)
            #return 2
        
        if test_main_methods(files[i], ftype[i], params[i]) != 0:
            print "Failed main methods test with " % ftype[i]
            exit(3)
            #return 3
    print "Format_test Done"
    #return 0
    exit(0)
