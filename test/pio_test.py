#! /usr/bin/env python

'''
 @package   pNbody
 @file      pio_test.py
 @brief     Test parallel reading/writing
 @copyright GPLv3
 @author    Loic Hausammann <loic_hausammann@hotmail.com>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

import filecmp
import sys
import pNbody as pn
from pNbody import mpi
import os

data_dir = "test/data/"
tmpfile = data_dir + "tmp.dat"

def test_parallel(filename, ftype, paramsfile):
    """
    Read a file, write in parallel, read it in parallel and then write it (in serial).
    The test compare the input file with the serial written one.
    :param string filename: File to test
    :param string ftype: Format of the file
    :param string paramsfile: Name of the params file
    :returns: 0 if ok, 1 if not ok
    """

    if mpi.mpi_IsMaster():
        print "TEST reading and writing with %s format" % ftype
        print "Reading..."
    nb = pn.Nbody(filename, unitsfile=paramsfile, ftype=ftype, pio="no")
    if mpi.mpi_IsMaster():
        print "Reading Done."
    nb.set_pio("yes")
    nb.rename(tmpfile)
    if mpi.mpi_IsMaster():
        print "Writing in parallel..."
    nb.write()
    if mpi.mpi_IsMaster():
        print "Writing in parallel Done."
        print "Reading in parallel..."
    mpi.mpi_barrier()
    nb = pn.Nbody(tmpfile, unitsfile=paramsfile, ftype=ftype, pio="yes")
    if mpi.mpi_IsMaster():
        print "Reading in parellel Done."
    nb.set_pio("no")
    if mpi.mpi_IsMaster():
        print "Writing..."
    nb.write()
    if mpi.mpi_IsMaster():
        print "Writing Done."
    mpi.mpi_barrier()
    if (ftype == "gh5"):
        err = os.system("h5diff -c %s %s/tmp.dat" % (filename, data_dir))
        return err
    else:
        return not filecmp.cmp(filename, tmpfile)



if __name__ == "__main__":
    files = ["snap.dat", "snap.hdf5"]
    params = ["params", None]
    ftype = ["gadget", "gh5"]

    N = len(files)
    for i in range(N):
        files[i] = data_dir + files[i]
        if params[i] is not None:
            params[i] = data_dir + params[i]

    for i in range(N):
        if test_parallel(files[i], ftype[i], params[i]) != 0:
            print "ERROR: Parallel Reading and Writing for %s is broken!" % ftype[i]
            exit()

