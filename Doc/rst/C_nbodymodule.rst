the C nbodymodule module
**********************

.. currentmodule:: pNbody.nbodymodule

.. automodule:: pNbody.nbodymodule
   :members:
