Default parameters
**********************

To see what default parameters **pNbody** uses, type::

  pNbody_show-parameters

The script returns the parameters taken from the files
*defaultparameters* and *unitsparameters*.
Their current values are displayed::

  parameters in /home/leo/local/lib/python2.6/site-packages/pNbody/config/defaultparameters

  ----------------------------------------------------------------------------------------------------
                            name                          meaning             value (type)
  ----------------------------------------------------------------------------------------------------
                             obs :                       observer =            None (ArrayObs)
                              xp :             observing position =            None (List)
                              x0 :           position of observer =            None (List)
                           alpha :              angle of the head =            None (Float)
                            view :                           view =              xz (String)
                           r_obs :          dist. to the observer =   201732.223771 (Float)
                            clip :                    clip planes = (100866.11188556443, 403464.44754225772) (Tuple)
                             cut :                cut clip planes =              no (String)
                             eye :                name of the eye =            None (String)
                        dist_eye :          distance between eyes =         -0.0005 (Float)
                             foc :                          focal =           300.0 (Float)
                           persp :                    perspective =             off (String)
                           shape :             shape of the image =      (512, 512) (Tuple)
                            size :                   pysical size =    (6000, 6000) (Tuple)
                            frsp :                           frsp =             0.0 (Float)
                           space :                          space =             pos (String)
                            mode :                           mode =               m (String)
                       rendering :                 rendering mode =             map (String)
                     filter_name :             name of the filter =            None (String)
                     filter_opts :                 filter options =  [10, 10, 2, 2] (List)
                           scale :                          scale =             log (String)
                              cd :                             cd =             0.0 (Float)
                              mn :                             mn =             0.0 (Float)
                              mx :                             mx =             0.0 (Float)
                             l_n :               number of levels =              15 (Int)
                           l_min :                      min level =             0.0 (Float)
                           l_max :                      max level =             0.0 (Float)
                            l_kx :                           l_kx =              10 (Int)
                            l_ky :                           l_ky =              10 (Int)
                         l_color :                    level color =               0 (Int)
                         l_crush :               crush background =              no (String)
                        b_weight :                box line weight =               0 (Int)
                         b_xopts :                 x axis options =            None (Tuple)
                         b_yopts :                 y axis options =            None (Tuple)
                         b_color :                     line color =             255 (Int)

  parameters in /home/leo/local/lib/python2.6/site-packages/pNbody/config/unitsparameters

  ----------------------------------------------------------------------------------------------------
                            name                          meaning             value (type)
  ----------------------------------------------------------------------------------------------------
                              xi :         hydrogen mass fraction =            0.76 (Float)
                      ionisation :                ionisation flag =               1 (Int)
                      metalicity :               metalicity index =               4 (Int)
                            Nsph :        number of sph neighbors =              50 (Int)
                           gamma :                adiabatic index =   1.66666666667 (Float)
                     coolingfile :                   Cooling file = ~/.Nbody/cooling.dat (String)
                     HubbleParam :                    HubbleParam =             1.0 (Float)
                UnitLength_in_cm :               UnitLength in cm =       3.085e+21 (Float)
                   UnitMass_in_g :                  UnitMass in g =    4.435693e+44 (Float)
        UnitVelocity_in_cm_per_s :       UnitVelocity in cm per s =   97824708.2699 (Float)
