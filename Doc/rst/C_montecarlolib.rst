the C montecarlolib module
**********************

.. currentmodule:: pNbody.montecarlolib

.. automodule:: pNbody.montecarlolib
   :members:
