the mpi module
**********************

.. currentmodule:: pNbody.mpi

.. automodule:: pNbody.mpi
   :members:
