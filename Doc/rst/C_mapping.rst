the C mapping module
**********************

.. currentmodule:: pNbody.mapping

.. automodule:: pNbody.mapping
   :members:
