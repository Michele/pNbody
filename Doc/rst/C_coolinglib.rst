the C coolinglib module
**********************

.. currentmodule:: pNbody.coolinglib

.. automodule:: pNbody.coolinglib
   :members:
