Installing from source
**********************

Decompress the tarball
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Decompress the tarball file::

  tar -xzf pNbody-4.x.tar.gz
 
 
enter the directory::

  cd pNbody-4.x


Compile
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The compilation is performed using the standard command::  

  python setup.py build
 
 
Install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
Now, depending on your python installation you need to be root. 
The module is installed with the following command::   

  python setup.py install
  
  
If one wants to install in another directory (not being root) than the default
python one, it is possible to use the standard ``--prefix`` option::

  python setup.py install  --prefix other_directory

