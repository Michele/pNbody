the C PyGadget module
**********************

This mpdule is currently not completely integrated to **pNbody**.
It is part of the **pNbody** package but must be compiled
separately.
For mpi, use:: 
  
  export CC=mpirun
  
  

.. currentmodule:: PyGadget.gadget

.. automodule:: PyGadget.gadget
   :members:
