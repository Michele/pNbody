Installation
**********************

pNbody is curently only supported by linux.

.. toctree::
   :maxdepth: 2
   
   Prerequiste
   Installing_from_tarball
   Setting_up_your_environment
   Test_the_installation
   Default_configurations
   Default_parameters
   Examples


