the C treelib module
**********************

.. currentmodule:: pNbody.treelib

.. automodule:: pNbody.treelib
   :members:
