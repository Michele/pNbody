the plummer module
**********************

.. currentmodule:: pNbody.plummer

.. automodule:: pNbody.plummer
   :members:
