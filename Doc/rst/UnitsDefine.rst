.. currentmodule:: pNbody.units

Define a system of unit
*****************************************

In order to define a system of units, we need at least
to define three numbers, which fix the units of length,
the units of mass and the units of time.

In **pNbody**, there is two way to define a system of units, 
either constructing it directely from the object constructor
:func:`UnitSystem`,
or using the function  
:func:`Set_SystemUnits_From_Params`,


Define a system of unit using :func:`UnitSystem`
================================================


In this first possibility ,we decide explicitely what are the
basic units of our system of units. We need here to use units
defined in the :mod:`pNbody.units` module as :class:`pNbody.units.Units` objects ::


This gives::

  >>> from pNbody import units
  >>> system_of_units = units.UnitSystem('local',[units.Unit_kpc,  10**10*units.Unit_Ms, units.Unit_Myr])


Here, the first argument is the name of the unit system (useless) and the second, a list of basic units.
In this list, the first argument must be a unit lenght, the second a mass unit and the third a time unit.

To check we can call the :func:`UnitSystem.info` method::

  >>> system_of_units.info()
  units info
  >>> UnitLength_in_cm	       = 3.085e+21
  >>> UnitVelocity_in_cm_per_s = 9.78247e+07
  >>> UnitMass_in_g	       = 1.9891e+43
  

With gives the values of our units as a function of the basic units: centimeters,
gramm and centimeter per second.  



Define a system of unit using :func:`Set_SystemUnits_From_Params`
=================================================================


Another way to define a system of unit is defined giving at least three numbers, 
corresponding to the value of respectively centimeter (``UnitLength_in_cm``), 
gramm (``UnitMass_in_g``) and centimeter per second (``UnitVelocity_in_cm_per_s``)
in the desired system of unit. All are passed to the function :func:`Set_SystemUnits_From_Params`
using a dictionary::
 
   >>> from pNbody import units
   >>> params = {}
   >>> params['UnitLength_in_cm']         = 3.085e+21
   >>> params['UnitVelocity_in_cm_per_s'] = 9.78247e+07
   >>> params['UnitMass_in_g']	          = 1.9891e+43
   >>> system_of_units = units.Set_SystemUnits_From_Params(params)
   >>> system_of_units.info()
   units info
     UnitLength_in_cm	      =3.085e+21
     UnitVelocity_in_cm_per_s =9.78247e+07
     UnitMass_in_g	      =1.9891e+43



Define a system of unit using :func:`Set_SystemUnits_From_File`
===============================================================

Here, we use the information from either
a `Gadget <http://www.mpa-garching.mpg.de/gadget/right.html>`_  parameter files containing
similar lines::

  UnitLength_in_cm				3.085678e+21
  UnitMass_in_g					1.989e+43
  UnitVelocity_in_cm_per_s 			100000.0

or a **pNbody** units parameter file, 
containing the following lines::

  UnitLength_in_cm
  UnitLength in cm
  Float
  3e+21

  UnitMass_in_g
  UnitMass in g
  Float
  3e+43

  UnitVelocity_in_cm_per_s
  UnitVelocity in cm per s
  Float
  20000000.

In both case, we use the following command::

  >>> from pNbody import units
  >>> system_of_units = units.Set_SystemUnits_From_File(unitfile)






