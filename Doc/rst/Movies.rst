2D Movies
*********

The MilkyWay and its interstellar medium
========================================

.. raw:: html
         
	 <br>
           <iframe width="640" height="480" src="http://www.youtube.com/embed/io_sKTur9Bc" frameborder="0" allowfullscreen></iframe>

Download video : `here <http://obswww.unige.ch/~revaz/movies/2DMovies/agora-x264.avi>`_

Formation of the large scale structure of our Universe
======================================================

 
.. raw:: html
	 
	 <br>
	   <iframe width="640" height="480" src="http://www.youtube.com/embed/9M4XPJ1M5g4" frameborder="0" allowfullscreen></iframe>
 
Download video : `here <http://obswww.unige.ch/~revaz/movies/2DMovies/film-dSphC-box-right-x264.mp4>`_



The collision between the Milky Way and the Andromeda galaxy
============================================================

.. raw:: html
         
	 <br>
           <iframe width="640" height="480" src="http://www.youtube.com/embed/6aqsvkMp4ns" frameborder="0" allowfullscreen></iframe>

Download video : `here <http://obswww.unige.ch/~revaz/movies/2DMovies/Milkyway-Andromeda.mp4>`_



Isolated galaxies with spirals and bars
=======================================

.. raw:: html
	 
	 <br>
	   <iframe width="640" height="480" src="http://www.youtube.com/embed/xricAQDC0sE" frameborder="0" allowfullscreen></iframe>
 
Download video : `here <http://obswww.unige.ch/~revaz/movies/2DMovies/n6946.mp4>`_



Formation of a dwarf spheroidal galaxy in a ultra-high Zoom-in simulation
=========================================================================
   
.. raw:: html
	 
	 <br>
	   <iframe width="640" height="480" src="http://www.youtube.com/embed/6wNrW0Y42ss" frameborder="0" allowfullscreen></iframe>

Download video : `here <http://obswww.unige.ch/~revaz/movies/2DMovies/halo00000017b-x264.avi>`_



More movies
===========


  see :ref:`moremovies`.
