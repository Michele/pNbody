the libdisk module
**********************

.. currentmodule:: pNbody.libdisk

.. automodule:: pNbody.libdisk
   :members:
