the thermodyn module
**********************

.. currentmodule:: pNbody.thermodyn

.. automodule:: pNbody.thermodyn
   :members:
