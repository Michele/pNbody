Scripts dedicated to movies
***************************

This is a collection of scripts usefull to 
manipulate gmov movies.


Create movies
=============

mkgmov
------------------
Create a movie from a list of models.

.. program-output:: mkgmov -h


gmkgmov
------------------
Create a movie from a list of models.
This script should replace the previous one.

.. program-output:: gmkgmov -h



Display movies
==============
   
   
gmov
------------------
Interactive tool used to display a movie.

.. program-output:: gmov -h
  
   
   
   
   
Cut/Split/Merge, etc. movies
============================
      
      
addgmov
------------------
Extend a movie with other movies.
All movies must have the same geometry.

.. program-output:: addgmov -h



combinegmov
------------------
Combine (superimpose) the images of two movies.
The movies must have the same dimension and the same number of frames.

.. program-output:: combinegmov -h


cutgmov
------------------
Reduce the duration of a movie.

.. program-output:: cutgmov -h

infogmov
------------------
Give info on a movie.

.. program-output:: infogmov -h


mergegmov
------------------
Merge (geometrically) different movies.

.. program-output:: mergegmov -h

splitgmov
------------------
Cut a movie (geometry) into different movies.

.. program-output:: splitgmov -h


supgmov
------------------
Superimpose two movies.
The movies must have the same dimension and the same number of frames.

.. program-output:: supgmov -h



movie conversion
============================


gmov2gif
------------------
.. program-output:: gmov2gif -h

gmov2gmov
------------------
.. program-output:: gmov2gmov -h

gmov2gmova
------------------
.. program-output:: gmov2gmova -h

gmov2mov
------------------
.. program-output:: gmov2mov -h

gmov2mpeg
------------------
.. program-output:: gmov2mpeg -h

gmov2mpg
------------------
.. program-output:: gmov2mpg -h

gmov2ppm
------------------
.. program-output:: gmov2ppm -h


gavi2mp4
------------------
.. program-output:: gavi2mp4 -h




















