Overview
**********************


**pNbody** is a parallelized python module toolbox designed to manipulate and display
interactively very lage N-body systems.

Its oriented object approche allows the user to perform complicate manipulation 
with only very few commands.

As python is an interpreted language, the user can load an N-body system and explore it
interactively using the python interpreter. **pNbody** may also be used in python scripts.

The module also contains graphical facilities desinged to create maps of physical values of
the system, like density maps, temperture maps, velocites maps, etc. Stereo capabilities are
also implemented.

**pNbody** is not limited by file format. Each user may redefine in a parameter file how to read
its prefered format.

Its new parallel (mpi) facilities make it works on computer cluster without being limitted by
memory consumption. It has already been tested with several millions of particles. 



.. image:: ../images/cosmo.png
