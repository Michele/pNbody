the C myNumeric module
**********************

.. currentmodule:: pNbody.myNumeric

.. automodule:: pNbody.myNumeric
   :members:
