Examples
**********************

A series of examples is provided by **pNbody** in the 
``PNBODYPATH/examples``, where NBODYPATH is obtained
with the command::

  pNbody_show-path
 