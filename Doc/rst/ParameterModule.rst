the parameters module
**********************

.. currentmodule:: pNbody.parameters

.. automodule:: pNbody.parameters
   :members:
