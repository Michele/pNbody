Generating initial conditions
**********************

**pNbody** provides a lots of different way
to generatin initial conditions, ready to be
run with evolution codes. 

The generation of initial conditions is devided into
two parts. First, the generation of a mass profile by
distributing particles in space. Secondly, the determination
of the velocities for each of these particles. 


.. toctree::
   :maxdepth: 2
   
   GeneratingMassProfiles
   GeneratingVelocities
