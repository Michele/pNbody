the C cooling_with_metals module
**********************

.. currentmodule:: pNbody.cooling_with_metals

.. automodule:: pNbody.cooling_with_metals
   :members:
