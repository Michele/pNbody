Download
********

The current version of **pNbody** may be downloaded using the open source version control system `git <http://git-scm.com>`_::

  git clone https://gitlab.com/revaz/pNbody.git

Older version of **pNbody** may be downloaded here: http://obswww.unige.ch/~revaz/old.pNbody/Download/
