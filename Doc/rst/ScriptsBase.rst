Base operations
***************************

This is a collection of scripts dedicated to 
base operations



gpy
------------------
Open a model and enter into the python interpreter.
The model is automatically initialized and may be 
acessed via the ``nb`` variable::

  Usage: gpy [options] file

  Options:
    -h, --help  	  show this help message and exit
    --UnitLength_in_cm=UNITLENGTH_IN_CM
  			  UnitLength in cm
    --UnitMass_in_g=UNITMASS_IN_G
  			  UnitMass in g
    --UnitVelocity_in_cm_per_s=UNITVELOCITY_IN_CM_PER_S
  			  UnitVelocity in cm per s
    --param= FILE	  Gadget parameter file
    -t  TYPE		  type of the file
    --exec= STRING	  give command to execute before


gcmd
------------------
Execute operation in loop over a set of models.

.. program-output:: gcmd -h

