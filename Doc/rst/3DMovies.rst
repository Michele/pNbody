Stereo Movies
**********************

Very high quality stereo movies
===============================

Astronomie : L'origine du monde 

   .. image:: ../movies/saturn.jpg
     :width: 800 px
     :target: http://obswww.unige.ch/~revaz/movies/3DMovies/final_ComptoireSuisse_with_sound.mp4
   
MilkyWay Andromeda collision
  
   .. image:: ../movies/mwa.jpg
     :width: 800 px
     :target: http://obswww.unige.ch/~revaz/movies/3DMovies/MilkyWay-Andromeda.mp4

LCDM Simulation


   .. image:: ../movies/lcdm.jpg
     :width: 800 px
     :target: http://obswww.unige.ch/~revaz/movies/3DMovies/film01_gas_g1610q5.mp4

Portes Ouvertes EPFL (2010)


   .. image:: ../movies/epfl2010.jpg
     :width: 800 px
     :target: http://obswww.unige.ch/~revaz/movies/3DMovies/JourneeEPFL-x264+sound.mp4




Cosmological stereo movies
================================

See the `4DU page <http://obswww.unige.ch/~revaz/4DU/>`_



   .. image:: http://obswww.unige.ch/~revaz/movies/FOF6133/film10/10.gif
     :width: 200 px
 ========== =============================== ====================================================================================
 size	    type			     film										
 ========== =============================== ====================================================================================
 1024x512   native			    `film10_n.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film10/film10_n.mpg>`_ 
 2048x768   anaglyph			    `film10_a.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film10/film10_a.mpg>`_ 
 1024x512   crossed eyes		    `film10_c.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film10/film10_c.mpg>`_ 
 512x512    parallel eyes		    `film10_p.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film10/film10_p.mpg>`_ 
 512x512    parallel eyes with borders	    `film10_pb.mpg <http://obswww.unige.ch/~revaz/movies/FOF6133/film10/film10_pb.mpg>`_
 ========== =============================== ====================================================================================

   .. image:: http://obswww.unige.ch/~revaz/movies/FOF6133/film11/11.gif
     :width: 200 px
 ========== =============================== ====================================================================================
 size	    type			     film										
 ========== =============================== ====================================================================================
 1024x512   native			    `film11_n.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film11/film11_n.mpg>`_ 
 2048x768   anaglyph			    `film11_a.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film11/film11_a.mpg>`_ 
 1024x512   crossed eyes		    `film11_c.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film11/film11_c.mpg>`_ 
 512x512    parallel eyes		    `film11_p.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film11/film11_p.mpg>`_ 
 512x512    parallel eyes with borders	    `film11_pb.mpg <http://obswww.unige.ch/~revaz/movies/FOF6133/film11/film11_pb.mpg>`_
 ========== =============================== ====================================================================================

   .. image:: http://obswww.unige.ch/~revaz/movies/FOF6133/film12/12.gif
     :width: 200 px
 ========== =============================== ====================================================================================
 size	    type			     film										
 ========== =============================== ====================================================================================
 1024x512   native			    `film12_n.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film12/film12_n.mpg>`_ 
 2048x768   anaglyph			    `film12_a.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film12/film12_a.mpg>`_ 
 1024x512   crossed eyes		    `film12_c.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film12/film12_c.mpg>`_ 
 512x512    parallel eyes		    `film12_p.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film12/film12_p.mpg>`_ 
 512x512    parallel eyes with borders	    `film12_pb.mpg <http://obswww.unige.ch/~revaz/movies/FOF6133/film12/film12_pb.mpg>`_
 ========== =============================== ====================================================================================

   .. image:: http://obswww.unige.ch/~revaz/movies/FOF6133/film13/13.gif
     :width: 200 px
 ========== =============================== ====================================================================================
 size	    type			     film										
 ========== =============================== ====================================================================================
 1024x512   native			    `film13_n.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film13/film13_n.mpg>`_ 
 2048x768   anaglyph			    `film13_a.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film13/film13_a.mpg>`_ 
 1024x512   crossed eyes		    `film13_c.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film13/film13_c.mpg>`_ 
 512x512    parallel eyes		    `film13_p.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film13/film13_p.mpg>`_ 
 512x512    parallel eyes with borders	    `film13_pb.mpg <http://obswww.unige.ch/~revaz/movies/FOF6133/film13/film13_pb.mpg>`_
 ========== =============================== ====================================================================================


   .. image:: http://obswww.unige.ch/~revaz/movies/FOF6133/film15/15.gif
     :width: 200 px
 ========== =============================== ====================================================================================
 size	    type			     film										
 ========== =============================== ====================================================================================
 1024x512   native			    `film15_n.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film15/film15_n.mpg>`_ 
 2048x768   anaglyph			    `film15_a.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film15/film15_a.mpg>`_ 
 1024x512   crossed eyes		    `film15_c.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film15/film15_c.mpg>`_ 
 512x512    parallel eyes		    `film15_p.mpg  <http://obswww.unige.ch/~revaz/movies/FOF6133/film15/film15_p.mpg>`_ 
 512x512    parallel eyes with borders	    `film15_pb.mpg <http://obswww.unige.ch/~revaz/movies/FOF6133/film15/film15_pb.mpg>`_
 ========== =============================== ====================================================================================
     





Experimental stereo movies
================================

Galaxies interaction

   .. image:: http://obswww.unige.ch/~revaz//movies/film16/film.gif



