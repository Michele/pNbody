the liblog module
**********************

.. currentmodule:: pNbody.liblog

.. automodule:: pNbody.liblog
   :members:
