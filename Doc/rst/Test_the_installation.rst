Check the installation
**********************


You can check the installation by simply running the following
command::

  pNbody_checkall

This command must of course be in your path. This will be the case
if you did not specified any ``--prefix``. On the contrary if ``--prefix``
is set to for example, *localdir* you should have your *PATH* environment
variable should contains::

  localdir/bin

and you *PYTHONPATH* environment should contains::

    localdir/lib/python2.x/site-packages/

to ensure that the **pNbody** package will be found.

If everything goes well, you should see a lots of outputs on your screen,
as well as a window displaying an edge-on disk.

.. image:: ../images/edge-on-disk.png
  :width: 200 px


Close it when you see it.
The script should finally ends up with something like ::


  ########################################################################
  Good News ! pNbody with format gadget is working !
  ########################################################################

  You are currently using the following paths

  HOME               : /home/leo
  PNBODYPATH         : /home/leo/local/lib/python2.6/site-packages/pNbody
  CONFIGDIR          : /home/leo/local/lib/python2.6/site-packages/pNbody/config
  PARAMETERFILE      : /home/leo/local/lib/python2.6/site-packages/pNbody/config/defaultparameters
  UNITSPARAMETERFILE : /home/leo/local/lib/python2.6/site-packages/pNbody/config/unitsparameters
  PALETTEDIR         : /home/leo/local/lib/python2.6/site-packages/pNbody/config/rgb_tables
  PLUGINSDIR         : /home/leo/local/lib/python2.6/site-packages/pNbody/config/plugins
  OPTDIR             : /home/leo/local/lib/python2.6/site-packages/pNbody/config/opt
  FORMATSDIR         : /home/leo/local/lib/python2.6/site-packages/pNbody/config/formats
