Mailing list
************

To subscribe to the **pNbody** mailing list, please, follow `this link <https://lists.sourceforge.net/lists/listinfo/pnbody-users>`_.

The archives of the mailing list are stored `here <https://sourceforge.net/mailarchive/forum.php?forum_name=pnbody-users>`_.

