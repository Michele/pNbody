.. pNbody documentation master file, created by
   sphinx-quickstart on Wed Aug 24 16:29:02 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pNbody's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2
   
   rst/Overview
   rst/Download
   rst/Installation
   rst/Tutorial
   rst/Formats
   rst/Display
   rst/InitialConditions
   rst/Selection
   rst/Units
   rst/PhysicalQuantities
   rst/Grids
   rst/Reference
   rst/Scripts
   rst/Gallery
   rst/Movies   
   rst/3DMovies   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

