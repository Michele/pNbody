'''
 @package   pNbody
 @file      main.py
 @brief     Main
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from mpi4py import MPI
from mpi import *

class NbodyDefault:

  def __init__(self):

    self.mpi = MpiClass(MPI)
