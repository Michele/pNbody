'''
 @package   pNbody
 @file      test.py
 @brief     Test
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''
from main import *

from mpi4py import MPI


nb = NbodyDefault()

print nb.mpi.MPI == MPI

mpi =  MpiClass(MPI)

print nb.mpi.MPI == mpi.MPI

