'''
 @package   pNbody
 @file      mpi.py
 @brief     
 @copyright GPLv3
 @author    Yves Revaz <yves.revaz@epfl.ch>
 @section   COPYRIGHT  Copyright (C) 2017 EPFL (Ecole Polytechnique Federale de Lausanne)  LASTRO - Laboratory of Astrophysics of EPFL

 This file is part of pNbody.
'''

from numpy import *
import types
#import libutil
import sys




class MpiClass:

  def __init__(self,MPI=None):
    self.MPI=MPI    

    if MPI!=None:
      self.ThisTask = MPI.COMM_WORLD.Get_rank()
      self.NTask    = MPI.COMM_WORLD.Get_size()
      self.Procnm   = MPI.Get_processor_name()
 
    else:
      
      self.ThisTask = 0
      self.NTask    = 1
      self.Procnm   = ""
      
      
      


